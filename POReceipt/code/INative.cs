using System;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Collections.Generic;
using System.IO;

namespace POReceipt
{
	public interface INative
	{
		void setBusy (bool busy);

		void setBusyWithCancel(bool busy);

		void saveLoginInfo(string username, string password);

		string getLoggedInUsername();

		string getLoggedInPassword();

		void showHUD (string message);

		void hideHUD ();

		void OnPageAppear ();

		void setupAnalytics ();

		Task wait (int miliseconds);

		void log (object message);

		void showAlert (string message, string title = "");

		Task<bool> showConfirm (string message, string title = "");

		Task<bool> showConfirmYesNo (string message, string title = "");

		string checkFile (string resource);

		string readFile (string resource);

		byte[] readBytes (string resource);

		byte[] readBytesFromPath (string filePath);

		string[] getFiles (string resource, string pattern = null);

		string[] getDirectories (string resource, string pattern = null);

		string getDocumentPath();

		void deleteFile (string file);

		void deleteDirectory (string file);

		DateTime timeFile (string resource);

		string readAsset (string resource);

		void writeFile (string resource, string content);

		string writeFile (string resource, byte[] bytes);

		Task<string> showActionSheet (string message, params string[] options);

		void showError (Exception exception);

		// Khang added
		string readSampleData(string file);

		string getJSON (object obj);

		T getObject<T> (string json);

		void invokeLater (Action action);

		void navigateToMap (string label, string location);

		void navigateToMap (string label, double latitude, double longitude);

		void openURL (string url);

		void openFile (string filePath, string mimeType = null);

		Task<byte[]> resizeImage (byte[] imageData, float width, float height, bool allowUpscale);

		string setServiceEndpoint (string url);

		void authenticate (string domain, string username, string password);

		Task<bool> checkHost ();

		Task<bool> checkService ();

		Task<string> callService (string xml);

		bool isNetworkConnected ();

		Task<bool> isHostReachable (string host, int port);

		bool ifDebug (Action action = null);

		bool ifRelease (Action action = null);

		string escapeXML (string xml);

		string unescapeXML (string xml);

		byte[] compress (byte[] bytes);

		byte[] decompress (byte[] bytes);

		Task<double[]> geocodeAddress (string location);

		void writeBSON (string file, object obj);

		T readBSON<T> (string file);

		byte[] TakeScreenShot (View screenShotView, Size viewSize);

		void SaveScreenShot (string filePath, View screenShotView, Size viewSize);

		byte[] readResourceBytesSync(string resource);

		Task<JObject> CallRestApiAsync (string url);
		Task<JObject> CallRestApiAsyncV1(string url);

		void makeACall(string phoneNumber);

		Task<bool> CallPostRequest(string url, string data);
		Task<bool> CallPostRequestV1(string url, string data);
		Task<bool> CallPutRequest(string url, string data);

		string readResourceSync(string name);

		Size getPhysicalSize();

		Task<string> CallPeopleSoftRequest(string url, string data);

		void WireUpBackgroundServiceTask();

	 	void ExtractZipFile(string archiveFilenameIn, string outFolder);

		Task<string> callFinancialSoapAsync(string emplId);

		Task<string> callSoapAsync(string xmlPackage, string action);

		void PushLocalNotification(string alertBody, string alertAction);

        // Khang added 
        Task<bool> Logout(string token);
		Task<string> GetToken(string username, string password, string deviceName);
        Task<List<JObject>> GetPOJsonDataAsync(string token, string deviceName);
        Task<Dictionary<int, string>> GetSupplierLookupAsync(string token, string deviceName);
        void UpdateConfigs(string hostname, string serviceRoot, string environment, string version, string deviceName);
        Task<bool> ReceiveFullOrder(string token, string deviceName, int num, string type, string company);
        Task<bool> ReceivePartialOrder(string token, string deviceName, int poNum, string poType, string poCompany, Dictionary<float, JObject> lines);
        Task<string> SetOrderLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string text);
        Task<string> GetOrderLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum);
        Task<string> GetOrderText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix);
        Task<string> SetOrderText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, string text);
        Task<string> GetOrderFileList(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix);
        Task<string> GetOrderLineFileList(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum);
        Task<string> SetLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string fileLocation, byte[] imageBytes);
        Task<string> SetOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, string fileLocation, byte[] imageBytes);
        Task<Stream> GetOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum);
        Task<Stream> GetLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum);
        Task<string> DeleteOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum);
        Task<string> DeleteLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum);
        Task<List<JObject>> SearchPOJsonDataAsync(string token, string deviceName, string poNumber, bool typeEQUAL, string poType,
                                                                            string supplierName,
                                                                            string recent,
                                                                            DateTime fromD, DateTime toD,
                                                                            string projectCode,
                                                                            string user,
                                                                            string account);
        // Khang added 13/6
        //Task<Dictionary<float, float>> GetUnitPrice(string token, string deviceName, int poNum, string poType, string poCompany, Dictionary<float, JObject> lines);
	}
}

