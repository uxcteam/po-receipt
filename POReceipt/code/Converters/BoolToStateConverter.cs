using System;
using System.Globalization;
using Xamarin.Forms;

namespace POReceipt
{
	public class BoolToStateConverter: IValueConverter
	{

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool booleanValue = (bool)value;
			return (booleanValue ? "Open" : "Closed");
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool booleanValue = (bool)value;
			return (booleanValue ? "Open" : "Closed");
		}
	}
}

