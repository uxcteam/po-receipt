using System;
using System.Globalization;
using Xamarin.Forms;


namespace POReceipt
{
	public class NegateBooleanConverter : IValueConverter
	{

		public object ConvertBack (object value, Type targetType, object parameter, CultureInfo culture)
		{
			return !(bool)value;
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return !(bool)value;
		}
	}
}

