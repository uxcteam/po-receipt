﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace POReceipt
{
	public class StringToBoolConverter<T> : IValueConverter
	{
		public StringToBoolConverter()
		{
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var stringValue = (string)value;
			return string.IsNullOrEmpty(stringValue);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var stringValue = (string)value;
			return string.IsNullOrEmpty(stringValue);
		}
	}
}
