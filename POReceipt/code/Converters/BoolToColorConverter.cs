﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace POReceipt
{
	public class BoolToColorConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool booleanValue = (bool)value;
			return (booleanValue ? Color.Silver : Color.FromHex("#4A4A4A"));
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool booleanValue = (bool)value;
			return (booleanValue? Color.Silver : Color.FromHex("#4A4A4A"));
		}
	}
}
