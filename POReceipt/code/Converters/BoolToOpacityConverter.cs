using System;
using System.Globalization;
using Xamarin.Forms;

namespace POReceipt
{
	public class BoolToOpacityConverter : IValueConverter
	{

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool booleanValue = (bool)value;
			return (booleanValue ? 1f : 0.5f);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool booleanValue = (bool)value;
			return (booleanValue ? 1f : 0.5f);
		}
	}
}

