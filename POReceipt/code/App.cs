using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
//using POReceipt.Shared;

[assembly:XamlCompilation (XamlCompilationOptions.Compile)]

namespace POReceipt
{
	public class App : Application
	{
		public static INative Native;

		public static bool IsBackgrounded { get; private set; }
		public static int ScreenWidth = 0;
		public static string VERSION_NUMBER = "1_0_0_1";	// Added by Khang for local data management
		public static string VERSION_DATE = "01/04/2017";
		public static NavigationPage mainNavPage;
		public static bool signedIn;
		public static User UserData;
		public static List<Preset> presetList;
		public static List<PO> AllPOs;
		public static Dictionary<string, List<string>> PropertySourceDictionary;
		public static Settings Settings;

		public App (INative native)
		{
			Native = native;
			presetList = new List<Preset>();
			AllPOs = new List<PO>();
			//LoadUserData(userData);
			//LoadPOListStored();
			LoadPresetListStored();
			Settings = Settings.LoadSettings();
			var defaultPreset = GetDefaultPreset();
			if (defaultPreset != null)
			{
				var allPOsPage = new AllPOsScreen(defaultPreset);
				MainPage = new RootPage(allPOsPage);
			}
			else {
				var allPOsPage = new AllPOsScreen(new Preset());
				MainPage = new RootPage(allPOsPage);
			}
		}

		public static void LoadUserData(User user) {
			if (user == null)
				return;
			App.UserData = user;
			App.AllPOs = UserData.AllPOs;
			App.PropertySourceDictionary = Model.ExtractPropertySource(App.AllPOs);
		}


		public static Preset GetDefaultPreset() {
			if (App.presetList == null || App.presetList.Count == 0)
				return null;
			foreach (var preset in App.presetList) {
				if (preset.IsDefault)
					return preset;
			}
			return null;
		}

		public static void SaveNewPreset(Preset preset) {
			if (App.presetList != null) {
				preset.IsSaved = true;
				if (!App.presetList.Any(p => p.Name.Equals(preset.Name)))
				{
					App.presetList.Add(preset);
				}
				else { 
					for (int i = 0; i < App.presetList.Count; i++) {
						if (App.presetList[i].Name.Trim() == preset.Name.Trim()) {
							App.presetList[i] = preset;
						}
					}
				}
				MessagingCenter.Send<object>(Conts.MSG_UPDATE_PRESET_LIST_MENU, "updateList");
				App.StorePresetList(App.presetList);
			}
		}

		public static void DeletePreset(string presetName) { 
			if (App.presetList != null && App.presetList.Any(p => p.Name.Equals(presetName))) {
				App.presetList.Remove(App.presetList.Where(item => item.Name == presetName).First());
				MessagingCenter.Send<object>(App.Current.MainPage, Conts.MSG_UPDATE_PRESET_LIST_MENU);
				App.StorePresetList(App.presetList);
			}
		}

		public static void SavePresetChanges(Preset updatedPreset) {
			if (App.presetList != null && updatedPreset != null) {
				var oldItem = App.presetList.Where(item => item.Name == updatedPreset.Name).First();
				var index = App.presetList.IndexOf(oldItem);
				App.presetList[index] = updatedPreset;
				MessagingCenter.Send<object>(App.Current.MainPage, Conts.MSG_UPDATE_PRESET_LIST_MENU);
				App.StorePresetList(App.presetList);
			}
		}

		public static void SetPresetDefault(Preset updatedPreset)
		{
			if (App.presetList != null && updatedPreset != null)
			{
				//	var oldItem = App.presetList.Where(item => item.Name == updatedPreset.Name).First();
				//	var index = App.presetList.IndexOf(oldItem);
				//	App.presetList[index].IsDefault = updatedPreset.IsDefault = true;
				updatedPreset.IsDefault = true;
				foreach (var item in App.presetList) {
					if (item.Name == updatedPreset.Name)
					{
						item.IsDefault = true;
					}
					else {
						item.IsDefault = false;
					}
				}
				MessagingCenter.Send<object>(Current.MainPage, Conts.MSG_UPDATE_PRESET_LIST_MENU);
				App.StorePresetList(App.presetList);
			}
		}

		public static Preset GetPresetFromList(string name) {
			foreach (var preset in presetList) {
				if (preset.Name == name)
					return preset.GetClone();
			}
			return null;
		}

		public static bool CheckPresetNameExists(string name) {
			return App.presetList.Any(p => p.Name.Trim().Equals(name.Trim()));
		}

		public static void StorePresetList(List<Preset> list) { 
			var json = JsonConvert.SerializeObject(list);
			App.Native.writeFile(Conts.PRESET_SETTINGS_FILE_NAME, json);
		}

		public static void LoadPresetListStored() {
			var jsonStr = App.Native.readFile(Conts.PRESET_SETTINGS_FILE_NAME);
			if (!string.IsNullOrEmpty(jsonStr)) {
				var resultList = JsonConvert.DeserializeObject<List<Preset>>(jsonStr);
				if (resultList != null) {
					App.presetList = resultList;
				}
			}
		}

		public static void Logout() {
			if (App.UserData != null)
			{
				App.Settings.AccountRemember = false;
				Settings.StoreSettings(App.Settings);
			}
		}

		public static void SetMainPage<T> () where T : Page
		{
			Device.BeginInvokeOnMainThread (() => {
				var page = Activator.CreateInstance<T> ();

				SetMainPage (page);
			});
		}

		public static void SetMainPage (Page page)
		{
			SetMainPage (page, null);
		}

		public static void SetMainPage<T> (object BindingContext) where T : Page
		{
			Device.BeginInvokeOnMainThread (() => {
				var page = Activator.CreateInstance<T> ();

				SetMainPage (page, BindingContext);
			});
		}

		public static void SetMainPage (Page page, object BindingContext)
		{
			if (page == null) {
				return;
			}
			
			Device.BeginInvokeOnMainThread (() => {
				if (Application.Current.MainPage == null) { 
					Application.Current.MainPage = page;
					return;
				}

				if (Application.Current.MainPage.Navigation == null) {
					Application.Current.MainPage = page;
					return;
				}

				Application.Current.MainPage = new ContentPage () {
					BackgroundColor = FormUtil.ColorAccent
				};
				App.Native.invokeLater (() => {
					Application.Current.MainPage = page;

					if (BindingContext != null) {
						page.BindingContext = BindingContext;
					}
				});
			});
		}

		public static void SetPage<T> () where T : Page
		{
			Device.BeginInvokeOnMainThread (() => {
				var page = Activator.CreateInstance<T> ();

				SetPage (page);
			});
		}

		public static void SetPage (Page page)
		{
			if (page == null) {
				return;
			}
					
			Device.BeginInvokeOnMainThread (() => {
				Application.Current.MainPage = ActivityNavigationPage.newNavigationPage (page);
			});
		}

	}
}
