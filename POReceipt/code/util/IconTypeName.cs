using System;
using Xamarin.Forms;

namespace POReceipt
{
	public class IconTypeName
	{
		public const string People = "people.png";
		public const string Map = "map.png";
		public const string Admissions = "admissions.png";
		public const string Courses = "courses.png";
		public const string ShuttleTracker = "shuttle.png";
		public const string Dining = "dining.png";
		public const string Athletics = "athletics.png";
		public const string Library = "library.png";
		public const string News = "news.png";
		public const string Events = "event.png";
		public const string MyUnits = "myUnits.png";
		public const string Social = "social.png";
		public const string MyDetails = "details.png";
		public const string Alerts = "alerts.png";
		public const string MyAccount = "myFinances.png";
		public const string MyFinances = "myFinances.png";
		public const string Documents = "documents.png";
		public const string MyTimeTable = "timeTable.png";
		public const string FindAPC = "pc.png";
		public const string BuildingServices = "services.png";
		public const string Report = "Report.png";
		public const string Payments = "Payments.png";
		public const string Fees = "Frees.png";

		public const string Login = "login.png";
		public const string Logout = "logout.png";
		public const string About = "about.png";
		public const string Feedback = "feedback.png";
		public const string Settings = "settings.png";

		// Icons for side menu
		public const string MenuPeople = "menuPeople.png";
		public const string MenuMap = "menuMap.png";
		public const string MenuAdmissions = "menuAdmissions.png";
		public const string MenuCourses = "menuCourses.png";
		public const string MenuTransport = "menuTransport.png";
		public const string MenuDining = "menuDining.png";
		public const string MenuSport = "menuSport.png";
		public const string MenuLibrary = "menuLibrary.png";
		public const string MenuNews = "menuNews.png";
		public const string MenuEvent = "menuEvent.png";
		public const string MenuMyUnits = "menuMyUnits";
		public const string MenuMyDetails = "menuDetails.png";
		public const string MenuEmergency = "menuEmergency.png";
		public const string MenuMyAccount = "menuAccount.png";
		public const string MenuDocuments = "menuDocuments.png";
		public const string MenuMyFinances = "menuFinances.png";
		public const string MenuMyTimeTable = "menuTimetable.png";
		public const string MenuFindAPC = "menuPc.png";
		public const string MenuBuildingServices = "menuServices.png";

		public const string MenuLogin = "menuSignIn.png";
		public const string MenuLogout = "menuSignOut.png";
		public const string MenuAbout = "menuAbout.png";
		public const string MenuFeedback = "menuFeedBack.png";
		public const string MenuSettings = "menuSetting.png";
	}

	public class ThemeColor {
		public static Color Gray = Color.Gray;
		public static Color LightGray = Color.FromHex("#cccccc");
		public static Color Red = Color.FromHex("#ee1d25");
		public static Color LightRed = Color.FromHex("#ff7878");
		public static Color Red01 = Color.FromHex("#E46C6D");
		public static Color Blue1 = Color.FromHex("#3498db");
		public static Color Blue1_Dark = Color.FromHex("#2980b9");

		public static Color Blue2 = Color.FromHex("#3367D6");
		public static Color Blue2_Dark = Color.FromHex("#184AB6");

		public static Color Green = Color.FromHex("#16a085");


		public static Color DarkGreen = Color.Teal;
		public static Color DarkBlue = Color.FromHex("#2980b9");
		public static Color DarkBlue01 = Color.FromHex("#323A45");
		public static Color DarkBlue02 = Color.FromHex("#667383");
		public static Color DarkBlue03 = Color.FromHex("#A5ADBA");

	}
}

