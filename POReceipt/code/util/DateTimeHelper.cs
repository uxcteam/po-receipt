using System;
namespace POReceipt
{
	public class DateTimeHelper
	{
		public DateTimeHelper()
		{
		}

		public static string GetDaySuffix(int day)
		{
			switch (day)
			{
				case 1:
				case 21:
				case 31:
					return "st";
				case 2:
				case 22:
					return "nd";
				case 3:
				case 23:
					return "rd";
				default:
					return "th";
			}
		}

		public static string TimeAgo(DateTime dt)
		{
			if (dt > DateTime.Now)
				return "about sometime from now";
			TimeSpan span = DateTime.Now - dt;

			if (span.Days > 365)
			{
				int years = (span.Days / 365);
				if (span.Days % 365 != 0)
					years += 1;
				return String.Format("about {0} {1} ago", years, years == 1 ? "year" : "years");
			}

			if (span.Days > 30)
			{
				int months = (span.Days / 30);
				if (span.Days % 31 != 0)
					months += 1;
				return String.Format("about {0} {1} ago", months, months == 1 ? "month" : "months");
			}

			if (span.Days > 0)
				return String.Format("about {0} {1} ago", span.Days, span.Days == 1 ? "day" : "days");

			if (span.Hours > 0)
				return String.Format("about {0} {1} ago", span.Hours, span.Hours == 1 ? "hour" : "hours");

			if (span.Minutes > 0)
				return String.Format("about {0} {1} ago", span.Minutes, span.Minutes == 1 ? "minute" : "minutes");

			if (span.Seconds > 5)
				return String.Format("about {0} seconds ago", span.Seconds);

			if (span.Seconds <= 5)
				return "just now";

			return string.Empty;
		}

		public static double DateTimeToUnixTimestamp(DateTime dateTime)
		{
			return (TimeZoneInfo.ConvertTime(dateTime,TimeZoneInfo.Utc) -
				   new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;
		}

		public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
		{
			// Unix timestamp is seconds past epoch
			System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
			dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
			return dtDateTime;
		}
	}
}

