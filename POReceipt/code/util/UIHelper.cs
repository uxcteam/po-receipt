using System;
using Xamarin.Forms;
using System.Threading;

namespace POReceipt
{
	public class UIHelper
	{
		public UIHelper()
		{
		}

		public static void setPositionAtGrid(Grid grid, View view, int row, int column) { 
			grid.Children.Add(view, column, row);
			Grid.SetRow(view, row);
			Grid.SetColumn(view, column);
		}

		public static void setPositionAtGridWithRowSpan(Grid grid, View view, int row, int column, int rowSpan)
		{
			grid.Children.Add(view, column, row);
			Grid.SetRow(view, row);
			Grid.SetColumn(view, column);
			Grid.SetRowSpan(view, rowSpan);
		}

		public static void addClickEventForView(View view, EventHandler onClick, ClickAnimation animation) {
			
			var tap = new TapGestureRecognizer
			{
				Command = new Command(async (o) =>
				{
					if (!view.IsEnabled)
						return;
					if (animation == ClickAnimation.Scale)
					{
						await view.ScaleTo(0.8, 50, Easing.CubicOut);
						await view.ScaleTo(1, 50, Easing.CubicIn);
					}
					else if (animation == ClickAnimation.Fade) { 
						await view.FadeTo(0.7, 50, Easing.CubicOut);
						await view.FadeTo(1, 50, Easing.CubicIn);
					}

					if (onClick != null)
						onClick(view, EventArgs.Empty);
				}),
			};
			tap.NumberOfTapsRequired = 1;

			view.GestureRecognizers.Add(tap);
		}

		public static void addClickEventForView(View view, EventHandler onClick, ClickAnimation animation, uint time)
		{

			var tap = new TapGestureRecognizer
			{
				Command = new Command(async (o) =>
				{
					if (animation == ClickAnimation.Scale)
					{
						await view.ScaleTo(0.8, time, Easing.CubicOut);
						await view.ScaleTo(1, time, Easing.CubicIn);
					}
					else if (animation == ClickAnimation.Fade)
					{
						await view.FadeTo(0.7, time, Easing.CubicOut);
						await view.FadeTo(1, time, Easing.CubicIn);
					}

					if (onClick != null)
						onClick(view, EventArgs.Empty);
				}),
			};
			tap.NumberOfTapsRequired = 1;
			view.GestureRecognizers.Add(tap);
		}

		public static async void animateView(View view, ClickAnimation animation)
		{
			if (animation == ClickAnimation.Scale)
			{
				await view.ScaleTo(0.8, 50, Easing.CubicOut);
				await view.ScaleTo(1, 50, Easing.CubicIn);
			}
			else if (animation == ClickAnimation.Fade)
			{
				await view.FadeTo(0.7, 50, Easing.CubicOut);
				await view.FadeTo(1, 50, Easing.CubicIn);
			}
		}

		public static void ExpandGridRow(Grid grid, RowDefinition gridRow, int height, EventHandler onCompleted)
		{
			if (grid == null || gridRow == null)
				return;
			Animation animation;
			if (gridRow.Height.Value<height)
			{
				// Show
				animation = new Animation(
					callback: (d) =>
					{
					gridRow.Height = Clamp(d, 0, height);
					},
					start: gridRow.Height.Value,
					end: height,
					easing: Easing.Linear,
					finished: () =>
					{
						animation = null;

					}
				);
				animation.Commit(owner: grid, name: "AnimationTag", rate: 16, length: 200);
			} else
			{
				// Hide
				animation = new Animation(
					callback: (d) =>
					{
					gridRow.Height = Clamp(d, 0, double.MaxValue);
					},
					start: height,
					end: height,
					easing: Easing.Linear,
					finished: () =>
					{
						animation = null;
					});
				animation.Commit(owner: grid, name: "AnimationTag", rate: 16, length: 200);
			}

			if (onCompleted != null)
			{
				onCompleted(null, null);
			}
		}

		public static void DoCollapseAndExpand(Grid grid, RowDefinition gridRow, int height, EventHandler onCompleted)
		{
			if (grid == null || gridRow == null)
				return;
			Animation animation;
			if (gridRow.Height.Value < height)
			{
				// Show
				animation = new Animation(
					callback: (d) =>
					{
					gridRow.Height = Clamp(d, 0, height);
					},
					start: gridRow.Height.Value,
					end: height,
					easing: Easing.Linear,
					finished: () =>
					{
						animation = null;

					}
				);
				animation.Commit(owner: grid, name: "AnimationTag", rate: 16, length: 200);
			}
			else
			{
				// Hide
				animation = new Animation(
					callback: (d) =>
					{
					gridRow.Height = Clamp(d, 0, double.MaxValue);
					},
					start: height,
					end: 0,
					easing: Easing.Linear,
					finished: () =>
					{
						animation = null;
					});
				animation.Commit(owner: grid, name: "AnimationTag", rate: 16, length: 200);
			}

			if (onCompleted != null) {
				onCompleted(null, null);
			}
		}

		private static double Clamp(double value, double minValue, double maxValue)
		{
			if (value < minValue)
			{
				return minValue;
			}

			if (value > maxValue)
			{
				return maxValue;
			}

			return value;
		}

		public static string Truncate(string value, int maxChars)
		{
			return value.Length <= maxChars ? value : value.Substring(0, maxChars) + "...";
		}


	}

	public enum ClickAnimation
	{
		Scale, Fade, None
	}


}

