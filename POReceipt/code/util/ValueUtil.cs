using System;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace POReceipt
{
	public static class ValueUtil
	{
		public static bool IsEmpty (params string[] values)
		{
			if (values == null || values.Length == 0) {
				return true;
			}

			foreach (var str in values) {
				if (!string.IsNullOrWhiteSpace (str)) {
					return false;
				}
			}

			return true;
		}

		public static string joinStrings (string separator, params string[] values)
		{
			if (values == null || values.Length == 0) {
				return string.Empty;
			}

			var list = new List<string> ();
			foreach (var str in values) {
				if (!string.IsNullOrWhiteSpace (str)) {
					list.Add (str);
				}
			}

			return string.Join (separator, list);
		}

		public static readonly XDocument EMPTY_DOCUMENT = new XDocument ();

		public static XDocument ParseXML (string xml)
		{
			if (string.IsNullOrEmpty (xml)) {
				return EMPTY_DOCUMENT;
			}

			try {
				return XDocument.Parse (xml, LoadOptions.None);
			} catch {
				return EMPTY_DOCUMENT;
			}
		}

		public static List<string> valueStrings (IEnumerable<XElement> elems, string name)
		{
			var strTypes = new List<string> ();

			if (elems != null) {
				var types = elems.Where (e => e.Name == XName.Get (name));
				if (types != null) {
					foreach (var type in types) {
						var strType = (type.Value ?? "").Trim ();
						if (strType.Length > 0) {
							strTypes.Add (type.Value);
						}
					}
				}
			}

			return strTypes;
		}

		public static int values (XDocument doc, string name, Action<IEnumerable<XElement>> onEach, int maxCount = 0)
		{
			var count = 0;

			if (doc != null) {
				if (doc.Root != null) {
					var all = doc.Root.Descendants ();
					if (all != null) {
						var elems = all.Where (e => e.Name.LocalName == name);
						if (elems != null) {
							foreach (var elem in elems) {
								var nodes = elem.Descendants ();
								if (nodes != null && nodes.Any ()) {
									count++;

									if (onEach != null) {
										onEach (nodes);
									}

									if (maxCount > 0 && count == maxCount) {
										break;
									}
								}
							}
						}
					}
				}
			}

			return count;
		}

		public static float valueFloat (IEnumerable<XElement> elems, string name)
		{
			var str = value (elems, name);
			float val;
			if (float.TryParse (str, out val)) {
				return val;
			}
			return 0f;
		}

		public static int valueInt (IEnumerable<XElement> elems, string name)
		{
			var str = value (elems, name);
			int val;
			if (int.TryParse (str, out val)) {
				return val;
			}
			return 0;
		}

		public static bool valueBool (IEnumerable<XElement> elems, string name)
		{
			var val = value (elems, name);

			if (string.Equals ("true", val, StringComparison.OrdinalIgnoreCase)) {
				return true;
			}

			if (string.Equals ("yes", val, StringComparison.OrdinalIgnoreCase)) {
				return true;
			}

			return false;
		}

		public static string value (IEnumerable<XElement> elems, string name)
		{
			return value (elems, e => e.Name.LocalName == name);
		}

		public static string value (IEnumerable<XElement> elems, Func<XElement, bool> exp)
		{
			if (elems == null || !elems.Any ()) {
				return string.Empty;
			}
			var node = elems.FirstOrDefault (exp);
			if (node == null) {
				return string.Empty;
			}
			return (node.Value ?? string.Empty).Trim ();
		}

		public static DateTime combineDateTime (string dateString, string dateFormat, string timeString, string timeFormat)
		{
			var hasDate = !string.IsNullOrWhiteSpace (dateString) && !string.IsNullOrWhiteSpace (dateFormat);
			var hasTime = !string.IsNullOrWhiteSpace (timeString) && !string.IsNullOrWhiteSpace (timeFormat);

			if (hasDate && hasTime) {
				return parseDate (dateString + " " + timeString, dateFormat + " " + timeFormat);
			} else if (hasDate) {
				return parseDate (dateString, dateFormat);
			} else if (hasTime) {
				return parseDate (timeString, timeFormat);
			}

			return DateTime.MinValue;
		}

		public static string date (string val, string format = "ddd, dd MMM yyyy, hh:mm tt")
		{
			if (string.IsNullOrWhiteSpace (val)) {
				return string.Empty;
			}

			var _date = parseDate (val);
			if (_date != DateTime.MinValue) {
				return _date.ToString (format);
			}
			return val;
		}

		public static DateTime parseDate (string val, string format)
		{
			var emptyDate = DateTime.MinValue;

			if (string.IsNullOrWhiteSpace (val) || string.IsNullOrWhiteSpace (format)) {
				return emptyDate;
			}

			val = val.Trim ();
			format = format.Trim ();

			DateTime dateTime;
			if (DateTime.TryParseExact (val, format, 
				    System.Globalization.CultureInfo.InvariantCulture, 
				System.Globalization.DateTimeStyles.None, 
				    out dateTime)) {
				return dateTime;
			}
			return emptyDate;
		}

		public static DateTime parseDate (string val)
		{
			var emptyDate = DateTime.MinValue;

			if (string.IsNullOrWhiteSpace (val)) {
				return emptyDate;
			}

			val = val.Trim ().Split (new char[]{ '.' }) [0];
			DateTime _date;
			if (DateTime.TryParse (val, out _date)) {
				return _date;
			}
			return emptyDate;
		}

		public static string fixNotes (string str)
		{
			if (string.IsNullOrWhiteSpace (str)) {
				return string.Empty;
			}
			return str.Replace ("* ", "\n• ").Replace ("*.", ".").Replace ("  ", " ").Trim ();
		}

		public static string fixTitle (string str)
		{
			return ValueUtil.fixHyphens (ValueUtil.fixCommas (str)).Replace ("/", " / ").Replace ("  ", " ");
		}

		public static string fixHyphens (string str)
		{
			if (string.IsNullOrWhiteSpace (str)) {
				return string.Empty;
			}
			return str.Replace ("-", " - ").Replace ("  ", " ").Trim ();
		}

		public static string fixCommas (string str)
		{
			if (string.IsNullOrWhiteSpace (str)) {
				return string.Empty;
			}
			return str.Replace (",", ", ").Replace ("  ", " ").Trim ();
		}

		public static string titleCase (string value)
		{
			if (string.IsNullOrWhiteSpace (value)) {
				return string.Empty;
			}

			var str = "";
			var words = value.Split (new string[]{ "," }, StringSplitOptions.RemoveEmptyEntries);
			foreach (var _word in words) {
				if (string.IsNullOrWhiteSpace (_word)) {
					continue;
				}
				var word = _word.Trim ();

				if (word.Length < 3 || containsNumbers (word)) {
					word = word.ToUpper ();
				} else {
					word = word.Substring (0, 1).ToUpper () + word.Substring (1).ToLower ();
				}

				if (str.Length > 0) {
					str += ", ";
				}
				str += word;
			}

			return str;
		}

		public static bool containsNumbers (string str)
		{
			foreach (var c in "0123456789".ToCharArray()) {
				if (str.IndexOf (c) > -1) {
					return true;
				}
			}
			return false;
		}

		public static string makeKeyNumbers (params string[] args)
		{
			var nums = new List<string> ();

			foreach (var _param in args) {
				var param = (_param ?? "").Trim ();
				int val;
				if (int.TryParse (param, out val)) {
					nums.Add (val.ToString ("X"));
				} else {
					nums.Add (param);
				}
			}

			return makeKey (nums.ToArray ());
		}

		public static string makeKey (params string[] args)
		{
			Func<string, string> clean = (val) => {
				var t = "";
				var s = (val ?? "").Trim ().ToLowerInvariant ();
				var v = false;
				foreach (var c in s.ToCharArray()) {
					if (Char.IsLetterOrDigit (c)) {
						t += c;
						v = false;
					} else {
						if (!v) {
							t += "_";
							v = true;
						}
					}
				}
				return t;
			};

			var str = "";
			foreach (var arg in args) {
				if (str.Length > 0) {
					str += "-";
				}
				str += clean (arg);
			}

			return str;
		}

		public static string location (string address, bool addCountry = false, int maxParts = 0)
		{
			var arr = address.Split (new char[]{ ',' }, StringSplitOptions.RemoveEmptyEntries);

			var parts = new List<string> ();
			foreach (var el in arr) {
				var part = el.Trim ();
				if (part.Length == 0) {
					continue;
				}
				if (IsPostCode (part)) {
					continue;
				}
				if (parts.Count > 0) {
					if (containsNumbers (part)) {
						continue;
					}
					if (part.Length < 3) {
						continue;
					}
				}
				if (part.Contains ("(") && part.Contains (")")) {
					var posStart = part.IndexOf ("(");
					if (posStart > -1) {
						var posEnd = part.IndexOf (")", posStart);
						if (posEnd > posStart) {
							var paran = part.Substring (posStart, posEnd - posStart + 1);
							part = part.Replace (paran, "").Trim ();
							if (part.Length == 0) {
								continue;
							}
						}
					}
				}
				if (parts.Contains (part)) {
					continue;
				}
				parts.Add (part);
			}

			if (addCountry) {
				parts.Add ("UK");
			}

			if (maxParts > 0 && parts.Count > maxParts) {
				parts = parts.GetRange (0, maxParts);
			}

			var query = string.Join (", ", parts);

			query = query.Replace ("  ", " ").Trim ();

			return query;
		}

		// Added by Khang
		public static string addMinutes(string time, string format, int minutes, ref bool needToAdjustDate) {
			var dateTime = DateTime.ParseExact(time, format, System.Globalization.CultureInfo.InvariantCulture);
			var adjustedDateTime = dateTime.AddMinutes (minutes);
			if (adjustedDateTime.Date.CompareTo(dateTime.Date) != 0)
				needToAdjustDate = true;
			return adjustedDateTime.ToString (format);
		}

		// Added by Khang
		public static string addDay(string date, string format, int day)
		{
			var dateTime = DateTime.ParseExact(date, format, System.Globalization.CultureInfo.InvariantCulture);
			var adjustedDateTime = dateTime.AddDays(day);
		
			return adjustedDateTime.ToString(format);
		}

		public static bool IsPostCode (string postcode)
		{
			return (
			    Regex.IsMatch (postcode, "(^[A-PR-UWYZa-pr-uwyz][0-9][ ]*[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}$)") ||
			    Regex.IsMatch (postcode, "(^[A-PR-UWYZa-pr-uwyz][0-9][0-9][ ]*[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}$)") ||
			    Regex.IsMatch (postcode, "(^[A-PR-UWYZa-pr-uwyz][A-HK-Ya-hk-y][0-9][ ]*[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}$)") ||
			    Regex.IsMatch (postcode, "(^[A-PR-UWYZa-pr-uwyz][A-HK-Ya-hk-y][0-9][0-9][ ]*[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}$)") ||
			    Regex.IsMatch (postcode, "(^[A-PR-UWYZa-pr-uwyz][0-9][A-HJKS-UWa-hjks-uw][ ]*[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}$)") ||
			    Regex.IsMatch (postcode, "(^[A-PR-UWYZa-pr-uwyz][A-HK-Ya-hk-y][0-9][A-Za-z][ ]*[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}$)") ||
			    Regex.IsMatch (postcode, "(^[Gg][Ii][Rr][]*0[Aa][Aa]$)")
			);
		}
	}
}

