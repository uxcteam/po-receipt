using System;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace POReceipt
{
	public class ActivityNavigationPage : NavigationPage
	{
		public static bool TimeoutEnable = true;

		public static NavigationPage newNavigationPage (Page page)
		{
			state = -1;
			lastActivity = DateTime.MinValue;

			return new ActivityNavigationPage (page);
		}

		private static int state;
		private static DateTime lastActivity;

		private ActivityNavigationPage (Page page) : base (page)
		{
			
		}

		protected override void OnPropertyChanged (string propertyName = null)
		{
			base.OnPropertyChanged (propertyName);
		}

		protected override void OnParentSet ()
		{
			base.OnParentSet ();
		}

	}
}
