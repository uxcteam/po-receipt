﻿using System;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace POReceipt
{
	public class FileUtil
	{
		public FileUtil()
		{
		}

		private static ImageSource emptySource = ImageSource.FromStream(() => new MemoryStream());

		private static ImageSource getImage(string path)
		{
			try
			{
				if (!string.IsNullOrWhiteSpace(path))
				{
					var bytes = App.Native.readBytes(path);
					if (bytes != null && bytes.Length > 0)
					{
						return ImageSource.FromStream(() => GetStream(bytes));
					}
				}
			}
			catch (Exception e)
			{
				App.Native.log(e);
			}

			return emptySource;
		}

		public static Stream GetStream(byte[] bytes)
		{
			return new MemoryStream(bytes);
		}

		static byte[] readBytes(string input)
		{
			var str = App.Native.unescapeXML(input);
			if (string.IsNullOrWhiteSpace(str))
			{
				return new byte[0];
			}

			const string strStart = "<![CDATA[";
			const string strEnd = "]]>";
			var posStart = str.IndexOf(strStart, StringComparison.Ordinal);
			if (posStart > -1)
			{
				posStart += strStart.Length;
				var posEnd = str.IndexOf(strEnd, posStart, StringComparison.Ordinal);
				if (posEnd > posStart)
				{
					str = str.Substring(posStart, posEnd - posStart);
				}
			}

			var bytes = Convert.FromBase64String(str);

			bytes = App.Native.decompress(bytes);

			return bytes;
		}

		public static async Task<byte[]> GetBytes(Stream stream)
		{
			//var mem = new MemoryStream ();
			// return mem.ToArray (); // Commented by Khang

			using (var mem = new MemoryStream())
			{
				await stream.CopyToAsync(mem);
				return mem.ToArray();   // Added by Khang
			}
		}
	}
}
