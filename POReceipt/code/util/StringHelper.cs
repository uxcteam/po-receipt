﻿using System;
namespace POReceipt
{
    public static class StringHelper
    {
		public static string GetContentAroundKeyword(string keyword, string str)
		{
			var results = string.Empty;
			if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(str))
			{
				keyword = keyword.Trim().ToLower();
				var trimStr = str.Trim().ToLower();

				if (string.IsNullOrEmpty(trimStr))
					return results;
				var firstIndex = trimStr.IndexOf(keyword, StringComparison.Ordinal);

				if (firstIndex == -1)
					return results;

				var lastIndex = firstIndex + keyword.Length;
				bool metWhiteSpace = false;
				while (firstIndex > 0)
				{
					firstIndex--;
					if (trimStr[firstIndex] == ' ')
					{
						if (metWhiteSpace)
							break;
						metWhiteSpace = true;
					}
				}
				metWhiteSpace = false;
				while (lastIndex < trimStr.Length - 1)
				{
					lastIndex++;
					if (trimStr[lastIndex] == ' ')
					{
						if (metWhiteSpace)
							break;
						metWhiteSpace = true;
					}
				}
				var length = lastIndex < str.Length - 1 ? (lastIndex - firstIndex + 1) : (lastIndex - firstIndex);

				results = str.Substring(firstIndex, length);
			}

			return results;
		}
    }
}
