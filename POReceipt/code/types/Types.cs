using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Globalization;
using System.Collections.ObjectModel;
using Xamarin.Forms.Maps;

namespace POReceipt
{
	public static class Types
	{
		public class BoundValue : INotifyPropertyChanged
		{
			public Action<string, BoundValue> OnValue;

			private string stringValue;

			public string StringValue { 
				get {
					return stringValue;
				}
				set {
					stringValue = value;
					OnPropertyChanged ("StringValue");
				}
			}

			private DateTime dateValue;

			public DateTime DateValue { 
				get {
					return dateValue;
				}
				set {
					dateValue = value;
					OnPropertyChanged ("DateValue");
				}
			}

			private TimeSpan timeValue;

			public TimeSpan TimeValue { 
				get {
					return timeValue;
				}
				set {
					timeValue = value;
					OnPropertyChanged ("TimeValue");
				}
			}

			public void Bind (Label obj)
			{
				obj.BindingContext = this;

				obj.SetBinding (Label.TextProperty, "StringValue");
			}

			public void Bind (Entry obj)
			{
				obj.BindingContext = this;

				obj.SetBinding (Entry.TextProperty, "StringValue");
			}

			public void Bind (Button obj)
			{
				obj.BindingContext = this;

				obj.SetBinding (Button.TextProperty, "StringValue");
			}

			public void Bind (DatePicker obj)
			{
				obj.BindingContext = this;

				obj.SetBinding (DatePicker.DateProperty, "DateValue");
			}

			public void Bind (TimePicker obj)
			{
				obj.BindingContext = this;

				obj.SetBinding (TimePicker.TimeProperty, "TimeValue");
			}

			#region INotifyPropertyChanged implementation

			public event PropertyChangedEventHandler PropertyChanged;

			public void OnPropertyChanged (string propertyName = null)
			{
				if (PropertyChanged != null) {
					PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
				}

				if (OnValue != null) {
					OnValue (propertyName, this);
				}
			}

			#endregion
		}

		public class Log : INotifyPropertyChanged
		{
			public Color Color { get; set; }

			public DateTime TimeStamp { get; set; }

			public string Title { get; set; }

			public string Content { get; set; }

			#region INotifyPropertyChanged implementation

			public event PropertyChangedEventHandler PropertyChanged;

			public void OnPropertyChanged (string propertyName = null)
			{
				if (PropertyChanged != null) {
					PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
				}
			}

			#endregion
		}

		public class Notification : INotifyPropertyChanged
		{
			public DateTime Date { get; set; }

			public string Title { get; set; }

			public string Content { get; set; }

			#region INotifyPropertyChanged implementation

			public event PropertyChangedEventHandler PropertyChanged;

			public void OnPropertyChanged(string propertyName = null)
			{
				if (PropertyChanged != null)
				{
					PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
				}
			}

			#endregion
		}


		public class UserProfile : INotifyPropertyChanged
		{
			public string Name { get; set; }

			#region INotifyPropertyChanged implementation

			public event PropertyChangedEventHandler PropertyChanged;

			public void OnPropertyChanged(string propertyName = null)
			{
				if (PropertyChanged != null)
				{
					PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
				}
			}

			#endregion
		}


	}

}

