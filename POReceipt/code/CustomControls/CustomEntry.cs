﻿using System;
using Xamarin.Forms;

namespace POReceipt
{
	public class CustomEntry : Entry
	{
		public KeyboardType Type = CustomEntry.KeyboardType.Text;
		public CustomEntry()
		{
		}

		public enum KeyboardType { 
			Number,
			Text
		}
	}
}
