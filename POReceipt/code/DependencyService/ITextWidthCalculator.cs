﻿using System;
namespace POReceipt
{
	public interface ITextWidthCalculator
	{
		double CalculateWidth(string text);
	}
}
