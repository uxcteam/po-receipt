﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace POReceipt
{
	public interface IPicturePicker
	{
		Task<Stream> GetImageStreamAsync();
	}
}
