﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace POReceipt
{
	public interface IImageManager
	{
		Task<string> SavePhotoToDisk(ImageSource imgSrc, int Id);

		string GetPictureFromDisk(int id);
	}
}
