﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
//using POReceipt.Shared;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace POReceipt
{
    public partial class App : Application
    {
        public static INative Native;

        public static bool IsBackgrounded { get; private set; }
        public static int ScreenWidth = 0;
        public static string VERSION_NUMBER = "0_1_0_20";    // Added by Khang for local data management
        public static string VERSION_DATE = "23/10/2017";
        public static NavigationPage mainNavPage;
        public static bool signedIn;
        public static User UserData;
        public static List<Preset> presetList;
        public static List<PO> AllPOs;
        public static Dictionary<string, List<string>> PropertySourceDictionary;
        public static Dictionary<string, List<string>> PropertySourceDictionaryAllPOs;
        public static Settings Settings;
        public static string token;
        public static string deviceName = "testing";

        public App(INative native)
        {
            Native = native;
            presetList = new List<Preset>();
            AllPOs = new List<PO>();
            Settings = Settings.LoadSettings();
            LoadPresetListStored();
            var defaultPreset = GetDefaultPreset();
            if (defaultPreset != null)
            {
                var allPOsPage = new AllPOsScreen(defaultPreset)
                {
                    needRefreshingData = true
                };
                MainPage = new RootPage(allPOsPage);
            }
            else
            {
                var allPOsPage = new AllPOsScreen(new Preset(), true) {
                };
                MainPage = new RootPage(allPOsPage);
            }
        }

        public static Preset LoadDefaultPresetIfExist() {
            LoadPresetListStored();
            var defaultPreset = GetDefaultPreset();
            return defaultPreset;
        }

        public static void LoadUserData(User user)
        {
            if (user == null)
                return;
            App.UserData = user;
            UserDataManager.StoreUserData(App.UserData);
            App.AllPOs = UserData.AllPOs;
            //InitSourceLookups();
        }

        public static void InitSourceLookups() {
			App.PropertySourceDictionary = Model.ExtractPropertySource(App.AllPOs);
        }
        public static void InitSourceLookupsAll() {
            App.PropertySourceDictionaryAllPOs = Model.ExtractPropertySource(App.AllPOs);
        }

        public static Preset GetDefaultPreset()
        {
            if (App.presetList == null || App.presetList.Count == 0)
                return null;
            foreach (var preset in App.presetList)
            {
                if (preset.IsDefault)
                    return preset;
            }
            return null;
        }

        public static void SaveNewPreset(Preset preset)
        {
            if (App.presetList != null)
            {
                preset.IsSaved = true;
                if (!App.presetList.Any(p => p.Name.Equals(preset.Name)))
                {
                    App.presetList.Add(preset);
                }
                else
                {
                    for (int i = 0; i < App.presetList.Count; i++)
                    {
                        if (App.presetList[i].Name.Trim() == preset.Name.Trim())
                        {
                            App.presetList[i] = preset;
                        }
                    }
                }
                MessagingCenter.Send<object>(Conts.MSG_UPDATE_PRESET_LIST_MENU, "updateList");
                App.StorePresetList(App.presetList, App.UserData.Username);
            }
        }

        public static void DeletePreset(string presetName)
        {
            if (App.presetList != null && App.presetList.Any(p => p.Name.Equals(presetName)))
            {
                App.presetList.Remove(App.presetList.Where(item => item.Name == presetName).First());
                MessagingCenter.Send<object>(App.Current.MainPage, Conts.MSG_UPDATE_PRESET_LIST_MENU);
                App.StorePresetList(App.presetList, App.UserData.Username);
            }
        }

        public static void SavePresetChanges(Preset updatedPreset)
        {
            if (App.presetList != null && updatedPreset != null)
            {
                var oldItem = App.presetList.Where(item => item.Name == updatedPreset.Name).First();
                var index = App.presetList.IndexOf(oldItem);
                App.presetList[index] = updatedPreset;
                MessagingCenter.Send<object>(App.Current.MainPage, Conts.MSG_UPDATE_PRESET_LIST_MENU);
                App.StorePresetList(App.presetList, App.UserData.Username);
            }
        }

        public static void SetPresetDefault(Preset updatedPreset)
        {
            if (App.presetList != null && updatedPreset != null)
            {
                updatedPreset.IsDefault = true;
                foreach (var item in App.presetList)
                {
                    if (item.Name == updatedPreset.Name)
                    {
                        item.IsDefault = true;
                    }
                    else
                    {
                        item.IsDefault = false;
                    }
                }
                MessagingCenter.Send<object>(Current.MainPage, Conts.MSG_UPDATE_PRESET_LIST_MENU);
                App.StorePresetList(App.presetList, App.UserData.Username);
            }
        }

        public static Preset GetPresetFromList(string name)
        {
            foreach (var preset in presetList)
            {
                if (preset.Name == name)
                    return preset.GetClone();
            }
            return null;
        }

        public static bool CheckPresetNameExists(string name)
        {
            return App.presetList.Any(p => p.Name.Trim().Equals(name.Trim()));
        }

        public static void StorePresetList(List<Preset> list, string userName)
        {
            if (string.IsNullOrEmpty(userName))
                return;
            var json = JsonConvert.SerializeObject(list);
            var fileName = string.Format("{0}_{1}", userName.ToLower(), Conts.PRESET_SETTINGS_FILE_NAME);
            App.Native.writeFile(fileName, json);
        }

        public static void LoadPresetListStored()
        {
            if (App.Settings == null || string.IsNullOrEmpty(App.Settings.LogonUsername))
                return;
            var fileName = string.Format("{0}_{1}", App.Settings.LogonUsername.ToLower(), Conts.PRESET_SETTINGS_FILE_NAME);
            var jsonStr = App.Native.readFile(fileName);
            if (!string.IsNullOrEmpty(jsonStr))
            {
                var resultList = JsonConvert.DeserializeObject<List<Preset>>(jsonStr);
                if (resultList != null)
                {
                    App.presetList = resultList;
                }
            }
        }

        public static void Logout()
        {
            if (App.UserData != null)
            {
                App.Settings.Logon = false;
                if (!App.Settings.AccountRemember) {
                    App.Settings.LogonUsername = string.Empty;
                    App.Settings.LogonPassword = string.Empty;
                }
                Settings.StoreSettings(App.Settings);
                App.PropertySourceDictionary = new Dictionary<string, List<string>>();
                App.PropertySourceDictionaryAllPOs = new Dictionary<string, List<string>>();
                App.presetList = new List<Preset>();
                MessagingCenter.Send<object>(Current.MainPage, Conts.MSG_UPDATE_PRESET_LIST_MENU);
                MessagingCenter.Send<object>(Current.MainPage, Conts.MSG_LOGOUT);
            }
        }

        public static void SetMainPage<T>() where T : Page
        {
            Device.BeginInvokeOnMainThread(() => {
                var page = Activator.CreateInstance<T>();

                SetMainPage(page);
            });
        }

        public static void SetMainPage(Page page)
        {
            SetMainPage(page, null);
        }

        public static void SetMainPage<T>(object BindingContext) where T : Page
        {
            Device.BeginInvokeOnMainThread(() => {
                var page = Activator.CreateInstance<T>();

                SetMainPage(page, BindingContext);
            });
        }

        public static void SetMainPage(Page page, object BindingContext)
        {
            if (page == null)
            {
                return;
            }

            Device.BeginInvokeOnMainThread(() => {
                if (Application.Current.MainPage == null)
                {
                    Application.Current.MainPage = page;
                    return;
                }

                if (Application.Current.MainPage.Navigation == null)
                {
                    Application.Current.MainPage = page;
                    return;
                }

                Application.Current.MainPage = new ContentPage()
                {
                    BackgroundColor = FormUtil.ColorAccent
                };
                App.Native.invokeLater(() => {
                    Application.Current.MainPage = page;

                    if (BindingContext != null)
                    {
                        page.BindingContext = BindingContext;
                    }
                });
            });
        }

        public static void SetPage<T>() where T : Page
        {
            Device.BeginInvokeOnMainThread(() => {
                var page = Activator.CreateInstance<T>();

                SetPage(page);
            });
        }

        public static void SetPage(Page page)
        {
            if (page == null)
            {
                return;
            }

            Device.BeginInvokeOnMainThread(() => {
                Application.Current.MainPage = ActivityNavigationPage.newNavigationPage(page);
            });
        }

    }
}
