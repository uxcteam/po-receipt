﻿using System;
using Xamarin.Forms;
using Xamarin;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POReceipt
{
	public class AllPOsVM : ViewModelBase
	{
		public List<PO> AllPOs
		{
			set { this._allPOs = value; }
			get { return _allPOs; }
		}
		private List<PO> _allPOs;

		public List<PO> PresetAppliedPOs
		{
			set { this._presetAppliedPOs = value; }
			get { return _presetAppliedPOs; }
		}
		private List<PO> _presetAppliedPOs;

		public List<PO> DisplayingPOs
		{
			set { 
                this._displayingPOs = value; 
                this.OnPropertyChanged(nameof(DisplayingPOs));
			}
			get { return _displayingPOs; }
		}
		private List<PO> _displayingPOs;

		public Preset Preset
		{
			set
			{
				this._preset = value;
				this.OnPropertyChanged(nameof(Preset));
			}
			get
			{
				return _preset;
			}
		}
		private Preset _preset;

		private bool IsApplyingSearchKeyword = false;



		public void DoSearching(string key)
		{
			IsApplyingSearchKeyword = !string.IsNullOrEmpty(key);
			if (IsApplyingSearchKeyword)
			{
				var tempRecords = _presetAppliedPOs.Where(c => (c.MatchKeyword(key) && !c.FullyReceived));
                _displayingPOs = tempRecords.ToList();
			}
			else { 
                if (_presetAppliedPOs != null)
                _displayingPOs = _presetAppliedPOs.Where(c => c.MatchKeyword(key)).ToList();
			}
		}

        public async Task SyncPOData() {
            if (string.IsNullOrEmpty(App.token)) {
                App.token = await App.Native.GetToken(App.UserData.Username, App.UserData.Password, App.deviceName);

            }

            var PODataJson = await App.Native.GetPOJsonDataAsync(App.token, App.deviceName);
            var rawData = Model.ParsePOJson(PODataJson);
			var poList = Model.InitPOListFromData(rawData);
          
            if (poList != null && poList.Count() > 0) {
                App.AllPOs = poList; 
                RefreshPOListData();
                App.InitSourceLookups();
                App.InitSourceLookupsAll();
                SavePOListToLocalStorage();
            }
        }

		public void FilterByPreset(Preset preset)
		{
			var tempRecords = _allPOs.Where(c => (c.MatchPreset(preset) && !c.FullyReceived));
			this._presetAppliedPOs = tempRecords.ToList();
			this._displayingPOs = _presetAppliedPOs;
		}

        public async Task Searching(Preset preset) {
            if (string.IsNullOrEmpty(App.token))
            {
                App.token = await App.Native.GetToken(App.UserData.Username, App.UserData.Password, App.deviceName);

            }

            if (preset != null) {
                var PODataJson = await App.Native.SearchPOJsonDataAsync(App.token, App.deviceName,
                                                                        preset.PONumberSearchStr,
                                                                        preset.OrderTypeEQUAL,
                                                                        preset.OrderType, 
                                                                        preset.SupplierName, 
                                                                        preset.RecentDays.ToString(), 
                                                                        preset.FromDate, preset.ToDate, 
                                                                        preset.ProjectCode, 
                                                                        preset.UserID, 
                                                                        preset.AccountCode);
                var rawData = Model.ParsePOJson(PODataJson);
                var poList = Model.InitPOListFromData(rawData);

                if (poList != null && poList.Count() >= 0)
                {
                    App.AllPOs = poList.OrderBy(o => o.PONumber).ToList();
                    RefreshPOListData();
                    App.InitSourceLookups();
                    //SavePOListToLocalStorage();
                }
            }
        }

		public AllPOsVM()
		{
            //RefreshPOListData();
            _displayingPOs = new List<PO>();
		}

        private void RefreshPOListData() {
			_allPOs = App.AllPOs;
			_allPOs = GetPOsToBeReceived();
			_presetAppliedPOs = _allPOs;
			_displayingPOs = new List<PO>();

			foreach (var item in _allPOs)
			{
				_displayingPOs.Add(item);
			}
			_preset = new Preset();
        }

		public void UpdateCurrentPreset(Preset newPreset) { 
			if (newPreset == null)
				return;
			this._preset = newPreset;
		}

		public void SavePOListToLocalStorage() {
			App.UserData.AllPOs = _allPOs;
			App.AllPOs = _allPOs;
            App.LoadUserData(App.UserData);
			UserDataManager.StoreUserData(App.UserData);
		}

		public List<PO> GetPOsToBeReceived ()
		{
			var results = new List<PO>();
			if (this._allPOs != null)
			{
				foreach (var item in this._allPOs)
				{
					if (!item.FullyReceived) {
						results.Add(item);
					}
				}
			}
			return results;
		}
	}
}
