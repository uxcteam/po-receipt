﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace POReceipt
{
	public class SignInVM : ViewModelBase {
		public string Username
		{
			set
			{
				this._username = value;
				this.OnPropertyChanged(nameof(Username));
			}
			get { return _username; }
		}
		private string _username;

		public string Password
		{
			set
			{
				this._password = value;
				this.OnPropertyChanged(nameof(Password));
			}
			get { return _password; }
		}
		private string _password;

		public bool Remember
		{
			set
			{
				this._remember = value;
				this.OnPropertyChanged(nameof(Remember));
			}
			get { return _remember; }
		}
		private bool _remember = true;

		public SignInVM()
		{
            //_username = "redrock";
            //_password = "Redrock1!";
            var setting = Settings.LoadSettings();
            if (setting != null) {
                if (setting.AccountRemember) {
                    _username = setting.LogonUsername;
                    _password = setting.LogonPassword;
                }
            }

		}

		public async Task<bool> DoLogin() { 
			if (!doValidateCredentials()) { 
				App.Native.showAlert("Please check your username and password", "Sign in failed");
				return false;
			}

            var tokenStr = await App.Native.GetToken(_username, _password, App.deviceName);
            if (!string.IsNullOrEmpty(tokenStr)) {
                App.token = tokenStr;
				App.signedIn = true;
                try {

                    var user = new User() { Username = _username, Password = _password, IsRemembered = _remember};
                    App.LoadUserData(user);
					App.Settings.AccountRemember = _remember;
                    App.Settings.LogonUsername = _username;
                    App.Settings.LogonPassword = _password;
                    App.Settings.Logon = true;
					Settings.StoreSettings(App.Settings);

					MessagingCenter.Send<App, User>((App)Application.Current, Conts.MSG_LOGIN_SUCCESS, user);
					return true;
                } catch (Exception ex) {
                    App.Native.showError(ex);
                    return false;
                }
				

            } else {
                App.Native.showAlert("Please check your username and password", "Sign in failed");
                return false;
            }
		}

		bool doValidateCredentials()
		{
			if (string.IsNullOrEmpty(_username) || string.IsNullOrEmpty(_password))
				return false;

			if (string.IsNullOrWhiteSpace(_username) || string.IsNullOrWhiteSpace(_password))
				return false;
            
			return true;
		}
	}
}
