﻿using System;
namespace POReceipt
{
	public class Conts
	{
		public static string MSG_SELECT_UNSELECT_PODETAIL_ITEM = "SELECT_UNSELECT_PODETAIL_ITEM";
		public static string MSG_EDIT_PODETAIL_ITEM = "EDIT_PODETAIL_ITEM";
		public static string MSG_UPDATE_PRESET_LIST_MENU= "MSG_UPDATE_PRESET_LIST_MENU";
		public static string MSG_APPLY_PRESET = "MSG_APPLY_PRESET";
		public static string MSG_CLOSE_SIDE_MENU = "MSG_CLOSE_SIDE_MENU";
		public static string MSG_UPDATE_POLIST = "MSG_UPDATE_POLIST";
		public static string MSG_UPDATE_POLINES = "MSG_UPDATE_POLINES";
		public static string MSG_IMAGE_SELECTED = "MSG_IMAGE_SELECTED";
		public static string MSG_LOGIN_SUCCESS = "MSG_LOGIN_SUCCESS";
        public static string MSG_LOGOUT = "MSG_LOGOUT";


		public static string PRESET_SETTINGS_FILE_NAME = "PRESET_SETTINGS_FILE_NAME";
		public static string PHOTOS_FOLDER_NAME = "PHOTOS";
		public static string PO_DATA_FILE_NAME = "PO_DATA_FILE_NAME";
		public static string PHOTO_FOLDER_FILE_NAME = "PO_DATA_FILE_NAME";

		public static string PONUMBER_PROP_KEY = "PONUMBER_PROP_KEY";
		public static string SUPPLIER_PROP_KEY = "SUPPLIER_PROP_KEY";
		public static string PROJECTCODE_PROP_KEY = "PROJECTCODE_PROP_KEY";
		public static string USERID_PROP_KEY = "USERID_PROP_KEY";
		public static string ACCOUNTCODE_PROP_KEY = "ACCOUNTCODE_PROP_KEY";
		public static string ORDERTYPE_PROP_KEY = "ORDERTYPE_PROP_KEY";
		public static string FROMDATE_PROP_KEY = "FROMDATE_PROP_KEY";
		public static string TODATE_PROP_KEY = "TODATE_PROP_KEY";

		public static string TESTING_USERNAME = "redrock";
		public static string SETTINGS_FILE_NAME = "settings";
        public static string SERVER_CONFIGS_FILE_NAME = "serverConfigs";
        public static string DEVICEID_FILE_NAME = "deviceId";
		public Conts()
		{
		}

		public static string ACCENT_COLOR_HEX = "#D0011B"; // Red
		public static string DARK_COLOR_HEX = "#242A31"; // DARK BLUE
	}
}
