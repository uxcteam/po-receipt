﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;

namespace POReceipt
{
	public class UserDataManager
	{
		public UserDataManager()
		{
		}

		public static User CreateMockUser(string username, string password) {
			var user = new User()
			{
				Username = username,
				Password = password
			};
			return user;
		}

		public static User LoadUserDataFromStorage(string userName) {
			if (string.IsNullOrEmpty(userName))
			{
				return null;
			}
			var jsonStr = App.Native.readFile(userName);
			if (!string.IsNullOrEmpty(jsonStr))
			{
				var result = JsonConvert.DeserializeObject<User>(jsonStr);
				if (result != null)
				{
					return result;
				}
			}
			else {
                var newUser = CreateNewUser(userName, "a");
				StoreUserData(newUser);
				return newUser;
			}
			return null;
		}

		public static User CreateNewUser(string username, string password) {
			var user = new User()
			{
				Username = username,
				Password = password
			};
			// for testing
			//ResetDataForUser(user);

			return user;
		}

		public static void ResetDataForUser(User user) {
			if (user == null)
				return;
			user.AllPOs = LoadDummyPOData();
			StoreUserData(user);
		}

		public static void StoreUserData(User user) {
			if (user == null)
				return;
			var json = JsonConvert.SerializeObject(user);
			App.Native.writeFile(user.Username, json);
		}

		public static List<PO> LoadDummyPOData() {
			var jsonText = App.Native.readSampleData("vline_sampleData.json");
			var result = JsonConvert.DeserializeObject<List<OrderRaw>>(jsonText);
			try
			{
				result = result.OrderByDescending(x => DateTime.ParseExact(x.poDate, PO.DATE_FORMAT, CultureInfo.InvariantCulture)).ToList();
			}
			catch (Exception ex) {
				App.Native.showError(ex);
			}

			return Model.InitPOListFromData(result);
		}
	}
}
