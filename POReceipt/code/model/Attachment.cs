﻿using System;
using Xamarin.Forms;

namespace POReceipt
{
    public class Attachment : ViewModelBase
	{
		public ImageSource ImageSource { set; get; }
		public string FileName { set; get; }
		public string LocalFileURL { set; get; }
		public byte[] Bytes { set; get; }
        public bool Unsaved {
            get {
                return !_uploaded;  
            } 
        }
        public bool Uploaded { 
            set {
                _uploaded = value;
                OnPropertyChanged(nameof(Uploaded));
                OnPropertyChanged(nameof(Unsaved));
            } 
            get {
                return _uploaded;
            } 
        }
        bool _uploaded;

        public int Sequence { set; get; }

		public Attachment()
		{
		}
	}
}
