﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace POReceipt
{
	public class POLine : INotifyPropertyChanged
	{
		//public int POId { set; get; }
        public int PONumber { set; get; }
		public float LineNumber { set; get; }
		public string LineType { set; get; }
		public string LineDesc1 { set; get; }
		public string LineDesc2 { set; get; }
		public double QuantityTotal { set; get; }
		public double QuantityReceived { set; get; }

		public double QuantityRemaining
		{
			set
			{
		        this._quantityRemaining = value;
				OnPropertyChanged("PriceTotalDescription");
                OnPropertyChanged("QuantityRemaining");
                OnPropertyChanged("TotalPrice");
			}
			get { return this._quantityRemaining; }
		}
		private double _quantityRemaining;

		public double QuantityNew
		{
			set { 
				this.quantityNew = value;
				OnPropertyChanged("QuantityNew");
                OnPropertyChanged("UpdatedQuantityInfo");
                OnPropertyChanged("UpdatedQuantity");
			}
			get { return this.quantityNew; }
		}
		private double quantityNew;

		public bool UpdatedQuantity
		{
			set
			{
                this._updatedQuantity = value;
			}
			get { return (this.quantityNew > 0); }
		}
		private bool _updatedQuantity;

		public string DateNew { set; get; }
		public string Unit { set; get; }
		public double UnitPrice { set; get; }
		//public double TotalPrice { set; get; }

		public double TotalPrice
		{
			set
			{
		        this._totalPrice = value;
				OnPropertyChanged("TotalPrice");
			}
			get
			{
                if (QuantityRemaining <= 0)
                    return _totalPrice;
				return this.QuantityRemaining * UnitPrice;
			}
		}
		private double _totalPrice;

		public string Note { set; get; }
		public List<Attachment> Attachments { set; get; }
		public string SupplierMark { set; get; }

		public string AccountNumber
		{
			set
			{
				this._accountNumber = value;
				if (!string.IsNullOrEmpty(_accountNumber))
					_accountNumber = _accountNumber.Trim();
				OnPropertyChanged("AccountNumber");
			}
			get
			{
				return _accountNumber;
			}
		}
		private string _accountNumber;

		public string PriceTotalDescription { 
			set 
			{
                this._priceTotalDescription = value;
				OnPropertyChanged("PriceTotalDescription");
			}
			get {
                if (QuantityRemaining <= 0) 
                    return string.Format("{0} {1} x @ ${2}", QuantityRemaining, Unit, TotalPrice);                
				return string.Format("{0} {1} x ${2} = ${3}", QuantityRemaining, Unit, UnitPrice, TotalPrice);
			} 
		}
		private string _priceTotalDescription;

		public bool MatchesKeyword(string keyword) {
			var line1Matched = false;
			var line2Matched = false;
			if (!string.IsNullOrEmpty(LineDesc1)) {
				line1Matched = LineDesc1.Trim().ToLower().Contains(keyword.Trim().ToLower());
			}
			if (!string.IsNullOrEmpty(LineDesc2)) {
				line2Matched = LineDesc2.Trim().ToLower().Contains(keyword.Trim().ToLower());
			}
			return line1Matched || line2Matched;
		}

		public string GetKeywordMatchedContent(string keyword) {
			var result = string.Empty;
			if (!string.IsNullOrEmpty(LineDesc1)) {
				if (LineDesc1.Trim().ToLower().Contains(keyword.Trim().ToLower())) { 
					return StringHelper.GetContentAroundKeyword(keyword, LineDesc1);
				}
			}
			if (!string.IsNullOrEmpty(LineDesc2)) {
				if (LineDesc2.Trim().ToLower().Contains(keyword.Trim().ToLower())) {
                    return StringHelper.GetContentAroundKeyword(keyword, LineDesc2);
				}
			}
			return result;
		}

		public string UpdatedQuantityInfo
		{
			set
			{
                this._updatedQuantityInfo = value;
				OnPropertyChanged("UpdatedQuantityInfo");
			}
			get
			{
                if (QuantityNew > 0){
                    var str = string.Format("{0} {1}", QuantityNew, Unit);
                    if (!string.IsNullOrEmpty(LineRemark))
                        str += string.Format(" ({0})", LineRemark);
                    return str;   
                }
				else
					return string.Empty;
			}
		}
		private string _updatedQuantityInfo;

		public string RemainingToReceive
		{
			set
			{
	            this._remainingToReceive = value;
				OnPropertyChanged("RemainingToReceive");
			}
			get
			{
				return string.Format("{0} {1}", QuantityRemaining, Unit);
			}
		}
		private string _remainingToReceive;

		public string LineRemark
		{
			set
			{
				this._lineRemark = value;
				OnPropertyChanged("LineRemark");
			}
			get
			{
                return this._lineRemark;
			}
		}
		private string _lineRemark;

		public bool IsSelected { set; get; }
		public bool IsReceived { set; get; }
		public POLine()
		{
			Attachments = new List<Attachment>();
		}

		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		public void OnPropertyChanged(string propertyName = null)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		#endregion
	}
}
