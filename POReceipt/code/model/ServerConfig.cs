﻿using System;
namespace POReceipt
{
    public class ServerConfig
    {
        public string JDE_HOSTNAME { set; get; }
        public string JDE_SERVICE_ROOT { set; get; }
        public string JDE_ENVIRONMENT { set; get; }
        public string JDE_VERSION { set; get; }
    }
}
