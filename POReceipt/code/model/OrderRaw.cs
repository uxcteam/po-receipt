﻿using System;
namespace POReceipt
{
	public class OrderRaw
	{
		public int poId { set; get; }
		public int poNum { set; get; }
		public string poType { set; get; }
		public string poCompany { set; get; }
		public string poSuffix { set; get; }
		public string poDate { set; get; }
        public string requestDate { set; get; }
        public string promisedDeliveryDate { set; get; }
		public int suppId { set; get; }
		public string suppName { set; get; }
		public string projCode { set; get; }
		public string userId { set; get; }
		public float lineNum { set; get; }
		public string lineType { set; get; }
		public string lineDesc1 { set; get; }
		public string lineDesc2 { set; get; }
		public double quanRem { set; get; }
		public string unit { set; get; }
		public double priceUnit { set; get; }
		public double priceTot { set; get; }
		public string accCode { set; get; }
        public string suppRemark { set; get; }
		public OrderRaw()
		{
		}
	}
}
