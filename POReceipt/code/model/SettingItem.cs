using System;
namespace POReceipt
{
	public class SettingItem
	{
		public string Icon { set; get; } // should be fontawesome
		public string Title { set; get; } // should be fontawesome
		public Type PageTypeToNavigateTo { set; get; }
		public SettingItem()
		{
		}
	}
}

