﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace POReceipt
{
	public class User : INotifyPropertyChanged
	{
		public string Username { set; get; }
		public string Password { set; get; }
		public Dictionary<string, List<string>> PropertySourceDictionary { set; get; }
		public List<PO> AllPOs { set; get; }
		public bool IsRemembered { set; get; }

		public User()
		{
			Username = "username";

		}

		public bool Login(string userName, string password) {
			this.Username = userName;
			this.Password = password;

			return false;
		}

		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		public void OnPropertyChanged(string propertyName = null)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion	
	}
}
