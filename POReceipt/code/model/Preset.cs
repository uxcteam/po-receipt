using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace POReceipt
{
	public class Preset : INotifyPropertyChanged
	{
		public string Name
		{
			set
			{
				this._name = value;
				this.OnPropertyChanged(nameof(Name));
			}
			get
			{
				return _name;
			}
		}
		private string _name;

		public int PONumber
		{
			set
			{
				this._poNumber = value;
				this.OnPropertyChanged(nameof(PONumber));
			}
			get
			{
				return _poNumber;
			}
		}
		private int _poNumber;

		public string PONumberSearchStr { 
			set { 
				_poNumberSearchStr = value;
				this.OnPropertyChanged(nameof(PONumberSearchStr));
			}
			get { return _poNumberSearchStr; }
		}
		private string _poNumberSearchStr;

		public string SupplierID
		{
			set
			{
				this._supplierID = value;
				this.OnPropertyChanged(nameof(SupplierID));
			}
			get
			{
				return _supplierID;
			}
		}
	
		private string _supplierID;

		public string SupplierName
		{
			set
			{
				this._SupplierName = value;
				this.OnPropertyChanged(nameof(SupplierName));
			}
			get
			{
				return _SupplierName;
			}
		}

		private string _SupplierName;

		public string OrderType
		{
			set
			{
             	this._orderType = value;
				this.OnPropertyChanged(nameof(OrderType));
			}
			get
			{
				return _orderType;
			}
		}

		private string _orderType;

        public bool OrderTypeEQUAL
        {
            set
            {
                this._orderTypeEQUAL = value;
                this.OnPropertyChanged(nameof(OrderTypeEQUAL));
            }
            get
            {
                return _orderTypeEQUAL;
            }
        }

        private bool _orderTypeEQUAL = true;

		public string ProjectCode
		{
			set
			{
				this._projectCode = value;
				this.OnPropertyChanged(nameof(ProjectCode));
			}
			get
			{
				return _projectCode;
			}
		}
		private string _projectCode;

		public string UserID
		{
			set
			{
				this._userID = value;
				this.OnPropertyChanged(nameof(UserID));
			}
			get
			{
				return _userID;
			}
		}
		private string _userID;

		public string AccountCode
		{
			set
			{
				this._accountCode = value;
				this.OnPropertyChanged(nameof(AccountCode));
			}
			get
			{
				return _accountCode;
			}
		}
		private string _accountCode;

		public string PODate
		{
			set
			{
				this._PODate = value;
				this.OnPropertyChanged(nameof(PODate));
			}
			get
			{
				return _PODate;
			}
		}
		private string _PODate;

		public string POToDate
		{
			set
			{
				this._poToDate = value;
				this.OnPropertyChanged(nameof(POToDate));
			}
			get
			{
				return _poToDate;
			}
		}
	    private string _poToDate;


        public int RecentDays
        {
            set
            {
                this._recentDays = value;
                this.OnPropertyChanged(nameof(RecentDays));
            }
            get
            {
                return _recentDays;
            }
        }
        private int _recentDays;

        public Preset()
        {
            ToDate = DateTime.Now;
            FromDate = DateTime.Now;
        }

        public static Preset GetDefaultPreset() {
            var preset = new Preset()
            {
                OrderType = "ON",
                OrderTypeEQUAL = true,
                RecentDays = 30
            };
            return preset;
        }

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string propertyName = null)
		{
			var handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}

		public bool IsEmpty {
			set {
				this._isEmpty = value;
			}
			get {
				return (string.IsNullOrEmpty(this.PONumberSearchStr)) && string.IsNullOrEmpty(this.PODate)
						 && string.IsNullOrEmpty(this.SupplierName)
						 && string.IsNullOrEmpty(this.ProjectCode) && string.IsNullOrEmpty(this.UserID)
                        && string.IsNullOrEmpty(this.AccountCode) && string.IsNullOrEmpty(this.PODate) && this.RecentDays == 0
                                                                               && string.IsNullOrEmpty(OrderType);
			}
		}

		bool _isEmpty;

		public bool IsSaved
		{
			set
			{
				this._isSaved = value;
			}
			get
			{
				return _isSaved;
			}
		}

		bool _isSaved;

		public bool IsDefault
		{
			set
			{
             	this._isDefault = value;
			}
			get
			{
				return _isDefault;
			}
		}

		bool _isDefault;


		public void RemoveFilterByText(string text) {
			if (!string.IsNullOrEmpty(this._userID) && _userID.Equals(text))
			{
				_userID = string.Empty;
			}
			else if (!string.IsNullOrEmpty(this._poNumber.ToString()) && _poNumber.ToString().Equals(text))
			{
				_poNumber = 0;
			}
			else if (!string.IsNullOrEmpty(this._supplierID) && _supplierID.Equals(text))
			{
				_supplierID = string.Empty;
			}
			else if (!string.IsNullOrEmpty(this._projectCode) && _projectCode.Equals(text))
			{
				_projectCode = string.Empty;
			}
			else if (!string.IsNullOrEmpty(this._accountCode) && _accountCode.Equals(text))
			{
				_accountCode = string.Empty;
			}
			else if (!string.IsNullOrEmpty(this._SupplierName) && _SupplierName.Equals(text))
			{
				_SupplierName = string.Empty;
			}
			else if (!string.IsNullOrEmpty(this._PODate) && _PODate.Equals(text))
			{
				_PODate = string.Empty;
                this.FromDate = DateTime.Now;
                this.ToDate = DateTime.Now;
                _recentDays = 0;
			}
			else if (!string.IsNullOrEmpty(this._poToDate) && _poToDate.Equals(text))
			{
				_poToDate = string.Empty;
                _recentDays = 0;
			}
			else if (!string.IsNullOrEmpty(this._poNumberSearchStr) && _poNumberSearchStr.Equals(text)) {
				_poNumberSearchStr = string.Empty;
            } else if (text.Contains(RecentDays.ToString())) {
                _recentDays = 0;
            } else if (!string.IsNullOrEmpty(this._orderType) && text.Contains(OrderType)) {
                _orderType = string.Empty;
                _orderTypeEQUAL = true;
            }
		}

		public Preset GetClone() {
            return new Preset()
            {
                PONumber = this.PONumber,
                PONumberSearchStr = this.PONumberSearchStr,
                ProjectCode = this.ProjectCode,
                POToDate = this.POToDate,
                PODate = this.PODate,
                SupplierID = this.SupplierID,
                SupplierName = this.SupplierName,
                UserID = this.UserID,
                AccountCode = this.AccountCode,
                IsSaved = this.IsSaved,
                Name = this.Name,
                IsDefault = this.IsDefault,
                FromDate = this.FromDate,
                ToDate = this.ToDate,
                RecentDays = this.RecentDays,
                OrderTypeEQUAL = this.OrderTypeEQUAL,
                OrderType = this.OrderType
			};
		}

		public DateTime FromDate { set; get; }
		public DateTime ToDate { set; get; }
	}
}
