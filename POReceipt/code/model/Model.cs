using System;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using POReceipt.Messages;
using System.Threading;
using Xamarin.Forms;
using System.Globalization;
using Xamarin.Forms.Maps;

namespace POReceipt
{
	public static class Model
	{
		public class ServiceResponse
		{
			public bool Success;

			public string SyncTime = string.Empty;

			public string ResponseCode = string.Empty;

			public string Description = string.Empty;

			public bool Error;

			public Exception Exception;
		}

		private static DateTime lastRequested = DateTime.MinValue;

		public static async Task<string> service(string xml)
		{
			return await App.Native.callService(xml);
		}

		private static string SyncDateTime = string.Empty;
		private static DateTime SyncTime = DateTime.MinValue;
		public static TimeSpan SyncOffset = TimeSpan.MinValue;
		public static TimeSpan SyncDelta = TimeSpan.MinValue; // Khang


		public static string getSyncTime()
		{
			if (string.IsNullOrWhiteSpace(SyncDateTime))
			{
				return "0001-01-01T00:00:00";
			}
			return SyncDateTime;
		}

		public static string getServerTime()
		{
			return getServerDateTime().ToString("s");
		}

		public static DateTime getServerDateTime()
		{
			//			if (SyncOffset == TimeSpan.MinValue) {
			//				return DateTime.Now.ToUniversalTime ();
			//			} else {
			//				return DateTime.Now.Subtract (SyncOffset);
			//			}

			return DateTime.Now;

			//			if (SyncDelta == TimeSpan.MinValue) {
			//				return DateTime.Now.ToUniversalTime ();
			//			} else {
			//				return DateTime.Now.Subtract (SyncDelta);
			//			}

		}

		private static async Task<XDocument> readFromFile(string name)
		{
			try
			{
				var file = App.Native.checkFile(Path.Combine("Data", name));
				if (file != null)
				{
					var content = App.Native.readFile(file);
					if (content != null)
					{
						return ValueUtil.ParseXML(content);
					}
				}
			}
			catch (Exception e)
			{
				App.Native.log(e);
			}
			return null;
		}

		private static void writeToFile(string name, XDocument doc)
		{
			if (doc == null)
			{
				return;
			}
			try
			{
				App.Native.writeFile(Path.Combine("Data", name), doc.ToString());
			}
			catch (Exception e)
			{
				App.Native.log(e);
			}
		}

		public static void FlushCache()
		{
			try
			{
				App.Native.deleteDirectory("Data");
			}
			catch (Exception e)
			{
				App.Native.log(e);
			}
		}

		public static async Task<Types.UserProfile> CheckLogin(string userName, string password)
		{
			return null;
		}


		public static async Task<bool> handleResponse(JObject jObject, bool alert = true)
		{
			var taskCompletion = new TaskCompletionSource<bool>();

			if (jObject == null || (int)(jObject["ErrorCode"]) != 0)
			{
				string errorMessage = "System Error!";
				if (jObject != null)
					errorMessage = (string)(jObject["SystemError"]);

				if (alert)
				{
					App.Native.showError(new Exception(errorMessage));
				}
				else {
					App.Native.log(errorMessage);
				}
				taskCompletion.SetResult(false);
			}
			else
				taskCompletion.SetResult(true);

			return await taskCompletion.Task;
		}


		public static string RemoveNamespace(string xmlContent)
		{
			return xmlContent.Replace(" xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/services\">", ">");
		}

		private static string XmlContentEncode(string content)
		{
			return content.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;");
		}

		/// <summary>
		/// Backgrounds the running.
		/// </summary>
		/// <returns>The running.</returns>
		/// <param name="token">Token.</param>
		public static async Task BackgroundRunning(CancellationToken token)
		{
			App.Native.log("Starting Background Service");

			await Task.Run(async () =>
			{
				App.Native.log("Starting Background Service infinate while loop");
				while (true)
				{
					token.ThrowIfCancellationRequested();

					/// <summary>
					/// Beacon line up process
					/// </summary>
					//LineupBackgroundProcess();

					//Wait 30 secs
					await Task.Delay(30000);

					////Wait 10 seconds
					//await Task.Delay(10000); 

				}

			}, token);

		}

		public static string RemoveSoapNamespace(string xmlContent)
		{
			xmlContent = xmlContent.Replace(" xmlns=\"http://xmlns.oracle.com/Enterprise/Tools/services\"", "");
			xmlContent = xmlContent.Replace(" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"", "");
			xmlContent = xmlContent.Replace(" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"", "");
			xmlContent = xmlContent.Replace(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");
			xmlContent = xmlContent.Replace(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
			xmlContent = xmlContent.Replace(" xmlns = \"http://peoplesoft.com/SSR_RC_POST_PAYMENT1_RESPResponse\"", "");
				
			xmlContent = xmlContent.Replace("soapenv:Envelope", "soapenvEnvelope");
			xmlContent = xmlContent.Replace("soapenv:Body", "soapenvBody");
			return xmlContent;
		}

		public static List<PO> InitPOListFromData(List<OrderRaw> orderList) {
			List<PO> poList = new List<PO>();
			Dictionary<int, PO> poMappingDict = new Dictionary<int, PO>();

			foreach (var order in orderList) {

                var line = new POLine()
                {
                    PONumber = order.poNum,
                    LineNumber = order.lineNum,
                    LineType = order.lineType,
                    LineDesc1 = order.lineDesc1,
                    LineDesc2 = order.lineDesc2,
                    QuantityRemaining = order.quanRem,
                    Unit = order.unit,
                    UnitPrice = order.priceTot / order.quanRem,
                    TotalPrice = order.priceTot,
                    AccountNumber = order.accCode,
                    IsReceived = false // assume that no bad data returned
                };

				//if (line.QuantityRemaining > 0.0)
				//	line.IsReceived = false;
				//else
					//line.IsReceived = true;  

				if (!poMappingDict.ContainsKey(order.poNum))
				{
					var newPO = new PO()
					{
						POId = order.poId,
						PONumber = order.poNum,
						POType = order.poType,
						POCompany = order.poCompany,
						POSuffix = order.poSuffix,
						PODate = order.poDate,
						SupplierID = order.suppId,
						SupplierName = order.suppName,
						ProjectCode = order.projCode,
						UserID = order.userId,
						AccountCode = order.accCode,
                        SupplierRemark = order.suppRemark
					};
					if (line.IsReceived)
						newPO.ReceivedPOLines.Add(line);
					else
						newPO.POLines.Add(line);
					poMappingDict.Add(order.poNum, newPO);
					poList.Add(newPO);
				}
				else {
					var po = poMappingDict[order.poNum];
					if (line.IsReceived)
						po.ReceivedPOLines.Add(line);
					else
						po.POLines.Add(line);
				}
			}
            //var testingPo = poMappingDict[4510];

			return poList;
		}

		public static Dictionary<string, List<string>> ExtractPropertySource(List<PO> poList) {
			var resuts = new Dictionary<string, List<string>>();
			if (poList != null && poList.Count > 0)
			{
				var supplierList = new List<string>();
				var projectCodeList = new List<string>();
				var userIdList = new List<string>();
				var accountCodeList = new List<string>();

				foreach (var po in poList)
				{
					if (!string.IsNullOrEmpty(po.SupplierName))
					{
						if (!supplierList.Contains(po.SupplierName))
						{
							supplierList.Add(po.SupplierName);
						}
					}

					if (!string.IsNullOrEmpty(po.ProjectCode))
					{
						if (!projectCodeList.Contains(po.ProjectCode))
						{
							projectCodeList.Add(po.ProjectCode);
						}
					}

					if (!string.IsNullOrEmpty(po.UserID))
					{
						if (!userIdList.Contains(po.UserID))
						{
							userIdList.Add(po.UserID);
						}
					}

					if (!string.IsNullOrEmpty(po.AccountCode))
					{
						if (!accountCodeList.Contains(po.AccountCode))
						{
							accountCodeList.Add(po.AccountCode);
						}
					}
				}
				supplierList.Sort();
				resuts.Add(Conts.SUPPLIER_PROP_KEY, supplierList);
				resuts.Add(Conts.PROJECTCODE_PROP_KEY, projectCodeList);
				resuts.Add(Conts.USERID_PROP_KEY, userIdList);
				resuts.Add(Conts.ACCOUNTCODE_PROP_KEY, accountCodeList);
			}
			return resuts;
		}

        public static List<OrderRaw> ParsePOJson(List<JObject> rowset) {
            List<OrderRaw> orderRawList = new List<OrderRaw>();
			if (rowset != null)
			{
				foreach (var row in rowset)
				{

                    if (!IsAReceiptableJSONLine(row))
                        continue;
					try
					{
                        var lineNum = row["F4311_LNID"].Value<float>();
                        var poDate = row["F4301_TRDJ"].Value<string>();

                        var dd = poDate.Substring(6, 2);
                        var mm = poDate.Substring(4, 2);
                        var yyyy = poDate.Substring(0, 4);
                        poDate = string.Format("{0}/{1}/{2}", dd, mm, yyyy);
                          
                        var orderRaw = new OrderRaw()
                        {
                            poNum = row["F4301_DOCO"].Value<int>(),
                            poType = row["F4301_DCTO"].Value<string>(),
                            poCompany = row["F4301_KCOO"].Value<string>(),
                            suppId = row["F4301_AN8"].Value<int>(),
                            quanRem = row["F4311_UOPN"].Value<int>(),
                            unit = row["F4311_UOM"].Value<string>(),
                            priceTot = row["F4311_AOPN"].Value<float>(),
                            accCode = row["F4311_ANI"].Value<string>(),
                            poSuffix = row["F4301_SFXO"].Value<string>(),
                            lineNum = lineNum,
                            projCode = row["F4301_MCU"].Value<string>(),
							poDate = poDate,
                            lineDesc1 = row["F4311_DSC1"].Value<string>(),
                            lineDesc2 = row["F4311_DSC2"].Value<string>(),
                            userId = row["F4301_ORBY"].Value<string>(),
                            suppRemark = row["F4301_RMK"].Value<string>(),
                            lineType = row["F4311_LNTY"].Value<string>(),
                            suppName = !string.IsNullOrEmpty(row["F0101_ALPH"].Value<string>()) ? row["F0101_ALPH"].Value<string>() : string.Empty
						};

						orderRawList.Add(orderRaw);
					}
					catch (Exception ex)
					{
						App.Native.showError(ex);
					}
				}
			}
               return orderRawList;
        }

        private static bool IsAReceiptableJSONLine(JObject jsonLine) {
            try {
                var processByStr = jsonLine["ProcessBy"].Value<string>();
                var F4311_ANI = jsonLine["F4311_ANI"].Value<string>();
                var F4311_ITM = jsonLine["F4311_ITM"].Value<string>();
                var F4311_UOPN = jsonLine["F4311_UOPN"].Value<int>();
                var F4311_UORG = jsonLine["F4311_UORG"].Value<int>();
                var F4311_AOPN = jsonLine["F4311_AOPN"].Value<int>();
                var F4301_HOLD = jsonLine["F4301_HOLD"].Value<string>();
                var poNum = jsonLine["F4301_DOCO"].Value<int>();
                var F4311_WVID = jsonLine["F4311_WVID"].Value<int>();
              
                int processBy = 0;
                if (!string.IsNullOrEmpty(processByStr)) {
                    processBy = int.TryParse(processByStr, out processBy) ? processBy : 0;
                }

                if (processBy == 3 && (string.IsNullOrWhiteSpace(F4311_ANI) || F4311_ANI.Equals("0")))
                    return false;

                if (processBy == 2 && (string.IsNullOrWhiteSpace(F4311_ITM) || F4311_ITM.Equals("0")))
                    return false;

                if (F4311_UOPN == 0 && F4311_UORG != 0)
                    return false;
                
                if (F4311_AOPN == 0 && F4311_UORG != 0)
                    return false;

                if (F4311_AOPN == 0 && F4311_UORG == 0)
                    return false;

                if (F4311_WVID != 0)
                    return false;

                if (!string.IsNullOrWhiteSpace(F4301_HOLD))
                    return false;

            } catch (JsonException ex) {
                App.Native.showError(ex);
                return false;
            }

            return true;
        }

        public static List<Attachment> ParseFileListJson(string jsonStr) {
            List<Attachment> list = new List<Attachment>();
            try {
                var bodyJson = JObject.Parse(jsonStr);
                var mediaObjects = bodyJson["mediaObjects"].Value<JArray>();


				if (mediaObjects != null)
				{
					var array = mediaObjects.Select(jv => (JObject)jv).ToArray();

					for (int i = 0; i < array.Length; i++)
					{
						var obj = array[i];
                        bool isImage = obj["isImage"].Value<bool>();
                        if (isImage) {
                            string name = obj["itemName"].Value<string>();
                            int sequence = obj["sequence"].Value<int>();

                            var filePath = System.IO.Path.Combine(Conts.PHOTOS_FOLDER_NAME, name);
                            var localFilePath = App.Native.checkFile(filePath);

							var attachment = new Attachment()
							{
								FileName = name,
                                Uploaded = true,
                                LocalFileURL = localFilePath,
                                Sequence = sequence
							};
                            list.Add(attachment);
                        }
					}
				}
            } catch (JsonException ex) {
                App.Native.showError(ex);
                return list;
            }
            return list;
        }

        //{  
                //   "mediaObjects":[
                //      {  
                //         "data":"This is a testing",
                //         "file":"",
                //         "itemName":"Text1",
                //         "link":"",
                //         "moType":0,
                //         "queue":"",
                //         "sequence":1,
                //         "updateDate":"2017-10-13",
                //         "updateHourOfDay":21,
                //         "updateMinuteOfHour":9,
                //         "updateSecondOfMinute":58,
                //         "updateTimeStamp":210958,
                //         "updateUserID":"REDROCK",
                //         "hasValidTimestamp":true,
                //         "isDefaultImage":false,
                //         "isImage":false,
                //         "isMisc":false,
                //         "isOLE":false,
                //         "isShortCut":false,
                //         "isText":true,
                //         "isUpdated":false,
                //         "isURL":false

                //	  }
                //   ]
                //}
	}
}
