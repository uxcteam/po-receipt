﻿using System;
using Newtonsoft.Json;

namespace POReceipt
{
	public class Settings
	{
		public bool AccountRemember { set; get; }
        public bool Logon { set; get; }
		public string LogonUsername { set; get; }
        public string LogonPassword{ set; get; }
        public string DeviceName { set; get; }

		public Settings()
		{
			LogonUsername = Conts.TESTING_USERNAME;
		}

		public static void StoreSettings(Settings settings) {
			if (settings == null)
				return;
			
			var json = JsonConvert.SerializeObject(settings);
			App.Native.writeFile(Conts.SETTINGS_FILE_NAME, json);
		}

		public static Settings LoadSettings()
		{
			var jsonStr = App.Native.readFile(Conts.SETTINGS_FILE_NAME);
			if (!string.IsNullOrEmpty(jsonStr))
			{
				var result = JsonConvert.DeserializeObject<Settings>(jsonStr);
				if (result != null)
				{
					return result;
				}
			}
			var settings = new Settings();
			StoreSettings(settings);
			return settings;
		}

        public static string GetDeviceId() {
			var jsonStr = App.Native.readFile(Conts.DEVICEID_FILE_NAME);
            if (!string.IsNullOrEmpty(jsonStr))
            {
                var result = JsonConvert.DeserializeObject<Settings>(jsonStr);
                if (result != null)
                {
                    return result.DeviceName;
                }
                return string.Empty;
            } else {
				var uuid = Guid.NewGuid();
				string s = uuid.ToString();
                var setting = new Settings() { DeviceName = s };
				var json = JsonConvert.SerializeObject(setting);
                App.Native.writeFile(Conts.DEVICEID_FILE_NAME, json);
                return s;
            }
        }

        public static void StoreServerConfigs(ServerConfig configs)
        {
            if (configs == null)
                return;

            var json = JsonConvert.SerializeObject(configs);
            App.Native.writeFile(Conts.SERVER_CONFIGS_FILE_NAME, json);
        }

        public static ServerConfig LoadServerConfigs()
        {
            var jsonStr = App.Native.readFile(Conts.SERVER_CONFIGS_FILE_NAME);
            if (!string.IsNullOrEmpty(jsonStr))
            {
                var result = JsonConvert.DeserializeObject<ServerConfig>(jsonStr);
                if (result != null)
                {
                    return result;
                }
            }
            return null;
        }
	}
}
