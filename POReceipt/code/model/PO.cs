using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;
using Xamarin.Forms;
using HighlightMarker;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace POReceipt
{
	public class PO : INotifyPropertyChanged
	{
		public Color HIGHLIGHT_TEXT_COLOR = Color.Red;
		public Color NORMAL_TEXT_COLOR = Color.Red;

		public bool Selected { set; get; }
		public bool Received { set; get; }

		public bool DisplayAsSearchResult
		{
			set { this._displayAsSearchResult = value;
				OnPropertyChanged(nameof(DisplayAsSearchResult));
                OnPropertyChanged(nameof(Subtitle));
                OnPropertyChanged(nameof(HighlightText));
                OnPropertyChanged(nameof(HighlightSupplierName));
                OnPropertyChanged(nameof(HighlightItemTitle));
                OnPropertyChanged(nameof(HighlightProjectCode));
			}
			get
			{
				return _displayAsSearchResult;
			}
		}
		private bool _displayAsSearchResult;

		public FormattedString HighlightText
		{
			set {
				_highlightText = value;
			}
			get
			{
				return _highlightText;
			}
		}
		private FormattedString _highlightText;

		public FormattedString HighlightSupplierName
		{
			set
			{
				_highlightSupplierName = value;
			}
			get
			{
                if (_highlightSupplierName == null) {
                    _highlightSupplierName = new FormattedString();
                    _highlightSupplierName.Spans.Add(new Span() { Text = SupplierName, BackgroundColor = Color.Transparent });
                }
				return _highlightSupplierName;
			}
		}
		private FormattedString _highlightSupplierName;

		public FormattedString HighlightItemTitle
		{
			set
			{
				_highlightItemTitle = value;
			}
			get
			{
				if (_highlightItemTitle == null)
				{
					_highlightItemTitle = new FormattedString();
                    var titleText = ItemTitle;
                    if (!string.IsNullOrEmpty(titleText))
                        _highlightItemTitle.Spans.Add(new Span() { Text = titleText, BackgroundColor = Color.Transparent });
				}
				return _highlightItemTitle;
			}
		}
		private FormattedString _highlightItemTitle;


		public FormattedString HighlightProjectCode
		{
			set
			{
				_highlightProjectCode = value;
			}
			get
			{
				if (_highlightProjectCode == null)
				{
					_highlightProjectCode = new FormattedString();
                    _highlightProjectCode.Spans.Add(new Span() { Text = string.Format("BU/Project: {0}",ProjectCode), BackgroundColor = Color.Transparent });
				}
				return _highlightProjectCode;
			}
		}
		private FormattedString _highlightProjectCode;

		public FormattedString HighlightOrderDate
		{
			set
			{
				_highlightOrderDate = value;
			}
			get
			{
				if (_highlightOrderDate == null)
				{
					_highlightOrderDate = new FormattedString();
                    _highlightOrderDate.Spans.Add(new Span() { Text = string.Format("Date: {0}", PODate), BackgroundColor = Color.Transparent });
				}
				return _highlightOrderDate;
			}
		}
		private FormattedString _highlightOrderDate;

		public string ItemTitle
		{
			set { this._itemTitle = value;
                _highlightItemTitle = null;
                OnPropertyChanged(nameof(HighlightItemTitle));
			}
			get
			{
				return string.Format("{0} {1}", PONumber, POType);
			}
		}
		private string _itemTitle;


		public string Subtitle
		{
			set { this._subtitle = value; }
			get
			{
				if (this._displayAsSearchResult)
					return _subtitle;
				else
					return string.Empty;
			}
		}
		private string _subtitle;

		public string DetailTitle
		{
			set { this._detailTitle = value; }			
			get
			{
				return string.Format("{0} {1} ({2}/{3})", PONumber, POType, POCompany, POSuffix);
			}
		}
		private string _detailTitle;

		public string SupplierInfo
		{
			set { this._supplierInfo = value; }
			get
			{
				return string.Format("{0} ({1})", SupplierName, SupplierID);
			}
		}
		private string _supplierInfo;


		public int POId
		{
			set
			{
				this._poId = value;
			}
			get
			{
				return this._poId;

			}
		}
		private int _poId;

		public int PONumber
		{
			set
			{
				this._poNumber = value;
			}
			get
			{
				return this._poNumber;

			}
		}
		private int _poNumber;

		public string POType
		{
			set
			{
				this._poType = TrimValue(value);
			}
			get
			{
				return this._poType;

			}
		}
		private string _poType;

		public string POCompany
		{
			set
			{
		        this._poCompany = TrimValue(value);
			}
			get
			{
				return this._poCompany;

			}
		}
		private string _poCompany;

		public string POSuffix
		{
			set
			{
				this._poSuffix = TrimValue(value);
			}
			get
			{
				return this._poSuffix;

			}
		}
		private string _poSuffix;

		public int SupplierID
		{
			set
			{
		        this._supplierID = value;
			}
			get
			{
				return this._supplierID;

			}
		}
		private int _supplierID;

		public string SupplierName
		{
			set
			{
				this._supplierName = TrimValue(value);
			}
			get
			{
				return this._supplierName;

			}
		}
		private string _supplierName;

		public string ProjectCode
		{
			set
			{
                _highlightProjectCode = null;
				OnPropertyChanged(nameof(HighlightProjectCode));
				this._projectCode = TrimValue(value);
			}
			get
			{
				return this._projectCode;

			}
		}
		private string _projectCode;


		public string UserID
		{
			set
			{
				this._userID = TrimValue(value);
			}
			get
			{
				return this._userID;

			}
		}
		private string _userID;

		public string AccountCode
		{
			set
			{
				this._accountCode = TrimValue(value);
			}
			get
			{
				return this._accountCode;

			}
		}
		private string _accountCode;

		public double UnitPrice { set; get; }
		public string UnitPriceStr { set {
				this.UnitPrice = double.Parse(TrimValue(value));
			} get 
			{
				return this.UnitPrice.ToString();
			} 
		}
		public static string DATE_FORMAT = "dd/MM/yyyy";

		public string PODate
		{
			set
			{
				this._poDate = TrimValue(value);
				OnPropertyChanged("PODate");
				_highlightOrderDate = null;
                OnPropertyChanged(nameof(HighlightOrderDate));
			}

			get { return _poDate; }
		}

		public string SupplierRemark
		{
			set
			{
				this._supplierRemark = TrimValue(value);
			}
			get
            {
                return string.IsNullOrEmpty(this._supplierRemark) ? "N/A" : this._supplierRemark;
			}
		}
		private string _supplierRemark;


		private string _poDate;
		private DateTime _PODate;

		public string POToDate
		{
			set
			{
				this._poToDateStr = value;
				try
				{
					this._poToDate = DateTime.ParseExact(_poToDateStr, DATE_FORMAT, CultureInfo.InvariantCulture);
				}
				catch (Exception ex)
				{

				}

				OnPropertyChanged("POToDate");
			}

			get { return _poToDate.ToString(DATE_FORMAT); }
		}
		private string _poToDateStr;
		private DateTime _poToDate;

		public string Note { set; get; }

		public List<POLine> POLines { set; get; }
		public List<POLine> ReceivedPOLines { set; get; }
		public List<Attachment> Attachments { set; get; }

		public bool FullyReceived
		{
			set { this._fullyReceived = value; }
			get
			{
				return (this.POLines == null || this.POLines.Count == 0);
			}
		}

		public void FullyReceiveLines() {
			if (POLines != null && ReceivedPOLines != null) {
				foreach (var line in POLines)
				{
					ReceivedPOLines.Add(line);
				}
				POLines = new List<POLine>();
			}
		}

		private bool _fullyReceived;


		public PO()
		{
			this.POLines = new List<POLine>();
			this.Attachments = new List<Attachment>();
			this.ReceivedPOLines = new List<POLine>();
			this.PONumber = 0;
			this.ProjectCode = "N.A";
			this.AccountCode = "N.A";
			this.SupplierID = 0;
			this.UnitPrice = 0.0;
		}

		public void DoReceiveLine(POLine line) {
			if (this.POLines != null && this.POLines.Contains(line)) {
				if (line.QuantityNew > 0)
				{
					line.QuantityReceived += line.QuantityNew;
					line.QuantityRemaining -= line.QuantityNew;
					line.QuantityNew = 0;
					if (line.QuantityRemaining <= 0) {
						line.IsReceived = true;
                        this.ReceivedPOLines.Add(line);
                        //this.POLines.Remove(line);
					}
				}
				else {
					line.QuantityReceived += line.QuantityRemaining;
					line.QuantityRemaining = 0;
					line.IsReceived = true;
					this.ReceivedPOLines.Add(line);
					//this.POLines.Remove(line);
				}
			}
		}


        public async Task<bool> DoReceiveSelectedLines(Dictionary<float, JObject> linesDict)
        {
            return await App.Native.ReceivePartialOrder(App.token, App.deviceName, PONumber, POType, POCompany, linesDict);
        }

        public async Task<bool> DoReceiveAll() {



            if (await App.Native.ReceiveFullOrder(App.token, App.deviceName, PONumber, POType, POCompany))
            {
                foreach (var line in this.POLines)
                {
                    if (line.QuantityNew > 0)
                    {
                        line.QuantityReceived += line.QuantityNew;
                        line.QuantityRemaining -= line.QuantityNew;
                        line.QuantityNew = 0;
                        if (line.QuantityRemaining <= 0)
                        {
                            line.IsReceived = true;
                            this.ReceivedPOLines.Add(line);
                        }
                    }
                    else
                    {
                        line.QuantityReceived += line.QuantityRemaining;
                        line.QuantityRemaining = 0;
                        line.IsReceived = true;
                        this.ReceivedPOLines.Add(line);
                    }
                }

                var tmpList = new List<POLine>();

                foreach (var line in this.POLines)
                {
                    if (!line.IsReceived)
                    {
                        tmpList.Add(line);
                    }
                }

                this.POLines = tmpList;
                //this.POLines.Clear();
                return true;
            }

            return false;
		}

		// If you want to implement "*" only
		private static String WildCardToRegular(String value)
		{
			return "^" + Regex.Escape(value).Replace("\\*", ".*") + "$";
		}

		public bool MatchPreset(Preset preset)
		{
			if (!string.IsNullOrEmpty(preset.SupplierName))
			{
				if (this.SupplierName != preset.SupplierName)
					return false;
			}

			if (!string.IsNullOrEmpty(preset.PONumberSearchStr))
			{
				try
				{
					if (!Regex.IsMatch(this.PONumber.ToString(), WildCardToRegular(preset.PONumberSearchStr), RegexOptions.CultureInvariant | RegexOptions.IgnoreCase))
						return false;
				}
				catch (Exception ex) {
					App.Native.log(ex.Message);
					return false;
				}
			}

			if (!string.IsNullOrEmpty(preset.ProjectCode))
			{
				if (this.ProjectCode != preset.ProjectCode)
					return false;
			}

			if (!string.IsNullOrEmpty(preset.UserID))
			{
				if (this.UserID != preset.UserID)
					return false;
			}

			if (!string.IsNullOrEmpty(preset.AccountCode))
			{
				if (this.AccountCode != preset.AccountCode)
					return false;
			}

			if (!string.IsNullOrEmpty(preset.PODate))
			{
				//if (this.PODate != preset.PODate)
				//	return false;
				var dt = DateTime.ParseExact(PODate, PO.DATE_FORMAT, CultureInfo.InvariantCulture);
				if (dt < preset.FromDate || dt > preset.ToDate)
					return false;
			}

			return true;
		}

		// Khang updated for Vline demo
		public bool MatchKeyword(string key) {
			DisplayAsSearchResult = false;
            var result = false;

            this._subtitle = string.Empty;
			if (string.IsNullOrEmpty(key)) {
                this.HighlightText = new FormattedString();
                result = true;
			}
			key = key.ToLower().Trim();

			this._subtitle = MatchedText(key);

			if (!string.IsNullOrEmpty(_subtitle))
			{
				var highlighter = new HighlightMarker.HighlightMarker(_subtitle, key);
				this.HighlightText = new FormattedString();
                HighLightContent(highlighter, _subtitle, HighlightText);
				result = true;
			}
			else {
                this.HighlightText = new FormattedString();
			}

			// FITERING BY SUPPLIER NAME 
			if (!string.IsNullOrEmpty(this.SupplierName) && this.SupplierName.ToLower().Contains(key)) {
                var highlighter = new HighlightMarker.HighlightMarker(this.SupplierName, key);
                this.HighlightSupplierName = new FormattedString();
                HighLightContent(highlighter, this.SupplierName, HighlightSupplierName);
                result = true;
            } else {
                this.HighlightSupplierName = new FormattedString();
                this.HighlightSupplierName.Spans.Add(new Span() { Text = SupplierName, BackgroundColor = Color.Transparent });
            }

            // FITERING BY PO NUMBER 
            if (!string.IsNullOrEmpty(ItemTitle) && ItemTitle.ToLower().Contains(key))
			{
				var highlighter = new HighlightMarker.HighlightMarker(ItemTitle, key);
				this.HighlightItemTitle = new FormattedString();
                HighLightContent(highlighter, ItemTitle, HighlightItemTitle);
				result = true;
			}
			else
			{
				this.HighlightItemTitle = new FormattedString();
				this.HighlightItemTitle.Spans.Add(new Span() { Text = ItemTitle, BackgroundColor = Color.Transparent });
			}

			// FITERING BY PROJECT CODE 
            if (!string.IsNullOrEmpty(ProjectCode) && ProjectCode.ToLower().Contains(key))
			{
				var highlighter = new HighlightMarker.HighlightMarker(ProjectCode, key);
				this.HighlightProjectCode = new FormattedString();
                HighLightContent(highlighter, ProjectCode, HighlightProjectCode);
                HighlightProjectCode.Spans.Insert(0, new Span() { Text = "BU/Project: ", BackgroundColor = Color.Transparent });
				result = true;
			}
			else
			{
                this.HighlightProjectCode = null;
			}

			// FITERING BY ORDER DATE
            if (!string.IsNullOrEmpty(PODate) && PODate.ToLower().Contains(key))
			{
				var highlighter = new HighlightMarker.HighlightMarker(PODate, key);
				this.HighlightOrderDate = new FormattedString();
                HighLightContent(highlighter, PODate, HighlightOrderDate);
				HighlightOrderDate.Spans.Insert(0, new Span() { Text = "Date: ", BackgroundColor = Color.Transparent });
				result = true;
			}
			else
			{
				this.HighlightOrderDate = null;
			}



            this.DisplayAsSearchResult = result;
            return result;
		}

        private void HighLightContent(HighlightMarker.HighlightMarker marker, string content, FormattedString highlightTextUI) {
            if (highlightTextUI == null)
                highlightTextUI = new FormattedString();
			foreach (var item in marker)
			{
				if (item.IsHighlighted)
				{
					highlightTextUI.Spans.Add(new Span() { Text = content.Substring(item.FromIndex, item.Length), BackgroundColor = Color.Yellow });
				}
				else
				{
					highlightTextUI.Spans.Add(new Span() { Text = content.Substring(item.FromIndex, item.Length), BackgroundColor = Color.Transparent });
				}
			}
        }

		private string MatchedText(string keyword)
		{
			var result = string.Empty;
			var matchedTextList = new List<string>();
            if (string.IsNullOrEmpty(keyword))
                return result;
            
            if (POLines != null)
			{
				foreach (var line in POLines)
				{
					if (line.MatchesKeyword(keyword))
					{
						matchedTextList.Add(line.GetKeywordMatchedContent(keyword));
					}
				}
			}

			var nLineItem = matchedTextList.Count;

            // Added some PO properties
            if (!string.IsNullOrEmpty(this.Note) && this.Note.ToLower().Contains(keyword.ToLower()))
			{
				var matchedText = StringHelper.GetContentAroundKeyword(keyword, this.Note);
				matchedTextList.Insert(0, matchedText);
			}

			if (!string.IsNullOrEmpty(this.UserID) && this.UserID.ToLower().Contains(keyword.ToLower()))
			{
				var matchedText = StringHelper.GetContentAroundKeyword(keyword, this.UserID);
				matchedTextList.Insert(0, matchedText);
			}
            var nText = matchedTextList.Count;
			if (nText > 0) { 
				result += string.Format("({0}): ", nLineItem);
				for (int i = 0; i < nText; i++)
				{
					result += matchedTextList[i];
					result += (i < nText - 1) ? "..., " : "...";
				}
			}
			return result; 
		}

        private List<string> GetKeywordMatchedContent (string keyword) {
            var result = new List<string>();
            if (string.IsNullOrEmpty(keyword))
                return result;

            if (!string.IsNullOrEmpty(this.PONumber.ToString()) && this.PONumber.ToString().ToLower().Contains(keyword)) {
                result.Add((StringHelper.GetContentAroundKeyword(keyword, this._poNumber.ToString())));
            }

			if (!string.IsNullOrEmpty(this.PONumber.ToString()) && this.PONumber.ToString().ToLower().Contains(keyword))
			{
				result.Add((StringHelper.GetContentAroundKeyword(keyword, this._poNumber.ToString())));
			}

			if (!string.IsNullOrEmpty(this.PONumber.ToString()) && this.PONumber.ToString().ToLower().Contains(keyword))
			{
				result.Add((StringHelper.GetContentAroundKeyword(keyword, this._poNumber.ToString())));
			}
            return result;
        }

		private string TrimValue(string value) {
			return string.IsNullOrEmpty(value) ? string.Empty : value.Trim();
		}

		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		public void OnPropertyChanged(string propertyName = null)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		#endregion
	}
}
