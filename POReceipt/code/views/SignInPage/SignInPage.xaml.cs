using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POReceipt
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SignInPage : ContentPage
	{
		SignInVM _vm;
        bool IsExpandingConfigStack = false;
		public SignInPage()
		{
			InitializeComponent();
			initUI();
			_vm = new SignInVM();
			this.BindingContext = _vm;

            this.buttonLogin.Clicked += async (sender, e) =>
			{
                if (ValidateConfigs()) {
                    Settings.StoreServerConfigs(new ServerConfig()
                    {
                        JDE_HOSTNAME = hostNameEntry.Text.Trim(),
                        JDE_SERVICE_ROOT = serviceRootEntry.Text.Trim(),
                        JDE_ENVIRONMENT = environmentEntry.Text.Trim(),
                        JDE_VERSION = versionEntry.Text.Trim()
                    });

                    App.Native.UpdateConfigs(hostNameEntry.Text, serviceRootEntry.Text, environmentEntry.Text, versionEntry.Text, deviceNameEntry.Text);
                } else {
                    await DisplayAlert("Invalid configuration", "Please make sure all the configuration variables have been set correctly", "OK");
                    return;
                }

				if (_vm != null)
				{
                    Device.BeginInvokeOnMainThread( () => {
                        App.Native.setBusy(true);
                    });

					if (await _vm.DoLogin()) {
                        
						await this.Navigation.PopModalAsync(true);
                        Device.BeginInvokeOnMainThread(() => {
                            App.Native.setBusy(false);
                        });
                    } else {
                        Device.BeginInvokeOnMainThread(() => {
                            App.Native.setBusy(false);
                       });   
                    }

				}
			};

            UIHelper.addClickEventForView(systemConfigButtonStack,(sender, e) => {
                IsExpandingConfigStack = !IsExpandingConfigStack;
                Device.BeginInvokeOnMainThread( () => {
                    UIHelper.DoCollapseAndExpand(mainGrid, configStack, 420,null);
                    stackIcon.Source = (IsExpandingConfigStack) ? ImageSource.FromFile("iconCollapse_white") : ImageSource.FromFile("iconExpand_white");
                });
            }, ClickAnimation.Fade);

            deviceNameEntry.Text = Settings.GetDeviceId();
		}

        bool ValidateConfigs() {
            if (string.IsNullOrEmpty(hostNameEntry.Text) 
                || string.IsNullOrEmpty(serviceRootEntry.Text) 
                || string.IsNullOrEmpty(versionEntry.Text) 
                || string.IsNullOrEmpty(environmentEntry.Text)
                || string.IsNullOrEmpty(deviceNameEntry.Text)) {
                return false;
            }
            return true;
        }

		protected override void OnAppearing()
		{
			base.OnAppearing();
			//this.usernameEntry.Focus();
		}

		void initUI()
		{
			this.usernameEntry.Keyboard = Keyboard.Create(KeyboardFlags.All);
			NavigationPage.SetHasNavigationBar (this, false);
            var configs = Settings.LoadServerConfigs();

            if (configs != null) {
                hostNameEntry.Text = configs.JDE_HOSTNAME;
                serviceRootEntry.Text = configs.JDE_SERVICE_ROOT;
                environmentEntry.Text = configs.JDE_ENVIRONMENT;
                versionEntry.Text = configs.JDE_VERSION;
            }
		}
	}
}

