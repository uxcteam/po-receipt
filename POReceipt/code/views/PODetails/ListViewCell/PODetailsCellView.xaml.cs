﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class PODetailsCellView : ContentView
	{
		bool selected = false;

		public PODetailsCellView()
		{
			InitializeComponent();
			UIHelper.addClickEventForView(this.editIconStack, (sender, e) => { 
				MessagingCenter.Send(this.BindingContext, Conts.MSG_EDIT_PODETAIL_ITEM);
			}, ClickAnimation.Fade);

			UIHelper.addClickEventForView(this.mainGrid, (sender, e) => {
				selected = !selected;
				var poItem = (POLine)this.BindingContext;
				poItem.IsSelected = selected;
				this.selectedIcon.Source = selected ?
					ImageSource.FromFile("selected_icon")
					: ImageSource.FromFile("unselected_icon");
				MessagingCenter.Send(this.BindingContext, Conts.MSG_SELECT_UNSELECT_PODETAIL_ITEM);
			}, ClickAnimation.Fade);
		}
	}
}
