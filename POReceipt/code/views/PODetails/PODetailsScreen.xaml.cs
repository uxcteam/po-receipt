﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POReceipt
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PODetailsScreen : ContentPage
	{
		//public ObservableCollection<POLine> POLines;
		private PO po;

		public PODetailsScreen()
		{
			InitializeComponent();
		}

		public PODetailsScreen(PO po) : this()
		{
			this.po = po;
			this.BindingContext = po;
			InitUI();
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
		}

		void InitUI()
		{
			NavigationPage.SetBackButtonTitle(this, "");
			if (this.po != null && po.POLines != null) {
				//POLines = new ObservableCollection<POLine>(po.POLines);
				this.listView.ItemsSource = po.POLines;
                this.noItemsLabel.IsVisible = (po.POLines.Count == 0);
			}

			listView.ItemTapped += (object sender, ItemTappedEventArgs e) =>
			{
				if (e.Item == null) return;
				((ListView)sender).SelectedItem = null; // de-select the row

			};


			UIHelper.addClickEventForView(this.itemInfoGrid, (sender, e) =>
			{
				Device.BeginInvokeOnMainThread(async () =>
				{
					this.itemInfoGrid.IsEnabled = false;
					await this.Navigation.PushAsync(new PODetailScreen(this.po));
					this.itemInfoGrid.IsEnabled = true;
				});
			}, ClickAnimation.Fade);


			// Item edit button clicked handling
			MessagingCenter.Subscribe<object>(this, Conts.MSG_EDIT_PODETAIL_ITEM, (item) =>
			{
				Device.BeginInvokeOnMainThread(() =>
				{
					var line = (POLine)item;
					foreach (var l in po.POLines) {
                        if (l.LineNumber == line.LineNumber) { 
                            this.Navigation.PushAsync(new EditReceipt(l, po));
						}
					}
				});

			});

			// Item selected_unselected event
			MessagingCenter.Subscribe<object>(this, Conts.MSG_SELECT_UNSELECT_PODETAIL_ITEM, (item) =>
			{
				Device.BeginInvokeOnMainThread(() =>
				{
					UpdateButtonState();
				});      
			});

			// Line updated event
			MessagingCenter.Subscribe<App>((App)App.Current, Conts.MSG_UPDATE_POLINES, (item) =>
			{
				Device.BeginInvokeOnMainThread(() =>
				{
					this.listView.BeginRefresh();
					this.listView.ItemsSource = po.POLines;
					this.listView.EndRefresh();
				});
			});

			this.buttonReceiveAll.Clicked += ButtonReceiveAllClicked;
			this.buttonReceiveSelected.Clicked += ButtonReceiveSelectedClicked;
			UpdateButtonState();
		}

        private async Task<bool> ReceiveAll()
		{
            Device.BeginInvokeOnMainThread(() => {
                App.Native.setBusy(true);
            });
            if (await this.po.DoReceiveAll()) {
				Device.BeginInvokeOnMainThread(() => {
					App.Native.setBusy(false);
					this.listView.ItemsSource = po.POLines;
					this.noItemsLabel.IsVisible = (po.POLines.Count == 0);
				});
				MessagingCenter.Send<App>((App)Application.Current, Conts.MSG_UPDATE_POLIST);
                return true;
            }
            App.Native.setBusy(false);
            return false;
		}

        private async Task<bool> ReceiveSelected()
		{
			Device.BeginInvokeOnMainThread(() => {
				App.Native.setBusy(true);
			});

            var lines = new Dictionary<float, JObject>();

			foreach (var item in po.POLines)
			{
                var lineJObject = new JObject();
				if (item.IsSelected)
				{
                    if (item.QuantityNew > 0.0) {
                        lineJObject.Add("quanNew", item.QuantityNew); 
                    } else {
                        lineJObject.Add("quanNew", item.QuantityRemaining);
					}
					
					lineJObject.Add("lineNum", item.LineNumber);
                    lineJObject.Add("toBeReceived", true);
					lines.Add(item.LineNumber, lineJObject);
                } else {
					lineJObject.Add("quanNew", 0.0);    // if this has not been selected then put a zero value
    				lineJObject.Add("lineNum", item.LineNumber);
                    lineJObject.Add("toBeReceived", false);
    				lines.Add(item.LineNumber, lineJObject);
                }
			}

            if (await this.po.DoReceiveSelectedLines(lines))
            {

                App.Native.setBusy(false);
                foreach (var item in po.POLines)
                {
                    if (item.IsSelected)
                    {
                        po.DoReceiveLine(item);
                    }
                }


                var tmpList = new List<POLine>();
                foreach (var line in po.POLines)
                {
                    if (!line.IsReceived)
                    {
                        tmpList.Add(line);
                    }
                }

                po.POLines = tmpList;


                Device.BeginInvokeOnMainThread(() =>
                {

                    this.listView.ItemsSource = po.POLines;
                    this.noItemsLabel.IsVisible = (po.POLines.Count == 0);
                    MessagingCenter.Send<App>((App)Application.Current, Conts.MSG_UPDATE_POLIST);
                });

            }

            App.Native.setBusy(false);
            return true;
		}

		private void  ButtonReceiveAllClicked(object sender, EventArgs e)
		{
			//this.buttonReceiveAll.IsEnabled = false;
			Device.BeginInvokeOnMainThread(async () =>
			{
				var resultTask = await this.DisplayAlert("Warning", "Are you sure you want to receive all the items ?", "Yes", "No");
				if (resultTask)
				{
                    bool hasEditedlines = false;
                    foreach (var line in po.POLines)
                    {
                        line.IsSelected = true;
                        if (line.QuantityNew > 0)
                            hasEditedlines = true;
                    }

                    if (!hasEditedlines)
                        await ReceiveAll();
                    else
                        await ReceiveSelected();
					UpdateButtonState();
				}
			});
		}

		private void UpdateButtonState() {
			if (this.po.POLines != null)
			{
				this.buttonReceiveAll.IsEnabled = this.po.POLines.Count == 0 ? false : true;

				foreach (var line in po.POLines) {
					if (line.IsSelected)
					{
						this.buttonReceiveSelected.IsEnabled = true;
						return;
					}
				}
				this.buttonReceiveSelected.IsEnabled = false;
			}
		}

		private void ButtonReceiveSelectedClicked(object sender, EventArgs e)
		{
            //this.buttonReceiveSelected.IsEnabled = false;
			Device.BeginInvokeOnMainThread(async() =>
			{
				var resultTask = await this.DisplayAlert("Warning", "Are you sure you want to receive selected items ?", "Yes", "No");
				if (resultTask)
				{
                    bool hasEditedLines = false;
                    int selectedCount = 0;
                    foreach (var item in po.POLines)
                    {
                        if (item.QuantityNew > 0)
                            hasEditedLines = true;
                        selectedCount += item.IsSelected ? 1 : 0;
                    }

                    if (selectedCount == po.POLines.Count && !hasEditedLines)
                        await ReceiveAll();
                    else
					    await ReceiveSelected();
					UpdateButtonState();
				}
			});
		}
	}
}
