﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class ImageViewerScreen : ContentPage
	{
		public ImageViewerScreen()
		{
			InitializeComponent();
		}

        public ImageViewerScreen(Attachment attachment, PO po, POLine line, bool isOrderAttachment) : this() {
			
			this.Title = attachment.FileName;
			App.Native.setBusy(true);
			if (attachment != null) {
				if (!string.IsNullOrEmpty(attachment.LocalFileURL)) {
					var bytes = App.Native.readBytes(attachment.LocalFileURL);
					this.image.Source = ImageSource.FromStream(() => new MemoryStream(bytes));
                } else {
                    if (po != null) {
                        Task.Factory.StartNew(async () => {
                            Device.BeginInvokeOnMainThread(()=> {
                                App.Native.setBusy(true);
                            });
                            Stream stream = null;
                            if (isOrderAttachment) {
                                stream = await App.Native.GetOrderBinary(App.token, App.deviceName, po.PONumber, po.POType, po.POCompany, po.POSuffix, attachment.Sequence);
                            } else if (line != null) {
                                stream = await App.Native.GetLineBinary(App.token, App.deviceName, po.PONumber, po.POType, po.POCompany, po.POSuffix,line.LineNumber, attachment.Sequence);
                            }
                            Device.BeginInvokeOnMainThread(() => {
                                App.Native.setBusy(false);
                            });
                            if (stream != null)
                            {
                                var bytes = await FileUtil.GetBytes(stream);
                                var filePath = System.IO.Path.Combine(Conts.PHOTOS_FOLDER_NAME, attachment.FileName);
                                App.Native.writeFile(filePath, bytes);
                                attachment.LocalFileURL = filePath;
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    this.image.Source = ImageSource.FromStream(() => new MemoryStream(bytes));
                                });
                            }
                           
                        });
                    }
                }
			}
			App.Native.setBusy(false);
		}
	}
}
