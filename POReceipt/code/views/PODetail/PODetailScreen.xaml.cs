﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POReceipt
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PODetailScreen : ContentPage
	{
		PO po;
        bool notesUpdated = false;
        string notes = string.Empty;

		public PODetailScreen()
		{
			InitializeComponent();
			var save = new ToolbarItem
			{
				Text = "Save",
				Priority = 0,
				Command = new Command(OnSave)
			};
			ToolbarItems.Add(save);

        
			Device.BeginInvokeOnMainThread(async () => {
				App.Native.setBusy(true);
                var noteText = await App.Native.GetOrderText(App.token, App.deviceName, po.PONumber, po.POType, po.POCompany, po.POSuffix);

				if (!string.IsNullOrEmpty(noteText))
				{
                    notes = noteText;
					noteView.TextValue = noteText;
				}

                // Get attachment list
                var result = await App.Native.GetOrderFileList(App.token, App.deviceName, po.PONumber, po.POType, po.POCompany, po.POSuffix);
                if (!string.IsNullOrEmpty(result)) {
                    var attachmentList = Model.ParseFileListJson(result);
                    po.Attachments = attachmentList;
                    this.attachmentView.SetDataSource(po.Attachments);
                } else {
                    App.Native.showAlert("Failed to load attachments", "Failed");
                }

				App.Native.setBusy(false);
			});

		}

		public void OnSave() {
			if (this.po != null) {
				if (!string.IsNullOrEmpty(this.noteView.TextValue))
					this.po.Note = this.noteView.TextValue.Trim();
				if (this.attachmentView.Attachments != null && this.attachmentView.Attachments.Count > 0)
					this.po.Attachments = this.attachmentView.Attachments;	
				
				//App.Native.showAlert("The changes for this PO have been saved.", "Saved");

                Device.BeginInvokeOnMainThread(async () => {
                    if (notesUpdated) {
                        App.Native.setBusy(true);
                        var result = await App.Native.SetOrderText(App.token, App.deviceName, po.PONumber, po.POType, po.POCompany, po.POSuffix, po.Note);
                        App.Native.setBusy(false);
                    }
                    App.Native.setBusy(true);
                    foreach (var item in this.po.Attachments)
                    {
                        if (item.Uploaded)
                            continue;
                        try {
                            var result = await App.Native.SetOrderBinary(App.token, App.deviceName, this.po.PONumber, this.po.POType, po.POCompany, po.POSuffix, item.FileName, item.Bytes);
                            if (string.IsNullOrEmpty(result))
                            {
                                App.Native.showAlert("Failed to upload image, please try again", "Failed");
                                break;
                            }
                            else
                            {
                                item.Uploaded = true;
                            }

                        } catch (Exception ex) {
                            App.Native.showAlert(string.Format("Failed to upload attachment {0} - {1}",item.FileName, ex.Message ), "Failed");
                        }
                       
                    }


                    foreach (var item in this.attachmentView.AttachmentsToBeDeleted)
                    {
                        try
                        { 
                            var result = await App.Native.DeleteOrderBinary(App.token, App.deviceName, this.po.PONumber, this.po.POType, po.POCompany, po.POSuffix, item.Sequence);
                            if (string.IsNullOrEmpty(result))
                            {
                                App.Native.showAlert("Failed to delete image", "Failed");
                                break;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(item.LocalFileURL))
                                {
                                    App.Native.deleteFile(item.LocalFileURL);
                                }
                            }
                        } catch (Exception ex) {
                            App.Native.showAlert(string.Format("Failed to delete attachment {0} - {1}",item.FileName, ex.Message ), "Failed");
                        }
                    }
                    App.Native.setBusy(false);
					//MessagingCenter.Send<App>((App)Application.Current, Conts.MSG_UPDATE_POLIST);
					await this.Navigation.PopAsync();

                });
			}
		}

		public PODetailScreen(PO po) : this() {
			this.po = po;
			this.BindingContext = po;
			this.attachmentView.SetDataSource(po.Attachments);
            notes = po.Note ?? string.Empty;
			

            noteView.OnTextChanged += (sender, e) => {
                notesUpdated = !notes.Equals(noteView.TextValue);
            };
		}

        protected override void OnAppearing()
        {
            MessagingCenter.Subscribe<App, Attachment>((App)Application.Current, Conts.MSG_IMAGE_SELECTED, (s, e) =>
            {
                var attachment = (Attachment)e;
                if (attachment != null)
                {

                    Device.BeginInvokeOnMainThread(() => {
                        this.Navigation.PushAsync(new ImageViewerScreen(attachment, po, null, true));
                    });
                }
            });
            base.OnAppearing();
        }

		protected override void OnDisappearing()
		{
            MessagingCenter.Unsubscribe<App, Attachment>((App)Application.Current, Conts.MSG_IMAGE_SELECTED);
			base.OnDisappearing();
		}

        private bool ChangesDetected() {
            if (notesUpdated)
                return true;
            foreach (var item in this.po.Attachments) {
                if (!item.Uploaded)
                    return true;
            }

            if (this.attachmentView.AttachmentsToBeDeleted != null && this.attachmentView.AttachmentsToBeDeleted.Count > 0) {
                return true;
            }

            return false;
        }

	}
}
