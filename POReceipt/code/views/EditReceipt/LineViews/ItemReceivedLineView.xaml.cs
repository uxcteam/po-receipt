﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class ItemReceivedLineView : ContentView
	{

		public ItemReceivedLineView() { 
			InitializeComponent();
		}

		public ItemReceivedLineView(POLine line) : this()
		{
			if (line != null) {
				//this.itemLbl.Text = string.Format("{0}, Qty: {1}, Ext Price: ${2}", line.LineNumber,line.Quantity, line.ExtPrice);
			}
		}

		public ItemReceivedLineView(string title) : this() {
			this.itemLbl.Text = title;
		}
	}
}
