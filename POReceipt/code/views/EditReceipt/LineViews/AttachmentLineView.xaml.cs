﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Plugin.Media;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class AttachmentLineView : ContentView
	{
		public static readonly BindableProperty TitleProperty = BindableProperty.Create(propertyName: "Title",
			returnType: typeof(string),
			declaringType: typeof(AttachmentLineView),
			defaultValue: default(string));

		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set
			{
				SetValue(TitleProperty, value);
			}
		}

		public static readonly BindableProperty TextValueProperty = BindableProperty.Create(propertyName: "TextValue",
		returnType: typeof(string),
		declaringType: typeof(AttachmentLineView),
		defaultValue: default(string));

		public string TextValue
		{
			get { return (string)GetValue(TextValueProperty); }
			set
			{
				SetValue(TextValueProperty, value);
			}
		}
		public List<Attachment> Attachments { set; get; }
        public List<Attachment> AttachmentsToBeDeleted { set; get; }
		public EventHandler OnItemSelected;

		int currentHeight = 5;
		static int LINE_HEIGHT = 35;

		public AttachmentLineView()
		{
			InitializeComponent();
			this.Attachments = new List<Attachment>();
			this.cameraButton.Clicked += OnCameraButtonClicked;
			this.photoButton.Clicked += OnPhotoButtonClicked;
            AttachmentsToBeDeleted = new List<Attachment>();
		}

		public async void OnPhotoButtonClicked(object sender, EventArgs e) { 
			photoButton.IsEnabled = false;
			Stream stream = await DependencyService.Get<IPicturePicker>().GetImageStreamAsync();
			App.Native.setBusy(true);
			if (stream != null)
			{
				await AddNewItemLine(stream, true);

				//ImageSource imageSource = ImageSource.FromStream(() => stream);

				//AddNewPhotoLine(imageSource);
				photoButton.IsEnabled = true;
			}
			else
			{
				photoButton.IsEnabled = true;
			}
			App.Native.setBusy(false);
		}

		public async void OnCameraButtonClicked(object sender, EventArgs e) { 
			cameraButton.IsEnabled = false;
			await CrossMedia.Current.Initialize();

		    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
		    {
				cameraButton.IsEnabled = true;
				App.Native.showAlert("No camera available.", "No Camera");
		        return;
		    }

            if (Device.RuntimePlatform == Device.Android) {
                Device.BeginInvokeOnMainThread(() => {
                    App.Native.setBusy(true);
                });
            }
			
			var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions() { 
			});

			if (file == null)
			{
				cameraButton.IsEnabled = true;
                Device.BeginInvokeOnMainThread(() => {
                    App.Native.setBusy(false);
                });
				
				return;
			}


            if (Device.RuntimePlatform == Device.iOS) {
                Device.BeginInvokeOnMainThread(() => {
                    App.Native.setBusy(true);
                });
            }
	
			var stream = file.GetStream();
			//var bytes = await FileUtil.GetBytes(stream);
			await AddNewItemLine(stream, true);
			file.Dispose();
            Device.BeginInvokeOnMainThread(() => {
                App.Native.setBusy(false);
            });
			//AddNewPhotoLine(imageSource);
			cameraButton.IsEnabled = true;
		}

		public void SetDataSource(List<Attachment> attachments) {
			this.Attachments = attachments;
			Render();
		}

		private void Render() {
			if (this.Attachments == null)
				return;
			
			Device.BeginInvokeOnMainThread(() => {
				this.attachmentsStack.Children.Clear();
				currentHeight = 5;
				foreach (var item in this.Attachments)
				{
					currentHeight += LINE_HEIGHT;
					SpawnNewLineItemView(item);
				}
				UIHelper.ExpandGridRow(this.mainGrid, this.attachedItemRow, currentHeight, null);
			});
		}

		private async Task AddNewItemLine(Stream stream, bool resize) {
			if (stream == null)
				return;

			var bytes = await FileUtil.GetBytes(stream);
			if (resize)
                bytes = await App.Native.resizeImage(bytes, 1600, 1600, false);
            
			//App.Native.showAlert(bytes.Length.ToString());

			var fileName = "photo_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".jpg";
			var filePath = System.IO.Path.Combine(Conts.PHOTOS_FOLDER_NAME, fileName);
			var newAttachment = new Attachment() { FileName = fileName, LocalFileURL = filePath, Bytes = bytes };
			App.Native.writeFile(filePath, bytes);

			this.Attachments.Add(newAttachment);

			Device.BeginInvokeOnMainThread(() =>
			{
				currentHeight += LINE_HEIGHT;
				SpawnNewLineItemView(newAttachment);
				UIHelper.ExpandGridRow(this.mainGrid, this.attachedItemRow, currentHeight, null);        
			});
		} 

		private void AddNewPhotoLine(ImageSource imageSource) {
			if (imageSource == null)
				return;

			var fileName = "photo_" + DateTime.Now.ToString("yyyyMMddHHmmss");
			var newAttachment = new Attachment() { FileName = fileName, ImageSource = imageSource };
			this.Attachments.Add(newAttachment);

			currentHeight += LINE_HEIGHT;
			Device.BeginInvokeOnMainThread(() =>
			{
                SpawnNewLineItemView(newAttachment);
				UIHelper.ExpandGridRow(this.mainGrid, this.attachedItemRow, currentHeight, null);
			});
		}

		private void SpawnNewLineItemView(Attachment attachment) {
			var newAttachedItem = new AttachedItemView(attachment);
			newAttachedItem.HeightRequest = LINE_HEIGHT;
			newAttachedItem.OnRemoveAttachedItem += OnAttachedItemRemove;
			this.attachmentsStack.Children.Add(newAttachedItem);
		}

		public void OnAttachedItemRemove(object sender, EventArgs e) {
			var lineView = (AttachedItemView)sender;
			if (lineView != null) {
				this.attachmentsStack.Children.Remove(lineView);
				currentHeight -= 35;
				UIHelper.ExpandGridRow(this.mainGrid, this.attachedItemRow, currentHeight, null);

				if (lineView.attachment != null && this.Attachments != null) {

                    if (this.Attachments.Contains(lineView.attachment)) {
                        if (lineView.attachment.Uploaded)
                        {
                            AttachmentsToBeDeleted.Add(lineView.attachment);
                            UpdateMessageIfNeeded();
                            // Local file storaged will be deleted after the calling delete service
                        } else {
                            if (!string.IsNullOrEmpty(lineView.attachment.LocalFileURL)) {
                                App.Native.deleteFile(lineView.attachment.LocalFileURL);
                            }
                        }
                        this.Attachments.Remove(lineView.attachment);
					}
				}
			}
		}

        private void UpdateMessageIfNeeded() {
            Device.BeginInvokeOnMainThread(()=> {
                if (this.AttachmentsToBeDeleted != null ) {
                    if (this.AttachmentsToBeDeleted.Count > 0)
                    {
                        var pluralStr = AttachmentsToBeDeleted.Count > 1 ? "s" : string.Empty;
                        messageLbl.Text = string.Format("{0} attachment{1} removed, UNSAVED",
                                                        AttachmentsToBeDeleted.Count, pluralStr);
                        UIHelper.ExpandGridRow(mainGrid, messageRow, 40, null);
                    } else {
                        messageLbl.Text = string.Empty;
                        UIHelper.ExpandGridRow(mainGrid, messageRow, 0, null);
                    }
                }
            });
        }

		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			if (propertyName.Equals("Title"))
			{
				this.titleLbl.Text = this.Title;
			}
		}
	}
}
