﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class TextLinePickerView : ContentView
	{
		public EventHandler OnBookMarkClicked;

		public static readonly BindableProperty TitleProperty = BindableProperty.Create(propertyName: "Title",
			returnType: typeof(string),
			declaringType: typeof(TextLinePickerView),
			defaultValue: default(string));

		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set
			{
				SetValue(TitleProperty, value);
			}
		}

		public static readonly BindableProperty TextValueProperty = BindableProperty.Create(propertyName: "TextValue",
			returnType: typeof(string),
			declaringType: typeof(TextLinePickerView),
			defaultValue: default(string));

		public string TextValue
		{
			get { return (string)GetValue(TextValueProperty); }
			set
			{
				SetValue(TextValueProperty, value);
			}
		}

		public static readonly BindableProperty OptionSourceProperty = BindableProperty.Create(propertyName: "OptionSource",
			returnType: typeof(List<string>),
			declaringType: typeof(TextLinePickerView),
			defaultValue: default(List<string>));

		public List<string> OptionSource
		{
			get { return (List<string>)GetValue(OptionSourceProperty); }
			set
			{
                SetValue(OptionSourceProperty, value);
			}
		}

		public TextLinePickerView()
		{
			InitializeComponent();
            entryValue.TextChanged += (sender, e) => {
                TextValue = entryValue.Text;
            };
		}

        public void ToggleLookup(bool show) {
            Device.BeginInvokeOnMainThread(()=> {
                columnLookup.Width = show ? 40 : 0;
                clickArea.IsEnabled = show;
                //this.Content.IsEnabled = show;
                if (show) {
                    UIHelper.addClickEventForView(this.clickArea, (sender, e) =>
                    {
                        if (OnBookMarkClicked != null)
                        {
                            OnBookMarkClicked(this, EventArgs.Empty);
                        }
                    }, ClickAnimation.Fade);

                    UIHelper.addClickEventForView(this.Content, (sender, e) =>
                    {
                        if (OnBookMarkClicked != null)
                        {
                            OnBookMarkClicked(this, EventArgs.Empty);
                        }
                    }, ClickAnimation.Fade);
                }
            });
        }

		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			if (propertyName.Equals("Title"))
			{
				this.titleLbl.Text = this.Title;
			}
			else if (propertyName == "TextValue")
			{
				this.entryValue.Text = TextValue;
			}
		}
	}
}
