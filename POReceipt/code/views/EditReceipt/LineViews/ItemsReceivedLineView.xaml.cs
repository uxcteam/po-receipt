﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class ItemsReceivedLineView : ContentView
	{
		public static readonly BindableProperty TitleProperty = BindableProperty.Create(propertyName: "Title",
			returnType: typeof(string),
			declaringType: typeof(ItemsReceivedLineView),
			defaultValue: default(string));

		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set
			{
				SetValue(TitleProperty, value);
			}
		}

		public static readonly BindableProperty TextValueProperty = BindableProperty.Create(propertyName: "TextValue",
		returnType: typeof(string),
		declaringType: typeof(ItemsReceivedLineView),
		defaultValue: default(string));

		public string TextValue
		{
			get { return (string)GetValue(TextValueProperty); }
			set
			{
				SetValue(TextValueProperty, value);
			}
		}

		int currentHeight = 5;
		public ItemsReceivedLineView()
		{
			InitializeComponent();

		}

		public void SetDataSource(List<POLine> lines) {
			if (lines != null && lines.Count > 0)
			{
				for (int i = 0; i < lines.Count; i++)
				{
					currentHeight += 30;
					var newReceivedItem = new ItemReceivedLineView(lines[i]);
					newReceivedItem.HeightRequest = 30;
					this.receivedItemStack.Children.Add(newReceivedItem);
				};
			}
			else { 
				currentHeight += 30;
				var newReceivedItem = new ItemReceivedLineView("(No items received)");
				newReceivedItem.HeightRequest = 30;
                this.receivedItemStack.Children.Add(newReceivedItem);
			}
			UIHelper.ExpandGridRow(this.mainGrid, this.receivedItemRow, currentHeight, null);
		}

		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			if (propertyName.Equals("Title"))
			{
				this.titleLbl.Text = this.Title;
			}
		}
	}
}
