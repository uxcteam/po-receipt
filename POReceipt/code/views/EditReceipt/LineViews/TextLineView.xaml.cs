﻿using System;
using System.Linq;
using System.Collections.Generic;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class TextLineView : ContentView
	{
		public static readonly BindableProperty TitleProperty = BindableProperty.Create(propertyName: "Title",
			returnType: typeof(string),
			declaringType: typeof(TextLineView),
			defaultValue: default(string));

		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set
			{
				SetValue(TitleProperty, value);
			}
		}

		public static readonly BindableProperty TextValueProperty = BindableProperty.Create(propertyName: "TextValue",
			returnType: typeof(string),
			declaringType: typeof(TextLineView),
			defaultValue: default(string));

		public string TextValue
		{
			get { return (string)GetValue(TextValueProperty); }
			set
			{
				SetValue(TextValueProperty, value);
			}
		}

		public static readonly BindableProperty EditableProperty = BindableProperty.Create(propertyName: "Editable",
			returnType: typeof(bool),
			declaringType: typeof(TextLineView),
			defaultValue: true);

		public bool Editable
		{
			get { return (bool)GetValue(EditableProperty); }
			set
			{
				SetValue(EditableProperty, value);
			}
		}

		private CustomEntry.KeyboardType KeyboardType { set; get; }

		public void SetKeyboardType(Keyboard type)
		{
			this.entryValue.Keyboard = type;
		}

		public TextLineView()
		{
			InitializeComponent();
			entryValue.TextChanged += (sender, e) =>
			{
				
				this.TextValue = entryValue.Text;
			};
		}

		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			if (propertyName.Equals("Title"))
			{
				this.titleLbl.Text = this.Title;
			}
			else if (propertyName == "TextValue")
			{
				this.entryValue.Text = TextValue;
			}
			else if (propertyName == "Editable")
			{
				this.entryValue.IsEnabled = this.Editable;
			}
		}

		private string RemoveExtraText(string value)
		{
			var allowedChars = "0123456789";
			var cArray = value.ToCharArray();
			return new string(cArray.Where(c => allowedChars.Contains(c.ToString())).ToArray());
		}   
	}
}
