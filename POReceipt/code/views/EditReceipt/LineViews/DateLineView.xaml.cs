﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class DateLineView : ContentView
	{
		public static readonly BindableProperty TitleProperty = BindableProperty.Create(propertyName: "Title",
			returnType: typeof(string),
			declaringType: typeof(DateLineView),
			defaultValue: default(string));

		public string Title
		{
		get { return (string)GetValue(TitleProperty); }
		set
		{
		SetValue(TitleProperty, value);
		}
		}

		public static readonly BindableProperty TextValueProperty = BindableProperty.Create(propertyName: "TextValue",
			returnType: typeof(string),
			declaringType: typeof(DateLineView),
			defaultValue: default(string));

		public string TextValue
		{
			get { return (string)GetValue(TextValueProperty); }
			set
			{
				SetValue(TextValueProperty, value);
			}
		}

		public static readonly BindableProperty DateProperty = BindableProperty.Create(propertyName: "Date",
		returnType: typeof(DateTime),
		declaringType: typeof(DateLineView),
		defaultValue: default(DateTime));

		public DateTime Date
		{
			get { return (DateTime)GetValue(DateProperty); }
			set
			{
				SetValue(DateProperty, value);
			}
		}

		public EventHandler OnUnfocused;
		
		public DateLineView()
		{
			InitializeComponent();
			this.datePicker.Unfocused += (sender, e) => { 
				this.TextValue = this.datePicker.Date.ToString(PO.DATE_FORMAT);
				this.Date = this.datePicker.Date;
				if (OnUnfocused != null)
					OnUnfocused(this, EventArgs.Empty);
			};
			this.datePicker.DateSelected += (sender, e) => {
				this.TextValue = this.datePicker.Date.ToString(PO.DATE_FORMAT);
                this.Date = this.datePicker.Date;
			};
			UIHelper.addClickEventForView(this.clickArea, (sender, e) => {
				this.datePicker.Focus();
			}, ClickAnimation.Fade);
			UIHelper.addClickEventForView(this.Content, (sender, e) =>
			{
				this.datePicker.Focus();
			}, ClickAnimation.Fade);
		}

		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			if (propertyName.Equals("Title"))
			{
				this.titleLbl.Text = this.Title;
			}
			else if (propertyName == "TextValue")
			{
				this.entryValue.Text = TextValue;
			}
			else if (propertyName == "Date")
			{
				this.TextValue = Date.ToString(PO.DATE_FORMAT);
			}
		}
	}
}
