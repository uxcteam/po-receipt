﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class DescriptionLineView : ContentView
	{
		public static readonly BindableProperty TitleProperty = BindableProperty.Create(propertyName: "Title",
			returnType: typeof(string),
			declaringType: typeof(TextLineView),
			defaultValue: default(string));

		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set
			{
				SetValue(TitleProperty, value);
			}
		}

		public static readonly BindableProperty TextValueProperty = BindableProperty.Create(propertyName: "TextValue",
			returnType: typeof(string),
			declaringType: typeof(TextLineView),
			defaultValue: default(string));

		public string TextValue
		{
			get { return (string)GetValue(TextValueProperty); }
			set
			{
				SetValue(TextValueProperty, value);
			}
		}

		public static readonly BindableProperty EditableProperty = BindableProperty.Create(propertyName: "Editable",
			returnType: typeof(bool),
			declaringType: typeof(TextLineView),
			defaultValue: true);

		public bool Editable
		{
			get { return (bool)GetValue(EditableProperty); }
			set
			{
				SetValue(EditableProperty, value);
			}
		}
        public EventHandler OnTextChanged;

		public DescriptionLineView()
		{
			InitializeComponent();
			this.entryValue.TextChanged += (sender, e) => {
				this.TextValue = this.entryValue.Text;
                if (OnTextChanged != null) {
                    OnTextChanged(this, e);
                }
			};
		}

		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			if (propertyName.Equals("Title"))
			{
				this.titleLbl.Text = this.Title;
			}
			else if (propertyName == "TextValue")
			{
				this.entryValue.Text = TextValue;
			}
			else if (propertyName == "Editable")
			{
				this.entryValue.IsEnabled = this.Editable;
			}
		}
	}
}
