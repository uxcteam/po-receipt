﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class AttachedItemView : ContentView
	{
		public Attachment attachment;
		public EventHandler OnRemoveAttachedItem;

		public AttachedItemView()
		{
			InitializeComponent();

		}

		public AttachedItemView(Attachment attachment) : this() {
			this.itemLbl.Text = attachment.FileName;
			this.attachment = attachment;
            this.BindingContext = attachment;
			UIHelper.addClickEventForView(this.deleteIcon, (sender, e) =>
			{
				if (OnRemoveAttachedItem != null) {
					OnRemoveAttachedItem(this, EventArgs.Empty);
				}
			}, ClickAnimation.Fade);

			UIHelper.addClickEventForView(this.itemLbl, (sender, e) =>
			{
				MessagingCenter.Send<App, Attachment> ((App)App.Current, Conts.MSG_IMAGE_SELECTED, this.attachment);
			}, ClickAnimation.Fade);
		} 
	}
}
