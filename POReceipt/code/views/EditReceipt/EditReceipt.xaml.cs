﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POReceipt
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EditReceipt : ContentPage
	{
		private POLine POline;
        private PO po;
        bool notesUpdated = false;
        string notes = string.Empty;

		public EditReceipt()
		{
			InitializeComponent();
			var save = new ToolbarItem
			{
				Text = "Save",
				Priority = 0,
				Command = new Command(OnSave)
			};
			ToolbarItems.Add(save);
		}

		public EditReceipt(POLine line, PO po) : this()
		{
			this.BindingContext = line;
            this.po = po;
			this.POline = line;
			this.attachmentView.SetDataSource(this.POline.Attachments);
			this.qtyText.SetKeyboardType(Keyboard.Numeric);

            Device.BeginInvokeOnMainThread(async () => {
                App.Native.setBusy(true);
                var noteText = await App.Native.GetOrderLineText(App.token, App.deviceName, po.PONumber, po.POType, po.POCompany, po.POSuffix, POline.LineNumber);

                if (!string.IsNullOrEmpty(noteText)) 
                {
                    notes = noteText;
                    noteView.TextValue = noteText;
                }

				// Get attachment list
                var result = await App.Native.GetOrderLineFileList(App.token, App.deviceName, po.PONumber, po.POType, po.POCompany, po.POSuffix, line.LineNumber);
				if (!string.IsNullOrEmpty(result))
				{
					var attachmentList = Model.ParseFileListJson(result);
                    line.Attachments = attachmentList;
					this.attachmentView.SetDataSource(line.Attachments);
                } else {
                    App.Native.showAlert("Failed to load attachments", "Failed");
                }

				App.Native.setBusy(false);
			});
		
            noteView.OnTextChanged += (sender, e) => {
                notesUpdated = !notes.Equals(noteView.TextValue);
            };
		}

        protected override void OnAppearing()
        {
            MessagingCenter.Subscribe<App, Attachment>((App)Application.Current, Conts.MSG_IMAGE_SELECTED, (s, e) =>
            {
                var attachment = (Attachment)e;
                if (attachment != null)
                {

                    Device.BeginInvokeOnMainThread(() => {
                        this.Navigation.PushAsync(new ImageViewerScreen(attachment, po, POline, false));
                    });
                }
            });

            base.OnAppearing();
        }

		protected override void OnDisappearing()
		{
            MessagingCenter.Unsubscribe<App, Attachment>((App)Application.Current, Conts.MSG_IMAGE_SELECTED);
			base.OnDisappearing();
		}

		private bool DoValidation() {
			double newQty = 0;
			if (Double.TryParse(this.qtyText.TextValue, out newQty))
			{
				if (newQty > this.POline.QuantityRemaining)
				{
					App.Native.showAlert("Please make sure the quality to receive is less than the remaining quantity", "Invalid Quantity Input");
					return false;
				}
				else if (newQty <= 0)
				{
					App.Native.showAlert("Please make sure the quality is greater than zero", "Invalid Quantity Input");
					return false;
				}
				else {
					if (this.POline != null) {
						this.POline.QuantityNew = newQty;
					}
					return true;
				}
			}
			else
			{
				App.Native.showAlert("Please make sure the quantity is correct in format", "Invalid Quantity Input");
				return false;
			}
		}

        public void OnSave()
		{
			if (this.POline != null)
			{
				if (string.IsNullOrEmpty(this.qtyText.TextValue))
				{ 
					App.Native.showAlert("Please make sure the quantity is correct in format", "Invalid Quantity Input");
                    return;
				}

				if (DoValidation()) {
					if (!string.IsNullOrEmpty(this.noteView.TextValue))
						this.POline.Note = this.noteView.TextValue.Trim();
					if (this.attachmentView.Attachments != null && this.attachmentView.Attachments.Count > 0)
						this.POline.Attachments = this.attachmentView.Attachments;
                    if (!string.IsNullOrEmpty(this.lineRemarkText.TextValue))
                        this.POline.LineRemark = lineRemarkText.TextValue;

                    Device.BeginInvokeOnMainThread(async () => {
                        if (notesUpdated)
                        { 
                            App.Native.setBusy(true);
                            var result = await App.Native.SetOrderLineText(App.token, App.deviceName, this.po.PONumber, this.po.POType, po.POCompany, po.POSuffix, POline.LineNumber, POline.Note);
                            App.Native.setBusy(false);
                        }
                        App.Native.setBusy(true);
                        foreach (var item in this.POline.Attachments) {
                            try {
                                if (item.Uploaded)
                                    continue;

                                var result = await App.Native.SetLineBinary(App.token, App.deviceName, this.po.PONumber, this.po.POType, po.POCompany, po.POSuffix, POline.LineNumber, item.FileName, item.Bytes);
                                if (string.IsNullOrEmpty(result))
                                {
                                    App.Native.showAlert("Failed, Failed to upload image");
                                    break;
                                }
                                else
                                {
                                    item.Uploaded = true;
                                }
                            } catch (Exception ex) {
                                App.Native.showAlert("Failed", "Failed to upload the attachment " + item.FileName + " - " + ex.Message);
                            }

                           
                        }

                        foreach (var item in this.attachmentView.AttachmentsToBeDeleted)
                        {
                            try {
                                var result = await App.Native.DeleteLineBinary(App.token, App.deviceName, this.po.PONumber, this.po.POType, po.POCompany, po.POSuffix, this.POline.LineNumber, item.Sequence);
                                if (string.IsNullOrEmpty(result))
                                {
                                    await DisplayAlert("Failed", "Failed to delete the attachment " + item.FileName, "OK");
                                    break;
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(item.LocalFileURL))
                                    {
                                        App.Native.deleteFile(item.LocalFileURL);
                                    }
                                }
                            } catch (Exception ex) {
                                App.Native.showAlert("Failed", "Failed to delete the attachment " + item.FileName);
                            }

                           
                        }


                        App.Native.setBusy(false);
						//MessagingCenter.Send<App>((App)Application.Current, Conts.MSG_UPDATE_POLINES);
						//MessagingCenter.Send<App>((App)Application.Current, Conts.MSG_UPDATE_POLIST);
						await this.Navigation.PopAsync();
                    });

					
					//App.Native.showAlert("The changes for this line have been saved.", "Saved");
                    return;
				}
			}
            return;
		}
	}
}
