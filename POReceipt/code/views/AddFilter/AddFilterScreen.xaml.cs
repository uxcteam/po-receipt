using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class AddFilterScreen : ContentPage
	{
		public EventHandler OnSavePreset;
        bool DateRangeOption = false;
        public bool searchingOverAllPOS = false;

		private Preset currentPreset;

        public AddFilterScreen(Preset preset, bool searchingOverAllPOS) : this() {
            _dateRangeOption = false;
            SwithDateOption(_dateRangeOption);
			this.BindingContext = preset;
			currentPreset = preset;
            this.searchingOverAllPOS = searchingOverAllPOS;
            poTypeText.SetEQUAL(preset.OrderTypeEQUAL);
            poTypeText.TextValue = string.IsNullOrEmpty(preset.OrderType) ? "" : preset.OrderType;
            poNumberText.TextValue = !string.IsNullOrEmpty(preset.PONumberSearchStr) ? preset.PONumberSearchStr : string.Empty;
            if (preset.RecentDays == -1) {
                SwithDateOption(true);
                poFromDayText.Date = preset.FromDate;
                poFromDayText.TextValue = preset.FromDate.ToString(PO.DATE_FORMAT);
                poToDayText.Date = preset.ToDate;
                poToDayText.TextValue = preset.ToDate.ToString(PO.DATE_FORMAT);
            } else {
                SwithDateOption(false);
                recentText.Text = RecentDaysNumberToOptionText(preset.RecentDays);
            }

            InitUI();
		}

		public AddFilterScreen()
		{
			InitializeComponent();
		}

        private void InitUI() {
            var save = new ToolbarItem
            {
                Text = "Add",
                Priority = 0,
                Command = new Command((obj) => {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        if (OnSavePreset != null)
                        {
                            OnSavePreset(getCurrentPreset(), EventArgs.Empty);
                        }
                        await this.Navigation.PopAsync();
                    });
                })
            };

            ToolbarItems.Add(save);
            this.poNumberText.SetKeyboardType(Keyboard.Telephone);
            this.poSupplierDescText.OnBookMarkClicked += OnBookMarkClicked;
            this.poUserIdText.OnBookMarkClicked += OnBookMarkClicked;
            this.poAccountCodeText.OnBookMarkClicked += OnBookMarkClicked;
            this.poProjectCodeText.OnBookMarkClicked += OnBookMarkClicked;
            if (this.poNumberText.TextValue == "0")
                this.poNumberText.TextValue = string.Empty;
            this.poFromDayText.OnUnfocused += OnFromDateUnfocused;
            this.poToDayText.OnUnfocused += OnToDateUnfocused;

            if (App.PropertySourceDictionary != null)
            {
                var dict = new Dictionary<string, List<string>>();
                if (searchingOverAllPOS)
                {
                    if (App.PropertySourceDictionaryAllPOs != null)
                    {
                        dict = App.PropertySourceDictionaryAllPOs;
                    }
                    else
                    {
                        dict = new Dictionary<string, List<string>>();
                    }
                }
                else
                {
                    dict = App.PropertySourceDictionary;
                }

                try
                {
                    if (dict.ContainsKey(Conts.PROJECTCODE_PROP_KEY))
                    {
                        this.poProjectCodeText.OptionSource = dict[Conts.PROJECTCODE_PROP_KEY];
                        this.poProjectCodeText.ToggleLookup(true);
                    }
                    else
                    {
                        this.poProjectCodeText.ToggleLookup(false);
                    }
                    if (dict.ContainsKey(Conts.USERID_PROP_KEY))
                    {
                        this.poUserIdText.OptionSource = dict[Conts.USERID_PROP_KEY];
                        this.poUserIdText.ToggleLookup(true);
                    }
                    else
                    {
                        this.poUserIdText.ToggleLookup(false);
                    }
                    if (dict.ContainsKey(Conts.ACCOUNTCODE_PROP_KEY))
                    {
                        this.poAccountCodeText.OptionSource = dict[Conts.ACCOUNTCODE_PROP_KEY];
                        this.poAccountCodeText.ToggleLookup(true);
                    }
                    else
                    {
                        this.poAccountCodeText.ToggleLookup(false);
                    }
                    if (dict.ContainsKey(Conts.SUPPLIER_PROP_KEY))
                    {
                        this.poSupplierDescText.OptionSource = dict[Conts.SUPPLIER_PROP_KEY];
                        this.poSupplierDescText.ToggleLookup(true);
                    }
                    else
                    {
                        this.poSupplierDescText.ToggleLookup(false);
                    }
                }
                catch (Exception ex)
                {
                    DisplayAlert("Exception Error", ex.Message, "OK");
                }
            }
            //if (this.currentPreset != null)
            //{
            //    if (string.IsNullOrEmpty(currentPreset.PODate))
            //    {
            //        this.poToDayText.TextValue = string.Empty;
            //        this.poFromDayText.TextValue = string.Empty;
            //    }
            //}
            UIHelper.addClickEventForView(rangeStack, (sender, e) => {
                _dateRangeOption = true;
                SwithDateOption(_dateRangeOption);
            }, ClickAnimation.Fade);
            UIHelper.addClickEventForView(recentStack, (sender, e) => {
                _dateRangeOption = false;
                SwithDateOption(_dateRangeOption);
            }, ClickAnimation.Fade);

            UIHelper.addClickEventForView(dateRecentPickerStack, async (sender, e) => {
                var action = await DisplayActionSheet("Select ", "Cancel", null, "Last 7 days", "Last 14 days", "Last 30 days", "Last 60 days", "All dates");
                if (!string.IsNullOrEmpty(action) && !action.Equals("Cancel"))
                {
                    recentText.Text = action;
                }
            }, ClickAnimation.Fade);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        bool _dateRangeOption = true;

        void SwithDateOption(bool dateRangeOption) {
            DateRangeOption = dateRangeOption;
            rangeStack.BackgroundColor = DateRangeOption ? Color.White : Color.FromHex("#ecf0f1");
            recentStack.BackgroundColor = DateRangeOption ? Color.FromHex("#ecf0f1") : Color.White;
            dateRecentPickerStack.IsVisible = !dateRangeOption;
            poFromDayText.IsVisible = dateRangeOption;
            poToDayText.IsVisible = dateRangeOption;

            if (dateRangeOption) {
            }
        }

		private void OnFromDateUnfocused(object sender, EventArgs e) {
			if (!string.IsNullOrEmpty(this.poFromDayText.TextValue)) {
				if (poFromDayText.Date > poToDayText.Date || string.IsNullOrEmpty(poToDayText.TextValue)) {
					poToDayText.Date = poFromDayText.Date;
					poToDayText.TextValue = poFromDayText.TextValue;
				}
			}
		}

		private void OnToDateUnfocused(object sender, EventArgs e) {
			if (!string.IsNullOrEmpty(this.poToDayText.TextValue)) {
				if (string.IsNullOrEmpty(poFromDayText.TextValue)) {
					poFromDayText.TextValue = poToDayText.TextValue;
					poFromDayText.Date = poToDayText.Date;
				} else if (poFromDayText.Date > poToDayText.Date) {
					poToDayText.Date = poFromDayText.Date;
					poToDayText.TextValue = poFromDayText.TextValue;
					App.Native.showAlert("Please make sure the Date To to be after or equals to Date From", "Invalid Input");
				}
			}
		}

		private void OnBookMarkClicked(object sender, EventArgs e) {
			var textField = (TextLinePickerView)sender;
			if (textField != null) {
				Device.BeginInvokeOnMainThread(async () =>
				{
					if (!textField.IsEnabled)
						return;
					textField.IsEnabled = false;
					await Navigation.PushAsync(new PicklistScreen(textField));
					textField.IsEnabled = true;
				});
			}
		}

		private string hanldePONumberString(string POnumber) {
			if (string.IsNullOrEmpty(POnumber))
				return string.Empty;
			Regex not_num_period = new Regex("[^0-9*]");
			return not_num_period.Replace(POnumber, "");
		}

		private Preset getCurrentPreset()
		{
			if (currentPreset == null)
				currentPreset = new Preset() { Name = "Applying filter(s)" };
			else
			{
				currentPreset.Name = "Applying filter(s)";
				currentPreset.IsSaved = false;
			}

			if (!string.IsNullOrEmpty(this.poNumberText.TextValue))
			{
				var numberStr = hanldePONumberString(this.poNumberText.TextValue);
				currentPreset.PONumberSearchStr = numberStr;
                this.poNumberText.TextValue = numberStr;
            } else {
                currentPreset.PONumberSearchStr = string.Empty;
            }

			if (!string.IsNullOrEmpty(this.poSupplierDescText.TextValue))
			{
				currentPreset.SupplierName = this.poSupplierDescText.TextValue.Trim();
            } else {
                currentPreset.SupplierName = string.Empty;
            }

			if (!string.IsNullOrEmpty(this.poProjectCodeText.TextValue))
				currentPreset.ProjectCode = this.poProjectCodeText.TextValue.Trim();
            else {
                currentPreset.ProjectCode = string.Empty;
            }
			if (!string.IsNullOrEmpty(this.poUserIdText.TextValue))
				currentPreset.UserID = this.poUserIdText.TextValue.Trim();
            else {
                currentPreset.UserID = string.Empty;
            }

			if (!string.IsNullOrEmpty(this.poAccountCodeText.TextValue))
				currentPreset.AccountCode = this.poAccountCodeText.TextValue.Trim();
            else {
                currentPreset.AccountCode = string.Empty;
            }

	
            if (!string.IsNullOrEmpty(this.poTypeText.TextValue))
            {
                currentPreset.OrderType = this.poTypeText.TextValue;
                currentPreset.OrderTypeEQUAL = !this.poTypeText.NotType;
            } else {
                currentPreset.OrderType = string.Empty;
            }

            if (_dateRangeOption) {
                if (!string.IsNullOrEmpty(this.poFromDayText.TextValue))
                {
                    currentPreset.FromDate = this.poFromDayText.Date;
                }

                if (!string.IsNullOrEmpty(this.poToDayText.TextValue))
                {
                    currentPreset.ToDate = poToDayText.Date;
                }

                if (currentPreset.FromDate < currentPreset.ToDate)
                {
                    currentPreset.PODate = string.Format("{0}-{1}", currentPreset.FromDate.ToString(PO.DATE_FORMAT), currentPreset.ToDate.ToString(PO.DATE_FORMAT));
                }
                else if (!string.IsNullOrEmpty(this.poFromDayText.TextValue))
                {
                    currentPreset.PODate = currentPreset.FromDate.ToString(PO.DATE_FORMAT);
                }
                currentPreset.RecentDays = -1;
            } else {
                if (!string.IsNullOrEmpty(recentText.Text))
                {
                    switch (recentText.Text)
                    {
                        case "Last 7 days":
                            currentPreset.RecentDays = 7;
                            break;
                        case "Last 14 days":
                            currentPreset.RecentDays = 14;
                            break;
                        case "Last 30 days":
                            currentPreset.RecentDays = 30;
                            break;
                        case "Last 60 days":
                            currentPreset.RecentDays = 60;
                            break;
                        case "All dates":
                            currentPreset.RecentDays = 0;
                            break;
                    }
                }
            }

			return currentPreset;
		}

        private string RecentDaysNumberToOptionText (int recentDays) {
            
            if (recentDays >= 0)
            {
                var text = string.Empty;
                switch (recentDays)
                {
                    case 7:
                        text = "Last 7 days";
                        break;
                    case 14:
                        text = "Last 14 days";
                        break;
                    case 30:
                        text = "Last 30 days";
                        break;
                    case 60:
                        text = "Last 60 days";
                        break;
                    case 0:
                        text = "All dates";
                        break;
                }

                return text;
            }

            return string.Empty;
        }
	}
}
