﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class PicklistScreen : ContentPage
	{
		public TextLinePickerView textField;
		public List<string> options;

		public PicklistScreen()
		{
			InitializeComponent();
			this.listView.ItemTapped += OnItemTapped;
		}

		public PicklistScreen(TextLinePickerView textField) : this() {
			this.textField = textField;
			this.options = textField.OptionSource;
			this.listView.ItemsSource = options;
			this.Title = textField.Title;
			this.searchBar.TextChanged += SearchBar_OnTextChanged;
			UIHelper.addClickEventForView(this.noneOptionStack, (sender, e) => { 
				textField.TextValue = string.Empty;
				Device.BeginInvokeOnMainThread(() =>
				{
					this.Navigation.PopAsync();
				});
			}, ClickAnimation.Fade);
		}

		private void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
		{
			this.listView.BeginRefresh();
			if (!string.IsNullOrWhiteSpace(e.NewTextValue))
			{
				var tempRecords = options.Where(c => c.ToLower().Contains(e.NewTextValue.Trim().ToLower()));
				var results = tempRecords;
				this.listView.ItemsSource = tempRecords;
			}
			else
			{
				listView.ItemsSource = options;
				//this.notResultsLabel.IsVisible = false;
			}

			listView.EndRefresh();
		}

		public void OnItemTapped(object sender, EventArgs e) {
			if (textField != null) {
				textField.TextValue = this.listView.SelectedItem.ToString();
			}
			Device.BeginInvokeOnMainThread(() => {
				this.Navigation.PopAsync();
			});
		}
	}
}
