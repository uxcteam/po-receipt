﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace POReceipt
{
	public partial class AddingPresetPopup : ContentPage
	{
		public EventHandler OnSave;

		public AddingPresetPopup()
		{
			InitializeComponent();
           	this.buttonSave.Clicked += (sender, e) => {
				if	(string.IsNullOrEmpty(this.nameText.TextValue))
				{
					this.DisplayAlert("Empty name", "Please enter the name for this preset","OK");
					return;
				}				
				if (App.CheckPresetNameExists(this.nameText.TextValue)) {

				   Device.BeginInvokeOnMainThread(async () =>
				   {
					   var resultTask = await this.DisplayAlert("Preset exist", "This name has been taken, do you want to overwrite this preset?", "Yes", "No");
					   if (resultTask)
					   {
						   ClosePopup();
					   }
				   });
					return;
				}

				ClosePopup();
			};

			this.buttonCancel.Clicked += (sender, e) =>
			{
				Device.BeginInvokeOnMainThread(async () =>
				{
					await this.Navigation.PopModalAsync();
				});
			};

		}

		private void ClosePopup() {
			if (OnSave != null)
			{
				var newPreset = new Preset()
				{
					Name = this.nameText.TextValue.Trim(),
					IsDefault = this.isDefaultSwitch.IsToggled
				};
				OnSave(newPreset, EventArgs.Empty);
			}

			Device.BeginInvokeOnMainThread(async() => {
				await this.Navigation.PopModalAsync();
			});
		}
	}
}
