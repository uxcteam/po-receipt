﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace POReceipt
{
	public partial class SettingsScreen : ContentPage
	{
		public SettingsScreen()
		{
			InitializeComponent();
			this.listView.ItemsSource = new List<string>() { "remove" };
			this.listView.ItemTapped += async (sender, e) => {
				this.listView.SelectedItem = null;
				if (!listView.IsEnabled)
					return;
				this.listView.IsEnabled = false;
				if (await App.Native.showConfirm("Do you wish to reset all the dummy data?", "Reset Dummy Data"))
				{
					UserDataManager.ResetDataForUser(App.UserData);
					App.Native.showAlert("The dummy data has been reset", "Successfully");
				}
                this.listView.IsEnabled = true;
				//var result = await App.Native.showConfirm("Do you really want to reset all the dummy data?", "Reset Dummy Data");

			};
		}
	}
}
