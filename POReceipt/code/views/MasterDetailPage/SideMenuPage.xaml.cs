using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using FormsPlugin.Iconize;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POReceipt
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SideMenuPage : ContentPage
	{
		public ListView listView;
		MenuItem SignInOutItem;
		public EventHandler<ItemTappedEventArgs> OnItemTapped;

		public SideMenuPage()
		{
			InitializeComponent();

			var menuBackButton = new ContentView
			{
				HorizontalOptions = LayoutOptions.Start,
			};

			initListView();

			var layout = new StackLayout
			{
				Spacing = 0,
				VerticalOptions = LayoutOptions.FillAndExpand
			};


			////layout.Children.Add(menuLabel);
			//layout.Children.Add(menuBackButton);
			layout.Children.Add(listView);
			Content = layout;
			initMenu();
			MessagingCenter.Subscribe<object>(this, Conts.MSG_UPDATE_PRESET_LIST_MENU, (item) =>
			{
				Device.BeginInvokeOnMainThread(() =>
				{
					initMenu();
				});       
			});
		}
		void initListView() {
			listView = new ListView();
			listView.VerticalOptions = LayoutOptions.FillAndExpand;
			listView.BackgroundColor = Color.FromHex("#242A31");
			listView.Footer = null;
			listView.SeparatorVisibility = SeparatorVisibility.None;
			listView.SeparatorColor = Color.Transparent;
			listView.HasUnevenRows = true;
			var cell = new DataTemplate(typeof(MenuCell));


			this.listView.IsGroupingEnabled = false;
			this.listView.ItemTapped += this.OnItemTapped;
			this.listView.ItemTapped += (sender, e) =>
			{
				this.listView.SelectedItem = null;
			};
			//this.listView.HasUnevenRows = true;
			this.listView.RowHeight = 45;
			listView.ItemTemplate = cell;
		}
		void initMenu()
		{
			var menuItems = new MenuItemGroup("");

			menuItems.Add(new MenuItem { Title = "All POs", TargetType = typeof(AllPOsScreen), IconSource = "list_icon", Enable = true 
					,HasBottomLine = App.presetList.Count == 0});
			for (int i = 0; i < App.presetList.Count; i++) { 
				menuItems.Add(new MenuItem { Title = App.presetList[i].Name, 
					Enable = true, 
					HasBottomLine = (i == App.presetList.Count - 1),
					TargetType = typeof(Preset),
					Description = App.presetList[i].IsDefault ? "(Default)" : ""
				});
			}




            menuItems.Add(new MenuItem { Title = "About", IconSource = "about_icon", TargetType = typeof(AboutPage), Enable = true });
			SignInOutItem = new MenuItem { Title = "Sign Out", IconSource = "exit_icon", TargetType = typeof(SignInPage), Enable = true, HasBottomLine=true };
			menuItems.Add(SignInOutItem);

			listView.BeginRefresh();
			listView.ItemsSource = menuItems;
			listView.EndRefresh();
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			initMenu();
		}
	}
}

