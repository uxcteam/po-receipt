using System;
using Xamarin.Forms;

namespace POReceipt
{
	public class MenuHeaderCell : ViewCell
	{
		public MenuHeaderCell()
		{
			Height = 1.0;

			var title = new Label
			{
				//FontSize = Device.GetNamedSize(NamedSize.Small, this),
				FontSize = 14,
		        FontAttributes = FontAttributes.Bold,
		        //TextColor = Color.FromRgb(127, 140, 141),
				TextColor = ThemeColor.DarkBlue01,
		        VerticalOptions = LayoutOptions.Center
		      };

		      title.SetBinding(Label.TextProperty, "Name");

			var box = new BoxView
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = 1.0,
				BackgroundColor = Color.FromHex("#424244")
		     };
			Frame objFrame_Outer = new Frame
			{
				Padding = new Thickness(0, 0, 0, 0),
				Content = box
			};

			View = objFrame_Outer;  

		}
	}
}

