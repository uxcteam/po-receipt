using System;

using Xamarin.Forms;

namespace POReceipt
{
	public class RootPage : MasterDetailPage
	{
		SideMenuPage menuPage;

		public RootPage() {
			menuPage = new SideMenuPage() {};
			Master = menuPage;
			menuPage.listView.ItemSelected += OnItemSelected;
            this.MasterBehavior = MasterBehavior.Popover;
			this.IsGestureEnabled = false;
			MessagingCenter.Subscribe<object>(this, Conts.MSG_CLOSE_SIDE_MENU, (obj) => {
				Device.BeginInvokeOnMainThread(() => {
					IsPresented = false;
				});
			});

            // TOken refreshing
            App.Native.WireUpBackgroundServiceTask();
		}


		public RootPage(Type pageType) : this()
		{
			Detail = new NavigationPage((Page)Activator.CreateInstance(pageType)) { BarTextColor = Color.White };
		}

		public RootPage(Page page) : this ()
		{
			Detail = new NavigationPage(page) { BarTextColor = Color.White };
		}

		void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as MenuItem;
			if (item != null)
			{
				if (item.TargetType != null)
				{
                    if (item.TargetType == typeof(AllPOsScreen))
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            var action = await DisplayActionSheet("Loading all the POs will take long time, do you want to apply a preset ?", "Cancel", null, "Yes, create a preset", "Load anyway");
                            if (!string.IsNullOrEmpty(action))
                            {
                                switch (action)
                                {
                                    case "Cancel":

                                        break;
                                    case "Yes, create a preset":
                                        var page = new AllPOsScreen(new Preset(), true);
                                        Detail = new NavigationPage(page)
                                        {
                                            BarTextColor = Color.White,
                                            Title = ""
                                        };
                                        break;
                                    case "Load anyway":
                                        var pageAllPOs = new AllPOsScreen(new Preset() { Name = "All POs" }, false);
                                        Detail = new NavigationPage(pageAllPOs)
                                        {
                                            BarTextColor = Color.White,
                                            Title = "All POs"
                                        };
                                        break;
                                }
                            }
                        });
                    }
					else if (item.TargetType == typeof(Preset))
					{
						var preset = App.GetPresetFromList(item.Title);
						var page = new AllPOsScreen(preset);
						Detail = new NavigationPage(page)
						{
							BarTextColor = Color.White,
							Title = item.Title
						};
					}
					else if (item.TargetType == typeof(SignInPage)) {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            App.Native.setBusy(true);
                            if (await App.Native.Logout(App.token)) {
                                App.Native.setBusy(false);
                                App.Logout();
                                await this.Navigation.PushModalAsync(new SignInPage());
                            } else {
                                App.Native.setBusy(false);
                                await DisplayAlert("Alert", "Failed to log out, please try again", "OK");
                            }
                        });
					} else {
						var page = (Page)Activator.CreateInstance(item.TargetType);
						Detail = new NavigationPage(page)
						{
							BarTextColor = Color.White
						};
					}
				}
				else
					DisplayAlert("Alert", "This feature is underdevelopment", "OK");
				menuPage.listView.SelectedItem = null;
				IsPresented = false;
			}
		}
	}
}


