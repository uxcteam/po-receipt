using System;
using System.Collections.ObjectModel;

namespace POReceipt
{
	public class MenuItem
	{
		public String Title { set; get; }
		public String IconSource { set; get; }
		public Type TargetType { set; get; }
		public bool Enable { set; get; }
		public bool Hidden { set; get; }
		public bool HasBottomLine { set; get; }
		public bool IsSignInOutItem { set; get; }
		public String Description { set; get; }

		public MenuItem()
		{
		}
	}

	public class MenuItemGroup : ObservableCollection<MenuItem>
	{
		public String Name { get; private set; }
		public bool showTitle { set; get; }

		public MenuItemGroup(String Name)
		{
			this.Name = Name;
		}
	}
}

