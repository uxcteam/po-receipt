using System;
using Xamarin.Forms;

namespace POReceipt
{
	public class CategoryHeaderCell : ViewCell
	{
		public CategoryHeaderCell()
		{
			Height = 25;

		      var title = new Label
		      {
		        FontSize = Device.GetNamedSize(NamedSize.Small, this),
		        FontAttributes = FontAttributes.Bold,
				TextColor = Color.FromHex("#1F1F1F"),
		        VerticalOptions = LayoutOptions.Center
		      };

		      title.SetBinding(Label.TextProperty, "Name");

		      View = new StackLayout
		      {
		        HorizontalOptions = LayoutOptions.FillAndExpand,
		        HeightRequest = 30,
				BackgroundColor = Color.FromHex("#CCCACA"),
		        Padding = 5,
		        Orientation = StackOrientation.Horizontal,
		        Children = { title }
		      };
		}
	}
}

