using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POReceipt
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FilterStackView : ContentView
    {
        public Preset Preset { set; get; }
        public int NumberOfItems { 
            get { return this.filterBoxesStack.Children.Count; }
        }
        public EventHandler OnSaveButtonClicked;
        public EventHandler OnSaveCurrentPresetButtonClicked;
        public EventHandler OnDeleteButtonClicked;
        public EventHandler OnSetDefaultButtonClicked;
        public EventHandler OnPresetUpdated;
        public bool HasChanged;
        public double TotalItemsWidth = 0;
        public double GetHeight() {
            return 0;
        }
        private int MAX_FILTER_ITEM_WIDTH = 300;
        public FilterStackView()
        {
            InitializeComponent();
            this.buttonSave.Clicked += (sender, e) => {
                if (this.OnSaveButtonClicked != null) {
                    OnSaveButtonClicked(this, EventArgs.Empty);
                }
            };

            this.buttonSaveCurrentPreset.Clicked += (sender, e) =>
            {
                if (this.OnSaveCurrentPresetButtonClicked != null)
                {
                    OnSaveCurrentPresetButtonClicked(this, EventArgs.Empty);
                }
            };

            this.buttonDelete.Clicked += (sender, e) =>
            {
                if (this.OnDeleteButtonClicked != null)
                {
                    OnDeleteButtonClicked(this, EventArgs.Empty);
                }
            };

            this.buttonSetDefault.Clicked += (sender, e) =>
            {
                if (this.OnSetDefaultButtonClicked != null)
                {
                    OnSetDefaultButtonClicked(this, EventArgs.Empty);
                }
            };
        }


        public void Clear() {
            this.filterBoxesStack.Children.Clear();
        }

        public void SetPresetWithChanges(Preset preset) {
            this.SetPreset(preset);
               this.HasChanged = true;
        }

        public void SetPreset(Preset preset) {
            if (preset == null)
                return;

            this.Preset = preset;

            Clear();
            TotalItemsWidth = 0;
            if (!string.IsNullOrEmpty(preset.PONumberSearchStr))
                AddFilterItem("PO Number", preset.PONumberSearchStr);
            if (!string.IsNullOrEmpty(preset.UserID))
                AddFilterItem("User", preset.UserID);
            if (!string.IsNullOrEmpty(preset.ProjectCode))
                AddFilterItem("BU/Project", preset.ProjectCode);
            if (!string.IsNullOrEmpty(preset.SupplierName))
                AddFilterItem("Supplier", preset.SupplierName);
            if (!string.IsNullOrEmpty(preset.AccountCode))
                AddFilterItem("Account", preset.AccountCode);
            if (!string.IsNullOrEmpty(preset.PODate) && preset.RecentDays == -1)
                AddFilterItem("Date", preset.PODate);
            if (preset.RecentDays > 0)
                AddFilterItem("Dates", "last " + preset.RecentDays.ToString() + " days");
            if (!string.IsNullOrEmpty(preset.OrderType))
            {
                var operatorStr = preset.OrderTypeEQUAL ? "" : "NOT ";
                AddFilterItem("Type", operatorStr + preset.OrderType);
            }
            TogglePresetButtons(preset.IsSaved);
            HasChanged = false;
            this.buttonSaveCurrentPreset.IsEnabled = HasChanged;
            this.buttonSetDefault.IsEnabled = !preset.IsDefault;
        }

        private void OnItemRemoved(object sender, EventArgs e) {
            var filterBoxItem = (FilterViewBox)sender;
            this.Preset.RemoveFilterByText(filterBoxItem.TextValue);
            this.filterBoxesStack.Children.Remove(filterBoxItem);
            if (OnPresetUpdated != null) {
                OnPresetUpdated(this.Preset, EventArgs.Empty);
            }
            HasChanged = true;
            this.buttonSaveCurrentPreset.IsEnabled = true;
        }

        private void AddFilterItem(string title, string filterValue)
        {
            var str = string.Format("{0}    {1}", title, filterValue);

            var textWidth = (int)DependencyService.Get<ITextWidthCalculator>().CalculateWidth(str);
            var widthNeeded = 30;
            widthNeeded += (textWidth > MAX_FILTER_ITEM_WIDTH) ? MAX_FILTER_ITEM_WIDTH : textWidth;

            TotalItemsWidth += widthNeeded;

            this.filterBoxesStack.Children.Add(new FilterViewBox()
            {
                Title = title,
                TextValue = filterValue,
                OnRemoved = OnItemRemoved,
                WidthRequest = widthNeeded
            });
        }

        private void TogglePresetButtons(bool savedPreset)
        {
            this.buttonSave.IsVisible = !savedPreset;
            this.buttonSaveCurrentPreset.IsVisible = savedPreset;
            this.buttonDelete.IsVisible = savedPreset;
            this.buttonSetDefault.IsVisible = savedPreset;
        }
    }
}
