﻿using System;
using System.Collections.Generic;
using POReceipt;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POReceipt
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FilterViewBox : ContentView
	{
		public static readonly BindableProperty TitleProperty = BindableProperty.Create(propertyName: "Title",
		returnType: typeof(string),
		declaringType: typeof(FilterViewBox), defaultValue: "(title)");

		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set
			{
				SetValue(TitleProperty, value);
			}
		}

		public static readonly BindableProperty TextValueProperty = BindableProperty.Create(propertyName: "TextValue",
		returnType: typeof(string),
		declaringType: typeof(FilterViewBox),
		defaultValue: default(string));

		public string TextValue
		{
			get { return (string)GetValue(TextValueProperty); }
			set
			{
                SetValue(TextValueProperty, value);
			}
		}

		public static readonly BindableProperty OnRemovedProperty = BindableProperty.Create(propertyName: "OnRemoved",
		returnType: typeof(EventHandler),
		declaringType: typeof(FilterViewBox),
		defaultValue: default(EventHandler));

		public EventHandler OnRemoved
		{
			get { return (EventHandler)GetValue(OnRemovedProperty); }
			set
			{
                SetValue(OnRemovedProperty, value);
			}
		}

		public bool IsRemoved { set; get; }

		public FilterViewBox()
		{
			InitializeComponent();
            this.IsVisible = !string.IsNullOrEmpty(this.TextValue);
			IsRemoved = false;
			UIHelper.addClickEventForView(this.removeIcon, (sender, e) => {
				this.IsVisible = false;
				IsRemoved = true;
				if (this.OnRemoved != null) {
					this.OnRemoved(this, EventArgs.Empty);
				}
 			}, ClickAnimation.Fade);

		}

		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			if (propertyName.Equals("TextValue"))
			{
				Device.BeginInvokeOnMainThread(() =>
				{
					this.textLbl.Text = this.TextValue;
					this.IsVisible = !string.IsNullOrEmpty(this.TextValue);
				});
			}
			else if (propertyName.Equals("Title")) { 
				Device.BeginInvokeOnMainThread(() =>
				{
					this.titleLbl.Text = this.Title + " ";
				});
			}
		}
	}
}
