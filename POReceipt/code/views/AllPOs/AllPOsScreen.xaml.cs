﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace POReceipt
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AllPOsScreen : ContentPage
	{
		private Preset currentPreset;
		private AllPOsVM _vm;
        public bool needRefreshingData = false;
        bool _loaded = false;
		public AllPOsScreen()
		{
			InitializeComponent();
		}

		public AllPOsScreen(Preset preset) : this()
		{
            Init();
            if (preset != null && App.Settings.Logon)
			{
                if (preset.IsEmpty)
                    AddFilterButtonClicked(this);
                else 
				    UpdateCurrentPreset(preset, true);
			}

			if (this._vm != null)
				_vm.Preset = preset;
			currentPreset = preset;
			this.Title = preset.Name;
		}

        public AllPOsScreen(Preset preset, bool firstLoad) : this()
        {
            Init();
            if (this._vm != null)
                _vm.Preset = preset;
            currentPreset = preset;
            this.Title = preset.Name;

            if (firstLoad && App.Settings.AccountRemember) {
                AddNewPreset();
            } else {
                UpdateCurrentPreset(currentPreset, true);
            }
        }

        void Init() {
            if (App.Settings.Logon && !App.Settings.AccountRemember || !App.Settings.Logon)
            {
                this.Navigation.PushModalAsync(new SignInPage());
            }
            else
            {
                var userData = UserDataManager.LoadUserDataFromStorage(App.Settings.LogonUsername);
                App.deviceName = Settings.GetDeviceId();
                App.LoadUserData(userData);
                var serverConfigsStored = Settings.LoadServerConfigs();
                if (serverConfigsStored != null)
                {
                    App.Native.UpdateConfigs(serverConfigsStored.JDE_HOSTNAME,
                                             serverConfigsStored.JDE_SERVICE_ROOT,
                                             serverConfigsStored.JDE_ENVIRONMENT,
                                             serverConfigsStored.JDE_VERSION,
                                             App.deviceName);
                }

                _vm = new AllPOsVM();
                this.BindingContext = _vm;
            }

            InitUI();
            SubscribeMessagingEvents();
        }

		void SubscribeMessagingEvents() {
			MessagingCenter.Subscribe<object>(this, Conts.MSG_APPLY_PRESET, (presetName) =>
			{
				var presetNameStr = (string)presetName;
				if (string.IsNullOrEmpty(presetNameStr))
				{
					var preset = App.GetPresetFromList(presetNameStr);
					if (preset != null)
					{
						UpdateCurrentPreset(preset, true);
					}
				}
			});

			MessagingCenter.Subscribe<App>((App)Application.Current, Conts.MSG_UPDATE_POLIST, async (sender) =>
			{
				RefreshList();
				if (_vm != null)
					_vm.SavePOListToLocalStorage();

				needRefreshingData = true;
				if (Device.RuntimePlatform == Device.Android)
				{
					await RefreshDataIfNeeded();
				}
			});


            MessagingCenter.Subscribe<object>(this, Conts.MSG_LOGOUT, (sender) =>
            {
                UpdateCurrentPreset(new Preset(), false);
                Device.BeginInvokeOnMainThread(()=>{
                    this.Title = string.Empty;
                    listView.ItemsSource = null;
                    _vm = null;
                });
            });

            MessagingCenter.Subscribe<App, User>((App)Application.Current, Conts.MSG_LOGIN_SUCCESS, async (arg1, obj) => {
				var user = (User)obj;
				if (user != null)
				{
					var savedUser = UserDataManager.LoadUserDataFromStorage(user.Username);
					_vm = new AllPOsVM();
                    needRefreshingData = false;
					this.BindingContext = _vm;

                    var defaultPreset = App.LoadDefaultPresetIfExist();
                    if (defaultPreset != null) {
                        needRefreshingData = true;
                        _vm.Preset = defaultPreset;
                        currentPreset = defaultPreset;
                        await RefreshDataIfNeeded();
                    } else {
                        AddNewPreset();    
                    }
				}

            });
		}

        private async Task<bool> RefreshDataIfNeeded() {
			if (needRefreshingData)
			{
				needRefreshingData = false;

				if (_vm != null)
				{
                    UpdateCurrentPreset(currentPreset, true);
				}
			}
            return false;
        }

		private void RefreshList() {
			UpdateCurrentPreset(this.currentPreset, true);
		}

		// Open the Pop-up to input a name and save current filters as a new Preset
		private async void OnOpenPresetNamePopup(object sender, EventArgs e)
		{
			var popup = new AddingPresetPopup();
			popup.OnSave += (newPreset, ev) =>
			{
				var newPresetInfo = (Preset)newPreset;
				if (newPresetInfo != null)
				{
					this.currentPreset.Name = newPresetInfo.Name;
					this.currentPreset.IsDefault = newPresetInfo.IsDefault;
					App.SaveNewPreset(currentPreset);
					if (newPresetInfo.IsDefault)
						App.SetPresetDefault(currentPreset);
                	this.filterStackView.SetPreset(currentPreset);
					FilterPOItemsByPreset(this.currentPreset);
				}
			};
			await Navigation.PushModalAsync(new NavigationPage(popup) { BarTextColor = Color.White });
		}

		private void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
		{
			var vm = (AllPOsVM)this.BindingContext;
			//this.listView.BeginRefresh();
			vm.DoSearching(e.NewTextValue);
			listView.ItemsSource = vm.DisplayingPOs;
			this.notResultsLabel.IsVisible = vm.DisplayingPOs.Count > 0 ? false : true;
			//listView.EndRefresh();
		}

		private void FilterPOItemsByPreset(Preset preset)
		{
			var vm = (AllPOsVM)this.BindingContext;
			if (vm == null)
				return;
			if (preset != null)
			{
                Device.BeginInvokeOnMainThread(async ()=> {
                    App.Native.setBusy(true);
                    if (preset.IsEmpty)
                    {
                        //await vm.SyncPOData();
                        App.Native.setBusy(false);
                        this.Title = "All POs";
                        AddFilterButtonClicked(this);
                    }
                    else
                    {
                        await vm.Searching(preset);
                        App.Native.setBusy(false);
                        this.Title = preset.Name;
                    }
                    listView.ItemsSource = vm.DisplayingPOs;
                    this.notResultsLabel.IsVisible = vm.DisplayingPOs.Count > 0 ? false : true;
                    searchBar.IsEnabled = vm.DisplayingPOs.Count > 0 ? true : false;
                });
			}
			else
			{
				listView.ItemsSource = vm.GetPOsToBeReceived();
				this.notResultsLabel.IsVisible = false;
                searchBar.IsEnabled = true;
				this.Title = "All POs";
			}
		}

        private void UpdateCurrentPreset(Preset newPreset, bool loadData)
		{
			if (newPreset == null)
				return;
			this.currentPreset = newPreset;

			this.filterStackView.SetPreset(newPreset);
			UpdateLayout();
            if (loadData)
			    FilterPOItemsByPreset(this.currentPreset);
		}

		private void UpdateLayout()
		{
			Device.BeginInvokeOnMainThread(() =>
			{
				var rowHeight = 80;
				int nFilters = this.filterStackView.NumberOfItems;
				if (nFilters == 0)
				{
					UIHelper.ExpandGridRow(this.mainGrid, this.filterViewRow, 0, (sender, e) => { });
					this.Title = "All POs";
					return;
				}
				var nRow = this.filterStackView.TotalItemsWidth / App.ScreenWidth;
				nRow += this.filterStackView.TotalItemsWidth % App.ScreenWidth > 0 ? 1 : 0;
				rowHeight += (int)nRow * 50;
				UIHelper.ExpandGridRow(this.mainGrid, this.filterViewRow, rowHeight, (sender, e) => { });
			});
		}

		private void InitUI()
		{
			this.listView.Footer = null;
			listView.ItemTapped += async (object sender, ItemTappedEventArgs e) =>
			{
				if (e.Item == null) return;
				if (!this.listView.IsEnabled) return;
				this.listView.IsEnabled = false;
				var selectedPO = (PO)e.Item;
				((ListView)sender).SelectedItem = null; // de-select the row
               
				await this.Navigation.PushAsync(new PODetailsScreen(selectedPO));
                this.listView.IsEnabled = true;
			};
            listView.RefreshCommand = new Command(async (obj) => {
				try
				{
					if (_vm != null)
					{
						//await _vm.SyncPOData();
						//_vm.SavePOListToLocalStorage();
						FilterPOItemsByPreset(currentPreset);
					}
				}
				catch (Exception ex) { 
                    listView.EndRefresh();
                    await DisplayAlert("Error", ex.Message, "OK");
                }
				listView.EndRefresh();
            });

			ToolbarItems.Add(new ToolbarItem
			{
				Text = "Add Filter",
				Order = ToolbarItemOrder.Primary,
				Command = new Command(AddFilterButtonClicked)
			});

			this.searchBar.TextChanged += SearchBar_OnTextChanged;
			this.filterStackView.OnSaveButtonClicked += OnOpenPresetNamePopup;
            this.filterStackView.OnPresetUpdated += OnFilterItemRemoved;
			this.filterStackView.OnDeleteButtonClicked += OnDeleteButtonClicked;
            this.filterStackView.OnSaveCurrentPresetButtonClicked += OnOpenPresetNamePopup;
			this.filterStackView.OnSetDefaultButtonClicked += OnSetDefaultButtonClicked;
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (currentPreset != null && currentPreset.IsEmpty && _loaded){
                AddNewPreset();
            } else if (!_loaded) {
                _loaded = true;
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            this.searchBar.Text = string.Empty;
        }

        public void AddNewPreset() {
            Device.BeginInvokeOnMainThread(() =>
            {
                MessagingCenter.Send<object>(this, Conts.MSG_CLOSE_SIDE_MENU);
                this.Navigation.PushAsync(new AddFilterScreen(Preset.GetDefaultPreset(), true)
                {
                    OnSavePreset = (s, ev) =>
                    {
                        var preset = (Preset)s;
                        UpdateCurrentPreset(preset, true);
                    }
                });
            });
        }

		public void AddFilterButtonClicked(object sender) {
			Device.BeginInvokeOnMainThread(() =>
			{
				MessagingCenter.Send<object>(this, Conts.MSG_CLOSE_SIDE_MENU);
                this.Navigation.PushAsync(new AddFilterScreen(currentPreset.GetClone(), false)
				{
					OnSavePreset = (s, ev) =>
					{
						var preset = (Preset)s;
						UpdateCurrentPreset(preset, true);
					}
				});
			});
		}

		public void OnFilterItemRemoved(object updatedPreset, EventArgs e)
		{
			var preset = (Preset)updatedPreset;
			UpdateCurrentPreset(currentPreset, true);
		}

		public void OnDeleteButtonClicked(object sender, EventArgs e)
		{
			Device.BeginInvokeOnMainThread(async () =>
			{
				if (await this.DisplayAlert("Alert!", "Do you want to delete this preset ?", "Yes", "No"))
				{
					App.DeletePreset(currentPreset.Name);
                    UpdateCurrentPreset(new Preset(), false);
                    var action = await DisplayActionSheet("Loading all the POs will take long time, do you want to apply a preset ?", "Cancel", null, "Yes, create a preset", "Load anyway");
                    if (!string.IsNullOrEmpty(action))
                    {
                        switch (action)
                        {
                            case "Cancel":

                                break;
                            case "Yes, create a preset":
                                AddNewPreset();
                                break;
                            case "Load anyway":
                                UpdateCurrentPreset(new Preset() { Name = "All POs" }, true);  // Empty preset
                                break;
                        }
                    }
				}
			});
		}

		public void OnSaveCurrentPresetButtonClicked(object sender, EventArgs e)
		{
			Device.BeginInvokeOnMainThread(async() =>
			{
				if (await this.DisplayAlert("Alert!", "Do you want to save this as a new preset?", "Yes", "No"))
				{
					App.SavePresetChanges(currentPreset);
					UpdateCurrentPreset(currentPreset, true);
				}
			});
		}

		public void OnSetDefaultButtonClicked(object sender, EventArgs e)
		{
			Device.BeginInvokeOnMainThread(async() =>
			{
				if (await this.DisplayAlert("Alert!", "Do you want to set this preset as Default?", "Yes", "No"))
				{
					App.SetPresetDefault(currentPreset);
					UpdateCurrentPreset(currentPreset, true);
				}
			});
		}
	}
}
