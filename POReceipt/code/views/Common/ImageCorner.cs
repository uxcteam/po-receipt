using System;
using Xamarin.Forms;

namespace POReceipt
{
	public class ImageCorner: Image
	{
		public ImageCorner()
		{
		}

		/// <summary>
		/// Thickness property of border
		/// </summary>
		public static readonly BindableProperty BorderThicknessProperty =
		  BindableProperty.Create(propertyName: nameof(BorderThickness),
			  returnType: typeof(int),
			  declaringType: typeof(ImageCorner),
			  defaultValue: 0);

		/// <summary>
		/// Border thickness of circle image
		/// </summary>
		public int BorderThickness
		{
			get { return (int)GetValue(BorderThicknessProperty); }
			set { SetValue(BorderThicknessProperty, value); }
		}

		/// <summary>
		/// The corner radius property.
		/// </summary>
		public static readonly BindableProperty CornerRadiusProperty =
		  BindableProperty.Create(propertyName: nameof(CornerRadius),
			  returnType: typeof(float),
			  declaringType: typeof(ImageCorner),
			  defaultValue: 5.0f);

		/// <summary>
		/// Gets or sets the corner radius.
		/// </summary>
		/// <value>The corner radius.</value>
		public float CornerRadius
		{
			get { return (float)GetValue(CornerRadiusProperty); }
			set { SetValue(CornerRadiusProperty, value); }
		}

		/// <summary>
		/// Color property of border
		/// </summary>
		public static readonly BindableProperty BorderColorProperty =
			BindableProperty.Create(propertyName: nameof(BorderColor),
			  returnType: typeof(Color),
			  declaringType: typeof(ImageCorner),
			  defaultValue: Color.White);


		/// <summary>
		/// Border Color of circle image
		/// </summary>
		public Color BorderColor
		{
			get { return (Color)GetValue(BorderColorProperty); }
			set { SetValue(BorderColorProperty, value); }
		}

		/// <summary>
		/// Color property of fill
		/// </summary>
		public static readonly BindableProperty FillColorProperty =
			BindableProperty.Create(propertyName: nameof(FillColor),
			  returnType: typeof(Color),
			  declaringType: typeof(ImageCorner),
			  defaultValue: Color.Transparent);

		/// <summary>
		/// Fill color of circle image
		/// </summary>
		public Color FillColor
		{
			get { return (Color)GetValue(FillColorProperty); }
			set { SetValue(FillColorProperty, value); }
		}

	}
}

