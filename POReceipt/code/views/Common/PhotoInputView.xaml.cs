using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Plugin.Media;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class PhotoInputView : ContentView
	{
		List<PhotoTakenItem> Items;
		public PhotoInputView()
		{
			InitializeComponent();
			Items = new List<PhotoTakenItem>();
			this.addIcon.Text = "fa-plus";
			Init();
		}

		public bool ReadOnly;

		protected void Init()
		{
			UIHelper.addClickEventForView(this.addPhotoStack, (sender, e) => {
				takePhoto();
				//takePhotoTest();
			}, ClickAnimation.Fade);
		}

		void addBox(string filePath)
		{
			var newPhotoTaken = new PhotoTakenItem(
			(obj) =>
			{
				if (!string.IsNullOrEmpty(filePath)) {
					Navigation.PushAsync(new PhotoViewerPage(filePath));
				}
			},async (obj) =>
			{
				if (await Application.Current.MainPage.DisplayAlert("Confirm", "Would you like to delete this photo?", "Yes", "No")) { 
					foreach (var item in Items)
					{
						if (item.FilePath == filePath)
						{
							this.itemsStack.Children.Remove(item);
						}
					}
				}

			}, filePath)
			{ HeightRequest = 40 };

			Items.Add(newPhotoTaken);	

			this.itemsStack.Children.Add(newPhotoTaken);
			//CellGrid.RowDefinitions.Add(new RowDefinition()
			//{
			//	Height = 50,
			//});

			//CellGrid.Children.Add(PhotoBox, 0, 1);
			//Grid.SetColumnSpan(PhotoBox, 3);
		}

		public void takePhotoTest()
		{
			var bytes = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };
			//var filePathImage = Path.Combine(App.Native.getDocumentPath(), @"logo.png");
			//var bytes = App.Native.readBytesFromPath(filePathImage);
			//var image = ImageSource.FromFile("logo.png");
			var timeStamp = Model.getServerDateTime();
			var fileName = System.IO.Path.Combine("Photos", "img-" + timeStamp.ToFileTimeUtc().ToString("X") + ".jpg");
			var filePath = App.Native.writeFile(fileName, bytes);
			addBox(filePath);
		}

		public async void takePhoto()
		{
			//Missing detect iOS and Android?

			var bytes = await TakePhotoBytes();

			if (bytes != null)
			{
				var timeStamp = DateTime.Now.ToString("yyyMMddHHmmssfff");
				var fileName = System.IO.Path.Combine("Photos", "img-" + timeStamp + ".jpg");
				var filePath = App.Native.writeFile(fileName, bytes);

				Device.BeginInvokeOnMainThread(() => { 
					addBox(filePath);
				});

			}
		}

		public static async Task<byte[]> TakePhotoBytes(int maxWidth = 1024, int maxHeight = 1024)
		{
			await CrossMedia.Current.Initialize();  // Added by Khang for investigating slow Camera

			if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
			{
				await Xamarin.Forms.Application.Current.MainPage.DisplayAlert("Notice", "No camera available.", "OK");
				return null;
			}

			var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
			{
				Directory = "Photos",
				Name = "photo.jpg"
			});
			GC.Collect();

			App.Native.log("FINISH TAKING PHOTO");
			if (file == null)
			{
				return null;
			}

			App.Native.setBusy(true);
			App.Native.showHUD("Resizing image...");
			var bytes = await GetBytes(file.GetStream());
			file.Dispose();

			var resizedBytes = await App.Native.resizeImage(bytes, maxWidth, maxHeight, false);
			App.Native.hideHUD();
			App.Native.setBusy(false);
			return resizedBytes;
		}

		public static async Task<byte[]> GetBytes(Stream stream)
		{
			//var mem = new MemoryStream ();
			// return mem.ToArray (); // Commented by Khang

			using (var mem = new MemoryStream())
			{
				await stream.CopyToAsync(mem);
				return mem.ToArray();   // Added by Khang
			}
		}


		public void Reset()
		{
			Items.Clear();
			Items = new List<PhotoTakenItem>();
		}
	}
}

