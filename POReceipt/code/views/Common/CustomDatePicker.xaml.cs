using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class CustomDatePicker : ContentView
	{
		public EventHandler OnValueChanged { set; get; }

		public static readonly BindableProperty FontSizeProperty = BindableProperty.Create(propertyName: "FontSize",
			returnType: typeof(int),
			declaringType: typeof(CustomDatePicker),
			defaultValue: default(int));

		public int FontSize
		{
			get { return (int)GetValue(FontSizeProperty); }
			set
			{
				SetValue(FontSizeProperty, value);
			}
		}

		public static readonly BindableProperty PlaceHolderProperty = BindableProperty.Create(propertyName: "PlaceHolder",
			returnType: typeof(string),
			declaringType: typeof(CustomDatePicker),
			defaultValue: default(string));

		public string PlaceHolder
		{
			get { return (string)GetValue(PlaceHolderProperty); }
			set
			{
				SetValue(PlaceHolderProperty, value);
			}
		}

		public static readonly BindableProperty SelectedValueProperty = BindableProperty.Create(propertyName: "SelectedValue",
			returnType: typeof(DateTime),
			declaringType: typeof(CustomDatePicker),
			defaultValue: default(DateTime));

		public DateTime SelectedValue
		{
			get { return (DateTime)GetValue(SelectedValueProperty); }
			set
			{
				SetValue(SelectedValueProperty, value);
			}
		}



		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			if (propertyName.Equals("FontSize"))
			{
				this.label.FontSize = FontSize;
			}
			else if (propertyName == "SelectedValue")
			{
				this.label.Text = SelectedValue.ToString("dd'/'MM'/'yyyy");
			}
			else if (propertyName == "PlaceHolder")
			{
				Reset();
			}
		}


		public void SetValue(DateTime value)
		{
			this.picker.Date = value;
			this.SelectedValue = value;
			this.label.Text = value.ToString("dd'/'MM'/'yyyy");
		}

		public CustomDatePicker()
		{
			InitializeComponent();
			Reset();
			this.picker.DateSelected += (sender, e) =>
			{
				this.label.TextColor = Color.FromHex("#333333");
				this.label.Text = this.picker.Date.ToString("dd'/'MM'/'yyyy");
				this.SelectedValue = this.picker.Date;
				if (OnValueChanged != null)
					OnValueChanged(this, EventArgs.Empty);
			};
			UIHelper.addClickEventForView(this.grid, (obj, e) =>
			{
				Device.BeginInvokeOnMainThread(() =>
				{
					this.picker.Focus();
				});

			}, ClickAnimation.None);
		}

		public void Reset()
		{
			this.SelectedValue = DateTime.MinValue;
			this.label.TextColor = Color.Silver;
		}
	}
}
