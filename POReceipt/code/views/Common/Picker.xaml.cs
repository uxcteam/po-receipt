using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class Picker : ContentView
	{
		public static readonly BindableProperty FontSizeProperty = BindableProperty.Create(propertyName: "FontSize",
			returnType: typeof(int),
			declaringType: typeof(Picker),
			defaultValue: default(int));

		public int FontSize
		{
			get { return (int)GetValue(FontSizeProperty); }
			set { SetValue(FontSizeProperty, value);
			}
		}

		public static readonly BindableProperty PlaceHolderProperty = BindableProperty.Create(propertyName: "PlaceHolder",
			returnType: typeof(string),
			declaringType: typeof(Picker),
			defaultValue: default(string));

		public string PlaceHolder
		{
			get { return (string)GetValue(PlaceHolderProperty); }
			set
			{
				SetValue(PlaceHolderProperty, value);
			}
		}

		public static readonly BindableProperty SelectedValueProperty = BindableProperty.Create(propertyName: "SelectedValue",
			returnType: typeof(string),
			declaringType: typeof(Picker),
			defaultValue: default(string));

		public string SelectedValue
		{
			get { return (string)GetValue(SelectedValueProperty); }
			set
			{
				SetValue(SelectedValueProperty, value);
			}
		}



		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			if (propertyName.Equals("FontSize"))
			{
				this.label.FontSize = FontSize;
			}
			else if (propertyName == "SelectedValue")
			{
				this.label.Text = SelectedValue;
			} else if (propertyName == "PlaceHolder")
			{
				Reset();
			}
		}


		public void SetValue(string value) {
			if (this.picker.Items.Contains(value)) {
				this.picker.SelectedIndex = picker.Items.IndexOf(value);
				this.SelectedValue = value;
				this.label.Text = value;
			}
		}

		public Picker()
		{
			InitializeComponent();
			this.arrowLabel.Text = "fa-sort-asc";
			Reset();
			this.picker.SelectedIndexChanged += (sender, e) => {
				this.label.TextColor = Color.FromHex("#333333");
				this.label.Text = this.picker.Items[this.picker.SelectedIndex];
				this.SelectedValue = label.Text;
			};
			setDataType(PickerDataTemplete.Relationship);
			UIHelper.addClickEventForView(this.grid, (obj, e) =>
			{
				Device.BeginInvokeOnMainThread(() => {
					this.picker.Focus();
				});

			}, ClickAnimation.None);
		}

		public void Reset() {
			this.SelectedValue = string.IsNullOrEmpty(this.PlaceHolder) ? "Select Value" : this.PlaceHolder;
			this.label.TextColor = Color.FromHex("#878a8b");
		}

		public void setDataType(PickerDataTemplete type) {
			this.picker.Items.Clear();

			Dictionary<string, string> pickerData = new Dictionary<string, string>();
			if (type == PickerDataTemplete.Relationship)
			{
				pickerData = RelationshipPickerData;

			}
			else if (type == PickerDataTemplete.Country)
			{
				pickerData = CountryPickerData;
			}
			else if (type == PickerDataTemplete.OverallRating)
			{
				pickerData = OverallRatingPickerData;
			}
			else if (type == PickerDataTemplete.Building)
			{
				pickerData = BuildingPickerData;
			}
			else if (type == PickerDataTemplete.BuildingService) { 
				pickerData = BuildingServicePickerData;
			}


			foreach (string item in pickerData.Keys)
			{
				this.picker.Items.Add(item);
			}

		}

		public static Dictionary<string, string> BuildingPickerData = new Dictionary<string, string>
		{
			{"Red Campus - Science and Technology", "Red Campus - Science and Technology"},
			{"Red Campus - Law and Politics", "Red Campus - Law and Politics"},
			{"Red Campus - Commerce", "Red Campus - Commerce"},
			{"Great", "Great"},
			{"Library", "Library"},
			{"Car Park", "Car Park"},
			{"Jones Oval", "Jones Oval"},
			{"Smith Park", "Smith Park"}
		};

		public static Dictionary<string, string> BuildingServicePickerData = new Dictionary<string, string>
		{
			{"Toilets", "Toilets"},
			{"Lights", "Lights"},
			{"Door", "Door"},
			{"Locks", "Locks"},
			{"IT Equipment", "IT Equipment"},
			{"Windows", "Windows"},
			{"Signs", "Signs"},
			{"Parking", "Parking"},
			{"Rubbish", "Rubbish"},
			{"Graffiti", "Graffiti"},
			{"Other", "Other"}
		};


		public static Dictionary<string, string> OverallRatingPickerData = new Dictionary<string, string>
		{
			{"Great", "Great"},
			{"Good", "Good"},
			{"Average", "Average"},
			{"Poor", "Poor"},
			{"Terrible", "Terrible"}
		};

		public static Dictionary<string, string> RelationshipPickerData = new Dictionary<string, string>
		{
			{"Mother", "Mother"},
			{"Spouse", "Spouse"},
			{"Father", "Father"},
			{"Grandmother", "Grandmother"},
			{"Grandfather", "Grandfather"},
			{"Brother", "Brother"},
			{"Sister", "Sister"},
			{"Son", "Son"},
		};

		public static Dictionary<string, string> CountryPickerData = new Dictionary<string, string>
		{
			  {   "Afghanistan",    "AF"},
			  {   "Åland Islands",    "AX"},
			  {   "Albania",    "AL"},
			  {   "Algeria",    "DZ"},
			  {   "American Samoa",    "AS"},
			  {   "AndorrA",    "AD"},
			  {   "Angola",    "AO"},
			  {   "Anguilla",    "AI"},
			  {   "Antarctica",    "AQ"},
			  {   "Antigua and Barbuda",    "AG"},
			  {   "Argentina",    "AR"},
			  {   "Armenia",    "AM"},
			  {   "Aruba",    "AW"},
			  {   "Australia",    "AU"},
			  {   "Austria",    "AT"},
			  {   "Azerbaijan",    "AZ"},
			  {   "Bahamas",    "BS"},
			  {   "Bahrain",    "BH"},
			  {   "Bangladesh",    "BD"},
			  {   "Barbados",    "BB"},
			  {   "Belarus",    "BY"},
			  {   "Belgium",    "BE"},
			  {   "Belize",    "BZ"},
			  {   "Benin",    "BJ"},
			  {   "Bermuda",    "BM"},
			  {   "Bhutan",    "BT"},
			  {   "Bolivia",    "BO"},
			  {   "Bosnia and Herzegovina",    "BA"},
			  {   "Botswana",    "BW"},
			  {   "Bouvet Island",    "BV"},
			  {   "Brazil",    "BR"},
			  {   "British Indian Ocean Territory",    "IO"},
			  {   "Brunei Darussalam",    "BN"},
			  {   "Bulgaria",    "BG"},
			  {   "Burkina Faso",    "BF"},
			  {   "Burundi",    "BI"},
			  {   "Cambodia",    "KH"},
			  {   "Cameroon",    "CM"},
			  {   "Canada",    "CA"},
			  {   "Cape Verde",    "CV"},
			  {   "Cayman Islands",    "KY"},
			  {   "Central African Republic",    "CF"},
			  {   "Chad",    "TD"},
			  {   "Chile",    "CL"},
			  {   "China",    "CN"},
			  {   "Christmas Island",    "CX"},
			  {   "Cocos (Keeling) Islands",    "CC"},
			  {   "Colombia",    "CO"},
			  {   "Comoros",    "KM"},
			  {   "Congo",    "CG"},
			  {   "Congo, The Democratic Republic of the",    "CD"},
			  {   "Cook Islands",    "CK"},
			  {   "Costa Rica",    "CR"},
			  {   "Croatia",    "HR"},
			  {   "Cuba",    "CU"},
			  {   "Cyprus",    "CY"},
			  {   "Czech Republic",    "CZ"},
			  {   "Denmark",    "DK"},
			  {   "Djibouti",    "DJ"},
			  {   "Dominica",    "DM"},
			  {   "Dominican Republic",    "DO"},
			  {   "Ecuador",    "EC"},
			  {   "Egypt",    "EG"},
			  {   "El Salvador",    "SV"},
			  {   "Equatorial Guinea",    "GQ"},
			  {   "Eritrea",    "ER"},
			  {   "Estonia",    "EE"},
			  {   "Ethiopia",    "ET"},
			  {   "Falkland Islands",    "FK"},
			  {   "Faroe Islands",    "FO"},
			  {   "Fiji",    "FJ"},
			  {   "Finland",    "FI"},
			  {   "France",    "FR"},
			  {   "French Guiana",    "GF"},
			  {   "French Polynesia",    "PF"},
			  {   "French Southern Territories",    "TF"},
			  {   "Gabon",    "GA"},
			  {   "Gambia",    "GM"},
			  {   "Georgia",    "GE"},
			  {   "Germany",    "DE"},
			  {   "Ghana",    "GH"},
			  {   "Gibraltar",    "GI"},
			  {   "Greece",    "GR"},
			  {   "Greenland",    "GL"},
			  {   "Grenada",    "GD"},
			  {   "Guadeloupe",    "GP"},
			  {   "Guam",    "GU"},
			  {   "Guatemala",    "GT"},
			  {   "Guernsey",    "GG"},
			  {   "Guinea",    "GN"},
			  {   "Guinea-Bissau",    "GW"},
			  {   "Guyana",    "GY"},
			  {   "Haiti",    "HT"},
			  {   "Heard Island and Mcdonald Islands",    "HM"},
			  {   "Holy See (Vatican City State)",    "VA"},
			  {   "Honduras",    "HN"},
			  {   "Hong Kong",    "HK"},
			  {   "Hungary",    "HU"},
			  {   "Iceland",    "IS"},
			  {   "India",    "IN"},
			  {   "Indonesia",    "ID"},
			  {   "Iran, Islamic Republic Of",    "IR"},
			  {   "Iraq",    "IQ"},
			  {   "Ireland",    "IE"},
			  {   "Isle of Man",    "IM"},
			  {   "Israel",    "IL"},
			  {   "Italy",    "IT"},
			  {   "Jamaica",    "JM"},
			  {   "Japan",    "JP"},
			  {   "Jersey",    "JE"},
			  {   "Jordan",    "JO"},
			  {   "Kazakhstan",    "KZ"},
			  {   "Kenya",    "KE"},
			  {   "Kiribati",    "KI"},
			  {   "Korea, Democratic People",    "KP"},
			  {   "Korea",    "KR"},
			  {   "Kuwait",    "KW"},
			  {   "Kyrgyzstan",    "KG"},
			  {   "Lao People Democratic Republic",    "LA"},
			  {   "Latvia",    "LV"},
			  {   "Lebanon",    "LB"},
			  {   "Lesotho",    "LS"},
			  {   "Liberia",    "LR"},
			  {   "Libyan Arab Jamahiriya",    "LY"},
			  {   "Liechtenstein",    "LI"},
			  {   "Lithuania",    "LT"},
			  {   "Luxembourg",    "LU"},
			  {   "Macao",    "MO"},
			  {   "Macedonia, The Former Yugoslav Republic of",    "MK"},
			  {   "Madagascar",    "MG"},
			  {   "Malawi",    "MW"},
			  {   "Malaysia",    "MY"},
			  {   "Maldives",    "MV"},
			  {   "Mali",    "ML"},
			  {   "Malta",    "MT"},
			  {   "Marshall Islands",    "MH"},
			  {   "Martinique",    "MQ"},
			  {   "Mauritania",    "MR"},
			  {   "Mauritius",    "MU"},
			  {   "Mayotte",    "YT"},
			  {   "Mexico",    "MX"},
			  {   "Micronesia, Federated States of",    "FM"},
			  {   "Moldova, Republic of",    "MD"},
			  {   "Monaco",    "MC"},
			  {   "Mongolia",    "MN"},
			  {   "Montserrat",    "MS"},
			  {   "Morocco",    "MA"},
			  {   "Mozambique",    "MZ"},
			  {   "Myanmar",    "MM"},
			  {   "Namibia",    "NA"},
			  {   "Nauru",    "NR"},
			  {   "Nepal",    "NP"},
			  {   "Netherlands",    "NL"},
			  {   "Netherlands Antilles",    "AN"},
			  {   "New Caledonia",    "NC"},
			  {   "New Zealand",    "NZ"},
			  {   "Nicaragua",    "NI"},
			  {   "Niger",    "NE"},
			  {   "Nigeria",    "NG"},
			  {   "Niue",    "NU"},
			  {   "Norfolk Island",    "NF"},
			  {   "Northern Mariana Islands",    "MP"},
			  {   "Norway",    "NO"},
			  {   "Oman",    "OM"},
			  {   "Pakistan",    "PK"},
			  {   "Palau",    "PW"},
			  {   "Palestinian Territory, Occupied",    "PS"},
			  {   "Panama",    "PA"},
			  {   "Papua New Guinea",    "PG"},
			  {   "Paraguay",    "PY"},
			  {   "Peru",    "PE"},
			  {   "Philippines",    "PH"},
			  {   "Pitcairn",    "PN"},
			  {   "Poland",    "PL"},
			  {   "Portugal",    "PT"},
			  {   "Puerto Rico",    "PR"},
			  {   "Qatar",    "QA"},
			  {   "Reunion",    "RE"},
			  {   "Romania",    "RO"},
			  {   "Russian Federation",    "RU"},
			  {   "RWANDA",    "RW"},
			  {   "Saint Helena",    "SH"},
			  {   "Saint Kitts and Nevis",    "KN"},
			  {   "Saint Lucia",    "LC"},
			  {   "Saint Pierre and Miquelon",    "PM"},
			  {   "Saint Vincent and the Grenadines",    "VC"},
			  {   "Samoa",    "WS"},
			  {   "San Marino",    "SM"},
			  {   "Sao Tome and Principe",    "ST"},
			  {   "Saudi Arabia",    "SA"},
			  {   "Senegal",    "SN"},
			  {   "Serbia and Montenegro",    "CS"},
			  {   "Seychelles",    "SC"},
			  {   "Sierra Leone",    "SL"},
			  {   "Singapore",    "SG"},
			  {   "Slovakia",    "SK"},
			  {   "Slovenia",    "SI"},
			  {   "Solomon Islands",    "SB"},
			  {   "Somalia",    "SO"},
			  {   "South Africa",    "ZA"},
			  {   "South Georgia and the South Sandwich Islands",    "GS"},
			  {   "Spain",    "ES"},
			  {   "Sri Lanka",    "LK"},
			  {   "Sudan",    "SD"},
			  {   "Suriname",    "SR"},
			  {   "Svalbard and Jan Mayen",    "SJ"},
			  {   "Swaziland",    "SZ"},
			  {   "Sweden",    "SE"},
			  {   "Switzerland",    "CH"},
			  {   "Syrian Arab Republic",    "SY"},
			  {   "Taiwan, Province of China",    "TW"},
			  {   "Tajikistan",    "TJ"},
			  {   "Tanzania, United Republic of",    "TZ"},
			  {   "Thailand",    "TH"},
			  {   "Timor-Leste",    "TL"},
			  {   "Togo",    "TG"},
			  {   "Tokelau",    "TK"},
			  {   "Tonga",    "TO"},
			  {   "Trinidad and Tobago",    "TT"},
			  {   "Tunisia",    "TN"},
			  {   "Turkey",    "TR"},
			  {   "Turkmenistan",    "TM"},
			  {   "Turks and Caicos Islands",    "TC"},
			  {   "Tuvalu",    "TV"},
			  {   "Uganda",    "UG"},
			  {   "Ukraine",    "UA"},
			  {   "United Arab Emirates",    "AE"},
			  {   "United Kingdom",    "GB"},
			  {   "United States",    "US"},
			  {   "United States Minor Outlying Islands",    "UM"},
			  {   "Uruguay",    "UY"},
			  {   "Uzbekistan",    "UZ"},
			  {   "Vanuatu",    "VU"},
			  {   "Venezuela",    "VE"},
			  {   "Viet Nam",    "VN"},
			  {   "Virgin Islands, British",    "VG"},
			  {   "Virgin Islands, U.S.",    "VI"},
			  {   "Wallis and Futuna",    "WF"},
			  {   "Western Sahara",    "EH"},
			  {   "Yemen",    "YE"},
			  {   "Zambia",    "ZM"},
			  {   "Zimbabwe",    "ZW"}
		};
	}


	public enum PickerDataTemplete
	{
		Relationship, Country,OverallRating, BuildingService, Building , None
	}
}

