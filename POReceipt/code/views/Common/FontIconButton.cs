using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using FormsPlugin.Iconize;

namespace POReceipt
{
	public class FontIconButton : StackLayout
	{
		public FontIconButton(string iconText, string titleText, int iconSize, int titleSize, Color iconColor)
		{
			this.Orientation = StackOrientation.Vertical;
			this.HorizontalOptions = LayoutOptions.Fill;
			this.VerticalOptions = LayoutOptions.Fill;
			//this.Padding = new Thickness(0, 15, 0, 10);
			this.Spacing = 0;
			//this.BackgroundColor = Color.Teal;

			var grid = new Grid()
			{
				RowDefinitions = new RowDefinitionCollection() {
					new RowDefinition() { Height = new GridLength(20, GridUnitType.Absolute)},
					new RowDefinition() { Height = new GridLength(1, GridUnitType.Star)},
					new RowDefinition() { Height = new GridLength(1, GridUnitType.Star)},
					new RowDefinition() { Height = new GridLength(10, GridUnitType.Absolute)}
				}
			};
			var fontIcon = new IconLabel()
			{
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
				VerticalTextAlignment = TextAlignment.End,
				HorizontalTextAlignment = TextAlignment.Center,
				FontSize = iconSize,
				TextColor = iconColor,
				Text = iconText,
				//BackgroundColor = Color.Blue
			};

			var title = new Label()
			{
				FontSize = titleSize,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.StartAndExpand,
				Text = titleText,
				TextColor = iconColor,
				//BackgroundColor = Color.Red
			};

			UIHelper.setPositionAtGrid(grid, fontIcon, 1, 0);
			UIHelper.setPositionAtGrid(grid, title, 2, 0);

			this.Children.Add(grid);
		}
	}
}

