using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;

namespace POReceipt
{
	public class MapDirection_old : ContentPage
	{

		public string gotoAddress;
		public float latitude;
		public float longitude;

		private string fromAddress;
		private Types.Location curLocation;
		private bool noGPS;

		HtmlWebViewSource _htmlWebSource;

		public MapDirection_old() : base()
		{
			App.Native.setBusy(true);
			Title = "Map";
			//ToolbarItems.Add(new ToolbarItem
			//{
			//	Text = "Show Me",
			//	Order = ToolbarItemOrder.Primary,
			//	Command = new Command(() => this.Navigation.PushAsync(new ARPage()
			//	{
			//		Location = new Types.PointOfInterest()
			//		{
			//			Name = gotoAddress,
			//			Latitude = latitude,
			//			Longitude = longitude
			//		}
			//	}))
			//});
		}

		NativeWebView webView;

		protected override void OnAppearing()
		{
			base.OnAppearing();
			if (Content == null)
			{
				Task.Factory.StartNew(async () =>
				{
					curLocation = await App.Native.getLocation();
					if (!curLocation.Success)
					{
						this.fromAddress = this.gotoAddress;
						noGPS = true;
					}

					Device.BeginInvokeOnMainThread(() =>
					{
						loadDocument();
						App.Native.setBusy(false);
					});
				});
			}
		}

		private void loadDocument()
		{

			var size = App.Native.getPhysicalSize();

			var htmlContent = App.Native.readResourceSync("mapDirection.htm");
			//htmlContent = htmlContent.Replace("{WIDTH}", string.Format("{0}", size.Width));
			//htmlContent = htmlContent.Replace("{HEIGHT}", string.Format("{0}", size.Height));

			htmlContent = htmlContent.Replace("'{FROM_LAT}'", string.Format("{0}", this.curLocation.Latitude));
			htmlContent = htmlContent.Replace("'{FROM_LON}'", string.Format("{0}", this.curLocation.Longitude));

			htmlContent = htmlContent.Replace("'{TO_LAT}'", string.Format("{0}", this.latitude));
			htmlContent = htmlContent.Replace("'{TO_LON}'", string.Format("{0}", this.longitude));

			_htmlWebSource = new HtmlWebViewSource
			{
				Html = htmlContent
			};

			webView = new NativeWebView
			{
				Source = _htmlWebSource
			};

			var grid = new Grid() { Margin= new Thickness(0,0,0,0)};
			grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
			grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(40, GridUnitType.Absolute) });
			var showMeButton = new Button()
			{
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Fill,
				Text = "Show Me",
				BackgroundColor = Color.Gray,
				TextColor = Color.White
			};
			showMeButton.Clicked += (sender, e) => { 
				this.Navigation.PushAsync(new ARPage()
				{
					Location = new Types.PointOfInterest()
					{
						Name = gotoAddress,
						Latitude = latitude,
						Longitude = longitude
					}
				});
			};
			//UIHelper.setPositionAtGrid(grid, webView, 0, 0);
			//UIHelper.setPositionAtGrid(grid, showMeButton, 1, 0);
			Content = webView;
		}

		protected override void OnDisappearing()
		{
			//if (webView != null)
			//{
			//	webView.IsVisible = false;
			//	webView = null;
			//}

			GC.Collect();

			base.OnDisappearing();
		}

		protected override bool OnBackButtonPressed()
		{
			if (webView != null)
			{
				webView.IsVisible = false;
				webView = null;
			}

			GC.Collect();

			return base.OnBackButtonPressed();
		}
	}
}