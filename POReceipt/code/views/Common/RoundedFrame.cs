using System;
using Xamarin.Forms;

namespace POReceipt
{
	public class RoundedFrame : Frame
	{
		public float Radius { set; get; }
		public double BorderWidth { set; get; }
		public Color BorderColor { set; get; }
		public RoundedFrame()
		{
		}
	}
}

