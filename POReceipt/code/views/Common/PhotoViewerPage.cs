using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Threading.Tasks;

namespace POReceipt
{
	public class PhotoViewerPage : ContentPage
	{
		int _currentRotate = 0;
		string _fileUri = "";
		HtmlWebViewSource _htmlWebSource;

		public PhotoViewerPage(string filePath) : base ()
		{
			App.Native.setBusy(true);
			this._fileUri = filePath;
			Title = "Photo";


		}

		NativeWebView webView;

		protected override void OnAppearing()
		{
			base.OnAppearing();

			if (Content == null)
			{
				Task.Factory.StartNew(() =>
				{
					Device.BeginInvokeOnMainThread(() =>
					{
						loadDocument();

						App.Native.setBusy(false);
					});
				});
			}
		}

		private void loadDocument()
		{
			//_fileUri = "file:///";
			var uri = new Uri(_fileUri);

			_htmlWebSource = new HtmlWebViewSource
			{
				Html = @"
							<html><head><style>
								img {
									width: 100%;
								}
							</style></head>
							<body>
								<img class='rotate90' src='" + uri.AbsoluteUri + @"'>
							</body>						
							</html>"
			};

			webView = new NativeWebView
			{
				Source = _htmlWebSource
			};

			Content = webView;

			int pos = uri.AbsoluteUri.LastIndexOf("/", StringComparison.Ordinal) + 1;
			var fileName = uri.AbsoluteUri.Substring(pos, uri.AbsoluteUri.Length - pos);
			this.Title = fileName;
		}

		protected override void OnDisappearing()
		{
			if (webView != null)
			{
				webView.IsVisible = false;
				webView = null;
			}

			GC.Collect();

			base.OnDisappearing();
		}

		protected override bool OnBackButtonPressed()
		{
			if (webView != null)
			{
				webView.IsVisible = false;
				webView = null;
			}

			GC.Collect();

			return base.OnBackButtonPressed();
		}
	}
}

