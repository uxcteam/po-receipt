using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;

namespace POReceipt
{
	public class MapBuilding_old : ContentPage
	{

		public string gotoAddress;
		public float latitude;
		public float longitude;



		private string fromAddress;
		private Types.Location curLocation;
		private bool noGPS;

		HtmlWebViewSource _htmlWebSource;

		public MapBuilding_old() : base()
		{
			App.Native.setBusy(true);
			Title = "University Building";

		}

		NativeWebView webView;

		protected override void OnAppearing()
		{
			base.OnAppearing();
			if (Content == null)
			{
				Task.Factory.StartNew(async () =>
				{
					curLocation = await App.Native.getLocation();
					if (!curLocation.Success)
					{
						this.fromAddress = this.gotoAddress;
						noGPS = true;
					}

					Device.BeginInvokeOnMainThread(() =>
					{
						loadDocument();
						App.Native.setBusy(false);
					});
				});
			}
		}

		private void loadDocument()
		{

			ToolbarItems.Add(new ToolbarItem()
			{
				Text = "Near By",
				Command = new Command(async (obj) =>
				{
					string[] nearByText    = { "Hospital", "ATM", "Bakery", "Book store", "Cafe", "Restaurant", "Parking", "Police" };
					//string[] nearByKeyword = { "hospital", "atm", "bakery", "book_store", "cafe", "restaurant", "parking", "police" };
					var action = await DisplayActionSheet("Search places near by", 
					                                      "Cancel", null,  nearByText);

					if (action != "Cancel" && !string.IsNullOrEmpty(action))
					{
						var keyword = "";
						if (action == "Hospital")
						{
							keyword = "hospital";
						}
						else if (action == "ATM")
						{
							keyword = "atm";
						}
						else if (action == "Bakery")
						{
							keyword = "bakery";
						}
						else if (action == "Book store")
						{
							keyword = "book_store";
						}
						else if (action == "Cafe")
						{
							keyword = "cafe";
						}
						else if (action == "Restaurant")
						{
							keyword = "restaurant";
						}
						else if (action == "Parking")
						{
							keyword = "parking";
						}
						else if (action == "Police")
						{
							keyword = "police";
						}


					}
				})
			});

			var size = App.Native.getPhysicalSize();

			var htmlContent = App.Native.readResourceSync("mapBuilding.htm");
			//htmlContent = htmlContent.Replace("{WIDTH}", string.Format("{0}", size.Width));
			//htmlContent = htmlContent.Replace("{HEIGHT}", string.Format("{0}", size.Height));

			//htmlContent = htmlContent.Replace("{FROM_LAT}", string.Format("{0}", this.curLocation.Latitude));
			//htmlContent = htmlContent.Replace("{FROM_LON}", string.Format("{0}", this.curLocation.Longitude));

			//htmlContent = htmlContent.Replace("{TO_LAT}", string.Format("{0}", this.latitude));
			//htmlContent = htmlContent.Replace("{TO_LON}", string.Format("{0}", this.longitude));

			//if (this.latitude == 0 || this.longitude == 0)
			//	htmlContent = htmlContent.Replace("{USE_LAT_LON}", "0");
			//else
			//	htmlContent = htmlContent.Replace("{USE_LAT_LON}", "1");
	
			//htmlContent = htmlContent.Replace("{TO_ADDRESS}", this.gotoAddress);

			_htmlWebSource = new HtmlWebViewSource
			{
				Html = htmlContent
			};

			webView = new NativeWebView
			{
				Source = _htmlWebSource
			};

			Content = webView;
		}

		protected override void OnDisappearing()
		{
			if (webView != null)
			{
				webView.IsVisible = false;
				webView = null;
			}

			GC.Collect();

			base.OnDisappearing();
		}

		protected override bool OnBackButtonPressed()
		{
			if (webView != null)
			{
				webView.IsVisible = false;
				webView = null;
			}

			GC.Collect();

			return base.OnBackButtonPressed();
		}
	}
}