using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;

namespace POReceipt
{
	public class GridView : Grid
	{
		public int Column { set; get; }
		public int DefaultCellHeight { set; get; }

		/// <summary>
		/// Items Sourc Property
		/// </summary>
		public static readonly BindableProperty SourceProperty =
		  BindableProperty.Create(propertyName: nameof(Source),
			  returnType: typeof(List<String>),
			  declaringType: typeof(GridView),
			                      defaultValue: null);

		/// <summary>
		/// Items Source 
		/// </summary>
		public List<String> Source
		{
			get { return (List<String>)GetValue(SourceProperty); }
			set { SetValue(SourceProperty, value);
				this.Source = value;
				doInit();
			}
		}

		/// <summary>
		/// Items Sourc Property
		/// </summary>
		public static readonly BindableProperty CommandProperty =
			BindableProperty.Create(propertyName: nameof(Command),
			  returnType: typeof(Command),
			  declaringType: typeof(GridView),
								  defaultValue: null);

		/// <summary>
		/// Items Source 
		/// </summary>
		public Command Command
		{
			get { return (Command)GetValue(CommandProperty); }
			set
			{
				SetValue(CommandProperty, value);
				this.Command = value;
			}
		}

		public GridView()
		{
			this.Column = 3;    // Default
			this.DefaultCellHeight = 140;	// Default
		}


		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);
			if (propertyName == SourceProperty.PropertyName) {
				doInit();
			}
		}

		void doInit() {
			if (this.Source != null)
			{
				var RowDefinitionsList = new RowDefinitionCollection();
				var ColumnDefinitionsList = new ColumnDefinitionCollection();

				var nRow = this.Source.Count / Column;
				nRow += (this.Source.Count % Column > 0) ? 1 : 0;


				for (int i = 0; i < nRow; i++)
				{
					RowDefinitionsList.Add(new RowDefinition { Height = new GridLength(DefaultCellHeight, GridUnitType.Absolute) });// default
				}

				for (int i = 0; i < Column; i++) {
					ColumnDefinitionsList.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)});
				}

				this.ColumnDefinitions = ColumnDefinitionsList;
				this.RowDefinitions = RowDefinitionsList;

				var row = 0;
				var column = 0;
				foreach (var item in this.Source) {
					var text = item.ToString();
					if (row < nRow && column < this.Column)
					{
						spawnItem(text, row, column);
					}
					row = (column == this.Column - 1) ? row + 1 : row;
					column = (column < this.Column - 1) ? column + 1 : 0;

				}

			}
		}

		void spawnItem(string title, int row, int column) { 
			var itemView = new FontIconButton("md-folder-open", title, 50, 14, Color.Gray);
			UIHelper.addClickEventForView(itemView, (sender, e) => {
				if (this.Command != null) {
					Command.Execute(title);
				}
			}, ClickAnimation.Scale);

			UIHelper.setPositionAtGrid(this, itemView, row, column);
		}
	}
}

