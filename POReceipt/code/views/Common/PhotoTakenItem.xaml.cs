using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace POReceipt
{
	public partial class PhotoTakenItem : ContentView
	{
		public string FilePath { set; get; }
		public PhotoTakenItem(Action<object> OnView, Action<object> OnDelete, string filePath)
		{
			InitializeComponent();
			this.FilePath = filePath;
			this.attachmentIcon.Text = "md-attachment";
			this.viewIcon.Text = "fa-eye";
			this.deleteIcon.Text = "fa-trash-o";
			populateFileName();
			UIHelper.addClickEventForView(this.deleteIcon, (sender, e) => {
				OnDelete(this);
				App.Native.deleteFile(FilePath);
			}, ClickAnimation.Fade);

			UIHelper.addClickEventForView(this.viewIcon, (sender, e) =>
			{
				OnView(this);
			}, ClickAnimation.Fade);
		}

		void populateFileName() {
			if (!string.IsNullOrEmpty(FilePath)) {
				int pos = FilePath.LastIndexOf("/", StringComparison.Ordinal) + 1;
				var fileName = FilePath.Substring(pos, FilePath.Length - pos);
				this.fileNameLabel.Text = fileName;
			}
		}
	}
}

