﻿This file shows you how to use the Crypto Generator and includes an example below. 

The Crypto class only has two methods. Crypto.Create and Crypto.isMatch.

Methods Explained:

Crypto.Create takes in the string that needs to be salted and hashed. Crypto.Create(YOUR KEYWORD) This will
return a SecurityKey object. This object has two properties, Id and SaltedKeyword. The Id is of type Guid and the SaltedKeyword
is a string. You will need to SAVE/STORE both values to use the other method.

***THIS IS A CASE SENSITIVE MATCH***

Crypto.isMatch takes in three parameters the string keyword, the original Guid that you stored, and the original SaltedKeyword that you stored.
Crypto.isMatch(KEYWORD, SAVED GUID, SAVED SALTEDKEYWORD)
If the keyword provided matches the saved salted keyword the method returns true and if not, then false. 

***THIS IS CASE SENSITIVE MATCH***

See example:
    Console.Write("Enter the keyword:");

    var keyword = Console.ReadLine(); //Get a keyword that needs to be hashed.

    var result1 = Crypto.Create(keyword); //Pass the keyword into the Crypto.Create method.

    /*
        This returns an object that contains the Guid and the SaltedKeyword that MUST BE stored
                
        The stored values will be provided later to compare any keyword by calling Crypto.isMatch.
    */

    Console.WriteLine(result1.Id); //This is used to illustrate our results. Note: that you would store the Id.

    Console.WriteLine(result1.SaltedKeyword); //This is used to illustrate our results. Note: you will store the SaltedKeyword.

    Console.WriteLine("Re-enter the keyword:"); //This will be used to compare a new keyword with our original.

    var checkKeyword = Console.ReadLine();

    var result2 = Crypto.isMatch(checkKeyword, result1.Id, result1.SaltedKeyword); //Check the new keyword provided by calling Crypto.isMatch.

    /*
        Notice that we had to pass in the original Guid id and the SaltedKeyword. You will need to store these values from the original 
        Crypto.Create method.

   */

    Console.WriteLine(result2); //Print the results which is true or false. 

    Console.ReadLine();