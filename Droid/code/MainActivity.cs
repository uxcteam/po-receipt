using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Locations;
using Android.Webkit;

using Xamarin.Forms;
using Java.IO;
using System.Collections.Generic;
using Android.Provider;
using Android.Net;
using System.IO;
using Android.Graphics;
using Xamarin.Forms.Platform.Android;
using Newtonsoft.Json.Linq;
using System.Xml;
using Android.Content.Res;
// add the HockeyApp namespace
using HockeyApp;

namespace POReceipt.Droid
{
	[Activity(
		Label = "PO Receipting",
		Theme = "@style/CustomActionBarTheme",
		//		Theme = "@android/AppCombat.Theme",
		Icon = "@android:color/transparent",
		ScreenOrientation = ScreenOrientation.Portrait,
		ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]

	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity, INative
	{
		// Field, property, and method for Picture Picker
		public static readonly int PickImageId = 1000;

		public TaskCompletionSource<Stream> PickImageTaskCompletionSource { set; get; }

		public static class AppMediaData
		{
			public static Java.IO.File _file;
			public static Java.IO.File _dir;
			public static Bitmap bitmap;
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent intent)
		{
			base.OnActivityResult(requestCode, resultCode, intent);
			if (requestCode == PickImageId)
			{
				if ((resultCode == Result.Ok) && (intent != null))
				{
					Android.Net.Uri uri = intent.Data;
					Stream stream = ContentResolver.OpenInputStream(uri);

					// Set the Stream as the completion of the Task
					PickImageTaskCompletionSource.SetResult(stream);
				}
				else
				{
					PickImageTaskCompletionSource.SetResult(null);
				}
			}
		}

		public string readSampleData(string fileName)
		{

			var content = string.Empty;
			AssetManager assets = this.Assets;
			using (StreamReader sr = new StreamReader(assets.Open(fileName)))
			{
				content = sr.ReadToEnd();
			}
			return content;
		}


		EventHandler onFinishTakingPhoto;
		AlertDialog alertDialog;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			setupAnalytics ();
			Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.FontAwesomeModule())
						  .With(new Plugin.Iconize.Fonts.MaterialModule());

			global::Xamarin.Forms.Forms.Init (this, bundle);


			doInit (this, bundle);

			FormsPlugin.Iconize.Droid.IconControls.Init();

			LoadApplication (new App (this));
			//var tv = FindViewById<TextView>(Resource.Id.systemUiFlagTextView);
		}

		private void doInit (MainActivity mainActivity, Bundle bundle)
		{
			//Xamarin.FormsMaps.Init (mainActivity, bundle);
		}

		public override void OnLowMemory ()
		{
			base.OnLowMemory ();

			GC.Collect ();
		}

		protected override void OnDestroy ()
		{
			
			base.OnDestroy ();
		}


		#region INative implementation

		public void makeACall(string phoneNumber) {
			var uri = Android.Net.Uri.Parse("tel:" + phoneNumber);
			var intent = new Intent(Intent.ActionView, uri);
			StartActivity(intent);
		}

		public void openFile (string filePath, string mimeType = null)
		{
			setBusy (true);

			string type = mimeType;

			Task.Factory.StartNew (() => {
				Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
					try {
						var extPath = Android.OS.Environment.ExternalStorageDirectory.Path;
						var extFile = System.IO.Path.Combine (extPath, "file." + System.IO.Path.GetExtension (filePath));
						var bytes = System.IO.File.ReadAllBytes (filePath);
						System.IO.File.WriteAllBytes (extFile, bytes);

						var file = new Java.IO.File (extFile);

						type = mimeType; 
						if (string.IsNullOrEmpty (type)) {
							var map = MimeTypeMap.Singleton;
							var ext = MimeTypeMap.GetFileExtensionFromUrl (file.Name);
							type = map.GetMimeTypeFromExtension (ext);

							if (type == null) {
								type = "application/*";
							}
						}

						var intent = new Intent ();
						intent.AddFlags (ActivityFlags.NewTask);
						intent.SetAction (Intent.ActionView);

						var uri = Android.Net.Uri.FromFile (file);
						intent.SetDataAndType (uri, type);

						StartActivity (intent);
					} catch (ActivityNotFoundException ex) {
						showAlert (
							"No suitable document viewer was found on your device to open this document type"
							+ (string.IsNullOrEmpty (type) ? "" : " (" + type + ")"), "Notice"); 
					} catch (Exception e) {
						log (e);
					}

					setBusy (false);
				});
			});
		}

		public string[] getFiles (string resource, string pattern = null)
		{
			return Shared.FileUtil.getFiles (resource, pattern);
		}

		public string[] getDirectories (string resource, string pattern = null)
		{
			return Shared.FileUtil.getDirectories (resource, pattern);
		}

		public string getDocumentPath ()
		{
			return Shared.FileUtil.getDocumentPath ();
		}

		public void writeBSON (string file, object obj)
		{
			Shared.JsonUtil.writeBSON (file, obj);
		}

		public T readBSON<T> (string file)
		{
			return Shared.JsonUtil.readBSON<T> (file);
		}

		public void saveLoginInfo(string username, string password)
		{
			// Update latter
		}

		public string getLoggedInUsername() {
			// Update latter
			return string.Empty;
		}

		public string getLoggedInPassword() {
			// Update latter
			return string.Empty;
		}

		public DateTime timeFile (string resource)
		{
			return Shared.FileUtil.timeFile (resource);
		}

		public async Task<string> showActionSheet (string message, params string[] options)
		{
			return await Shared.Util.showActionSheet (message, options);
		}

		public async Task<bool> checkHost ()
		{
			return false; //await Shared.Service.checkHost ();
		}

		public void deleteFile (string file)
		{
			Shared.FileUtil.deleteFile (file);
		}

		public void deleteDirectory (string file)
		{
			Shared.FileUtil.deleteDirectory (file);
		}

		public async Task<double[]> geocodeAddress (string location)
		{
			try {
				var geo = new Geocoder (this);

				var addresses = await geo.GetFromLocationNameAsync (location, 1);

				if (addresses != null && addresses.Count > 0) {
					var address = addresses.First ();
					if (address != null) {
						if (address.HasLatitude && address.HasLongitude) {
							return new double[]{ address.Latitude, address.Longitude };
						}
					}
				}
			} catch (Exception e) {
				log (e);
			}

			return null;
		}

		public async Task<bool> isHostReachable (string host, int port)
		{
			return await Shared.Util.isHostReachable (host, port);
		}

		public bool ifRelease (Action action = null)
		{
			return Shared.Util.ifRelease (action);
		}

		public byte[] compress (byte[] bytes)
		{
			return Shared.ConvUtil.compress (bytes);
		}

		public byte[] decompress (byte[] bytes)
		{
			return Shared.ConvUtil.decompress (bytes);	
		}

		public string unescapeXML (string xml)
		{
			return Shared.XmlUtil.unescapeXML (xml);
		}

		public string escapeXML (string xml)
		{
			return Shared.XmlUtil.escapeXML (xml);
		}

		public string checkFile (string resource)
		{
			return Shared.FileUtil.checkFile (resource);
		}

		public string readFile (string resource)
		{
			return Shared.FileUtil.readFile (resource);
		}

		public byte[] readBytes (string resource)
		{
			return Shared.FileUtil.readBytes (resource);
		}

		public void writeFile (string resource, string content)
		{
			Shared.FileUtil.writeFile (resource, content);
		}

		public string writeFile (string resource, byte[] bytes)
		{
			return Shared.FileUtil.writeFile (resource, bytes);
		}

		public bool ifDebug (Action action = null)
		{
			return Shared.Util.ifDebug (action);
		}

		public bool isNetworkConnected ()
		{
			return Shared.Util.isNetworkConnected ();
		}

		public string setServiceEndpoint (string url)
		{
			return "";// Shared.Service.setServiceEndpoint (url);
		}

		public void authenticate (string domain, string username, string password)
		{
			//Shared.Service.authenticate (domain, username, password);
		}

		public async Task<bool> checkService ()
		{
			return false;// await Shared.Service.checkService ();
		}

		public async Task<string> callService (string xml)
		{
			return "";//await Shared.Service.callServiceAsync (xml);
		}

		public async Task<byte[]> resizeImage (byte[] imageData, float width, float height, bool allowUpscale)
		{
			return await Shared.Util.resizeImage (imageData, width, height, allowUpscale);
		}

		public void openURL (string url)
		{
			Shared.Util.openURL (url);
		}

		public void navigateToMap (string label, string location)
		{
			Shared.Util.navigateToMap (label, location);
		}

		public void navigateToMap (string label, double latitude, double longitude)
		{
			Shared.Util.navigateToMap (label, latitude, longitude);
		}

		public void showError (Exception exception)
		{
			Shared.Util.showError (exception);
		}

		public string getJSON (object obj)
		{
			return Shared.JsonUtil.getJSON (obj);
		}

		public T getObject<T> (string json)
		{
			return Shared.JsonUtil.getObject<T> (json);
		}

		public void invokeLater (Action action)
		{
			Shared.Util.invokeLater (action);
		}

		public async Task<bool> showConfirm (string message, string title = "")
		{
			return await Shared.Util.showConfirm (message, title);
		}

		public async Task<bool> showConfirmYesNo (string message, string title = "")
		{
			return await Shared.Util.showConfirmYesNo (message, title);
		}

		public void showAlert (string message, string title = "")
		{
			//Shared.Util.showAlert (message, title); Commented by Khang
			// Added by Khang to avoid duplication
			if (alertDialog != null && alertDialog.IsShowing) 
				return;
			
			var builder = new AlertDialog.Builder (this);
			builder.SetTitle (title);
			builder.SetMessage (message);
			builder.SetPositiveButton("OK", (sender, e) => { });

			alertDialog = builder.Create ();
			RunOnUiThread (() => {
				alertDialog.Show();
			} );
		}

		public void log (object message)
		{
			Shared.Util.log (message);
		}

		public async Task wait (int miliseconds)
		{
			await Shared.Util.wait (miliseconds);
		}

		public string readAsset (string resource)
		{
			try {
				return Shared.FileUtil.readFileContents (Assets.Open (resource, Android.Content.Res.Access.Buffer));
			} catch {
				return null;
			}
		}

        public const string HOCKEYAPP_APPID = "8aab8218a4e34d549e3a8b97cb639b04";

        public void setupAnalytics()
        {
            CrashManager.Register(this, HOCKEYAPP_APPID);
        }

		public void setBusy (bool busy)
		{
			if (busy) {
				showHUD ("Please wait");
			} else {
				hideHUD ();
			}
		}

		public void showHUD (string message)
		{
			try {
				var context = Xamarin.Forms.Forms.Context;

				AndroidHUD.AndHUD.Shared.Show (context, message, maskType: AndroidHUD.MaskType.Black);
			} catch (Exception e) {
				log (e);
			}
		}

		public void hideHUD ()
		{
			try {
				var context = Xamarin.Forms.Forms.Context;

				AndroidHUD.AndHUD.Shared.Dismiss (context);
			} catch (Exception e) {
				log (e);
			}
		}

		public void OnPageAppear ()
		{
			// Not needed for Android
		}

//		public static Android.Content.Context Context { get; private set; }
//
//		public override View OnCreateView(View parent, string name, Context context, IAttributeSet attrs)
//		{
//			MainActivity.Context = context;
//			return base.OnCreateView(parent, name, context, attrs);
//		}

		#endregion

		Intent serviceIntent;

		// Added by Khang
		public void OnLogout () {

		}

		public Size GetImageSize (string imagePath)
		{
			var bitmap = BitmapFactory.DecodeFile (imagePath);

			int imageHeight = bitmap.Height;
			int imageWidth = bitmap.Width;

			return new Size((double)imageWidth, (double)imageHeight);
		}

		public byte[] readBytesFromPath (string filePath)
		{
			return Shared.FileUtil.readBytesFromPath (filePath);
		}

		public byte[] readResourceBytesSync(string resource)
		{
			return Shared.FileUtil.readResourceBytesSync(resource);
		}

		public byte[] TakeScreenShot (Xamarin.Forms.View screenShotView, Size viewSize)
		{
//			// create tempfile
//			var timeStamp = Model.getServerDateTime();
//			var fileName = System.IO.Path.Combine ("Anno", "Anno-" + timeStamp.ToFileTimeUtc ().ToString ("X") + ".jpg");
//			var filePath = System.IO.Path.Combine (App.Native.getDocumentPath (), fileName);
//			var fileDir = System.IO.Path.GetDirectoryName (filePath);
//			Directory.CreateDirectory (fileDir);
//
//			SaveScreenShot (filePath, screenShotView, viewSize);
//
//			byte[] bytes = readBytesFromPath (filePath);
//
//			Shared.FileUtil.deleteFile (filePath);
//
//			return bytes;
//
//			view.DrawingCacheEnabled = true;
//
//			//Bitmap bitmap = view.GetDrawingCache(true);

			var rootView = ConvertFormsToNative(screenShotView, new Rectangle( 0, 0, viewSize.Width, viewSize.Height) );
			rootView.DrawingCacheEnabled = true;

			Bitmap bitmap = rootView.GetDrawingCache(true);

			byte[] bitmapData;

			using (var stream = new MemoryStream())
			{
				bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
				bitmapData = stream.ToArray();
			}

			return bitmapData;

		}

		public void SaveScreenShot (string filePath, Xamarin.Forms.View screenShotView, Size viewSize)
		{

			var rootView = ConvertFormsToNative(screenShotView, new Rectangle( 0, 0,viewSize.Width, viewSize.Height) );

			using (var screenshot = Android.Graphics.Bitmap.CreateBitmap(
				rootView.Width, 
				rootView.Height, 
				Android.Graphics.Bitmap.Config.Argb8888))
			{
				var canvas = new Android.Graphics.Canvas(screenshot);
				rootView.Draw(canvas);

				using (var screenshotOutputStream = new System.IO.FileStream(
					filePath, 
					System.IO.FileMode.Create))
				{
					//screenshot.Compress(Android.Graphics.Bitmap.CompressFormat.Png, 90, screenshotOutputStream);
					screenshot.Compress(Android.Graphics.Bitmap.CompressFormat.Jpeg, 45, screenshotOutputStream);
					screenshotOutputStream.Flush();
					screenshotOutputStream.Close();
				}
			}

		}

		public static ViewGroup ConvertFormsToNative(Xamarin.Forms.View view, Rectangle size)
		{
			var vRenderer = RendererFactory.GetRenderer (view);
			var viewGroup = vRenderer.ViewGroup;
			vRenderer.Tracker.UpdateLayout ();
			var layoutParams = new ViewGroup.LayoutParams ((int)size.Width, (int)size.Height);
			viewGroup.LayoutParameters = layoutParams;
			view.Layout (size);
			viewGroup.Layout (0, 0, (int)view.WidthRequest, (int)view.HeightRequest);
			return viewGroup; 
		}

		public async Task<JObject> CallRestApiAsync (string url)
		{
			return await Shared.Service.CallRestApiAsync(url);
		}

		public async Task<bool> CallPostRequest(string url, string data)
		{
			return await Shared.Service.CallPostRequest(url, data);
		}

		//public async Task<Types.Location> getLocation()
		//{
		//	return await Shared.Util.getLocation();
		//}

		//public async Task<string> getAddressFromLocation(Types.Location location)
		//{
		//	return await Shared.Util.getAddressFromLocation(location);
		//}

		public string readResourceSync(string name)
		{
			return Shared.FileUtil.readResourceSync(name);
		}

		public Size getPhysicalSize()
		{
			return new Size(800, 600); // will update later
		}

		public async Task<string> CallPeopleSoftRequest(string url, string data)
		{
			return await Shared.Service.CallPeopleSoftRequest(url, data);
		}

		public void WireUpBackgroundServiceTask()
		{
            serviceIntent = new Intent(this, typeof(LongRunningTaskService));
            StartService(serviceIntent);
		}


		public void ExtractZipFile(string archiveFilenameIn, string outFolder)
		{
			//Shared.FileUtil.ExtractZipFile(archiveFilenameIn, outFolder);
		}

		public async Task<string> callFinancialSoapAsync(string emplId)
		{
			return await Shared.Service.callFinancialSoapAsync(emplId);
		}

		public async Task<string> callSoapAsync(string xmlPackage, string action)
		{
			return await Shared.Service.callSoapAsync(xmlPackage, action);
		}

		public void setBusyWithCancel(bool busy)
		{
			setBusy(busy); // will update later
		}

		public void PushLocalNotification(string alertBody, string alertAction)
		{ 
			// will update later
		}

		public async Task<JObject> CallRestApiAsyncV1(string url)
		{
			return await Shared.Service.CallRestApiAsyncV1(url);
		}

		public async Task<bool> CallPutRequest(string url, string data)
		{
			return await Shared.Service.CallPutRequest(url, data);
		}

		public async Task<bool> CallPostRequestV1(string url, string data)
		{
			return await Shared.Service.CallPostRequestV1(url, data);
		}

		// Khang added for PO Receipt APIs
		public async Task<string> GetToken(string username, string password, string deviceName)
		{
			return await Shared.AIS.GetToken(username, password, deviceName);
		}

		public async Task<List<JObject>> GetPOJsonDataAsync(string token, string deviceName)
		{
			return await Shared.AIS.GetPOJsonDataAsync(token, deviceName);
		}

        public async Task<List<JObject>> SearchPOJsonDataAsync(string token, string deviceName, 
                                                                            string poNumber,
                                                                            bool typeEQUAL,
                                                                            string poType,
                                                                            string supplierName,
                                                                            string recent,
                                                                            DateTime fromD, DateTime toD,
                                                                            string projectCode,
                                                                            string user,
                                                                            string account)
        {
            return await Shared.AIS.SearchPOJsonDataAsync(token, deviceName, poNumber, typeEQUAL, poType, supplierName, recent, fromD, toD, projectCode, user, account);
        }

		public async Task<Dictionary<int, string>> GetSupplierLookupAsync(string token, string deviceName)
		{
			return await Shared.AIS.GetSuppliersLookup(token, deviceName);
		}

        public void UpdateConfigs(string hostname, string serviceRoot, string environment, string version, string deviceName)
        {
            Shared.ServiceConfiguration.JDE_HOSTNAME = hostname;
            Shared.ServiceConfiguration.JDE_SERVICE_ROOT = serviceRoot;
            Shared.ServiceConfiguration.JDE_ENVIRONMENT = environment;
            Shared.ServiceConfiguration.JDE_VERSION = version;
            Shared.ServiceConfiguration.JDE_DEVICE_NAME = deviceName;
			App.deviceName = deviceName;
        }

		public async Task<bool> ReceiveFullOrder(string token, string deviceName, int num, string type, string company)
		{
			return await Shared.AIS.ReceiveFullOrder(token, deviceName, num, type, company);
		}

		public async Task<bool> ReceivePartialOrder(string token, string deviceName, int poNum, string poType, string poCompany, Dictionary<float, JObject> lines)
		{
			return await Shared.AIS.ReceivePartialOrder(token, deviceName, poNum, poType, poCompany, lines);
		}

		public async Task<string> SetOrderLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string text)
		{
			return await Shared.AIS.SetOrderLineText(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, text);
		}

		public async Task<string> GetOrderLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum)
		{
			return await Shared.AIS.GetOrderLineText(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum);
		}

		public async Task<string> GetOrderText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix)
		{
			return await Shared.AIS.GetOrderText(token, deviceName, poNum, poType, poCompany, poSuffix);
		}

		public async Task<string> SetOrderText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, string text)
		{
			return await Shared.AIS.SetOrderText(token, deviceName, poNum, poType, poCompany, poSuffix, text);
		}

		public async Task<string> GetOrderFileList(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix)
		{
			return await Shared.AIS.GetOrderFileList(token, deviceName, poNum, poType, poCompany, poSuffix);
		}

		public async Task<string> GetOrderLineFileList(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum)
		{
			return await Shared.AIS.getOrderLineFileList(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum);
		}

        public async Task<string> SetLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string fileLocation, byte[] imageBytes)
        {
            return await Shared.AIS.SetLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, fileLocation, imageBytes);
        }

        public async Task<string> SetOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, string fileLocation, byte[] imageBytes)
        {
            return await Shared.AIS.SetOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, fileLocation, imageBytes);
        }

        public async Task<Stream> GetOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum)
        {
            return await Shared.AIS.GetOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, sequenceNum);
        }

        public async Task<Stream> GetLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum)
        {
            return await Shared.AIS.GetLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, sequenceNum);
        }

        public async Task<string> DeleteOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum)
        {
            return await Shared.AIS.DeleteOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, sequenceNum);
        }

        public async Task<string> DeleteLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum)
        {
            return await Shared.AIS.DeleteLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, sequenceNum);
        }

        public async Task<bool> Logout(string token) {
            return await Shared.AIS.Logout(token);
        }

	}

//	[Activity (Label = "urlentryclass")]
//	public class urlentryclass : Activity
//	{
//		protected override void OnCreate (Bundle bundle)
//		{
//			base.OnCreate (bundle);
//
//
//		}
//	}

}

