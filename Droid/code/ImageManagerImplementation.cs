﻿using System;
using System.IO;
using System.Threading.Tasks;
using Android.Graphics;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

namespace POReceipt.Droid
{
	public class ImageManagerImplementation : IImageManager
	{
		public ImageManagerImplementation()
		{
		}

		public string GetPictureFromDisk(int id)
		{
			throw new NotImplementedException();
		}

		public Task<string> SavePhotoToDisk(ImageSource imgSrc, int Id)
		{
			throw new NotImplementedException();
		}

		public async Task<string> SavePhotoToDisk(ImageSource imgSrc, string Id)

		{
			var renderer = new StreamImagesourceHandler();

			var photo = await renderer.LoadImageAsync(imgSrc, Android.App.Application.Context);

			var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

			string jpgFilename = System.IO.Path.Combine(documentsDirectory, Id + ".jpg");

			var stream = new FileStream(jpgFilename, FileMode.Create);

			photo.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);

			stream.Close();

			return jpgFilename;
		}
	}
}
