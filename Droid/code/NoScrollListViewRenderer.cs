using System;
using POReceipt;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(NoScrollListView), typeof(POReceipt.Droid.NoScrollListViewRenderer))]
namespace POReceipt.Droid
{
	public class NoScrollListViewRenderer: ListViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged(e);
			if (e.NewElement == null)
				return;

			if (Control != null)
			{
				
			}
		}
	}
}
