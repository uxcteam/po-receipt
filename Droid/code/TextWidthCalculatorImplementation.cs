﻿using System;
using Android.Content.Res;
using Android.Graphics;
using Android.Widget;
using POReceipt.Droid;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(TextWidthCalculatorImplementation))]
namespace POReceipt.Droid
{
	public class TextWidthCalculatorImplementation : ITextWidthCalculator
	{

		public double CalculateWidth(string text)
		{
			Rect bounds = new Rect();
			TextView textView = new TextView(Forms.Context);
			textView.TextSize = 12;
			textView.Paint.GetTextBounds(text, 0, text.Length, bounds);
			var length = bounds.Width();
            return length / Resources.System.DisplayMetrics.ScaledDensity;
		}
	}
}
