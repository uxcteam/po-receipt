using System;
using System.Threading;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer (typeof(POReceipt.NativeWebView), typeof(POReceipt.Droid.NativeWebViewRenderer))]
namespace POReceipt.Droid
{
	public class NativeWebViewRenderer : WebViewRenderer
	{
		public NativeWebViewRenderer () : base ()
		{
		}

		private bool init = false;

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			if (Control != null) {
				if (init) {
					return;
				}
				init = true;

				Control.Settings.BuiltInZoomControls = true;
				Control.Settings.DisplayZoomControls = true;
			}
		}
	}
}

