﻿using System;
using POReceipt;
using POReceipt.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace POReceipt.Droid
{
	public class CustomEditorRenderer : EditorRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				Control.SetPadding(10, 10, 10, 10);
				this.Control.SetBackgroundResource(Resource.Drawable.TextLineBG);
			}
		}
	}
}
