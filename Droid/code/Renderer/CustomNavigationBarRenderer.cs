﻿using System;
using Android.App;
using POReceipt.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.Platform.Android.AppCompat;

//[assembly: ExportRenderer(typeof(NavigationPage), typeof(CustomNavigationBarRenderer))]
namespace POReceipt.Droid
{
	public class CustomNavigationBarRenderer : NavigationRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<NavigationPage> e)
		{
			base.OnElementChanged(e);

			//ApplyCustomization();
		}

		protected override bool DrawChild(Android.Graphics.Canvas canvas, Android.Views.View child, long drawingTime)
		{
			//ApplyCustomization();
			return base.DrawChild(canvas, child, drawingTime);
		}

		void ApplyCustomization()
		{
			var activity = ((Activity)Context);
			var actionBar = activity.ActionBar;
			//actionBar.SetIcon(Resource.Drawable.icon);
			//actionBar.SetBackgroundDrawable(null);
			actionBar.Title = "  " + actionBar.Title;
		}
	}
}
