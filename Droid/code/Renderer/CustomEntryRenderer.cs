﻿using System;
using POReceipt;
using POReceipt.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace POReceipt.Droid
{
	public class CustomEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				Control.SetPadding(20, 10, 15, 10);
				this.Control.SetBackgroundResource(Resource.Drawable.TextLineBG);

				var customEntry = e.NewElement as CustomEntry;
				if (customEntry == null)
					return;
				if (customEntry.Type == CustomEntry.KeyboardType.Number)
				{
					this.Control.InputType = Android.Text.InputTypes.ClassPhone;
				}
			}
		}
	}
}
