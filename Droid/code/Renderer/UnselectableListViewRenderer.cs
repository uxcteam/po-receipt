﻿using System;
using POReceipt;
using POReceipt.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(UnselectableListView), typeof(UnselectableListViewRenderer))]
namespace POReceipt.Droid
{
	public class UnselectableListViewRenderer : ListViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				Control.SetSelector(Resource.Drawable.no_selector);
			}
		}
	}
}
