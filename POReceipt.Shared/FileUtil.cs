using System;
using Xamarin.Forms;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;

namespace POReceipt.Shared
{
	public static class FileUtil
	{
		public static string checkFile (string resource)
		{
			var filePath = Path.Combine (getDocumentPath (), resource);

			if (System.IO.File.Exists (filePath)) {
				if (new FileInfo (filePath).Length > 0) {
					return filePath;
				}
			}

			return null;
		}

		public static string readFileContents (Stream stream)
		{
			using (var buffer = new MemoryStream ()) {
				using (stream) {
					if (stream != null) {
						stream.CopyTo (buffer);
					}
				}

				return ConvUtil.getString (buffer.ToArray ());
			}
		}

		public static async Task<string> readFileContentsAsync (Stream stream)
		{
			using (var buffer = new MemoryStream ()) {
				using (stream) {
					if (stream != null) {
						await stream.CopyToAsync (buffer);
					}
				}

				return ConvUtil.getString (buffer.ToArray ());
			}
		}

		public static DateTime timeFile (string resource)
		{
			var filePath = Path.Combine (getDocumentPath (), resource);

			if (System.IO.File.Exists (filePath)) {
				return System.IO.File.GetLastWriteTime (filePath);
			}

			return DateTime.MinValue;
		}

		public static string readFile (string resource)
		{
			var filePath = Path.Combine (getDocumentPath (), resource);

			if (System.IO.File.Exists (filePath)) {
				return System.IO.File.ReadAllText (filePath);
			} else {
				return null;
			}
		}

		public static byte[] readBytes (string resource)
		{
			var filePath = Path.Combine (getDocumentPath (), resource);

			if (System.IO.File.Exists (filePath)) {
				return System.IO.File.ReadAllBytes (filePath);
			} else {
				return null;
			}
		}

		public static void deleteFile (string resource)
		{
			var filePath = Path.Combine (getDocumentPath (), resource);

			if (System.IO.File.Exists (filePath)) {
				System.IO.File.Delete (filePath);
			}
		}

		public static string[] getFiles (string resource, string pattern = null)
		{
			var path = Path.Combine (getDocumentPath (), resource);

			try {
				if (Directory.Exists (path)) {
					string[] files;
					if (string.IsNullOrWhiteSpace (pattern)) {
						files = Directory.GetFiles (path);
					} else {
						files = Directory.GetFiles (path, pattern);
					}

					if (files != null) {
						return files;
					}
				}
			} catch (Exception e) {
				Util.log (e);
			}

			return new string[0];
		}

		public static string[] getDirectories(string resource, string pattern = null)
		{
			var path = Path.Combine (getDocumentPath (), resource);

			try {
				if (Directory.Exists (path)) {
					string[] dirs;
					if (string.IsNullOrWhiteSpace (pattern)) {
						dirs = Directory.GetDirectories (path);
					} else {
						dirs = Directory.GetDirectories (path, pattern);
					}

					if (dirs != null) {
						return dirs;
					}
				}
			} catch (Exception e) {
				Util.log (e);
			}

			return new string[0];
		}

		public static void deleteDirectory (string resource)
		{
			var path = Path.Combine (getDocumentPath (), resource);

			if (Directory.Exists (path)) {
				Directory.Delete (path, true);
			}
		}

		public static void writeFile (string resource, string content)
		{
			var filePath = Path.Combine (getDocumentPath (), resource);

			var fileDir = Path.GetDirectoryName (filePath);

			Directory.CreateDirectory (fileDir);

			System.IO.File.WriteAllText (filePath, content);
		}

		public static string writeFile (string resource, byte[] bytes)
		{
			var filePath = Path.Combine (getDocumentPath (), resource);

			var fileDir = Path.GetDirectoryName (filePath);

			Directory.CreateDirectory (fileDir);

			System.IO.File.WriteAllBytes (filePath, bytes);

			return filePath;
		}

		public static string getDocumentPath ()
		{
			var path = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);

			if (Device.OS == TargetPlatform.iOS) {
				path = Path.Combine (path, "../Library");
			}

			//path = Path.Combine (path, App.Login.ServerID);
			path = Path.Combine (path, App.VERSION_NUMBER);	// Added by Khang

			return path;
		}

		public static bool doesResourceExist (string name)
		{
			var exists = false;

			#if __ANDROID__
			try {
				using (var stream = Forms.Context.Assets.Open (name)) {
					exists = true;
					stream.Close ();
				}
			} catch {
			}
			#else
			exists = System.IO.File.Exists (name);
			#endif

			return exists;
		}

		public static async Task<string> readResource (string name)
		{
			var content = string.Empty;

			#if __ANDROID__
			var taskCompletion = new TaskCompletionSource<string> ();

			Device.BeginInvokeOnMainThread (async () => {
				using (var outStream = new MemoryStream ()) {
					using (var stream = Forms.Context.Assets.Open (name, Android.Content.Res.Access.Buffer)) {
						await stream.CopyToAsync (outStream);
						stream.Close ();
					}

					await outStream.FlushAsync ();
					var bytes = outStream.ToArray ();
					var str = System.Text.Encoding.UTF8.GetString (bytes);

					taskCompletion.SetResult (str);
				}
			});

			content = await taskCompletion.Task;
			#else
			content = System.IO.File.ReadAllText (name);
			#endif

			return content;
		}

		public static async Task<byte[]> readResourceBytes (string name)
		{
			byte[] content = null;

			#if __ANDROID__
			var taskCompletion = new TaskCompletionSource<byte[]> ();

			Device.BeginInvokeOnMainThread (async () => {
				using (var outStream = new MemoryStream ()) {
					using (var stream = Forms.Context.Assets.Open (name, Android.Content.Res.Access.Buffer)) {
						await stream.CopyToAsync (outStream);
						stream.Close ();
					}

					await outStream.FlushAsync ();
					var bytes = outStream.ToArray ();

					taskCompletion.SetResult (bytes);
				}
			});

			content = await taskCompletion.Task;
			#else
			content = System.IO.File.ReadAllBytes (name);
			#endif

			return content;
		}

		public static byte[] readResourceBytesSync(string name)
		{
			byte[] content = null;

#if __ANDROID__

				using (var outStream = new MemoryStream())
				{
					using (var stream = Forms.Context.Assets.Open(name, Android.Content.Res.Access.Buffer))
					{
						stream.CopyTo(outStream);
						stream.Close();
					}

					outStream.Flush();
					var bytes = outStream.ToArray();

					return bytes;
				}


#else
			content = System.IO.File.ReadAllBytes (name);
#endif

			return content;
		}

		public static byte[] readBytesFromPath (string filePath)
		{
			if (System.IO.File.Exists (filePath)) {
				return System.IO.File.ReadAllBytes (filePath);
			} else {
				return null;
			}
		}

		public static string writeFileToPath (string filePath, byte[] bytes)
		{
			var fileDir = Path.GetDirectoryName (filePath);

			Directory.CreateDirectory (fileDir);

			System.IO.File.WriteAllBytes (filePath, bytes);

			return filePath;
		}

		public static string readResourceSync(string name)
		{
			var content = string.Empty;

#if __ANDROID__

			using (var outStream = new MemoryStream ()) {
				using (var stream = Forms.Context.Assets.Open (name, Android.Content.Res.Access.Buffer)) {
					stream.CopyTo(outStream);
					stream.Close();
				}

				outStream.Flush();
				var bytes = outStream.ToArray ();
				content = System.Text.Encoding.UTF8.GetString (bytes);
			}

#else
			content = System.IO.File.ReadAllText(name);
#endif
			return content;
		}

		public static void ExtractZipFile(string archiveFilenameIn, string outFolder) {
		    ZipFile zf = null;
		    try {
		        FileStream fs = System.IO.File.OpenRead(archiveFilenameIn);
		        zf = new ZipFile(fs);
		        //if (!String.IsNullOrEmpty(password)) {
		        //    zf.Password = password;     // AES encrypted entries are handled automatically
		        //}
		        foreach (ZipEntry zipEntry in zf) {
		            if (!zipEntry.IsFile) {
		                continue;           // Ignore directories
		            }
		            String entryFileName = zipEntry.Name;
		            // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
		            // Optionally match entrynames against a selection list here to skip as desired.
		            // The unpacked length is available in the zipEntry.Size property.

		            byte[] buffer = new byte[4096];     // 4K is optimum
		            Stream zipStream = zf.GetInputStream(zipEntry);

		            // Manipulate the output filename here as desired.
		            String fullZipToPath = Path.Combine(outFolder, entryFileName);
		            string directoryName = Path.GetDirectoryName(fullZipToPath);
		            if (directoryName.Length > 0)
		                Directory.CreateDirectory(directoryName);

		            // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
		            // of the file, but does not waste memory.
		            // The "using" will close the stream even if an exception occurs.
		            using (FileStream streamWriter = System.IO.File.Create(fullZipToPath)) {
		                StreamUtils.Copy(zipStream, streamWriter, buffer);
		            }
		        }
		    } finally {
		        if (zf != null) {
		            zf.IsStreamOwner = true; // Makes close also shut the underlying stream
		            zf.Close(); // Ensure we release resources
		        }
		    }
		}

	}
}

