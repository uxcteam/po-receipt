using System;
using System.IO;
using System.Xml;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace POReceipt.Shared
{
	public static class XmlUtil
	{
		public static List<XmlNode> getNodes (XmlNode source, string xpath)
		{
			var list = new List<XmlNode> ();

			getNodes (source, xpath, list.Add);

			return list;
		}

		public static void getNodes (XmlNode source, string xpath, Action<XmlNode> onEach)
		{
			if (onEach == null) {
				return;
			}

			var nodes = source.SelectNodes (xpath);

			if (nodes != null && nodes.Count > 0) {
				foreach (XmlNode node in nodes) {
					if (node != null) {
						onEach.Invoke (node);
					}
				}
			}
		}

		public static void getNode (XmlNode source, string xpath, Action<XmlNode> onNode)
		{
			if (onNode == null) {
				return;
			}

			var node = source.SelectSingleNode (xpath);

			if (node != null) {
				onNode.Invoke (node);
			}
		}

		public static string getValue (XmlNode elem, string xpath)
		{
			var node = elem.SelectSingleNode (xpath);
			if (node != null) {
				return (node.InnerText ?? "").Trim ();
			}
			return string.Empty;
		}

		public static XmlDocument getDocument (string xml)
		{
			var doc = new XmlDocument {
				PreserveWhitespace = false
			};

			if (string.IsNullOrWhiteSpace (xml)) {
				return doc;
			}

			var settings = new XmlReaderSettings {
				ConformanceLevel = ConformanceLevel.Document,
				IgnoreComments = true,
				IgnoreProcessingInstructions = true,
				IgnoreWhitespace = true,
			};
			var reader = XmlReader.Create (new StringReader (xml), settings);

			doc.Load (reader);
			doc.Normalize ();

			return doc;
		}

		public static string escapeURL (string str)
		{
			return System.Net.WebUtility.UrlEncode (str ?? "");
		}

		public static string escapeHTML (string str)
		{
			return System.Net.WebUtility.HtmlEncode (str ?? "");
		}

		public static string unescapeHTML (string str)
		{
			return System.Net.WebUtility.HtmlDecode (str ?? "");
		}

		public static string escapeXML (string xml)
		{
			if (string.IsNullOrWhiteSpace (xml)) {
				return string.Empty;
			}
			xml = xml.Trim ();

			var writer = new StringWriter ();

			using (var xtw = new XmlTextWriter (writer)) {
				xtw.WriteString (xml);
			}

			return writer.ToString ();
		}

		public static string unescapeXML (string xml)
		{
			if (string.IsNullOrWhiteSpace (xml)) {
				return string.Empty;
			}

			StringReader reader = new StringReader (xml);

			var settings = new XmlReaderSettings {
				ConformanceLevel = ConformanceLevel.Fragment,
				IgnoreComments = true,
				IgnoreWhitespace = true,
			};

			var str = string.Empty;
			using (XmlReader xmlReader = XmlReader.Create (reader, settings)) {
				xmlReader.MoveToContent ();

				str = xmlReader.ReadString ();
			}

			return (str ?? "").Trim ();
		}

		public static string stripTag (string source, string tag)
		{
			return ConvUtil.strip (source, "<" + tag + ">", "</" + tag + ">");
		}

		public static string cleanXML (string xml)
		{
			if (string.IsNullOrWhiteSpace (xml)) {
				return string.Empty;
			}

			try {
				var doc = parseXML (xml);
				xml = doc.InnerXml;

				var xdoc = XDocument.Parse (xml, LoadOptions.None);
				//var xdoc = XDocument.Parse (UnescapeXml(xml), LoadOptions.None);
			
				xml = xdoc.ToString ();
			} catch (Exception e) {
				Util.log (e);
			}

			return xml;
		}

		// Added by Khang
		public static string UnescapeXml(string s )
		{
			string encodedXml = s;
			if ( !string.IsNullOrEmpty( encodedXml ) )
			{
				encodedXml = encodedXml.Replace("/<", "&lt;");
			}
			return encodedXml;
		}

		public static XmlDocument parseXML (string xml)
		{
			xml = stripPrologue (xml);

			var reader = XmlReader.Create (ConvUtil.toStream (xml), new XmlReaderSettings { 
				ConformanceLevel = ConformanceLevel.Document,
				IgnoreComments = true,
				IgnoreProcessingInstructions = true,
				IgnoreWhitespace = true,
			});

			var doc = new XmlDocument {
				PreserveWhitespace = false,
			};
			doc.Load (reader);
			doc.Normalize ();

			return doc;
		}

		public static T parseXMLObject<T> (string xml) where T : class
		{
			var reader = XmlReader.Create (ConvUtil.toStream (xml), new XmlReaderSettings { 
				ConformanceLevel = ConformanceLevel.Document,
				IgnoreComments = true,
				IgnoreProcessingInstructions = true,
				IgnoreWhitespace = true,
			});
			return new XmlSerializer (typeof(T)).Deserialize (reader) as T;
		}

		public static string toXMLObject<T> (T obj)
		{
			var stringWriter = new StringWriter ();
			var writer = XmlWriter.Create (stringWriter, new XmlWriterSettings { 
				ConformanceLevel = ConformanceLevel.Document,
				OmitXmlDeclaration = true,
				Indent = false,
			});
			new XmlSerializer (typeof(T)).Serialize (writer, obj);
			writer.Flush ();
			return stringWriter.ToString ();
		}

		public static string stripPrologue (string xml)
		{	
			if (string.IsNullOrWhiteSpace (xml)) {
				return "";
			}
			xml = xml.Trim ();

			var posStart = xml.IndexOf ("<?");
			if (posStart == -1) {
				return xml;
			}

			var posEnd = xml.IndexOf ("?>", posStart);
			if (posEnd == -1) {
				return xml;
			}

			if (posEnd > posStart && posEnd + 2 <= xml.Length) {
				xml = xml.Substring (posEnd + 2).Trim ();
			}

			return xml;
		}
	}
}

