﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using ModernHttpClient;
using Newtonsoft.Json.Linq;

namespace POReceipt.Shared
{
    public class PurchaseOrders
    {
        string CONTROLIDS_FINDORDERS_APPSTACK = "1[10,11,12,14,16,40,43,90,91,92,25,87,17,26,34]";
        string CONTROLIDS_SELECTRECEIVEORDERS = "1[382,103,116,54,119,117,38,129,39,40,42,47,43,411,44,83]";
        string CONTROLIDS_FINDORDERS_DATASERVICE = "F4301.DOCO|F4301.DCTO|F4301.KCOO|F4301.SFXO|F4301.TRDJ|F4301.AN8|F4301.MCU|F4301.ORBY|F4301.RMK|F4311.LNID|F4311.LNTY|F4311.DSC1|F4311.DSC2|F4311.UOPN|F4311.UOM|F4311.AOPN|F4311.ANI|F4311.ITM|F4311.UORG|F4301.HOLD|F4311.WVID|F0101.ALPH";

        private string targetRootURL;
        private string targetAppstackURL;
        private string targetAppstackNextURL;
        private string targetPoservice;
        private string targetDataservice;
        private string targetDataserviceNext;


        public PurchaseOrders () {
            targetRootURL = string.Format("{0}{1}", ServiceConfiguration.JDE_HOSTNAME, ServiceConfiguration.JDE_SERVICE_ROOT);
            targetAppstackURL = string.Format("{0}{1}", targetRootURL, ServiceConfiguration.JDE_GETPOS);
            targetAppstackNextURL = string.Format("{0}{1}", targetRootURL, ServiceConfiguration.JDE_GETPOS_NEXT);

            targetPoservice = string.Format("{0}{1}", targetRootURL, ServiceConfiguration.JDE_POSERVICE);
            targetDataservice = string.Format("{0}{1}", targetRootURL, ServiceConfiguration.JDE_DATASERVICE);
            targetDataserviceNext = string.Format("{0}{1}", targetRootURL, ServiceConfiguration.JDE_DATASERVICE_NEXT);
        }


        public async System.Threading.Tasks.Task<List<JObject>> SearchAsync(string token, string deviceName, 
                                                                            string poNumber,
                                                                            bool typeEQUAL,
                                                                            string poType, 
                                                                            string supplierName, 
                                                                            string recent, 
                                                                            DateTime fromD, DateTime toD, 
                                                                            string projectCode,
                                                                            string user,
                                                                            string account)
        {
            List<JObject> rawLines = new List<JObject>();
            int page = 1; // First page request is a POST; subsequent page requests are GETs
            bool hasMoreRecords = true;
            int stackId = 0;
            int stateId = 0;
            string rid = "";

            // First lookup the three status codes used for 'open' POs and order type code
            var codesResponse = await JDEGetOpenStatusCodes(token, deviceName);
            if (!codesResponse.IsSuccessStatusCode)
            {
                App.Native.showAlert(string.Format("[JDEGetOpenStatusCodes]{0} - {1}", codesResponse.StatusCode.ToString(), codesResponse.ReasonPhrase), "Error");
                return rawLines;

            }
            var statusBodyStr = await codesResponse.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(statusBodyStr))
                return rawLines;
            var statusBodyJson = JObject.Parse(statusBodyStr);

            try {
                string code1 = statusBodyJson["processingOptions"]["szPO_ValidInStatus1_4"]["value"].Value<string>();
                string code2 = statusBodyJson["processingOptions"]["szPO_ValidInStatus2_10"]["value"].Value<string>();
                string code3 = statusBodyJson["processingOptions"]["szPO_ValidInStatus3_11"]["value"].Value<string>();
                string orderTypeCode = statusBodyJson["processingOptions"]["szPO_OrderTypeDef_2"]["value"].Value<string>();
                string processByValue = statusBodyJson["processingOptions"]["szProcessBy_42"]["value"].Value<string>();

                while (hasMoreRecords)
                {
                    var response = await JDESearchOpenOrders(token, deviceName, page, stackId, stateId, rid, code1, code2, code3, poNumber, orderTypeCode, typeEQUAL, poType, supplierName, recent, fromD, toD, projectCode, user, account);

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        App.Native.showAlert(string.Format("[JDEFindOpenOrders]{0} - {1}", response.StatusCode.ToString(), response.ReasonPhrase), "Error");
                        return rawLines;
                    }
                    var bodyStr = await response.Content.ReadAsStringAsync();
                    if (string.IsNullOrEmpty(bodyStr))
                        return rawLines;
                    var bodyJson = JObject.Parse(bodyStr);


                    if (bodyJson["fs_DATABROWSE_VQ37P431"] == null)
                    {
                        App.Native.showAlert("[JDEFindOpenOrders] - fs_DATABROWSE_VQ37P431 not found in the JSON data", "Error");
                        return rawLines;
                    }

                    var rowsetArray = bodyJson["fs_DATABROWSE_VQ37P431"]["data"]["gridData"]["rowset"].Value<JArray>();
                    //var rowsetArray = bodyJson["fs_DATABROWSE_V4301R"]["data"]["gridData"]["rowset"].Value<JArray>(); // old 

                    if (rowsetArray != null)
                    {
                        var array = rowsetArray.Select(jv => (JObject)jv).ToArray();
                        foreach (var row in array)
                        {
                            row.Add("ProcessBy", processByValue);
                        }

                        rawLines.AddRange(array);
                    }

                    hasMoreRecords = bodyJson["fs_DATABROWSE_VQ37P431"]["data"]["gridData"]["summary"]["moreRecords"].Value<bool>();
                    stackId = bodyJson["stackId"].Value<int>();
                    stateId = bodyJson["stateId"].Value<int>();
                    rid = bodyJson["rid"].Value<string>();
                    page++;
                }
            } catch (Exception ex) {
                App.Native.showAlert(string.Format("{0} - {1}", "Exception error", ex.Message), "Error");
            }


            return rawLines;
        }


        public async System.Threading.Tasks.Task<List<JObject>> GetOpenAsync(string token, string deviceName)
        {
            List<JObject> rawLines = new List<JObject>();
            int page = 1; // First page request is a POST; subsequent page requests are GETs
            bool hasMoreRecords = true;
            int stackId = 0;
            int stateId = 0;
            string rid = "";

            // First lookup the three status codes used for 'open' POs and order type code
            var codesResponse = await JDEGetOpenStatusCodes(token, deviceName);
            if (!codesResponse.IsSuccessStatusCode)
            {
                App.Native.showAlert(string.Format("[JDEGetOpenStatusCodes]{0} - {1}", codesResponse.StatusCode.ToString(), codesResponse.ReasonPhrase), "Error");
                return rawLines;

            }
            var statusBodyStr = await codesResponse.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(statusBodyStr))
                return rawLines;
            var statusBodyJson = JObject.Parse(statusBodyStr);
            try {
                string code1 = statusBodyJson["processingOptions"]["szPO_ValidInStatus1_4"]["value"].Value<string>();
                string code2 = statusBodyJson["processingOptions"]["szPO_ValidInStatus2_10"]["value"].Value<string>();
                string code3 = statusBodyJson["processingOptions"]["szPO_ValidInStatus3_11"]["value"].Value<string>();
                string orderTypeCode = statusBodyJson["processingOptions"]["szPO_OrderTypeDef_2"]["value"].Value<string>();
                string processByValue = statusBodyJson["processingOptions"]["szProcessBy_42"]["value"].Value<string>();

                while (hasMoreRecords)
                {
                    var response = await JDEFindAllOpenOrders(token, deviceName, page, stackId, stateId, rid, code1, code2, code3, orderTypeCode);

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        App.Native.showAlert(string.Format("[JDEFindOpenOrders]{0} - {1}", response.StatusCode.ToString(), response.ReasonPhrase), "Error");
                        return rawLines;
                    }
                    var bodyStr = await response.Content.ReadAsStringAsync();
                    if (string.IsNullOrEmpty(bodyStr))
                        return rawLines;
                    var bodyJson = JObject.Parse(bodyStr);


                    if (bodyJson["fs_DATABROWSE_VQ37P431"] == null)
                    {
                        App.Native.showAlert("[JDEFindOpenOrders] - fs_DATABROWSE_VQ37P431 not found in the JSON data", "Error");
                        return rawLines;
                    }

                    var rowsetArray = bodyJson["fs_DATABROWSE_VQ37P431"]["data"]["gridData"]["rowset"].Value<JArray>();
                    //var rowsetArray = bodyJson["fs_DATABROWSE_V4301R"]["data"]["gridData"]["rowset"].Value<JArray>(); // old 

                    if (rowsetArray != null)
                    {
                        var array = rowsetArray.Select(jv => (JObject)jv).ToArray();
                        foreach (var row in array)
                        {
                            row.Add("ProcessBy", processByValue);
                        }

                        rawLines.AddRange(array);
                    }

                    hasMoreRecords = bodyJson["fs_DATABROWSE_VQ37P431"]["data"]["gridData"]["summary"]["moreRecords"].Value<bool>();
                    stackId = bodyJson["stackId"].Value<int>();
                    stateId = bodyJson["stateId"].Value<int>();
                    rid = bodyJson["rid"].Value<string>();
                    page++;
                }
            } catch (Exception ex) {
                App.Native.showAlert(string.Format("{0} - {1}", "Exception error", ex.Message), "Error"); 
            }

            return rawLines;
        }

        private async Task<HttpResponseMessage> JDESearchOpenOrders(string token, string deviceName, 
                                                                    int page, int stackId, int stateId, 
                                                                    string rid, string code1, string code2, string code3, 
                                                                    string poNumber,
                                                                    string orderTypeCode,
                                                                    bool typeEQUAL,
                                                                    string poType,
                                                                    string supplierName,
                                                                    string recent,
                                                                    DateTime fromD, DateTime toD,
                                                                    string projectCode,
                                                                    string user,
                                                                    string account
                                                                    )
        {
            Uri uri = new Uri(targetDataservice);
            string bodyStr;
            if (page == 1)
            {
                bodyStr = MakeJsonPost_searchOpenOrders(token, deviceName, code1, code2, code3, poNumber, orderTypeCode, typeEQUAL, poType, supplierName, recent, fromD, toD, projectCode, user, account);
            }
            else
            {
                var parameters = new StringBuilder();
                parameters.Append(string.Format("?{0}={1}", "stackId", stackId));
                parameters.Append(string.Format("&{0}={1}", "stateId", stateId));
                parameters.Append(string.Format("&{0}={1}", "rid", Uri.EscapeDataString(rid)));
                parameters.Append(string.Format("&{0}={1}", "fullGridId", 1));
                parameters.Append(string.Format("&{0}={1}", "formOID", "V4301R"));
                parameters.Append(string.Format("&{0}={1}", "outputType", "GRID_DATA"));
                parameters.Append(string.Format("&{0}={1}", "token", Uri.EscapeDataString(token)));
                parameters.Append(string.Format("&{0}={1}", "deviceName", deviceName));
                parameters.Append(string.Format("&{0}={1}", "returnControlIDs", Uri.EscapeDataString(CONTROLIDS_FINDORDERS_DATASERVICE)));

                var url = string.Format("{0}{1}", targetDataserviceNext, parameters);
                uri = new Uri(url);
                var getClient = new HttpClient(new NativeMessageHandler());
                getClient.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;
                return await getClient.GetAsync(uri);
            }

            var client = new HttpClient(new NativeMessageHandler());
            client.MaxResponseContentBufferSize = ServiceConfiguration.MaxResponseContentBufferSize;
            client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;

            var content = new StringContent(bodyStr, Encoding.UTF8, "application/json");

            return await client.PostAsync(uri, content);
        }


        private async Task<HttpResponseMessage> JDEFindAllOpenOrders(string token, string deviceName, int page, int stackId, int stateId, string rid, string code1, string code2, string code3, string orderTypeCode)
        {
            Uri uri = new Uri(targetDataservice);
            string bodyStr;
            if (page == 1)
            {
                bodyStr = MakeJsonPost_findAllOpenOrders(token, deviceName, code1, code2, code3, orderTypeCode);
            }
            else
            {
                var parameters = new StringBuilder();
                parameters.Append(string.Format("?{0}={1}", "stackId", stackId));
                parameters.Append(string.Format("&{0}={1}", "stateId", stateId));
                parameters.Append(string.Format("&{0}={1}", "rid", Uri.EscapeDataString(rid)));
                parameters.Append(string.Format("&{0}={1}", "fullGridId", 1));
                parameters.Append(string.Format("&{0}={1}", "formOID", "V4301R"));
                parameters.Append(string.Format("&{0}={1}", "outputType", "GRID_DATA"));
                parameters.Append(string.Format("&{0}={1}", "token", Uri.EscapeDataString(token)));
                parameters.Append(string.Format("&{0}={1}", "deviceName", deviceName));
                parameters.Append(string.Format("&{0}={1}", "returnControlIDs", Uri.EscapeDataString(CONTROLIDS_FINDORDERS_DATASERVICE)));

                var url = string.Format("{0}{1}", targetDataserviceNext, parameters);
                uri = new Uri(url);
                var getClient = new HttpClient(new NativeMessageHandler());
                getClient.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;
                return await getClient.GetAsync(uri);
            }

            var client = new HttpClient(new NativeMessageHandler());
            client.MaxResponseContentBufferSize = ServiceConfiguration.MaxResponseContentBufferSize;
            client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;

            var content = new StringContent(bodyStr, Encoding.UTF8, "application/json");

            return await client.PostAsync(uri, content);
        }

        private string MakeJsonPost_findAllOpenOrders(string token, string deviceName, string code1, string code2, string code3, string orderTypeCode)
        {

            JObject objBuilder = new JObject();
            objBuilder.Add("token", token);
            objBuilder.Add("deviceName", deviceName);
            objBuilder.Add("targetName", "VQ37P431"); // applied business view
            //objBuilder.Add("targetName", "V4301R"); // old target

            objBuilder.Add("targetType", "view");
            objBuilder.Add("outputType", "GRID_DATA");
            objBuilder.Add("dataServiceType", "BROWSE");
            objBuilder.Add("maxPageSize", "5000");
            objBuilder.Add("returnControlIDs", CONTROLIDS_FINDORDERS_DATASERVICE);
            objBuilder.Add("enableNextPageProcessing", true);
            objBuilder.Add("nextPageTimeInterval1", "0");

            var query = new JObject();
            var complexQuery = new JArray();
            var complexQueryItem1 = new JObject();
            var complexQueryItem1_query = new JObject();
            var complexQueryItem1_query_conditions = new JArray();

            if (!orderTypeCode.Equals(" ") && !orderTypeCode.Equals("*")) {
                var complexQueryItem1_query_conditions_item1 = new JObject();
                complexQueryItem1_query_conditions_item1.Add("controlId", "F4301.DCTO");
                complexQueryItem1_query_conditions_item1.Add("operator", "EQUAL");
                var complexQueryItem1_query_conditions_item1_values = new JArray();
                var complexQueryItem1_query_conditions_item1_values_item1 = new JObject();
                complexQueryItem1_query_conditions_item1_values_item1.Add("specialValueId", "LITERAL");
                complexQueryItem1_query_conditions_item1_values_item1.Add("content", orderTypeCode);
                complexQueryItem1_query_conditions_item1_values.Add(complexQueryItem1_query_conditions_item1_values_item1);
                complexQueryItem1_query_conditions_item1.Add("value", complexQueryItem1_query_conditions_item1_values);
                complexQueryItem1_query_conditions.Add(complexQueryItem1_query_conditions_item1);
            }


         

            var complexQueryItem1_query_conditions_item2 = new JObject();
            complexQueryItem1_query_conditions_item2.Add("controlId","F4311.NXTR");
            complexQueryItem1_query_conditions_item2.Add("operator", "LIST");
            var complexQueryItem1_query_conditions_item2_values = new JArray();
            var complexQueryItem1_query_conditions_item2_values_item1 = new JObject();
            complexQueryItem1_query_conditions_item2_values_item1.Add("specialValueId", "LITERAL");
            complexQueryItem1_query_conditions_item2_values_item1.Add("content", code1);

            var complexQueryItem1_query_conditions_item2_values_item2 = new JObject();
            complexQueryItem1_query_conditions_item2_values_item2.Add("specialValueId", "LITERAL");
            complexQueryItem1_query_conditions_item2_values_item2.Add("content", code2);
            var complexQueryItem1_query_conditions_item2_values_item3 = new JObject();
            complexQueryItem1_query_conditions_item2_values_item3.Add("specialValueId", "LITERAL");
            complexQueryItem1_query_conditions_item2_values_item3.Add("content", code3);

            complexQueryItem1_query_conditions_item2_values.Add(complexQueryItem1_query_conditions_item2_values_item1);
            complexQueryItem1_query_conditions_item2_values.Add(complexQueryItem1_query_conditions_item2_values_item2);
            complexQueryItem1_query_conditions_item2_values.Add(complexQueryItem1_query_conditions_item2_values_item3);
            complexQueryItem1_query_conditions_item2.Add("value", complexQueryItem1_query_conditions_item2_values);


            complexQueryItem1_query_conditions.Add(complexQueryItem1_query_conditions_item2);

            complexQueryItem1_query.Add("matchType", "MATCH_ALL");
            complexQueryItem1_query.Add("condition", complexQueryItem1_query_conditions);
            complexQueryItem1.Add("query", complexQueryItem1_query);
            complexQuery.Add(complexQueryItem1);
            query.Add("complexQuery", complexQuery);
            objBuilder.Add("query", query);

            return objBuilder.ToString(Newtonsoft.Json.Formatting.None);
            }


        private string MakeJsonPost_searchOpenOrders(string token, string deviceName, 
                                                     string code1, string code2, string code3,
                                                     string poNumber,
                                                     string orderTypeCode,
                                                     bool typeEQUAL,
                                                     string poType,
                                                     string supplierName,
                                                     string recent,
                                                     DateTime fromD, DateTime toD,
                                                     string projectCode,
                                                     string user,
                                                     string account
                                                        )
        {

            JObject objBuilder = new JObject();
            objBuilder.Add("token", token);
            objBuilder.Add("deviceName", deviceName);
            objBuilder.Add("targetName", "VQ37P431"); // applied business view
            //objBuilder.Add("targetName", "V4301R"); // old target

            objBuilder.Add("targetType", "view");
            objBuilder.Add("outputType", "GRID_DATA");
            objBuilder.Add("dataServiceType", "BROWSE");
            objBuilder.Add("maxPageSize", "5000");
            objBuilder.Add("returnControlIDs", CONTROLIDS_FINDORDERS_DATASERVICE);
            objBuilder.Add("enableNextPageProcessing", true);
            objBuilder.Add("nextPageTimeInterval1", "0");

            var query = new JObject();
            var complexQuery = new JArray();
            var complexQueryItem1 = new JObject();
            var complexQueryItem1_query = new JObject();
            var complexQueryItem1_query_conditions = new JArray();

            //var complexQueryItem1_query_conditions_item1 = new JObject();
            //complexQueryItem1_query_conditions_item1.Add("controlId", "F4301.DCTO");
            //complexQueryItem1_query_conditions_item1.Add("operator", "EQUAL");
            //var complexQueryItem1_query_conditions_item1_values = new JArray();

            // Khang added to check the TYPE LOGICS
            if (!orderTypeCode.Equals(" ") && !orderTypeCode.Equals("*")) {

                if (!string.IsNullOrEmpty(poType)) {
                    if (typeEQUAL && !poType.Equals(orderTypeCode))
                        complexQueryItem1_query_conditions.Add(MakeTwoConditionsJsonObject("F4301.DCTO", "LIST", "LITERAL", orderTypeCode, "LITERAL", poType));
                    else if (!typeEQUAL && !poType.Equals(orderTypeCode))
                        complexQueryItem1_query_conditions.Add(MakeConditionJsonObject("F4301.DCTO", "EQUAL", "LITERAL", orderTypeCode));
                    else if (!typeEQUAL && poType.Equals(orderTypeCode)) {
                        complexQueryItem1_query_conditions.Add(MakeConditionJsonObject("F4301.DCTO", "NOT_EQUAL", "LITERAL", poType));
                    }
                } else {
                    complexQueryItem1_query_conditions.Add(MakeConditionJsonObject("F4301.DCTO", "EQUAL", "LITERAL", orderTypeCode));    
                }
            } else if (!string.IsNullOrEmpty(poType)) {
                var operatorCommand = typeEQUAL ? "EQUAL" : "NOT_EQUAL";
                complexQueryItem1_query_conditions.Add(MakeConditionJsonObject("F4301.DCTO", operatorCommand, "LITERAL", poType));
            }


            var complexQueryItem1_query_conditions_item2 = new JObject();
            complexQueryItem1_query_conditions_item2.Add("controlId", "F4311.NXTR");
            complexQueryItem1_query_conditions_item2.Add("operator", "LIST");
            var complexQueryItem1_query_conditions_item2_values = new JArray();
            var complexQueryItem1_query_conditions_item2_values_item1 = new JObject();
            complexQueryItem1_query_conditions_item2_values_item1.Add("specialValueId", "LITERAL");
            complexQueryItem1_query_conditions_item2_values_item1.Add("content", code1);

            var complexQueryItem1_query_conditions_item2_values_item2 = new JObject();
            complexQueryItem1_query_conditions_item2_values_item2.Add("specialValueId", "LITERAL");
            complexQueryItem1_query_conditions_item2_values_item2.Add("content", code2);
            var complexQueryItem1_query_conditions_item2_values_item3 = new JObject();
            complexQueryItem1_query_conditions_item2_values_item3.Add("specialValueId", "LITERAL");
            complexQueryItem1_query_conditions_item2_values_item3.Add("content", code3);

            complexQueryItem1_query_conditions_item2_values.Add(complexQueryItem1_query_conditions_item2_values_item1);
            complexQueryItem1_query_conditions_item2_values.Add(complexQueryItem1_query_conditions_item2_values_item2);
            complexQueryItem1_query_conditions_item2_values.Add(complexQueryItem1_query_conditions_item2_values_item3);
            complexQueryItem1_query_conditions_item2.Add("value", complexQueryItem1_query_conditions_item2_values);

            complexQueryItem1_query_conditions.Add(complexQueryItem1_query_conditions_item2);

            // PRESET FILTERING

            if (!string.IsNullOrEmpty(poNumber)) {
                complexQueryItem1_query_conditions.Add(MakeConditionJsonObject("F4301.DOCO", "EQUAL", "LITERAL", poNumber));
            }

            if (!string.IsNullOrEmpty(recent) && !recent.Equals("0") && !recent.Equals("-1"))
                complexQueryItem1_query_conditions.Add(MakeConditionJsonObject("F4301.TRDJ", "GREATER_EQUAL", "TODAY_MINUS_DAY", recent));
            else if (!string.IsNullOrEmpty(recent) && recent.Equals("-1")) {
                var fromDateStr = fromD.ToString(PO.DATE_FORMAT);
                var toDateStr = toD.ToString(PO.DATE_FORMAT);

                complexQueryItem1_query_conditions.Add(MakeTwoConditionsJsonObject("F4301.TRDJ", "BETWEEN", "LITERAL", fromDateStr, "LITERAL", toDateStr));

            }
            
            if (!string.IsNullOrEmpty(supplierName))
            {
                complexQueryItem1_query_conditions.Add(MakeConditionJsonObject("F0101.ALPH", WildcardFilteringTextToOperator(supplierName), "LITERAL", supplierName));
            }

            if (!string.IsNullOrEmpty(projectCode))
            {
                complexQueryItem1_query_conditions.Add(MakeConditionJsonObject("F4301.MCU", WildcardFilteringTextToOperator(projectCode), "LITERAL", projectCode));
            }

            if (!string.IsNullOrEmpty(user))
                complexQueryItem1_query_conditions.Add(MakeConditionJsonObject("F4301.ORBY", WildcardFilteringTextToOperator(user), "LITERAL", user));

            if (!string.IsNullOrEmpty(account))
                complexQueryItem1_query_conditions.Add(MakeConditionJsonObject("F4311.ANI", WildcardFilteringTextToOperator(account), "LITERAL", account));

            complexQueryItem1_query.Add("matchType", "MATCH_ALL");
            complexQueryItem1_query.Add("condition", complexQueryItem1_query_conditions);
            complexQueryItem1.Add("query", complexQueryItem1_query);
            complexQuery.Add(complexQueryItem1);
            query.Add("complexQuery", complexQuery);
            objBuilder.Add("query", query);

            return objBuilder.ToString(Newtonsoft.Json.Formatting.None);
        }

        private string WildcardFilteringTextToOperator(string text) {
            if (!string.IsNullOrEmpty(text) && text.Length > 1) {
                string patternEndWith = @"^\*\w+";    // *abc
                string patternStartWith = @"\w+\*$";      // abc*
                string patternContain = @"^\*\w+\*$";   // *abc*
                Match matchEndWith = Regex.Match(text, patternEndWith);
                Match matchStartWith = Regex.Match(text, patternStartWith);
                Match matchContain = Regex.Match(text, patternContain);

                if (matchEndWith.Success) {
                    return "STR_END_WITH";
                } else if (matchStartWith.Success) {
                    return "STR_START_WITH";
                } else if (matchContain.Success) {
                    return "STR_CONTAIN";
                }
            }

            return "EQUAL";
        }

        private JObject MakeConditionJsonObject(string controlId, string operatorValue, string specialValueId, string content) {
            var item = new JObject();
            item.Add("controlId", controlId);
            item.Add("operator", operatorValue);
            var item_values = new JArray();
            var item_values_item1 = new JObject();
            item_values_item1.Add("specialValueId", specialValueId);
            item_values_item1.Add("content", content);

            item_values.Add(item_values_item1);
            item.Add("value", item_values);
            return item;
        }

        private JObject MakeTwoConditionsJsonObject(string controlId, string operatorValue, string specialValueId1, string content1, string specialValueId2, string content2)
        {
            var item = new JObject();
            item.Add("controlId", controlId);
            item.Add("operator", operatorValue);
            var item_values = new JArray();
            var item_values_item1 = new JObject();
            item_values_item1.Add("specialValueId", specialValueId1);
            item_values_item1.Add("content", content1);

            var item_values_item2 = new JObject();
            item_values_item2.Add("specialValueId", specialValueId2);
            item_values_item2.Add("content", content2);


            item_values.Add(item_values_item1);
            item_values.Add(item_values_item2);
            item.Add("value", item_values);
            return item;
        }

        private async Task<HttpResponseMessage> JDEFindOpenOrders (string token, string deviceName, int num, string type, string company, int page, int stackId, int stateId, string rid) {
            Uri uri = new Uri(targetAppstackURL);
            string bodyStr;

            if (page == 1)
            {
                bodyStr = MakeJsonPost_findOpenOrders(token, deviceName, num, type, company);  
            }
            else
            {
                var parameters = new StringBuilder();
                parameters.Append(string.Format("?{0}={1}","stackId", stackId));
                parameters.Append(string.Format("&{0}={1}", "stateId", stateId));
                parameters.Append(string.Format("&{0}={1}", "rid", Uri.EscapeDataString(rid)));
                parameters.Append(string.Format("&{0}={1}", "fullGridId", 1));
                parameters.Append(string.Format("&{0}={1}", "formOID", "W4312F"));
                parameters.Append(string.Format("&{0}={1}", "outputType", "GRID_DATA"));
                parameters.Append(string.Format("&{0}={1}", "token", Uri.EscapeDataString(token)));
                parameters.Append(string.Format("&{0}={1}", "deviceName", deviceName));
                parameters.Append(string.Format("&{0}={1}", "returnControlIDs", Uri.EscapeDataString(CONTROLIDS_FINDORDERS_APPSTACK)));

                var url = string.Format("{0}{1}", targetAppstackNextURL, parameters);
                uri = new Uri(url);
                var getClient = new HttpClient(new NativeMessageHandler());
				getClient.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;
                return await getClient.GetAsync(uri);
            }

            var client = new HttpClient(new NativeMessageHandler());
			client.MaxResponseContentBufferSize = ServiceConfiguration.MaxResponseContentBufferSize;
			client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;

			var content = new StringContent(bodyStr, Encoding.UTF8, "application/json");

            return await client.PostAsync(uri, content);
        }

        private async Task<HttpResponseMessage> JDECloseStack(string token, string deviceName, int stackId, int stateId, string rid)
		{
			string jsonClose = MakeJsonPost_closeStack(token, deviceName, stackId, stateId, rid);
            Uri uri = new Uri(targetAppstackURL);
            var client = new HttpClient(new NativeMessageHandler());
			client.MaxResponseContentBufferSize = ServiceConfiguration.MaxResponseContentBufferSize;
			client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;

			var content = new StringContent(jsonClose, Encoding.UTF8, "application/json");
			return await client.PostAsync(uri, content);
		}

		private string MakeJsonPost_findOpenOrders(string token, string deviceName, int num, string type, string company)
		{
            JObject body = new JObject();
			body.Add("token", token);
			body.Add("deviceName", deviceName);
			body.Add("action", "open");
			body.Add("outputType", "GRID_DATA");
			JObject objFormRequest = new JObject();
            objFormRequest.Add("returnControlIDs", CONTROLIDS_FINDORDERS_APPSTACK);

            JArray arrFormActions = new JArray();
			if (num != 0)
			{
                var commandNum = new JObject();
                commandNum.Add("command","SetControlValue");
                commandNum.Add("value", num);
                commandNum.Add("controlID", "7");
                arrFormActions.Add(commandNum);
			}
			if (type != null)
			{
				var commandType = new JObject();
				commandType.Add("command", "SetControlValue");
				commandType.Add("value", type);
				commandType.Add("controlID", "9");
				arrFormActions.Add(commandType);
			}
			if (company != null)
			{
				var commandCom = new JObject();
				commandCom.Add("command", "SetControlValue");
				commandCom.Add("value", company);
				commandCom.Add("controlID", "63");
				arrFormActions.Add(commandCom);
			}

			var commandAction = new JObject();
			commandAction.Add("command", "DoAction");
			commandAction.Add("controlID", "find");
			arrFormActions.Add(commandAction);
			objFormRequest.Add("formActions", arrFormActions);
			objFormRequest.Add("formName", "P4312_W4312F");
            objFormRequest.Add("version", Shared.ServiceConfiguration.JDE_VERSION);
			objFormRequest.Add("formServiceAction", "R");
			body.Add("formRequest", objFormRequest);

			return body.ToString(Newtonsoft.Json.Formatting.None);
		}

        private string MakeJsonPost_selectOpenOrder(string token, string deviceName, int stackId, int stateId, string rid) {
            var jsonObject = new JObject();
            jsonObject.Add("token", token);
            jsonObject.Add("deviceName", deviceName);
            jsonObject.Add("action", "execute");
            jsonObject.Add("outputType", "GRID_DATA");

            var objActionRequest = new JObject();
            objActionRequest.Add("returnControlIDs", CONTROLIDS_SELECTRECEIVEORDERS);

            var jsonArrayFormObject = new JArray();
            var command1 = new JObject();
            command1.Add("command", "SelectRow");
            command1.Add("controlID", "1.0");
            var command2 = new JObject();
            command2.Add("command", "DoAction");
			command2.Add("controlID", "4");

            jsonArrayFormObject.Add(command1);
            jsonArrayFormObject.Add(command2);

            objActionRequest.Add("formActions", jsonArrayFormObject);
            objActionRequest.Add("formOID", "W4312F");

            jsonObject.Add("actionRequest", objActionRequest);
            jsonObject.Add("stackId", stackId);
            jsonObject.Add("stateId", stateId);
            jsonObject.Add("rid", rid);

            return jsonObject.ToString(Newtonsoft.Json.Formatting.None);
        }

        private string MakeJsonPost_closeStack (string token, string deviceName, int stackId, int stateId, string rid) {
			var jsonObject = new JObject();
			jsonObject.Add("token", token);
			jsonObject.Add("deviceName", deviceName);
			jsonObject.Add("action", "close");
			jsonObject.Add("outputType", "GRID_DATA");

            var actionRequest = new JObject();
            actionRequest.Add("returnControlIDs", CONTROLIDS_FINDORDERS_APPSTACK);
            actionRequest.Add("formOID", "W4312F");
            jsonObject.Add("actionRequest", actionRequest);
            jsonObject.Add("stackId", stackId);
            jsonObject.Add("stateId", stateId);
            jsonObject.Add("rid", rid);

            return jsonObject.ToString(Newtonsoft.Json.Formatting.None);
        }

		private String MakeJsonPost_fullyReceive(string token, string deviceName, int stackId, int stateId, string rid)
		{
			var jsonObject = new JObject();
			jsonObject.Add("token", token);
		    jsonObject.Add("deviceName", deviceName);
			jsonObject.Add("action", "execute");
			jsonObject.Add("outputType", "GRID_DATA");
			jsonObject.Add("allowCache", "true");

            var objActionRequest = new JObject();
			objActionRequest.Add("returnControlIDs", CONTROLIDS_SELECTRECEIVEORDERS);

            var arrFormActions = new JArray();
            var command1 = new JObject();
            command1.Add("command", "DoAction");
            command1.Add("controlID", "4");
			var command2 = new JObject();
			command2.Add("command", "DoAction");
			command2.Add("controlID", "4");
            arrFormActions.Add(command1);
            arrFormActions.Add(command2);

			objActionRequest.Add("formActions", arrFormActions);
            objActionRequest.Add("formOID", "W4312A");

			jsonObject.Add("actionRequest", objActionRequest);
			jsonObject.Add("stackId", stackId);
			jsonObject.Add("stateId", stateId);
			jsonObject.Add("rid", rid);

			return jsonObject.ToString(Newtonsoft.Json.Formatting.None);
		}

		private string MakeJsonPost_partiallyReceive(string token, string deviceName, int stackId, int stateId, string rid, Dictionary<float, JObject> lines)
		{

			var jsonObject = new JObject();
			jsonObject.Add("token", token);
			jsonObject.Add("deviceName", deviceName);
			jsonObject.Add("action", "execute");
			jsonObject.Add("outputType", "GRID_DATA");
			jsonObject.Add("allowCache", "true");

			var objActionRequest = new JObject();
			objActionRequest.Add("returnControlIDs", CONTROLIDS_SELECTRECEIVEORDERS);

			var arrFormActions = new JArray();
			var objGridAction = new JObject();
			objGridAction.Add("gridID", "1");

            var arrGridRowUpdateEvents = new JArray();
            List<float> keyList = lines.Keys.ToList<float>();

            foreach (var k in keyList) {
                var key = k;
                var lineJObject = lines[key];
                var toBeReceived = lineJObject["toBeReceived"].Value<bool>();

                var objUpdateEvent = new JObject();
                objUpdateEvent.Add("rowNumber", lineJObject["rowNum"].Value<string>());

                var arrGridColumnEvents = new JArray();

                if (toBeReceived) {
                    var command1 = new JObject();
                    command1.Add("command", "SetGridCellValue");
                    command1.Add("columnID", "116");
                    command1.Add("value", lineJObject["quanNew"].Value<float>());

                    var command2 = new JObject();
                    command2.Add("command", "SetGridCellValue");
                    command2.Add("columnID", "117");
                    var quanNew = lineJObject["quanNew"].Value<float>();
                    var unitPrice = lineJObject["unitPrice"].Value<float>();
                    var totalValue = (double)(quanNew * unitPrice);
                    command2.Add("value", totalValue);

                    arrGridColumnEvents.Add(command1);
                    arrGridColumnEvents.Add(command2);

                    if (lineJObject["remark"] != null && string.IsNullOrEmpty(lineJObject["remark"].Value<string>()))
                    {
                        var command3 = new JObject();
                        command3.Add("command", "SetGridCellValue");
                        command3.Add("columnID", "83");
                        command3.Add("value", lineJObject["remark"].Value<string>());
                        arrGridColumnEvents.Add(command3);
                    } 
                } else {
                    var commandNoReceive = new JObject();
                    commandNoReceive.Add("command", "SetGridCellValue");
                    commandNoReceive.Add("columnID", "382");
                    commandNoReceive.Add("value", null);

                    arrGridColumnEvents.Add(commandNoReceive);
                }


                objUpdateEvent.Add("gridColumnEvents", arrGridColumnEvents);
                arrGridRowUpdateEvents.Add(objUpdateEvent);
			}
			
			objGridAction.Add("gridRowUpdateEvents", arrGridRowUpdateEvents);
            var formAction = new JObject();
            formAction.Add("gridAction",objGridAction);
			arrFormActions.Add(formAction);

			var command = new JObject();
			command.Add("command", "DoAction");
			command.Add("controlID", "4");


            var command_button = new JObject();
            command_button.Add("command", "DoAction");
            command_button.Add("controlID", "4");

			arrFormActions.Add(command);
            arrFormActions.Add(command_button);

            objActionRequest.Add("formActions", arrFormActions);
			objActionRequest.Add("formOID", "W4312A");
			jsonObject.Add("actionRequest", objActionRequest);
			jsonObject.Add("stackId", stackId);
			jsonObject.Add("stateId", stateId);
			jsonObject.Add("rid", rid);

			return jsonObject.ToString(Newtonsoft.Json.Formatting.None);
		}

        public async Task<bool> ReceiveFullOrder(string token, string deviceName, int num, string type, string company)
        {
			// Find the record
			var response = await JDEFindOpenOrders(token, deviceName, num, type, company, 1, 0, 0, "");
            if (response.StatusCode != HttpStatusCode.OK)
            {
                App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode.ToString(), response.ReasonPhrase));
                return false;
            }
            try {
                var bodyStr = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(bodyStr))
                    return false;
                var bodyJson = JObject.Parse(bodyStr);
                int stackId = bodyJson["stackId"].Value<int>();
                int stateId = bodyJson["stateId"].Value<int>();
                string rid = bodyJson["rid"].Value<string>();

                // Select the record
                response = await JDESelectOpenOrder(token, deviceName, stackId, stateId, rid);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    App.Native.showAlert(string.Format("[SelectOpenOrder]{0} - {1}", response.StatusCode.ToString(), response.ReasonPhrase), "Error");
                    return false;
                }

                bodyStr = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(bodyStr))
                    return false;
                bodyJson = JObject.Parse(bodyStr);
                stackId = bodyJson["stackId"].Value<int>();
                stateId = bodyJson["stateId"].Value<int>();
                rid = bodyJson["rid"].Value<string>();


                if (bodyJson["fs_P4312_W4312F"] != null)
                {
                    var errors = bodyJson["fs_P4312_W4312F"]["errors"].Value<JArray>();
                    if (errors != null)
                    {
                        var errorArray = errors.Select(jv => (JObject)jv).ToArray();
                        if (errorArray.Length > 0)
                        {
                            var errorLog = new StringBuilder();
                            for (int i = 0; i < errorArray.Length; i++)
                            {
                                var obj = errorArray[i];
                                var errorItemStr = string.Format("{0}({1}) - {2}", obj["TITLE"].Value<string>(), obj["CODE"].Value<string>(), obj["DESC"].Value<string>());
                                errorLog.Append(errorItemStr);
                            }
                            await JDECloseStack(token, deviceName, stackId, stateId, rid);
                            App.Native.showAlert(errorLog.ToString(), "JDESelectOpenOrder");
                            return false;
                        }
                    }
                }


                // Fully receipt the record

                response = await JDEFullyReceiveOpenOrder(token, deviceName, stackId, stateId, rid);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode.ToString(), response.ReasonPhrase));
                    return false;
                }

                bodyStr = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(bodyStr))
                    return false;
                bodyJson = JObject.Parse(bodyStr);
                stackId = bodyJson["stackId"].Value<int>();
                stateId = bodyJson["stateId"].Value<int>();
                rid = bodyJson["rid"].Value<string>();


                if (bodyJson["fs_P4312_W4312A"] != null && bodyJson["fs_P4312_W4312A"]["errors"] != null)
                {
                    var receiptErrors = bodyJson["fs_P4312_W4312A"]["errors"].Value<JArray>();

                    if (receiptErrors != null)
                    {
                        var errorArray = receiptErrors.Select(jv => (JObject)jv).ToArray();
                        if (errorArray.Length > 0)
                        {
                            var errorLog = new StringBuilder();
                            for (int i = 0; i < errorArray.Length; i++)
                            {
                                var obj = errorArray[i];
                                var errorItemStr = string.Format("{0}({1}) - {2}", obj["TITLE"].Value<string>(), obj["CODE"].Value<string>(), obj["DESC"].Value<string>());
                                errorLog.Append(errorItemStr);
                            }
                            await JDECloseStack(token, deviceName, stackId, stateId, rid);
                            App.Native.showAlert(errorLog.ToString(), "JDEFullyReceiveOpenOrder");
                            return false;
                        }
                    }
                }
                var responseClose = JDECloseStack(token, deviceName, stackId, stateId, rid);
            } catch (Exception ex) {
                App.Native.showAlert(string.Format("{0} - {1}", "Exception error", ex.Message), "Error");
                return false;
            }
			

			// Close the stack
			
            return true;
		}

        public async Task<bool> ReceivePartialOrder(string token, string deviceName, int poNum, string poType, string poCompany, Dictionary<float, JObject> lines)
        {

			// Find the record
			var response = await JDEFindOpenOrders(token, deviceName, poNum, poType, poCompany, 1, 0, 0, "");
			if (response.StatusCode != HttpStatusCode.OK)
			{
				App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode.ToString(), response.ReasonPhrase));
				return false;
			}

            try
            {
    			var bodyStr = await response.Content.ReadAsStringAsync();
    			if (string.IsNullOrEmpty(bodyStr))
    				return false;
    			var bodyJson = JObject.Parse(bodyStr);
    			int stackId = bodyJson["stackId"].Value<int>();
    			int stateId = bodyJson["stateId"].Value<int>();
    			string rid = bodyJson["rid"].Value<string>();

    			// Select the record
    			response = await JDESelectOpenOrder(token, deviceName, stackId, stateId, rid);
    			if (response.StatusCode != HttpStatusCode.OK)
    			{
    				App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode.ToString(), response.ReasonPhrase));
    				return false;
    			}

    			bodyStr = await response.Content.ReadAsStringAsync();
    			if (string.IsNullOrEmpty(bodyStr))
    				return false;
    			bodyJson = JObject.Parse(bodyStr);
    			stackId = bodyJson["stackId"].Value<int>();
    			stateId = bodyJson["stateId"].Value<int>();
    			rid = bodyJson["rid"].Value<string>();

            // Here we need to build a mapping of row number (as seen on the JDE interface) to line number.
            // We our list of lines with numbers, so we're just adding the row number to that list.
         
                var jsonArrayRowset = bodyJson["fs_P4312_W4312A"]["data"]["gridData"]["rowset"].Value<JArray>();

                if (bodyJson["fs_P4312_W4312A"] != null && bodyJson["fs_P4312_W4312A"]["errors"] != null)
                {
                    var errors = bodyJson["fs_P4312_W4312A"]["errors"].Value<JArray>();

                    if (errors != null)
                    {

                        var errorArray = errors.Select(jv => (JObject)jv).ToArray();
                        if (errorArray.Length > 0)
                        {
                            var errorLog = new StringBuilder();
                            for (int i = 0; i < errorArray.Length; i++)
                            {
                                var obj = errorArray[i];
                                var errorItemStr = string.Format("{0}({1}) - {2}", obj["TITLE"].Value<string>(), obj["CODE"].Value<string>(), obj["DESC"].Value<string>());
                                errorLog.Append(errorItemStr);
                            }
                            App.Native.showAlert(errorLog.ToString(), "JDESelectOpenOrder");
                            return false;
                        }
                    }
                }

                if (jsonArrayRowset != null)
                {
                    var array = jsonArrayRowset.Select(jv => (JObject)jv).ToArray();

                    for (int i = 0; i < array.Length; i++)
                    {
                        var obj = array[i];

                        float lineNum = obj["z_LNID_44"].Value<float>();
                        float unitPrice = obj["z_UCSTR_119"].Value<float>();
                        var lineObject = lines[lineNum];
                        lineObject.Add("rowNum", i);
                        lineObject.Add("unitPrice", unitPrice);
                    }
                }

                // Partially receipt the record
                // Any lines that have not been selected in the app must have the value (quanNew) set to zero, or it will be receipted.

                response = await JDEPartiallyReceiveOpenOrder(token, deviceName, stackId, stateId, rid, lines);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode.ToString(), response.ReasonPhrase));
                    return false;
                }

                bodyStr = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(bodyStr))
                    return false;
                bodyJson = JObject.Parse(bodyStr);
                stackId = bodyJson["stackId"].Value<int>();
                stateId = bodyJson["stateId"].Value<int>();
                rid = bodyJson["rid"].Value<string>();

                if (bodyJson["fs_P4312_W4312A"] != null && bodyJson["fs_P4312_W4312A"]["errors"] != null)
                {
                    var receiptErrors = bodyJson["fs_P4312_W4312A"]["errors"].Value<JArray>();

                    if (receiptErrors != null)
                    {
                        var errorArray = receiptErrors.Select(jv => (JObject)jv).ToArray();
                        if (errorArray.Length > 0)
                        {
                            var errorLog = new StringBuilder();
                            for (int i = 0; i < errorArray.Length; i++)
                            {
                                var obj = errorArray[i];
                                var errorItemStr = string.Format("{0}({1}) - {2}", obj["TITLE"].Value<string>(), obj["CODE"].Value<string>(), obj["DESC"].Value<string>());
                                errorLog.Append(errorItemStr);
                            }
                            await JDECloseStack(token, deviceName, stackId, stateId, rid);
                            App.Native.showAlert(errorLog.ToString(), "JDEPartiallyReceiveOpenOrder");
                            return false;

                        }
                    }
                }


                // Close the stack
                var responseClose = JDECloseStack(token, deviceName, stackId, stateId, rid);
            } catch (Exception ex) {
                App.Native.showAlert(string.Format("{0} - {1}", "Exception error", ex.Message), "Error");
                return false;
            }
			return true;
        }

		private async Task<HttpResponseMessage> JDEFullyReceiveOpenOrder(String token, String deviceName, int stackId, int stateId, String rid)
		{
			string jsonSelectOpenOrder = MakeJsonPost_fullyReceive(token, deviceName, stackId, stateId, rid);
			Uri uri = new Uri(targetAppstackURL);
            var client = new HttpClient(new NativeMessageHandler());
			client.MaxResponseContentBufferSize = ServiceConfiguration.MaxResponseContentBufferSize;
			client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;

			var content = new StringContent(jsonSelectOpenOrder, Encoding.UTF8, "application/json");
			return await client.PostAsync(uri, content);
		}

        private async Task<HttpResponseMessage> JDEPartiallyReceiveOpenOrder(String token, String deviceName, int stackId, int stateId, String rid, Dictionary<float, JObject> lines)
		{
			string jsonSelectOpenOrder = MakeJsonPost_partiallyReceive(token, deviceName, stackId, stateId, rid, lines);
			Uri uri = new Uri(targetAppstackURL);
            var client = new HttpClient(new NativeMessageHandler());
			client.MaxResponseContentBufferSize = ServiceConfiguration.MaxResponseContentBufferSize;
			client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;

			var content = new StringContent(jsonSelectOpenOrder, Encoding.UTF8, "application/json");
			return await client.PostAsync(uri, content);
		}

        private async Task<HttpResponseMessage> JDESelectOpenOrder(string token, string deviceName, int stackId, int stateId, string rid)
		{
			string jsonSelectOpenOrder = MakeJsonPost_selectOpenOrder(token, deviceName, stackId, stateId, rid);
			Uri uri = new Uri(targetAppstackURL);
            var client = new HttpClient(new NativeMessageHandler());
			client.MaxResponseContentBufferSize = ServiceConfiguration.MaxResponseContentBufferSize;
			client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;

			var content = new StringContent(jsonSelectOpenOrder, Encoding.UTF8, "application/json");
			return await client.PostAsync(uri, content);
		}

        private async Task<HttpResponseMessage> JDEGetOpenStatusCodes(string token, string deviceName)
        {
            var jsonBody = MakeJsonPost_getOpenStatusCodes(token, deviceName);
            Uri uri = new Uri(targetPoservice);
            var client = new HttpClient(new NativeMessageHandler());
            client.MaxResponseContentBufferSize = ServiceConfiguration.MaxResponseContentBufferSize;
            client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;
            var content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            return await client.PostAsync(uri, content);
        }
   
        private string MakeJsonPost_getOpenStatusCodes(string token, string deviceName){
            var objBuilder = new JObject();
            objBuilder.Add("token", token);
            objBuilder.Add("deviceName", deviceName);
            objBuilder.Add("applicationName", "P4312");
            objBuilder.Add("version", ServiceConfiguration.JDE_VERSION);
            return objBuilder.ToString(Newtonsoft.Json.Formatting.None);
        }
    

        //public async Task<Dictionary<float, float>> GetUnitPrice() {
            
        //}
	}
}
