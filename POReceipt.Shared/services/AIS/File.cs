﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net.Http;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using POReceipt.Shared;
using System.Globalization;
using ModernHttpClient;

namespace POReceipt.Shared
{
    public class File
    {
        private string targetRootURL;
        private string targetFileGetTextURL;
        private string targetFileUpdateTextURL;
        private string targetFileListURL;
        private string targetFileUpload;
        private string targetFileDownload;
        private string targetFileDelete;

        private static string FAILED_STATUS_MESSAGE = "Request Fail";

        public File()
        {
            targetRootURL = string.Format("{0}{1}", ServiceConfiguration.JDE_HOSTNAME, ServiceConfiguration.JDE_SERVICE_ROOT);
            targetFileGetTextURL = string.Format("{0}{1}", targetRootURL, ServiceConfiguration.JDE_FILE_GETTEXT);
            targetFileUpdateTextURL = string.Format("{0}{1}", targetRootURL, ServiceConfiguration.JDE_FILE_UPDATETEXT);
            targetFileListURL = string.Format("{0}{1}", targetRootURL, ServiceConfiguration.JDE_FILE_LIST);
            targetFileUpload = string.Format("{0}{1}", targetRootURL, ServiceConfiguration.JDE_FILE_UPLOAD);
            targetFileDownload = string.Format("{0}{1}", targetRootURL, ServiceConfiguration.JDE_FILE_DONWLOAD);
            targetFileDelete = string.Format("{0}{1}", targetRootURL, ServiceConfiguration.JDE_FILE_DELETE);
        }

        public async Task<string> SetOrderLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string text)
        {
            var response = await JDESetLineText(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, text);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase));
                return null;
            }

            var bodyStr = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(bodyStr))
                return FAILED_STATUS_MESSAGE;
            var bodyJson = JObject.Parse(bodyStr);

            string updateStatus = "";
            if (bodyJson["updateTextStatus"] != null && string.IsNullOrEmpty(bodyJson["updateTextStatus"].Value<string>()))
            {
                updateStatus = bodyJson["updateTextStatus"].Value<string>();
            }

            return updateStatus;
        }

        private async Task<HttpResponseMessage> JDESetLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string text)
        {
            string jsonBody = MakeJsonPost_setLineText(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, text);
            return await Util.POST(targetFileUpdateTextURL, jsonBody);
        }

        private string MakeJsonPost_setLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string text)
        {
            StringBuilder keyBuilder = new StringBuilder();
            keyBuilder.Append(poNum).Append("|").Append(poCompany).Append("|").Append(poType).Append("|").Append(poSuffix).Append("|").Append(lineNum);

            var json = new JObject();
            json.Add("token", token);
            json.Add("deviceName", deviceName);
            json.Add("moStructure", "GT4311");

            var moKeyArr = new JArray();
            moKeyArr.Add(keyBuilder.ToString());

            json.Add("moKey", moKeyArr);

            json.Add("formName", "P4312_W4312A");
            json.Add("version", "ZJDE0001");
            json.Add("inputText", text);
            json.Add("appendText", false); // If false, overwrites. If true, appends.
            return json.ToString(Newtonsoft.Json.Formatting.None);
        }

        public async Task<string> GetOrderLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum)
        {
            var response = await JDEGetLineText(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase));
                return null;
            }

            var bodyStr = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(bodyStr))
                return null;
            var bodyJson = JObject.Parse(bodyStr);

            string text = "";
            if (bodyJson["text"] != null && !string.IsNullOrEmpty(bodyJson["text"].Value<string>()))
            {
                text = bodyJson["text"].Value<string>();
            }

            return text;
        }

        private async Task<HttpResponseMessage> JDEGetLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum)
        {
            var jsonBody = MakeJsonPost_getLineText(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum);

            var client = new HttpClient(new NativeMessageHandler());
            client.MaxResponseContentBufferSize = ServiceConfiguration.MaxResponseContentBufferSize;
            client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;

            var content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            var uri = new Uri(targetFileGetTextURL);
            return await client.PostAsync(uri, content);
        }

        private static string MakeJsonPost_getLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum)
        {
            StringBuilder keyBuilder = new StringBuilder();
            keyBuilder.Append(poNum).Append("|").Append(poCompany).Append("|").Append(poType).Append("|").Append(poSuffix).Append("|").Append(lineNum);

            var json = new JObject();
            json.Add("token", token);
            json.Add("deviceName", deviceName);
            json.Add("moStructure", "GT4311");

            var moKeyArr = new JArray();
            moKeyArr.Add(keyBuilder.ToString());

            json.Add("moKey", moKeyArr);

            json.Add("formName", "P4312_W4312A");
            json.Add("version", ServiceConfiguration.JDE_VERSION);

            return json.ToString(Newtonsoft.Json.Formatting.None);
        }


        #region Get Order Text
        public async Task<string> GetOrderText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix)
        {
            var response = await JDEGetOrderText(token, deviceName, poNum, poType, poCompany, poSuffix);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase));
                return null;
            }

            var bodyStr = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(bodyStr))
                return null;
            var bodyJson = JObject.Parse(bodyStr);

            string text = "";
            if (bodyJson["text"] != null && !string.IsNullOrEmpty(bodyJson["text"].Value<string>()))
            {
                text = bodyJson["text"].Value<string>();
            }

            return text;
        }

        private async Task<HttpResponseMessage> JDEGetOrderText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix)
        {
            string jsonBody = MakeJsonPost_getOrderText(token, deviceName, poNum, poType, poCompany, poSuffix);
            return await Util.POST(targetFileGetTextURL, jsonBody);
        }

        private string MakeJsonPost_getOrderText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix)
        {
            var keyBuilder = new StringBuilder();
            keyBuilder.Append(poNum).Append("|").Append(poCompany).Append("|").Append(poType).Append("|").Append(poSuffix);

            var json = new JObject();
            json.Add("token", token);
            json.Add("deviceName", deviceName);
            json.Add("moStructure", "GT4301");

            var moKeyArr = new JArray();
            moKeyArr.Add(keyBuilder.ToString());

            json.Add("moKey", moKeyArr);

            json.Add("formName", "P4312_W4312A");
            json.Add("version", ServiceConfiguration.JDE_VERSION);
            return json.ToString(Newtonsoft.Json.Formatting.None);
        }
        #endregion

        #region Set Order Text
        public async Task<string> SetOrderText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, string text)
        {
            var response = await JDESetOrderText(token, deviceName, poNum, poType, poCompany, poSuffix, text);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase));
                return null;
            }

            var bodyStr = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(bodyStr))
                return null;
            var bodyJson = JObject.Parse(bodyStr);

            string updateStatus = "";
            if (bodyJson["updateTextStatus"] != null && string.IsNullOrEmpty(bodyJson["updateTextStatus"].Value<string>()))
            {
                updateStatus = bodyJson["updateTextStatus"].Value<string>();
            }

            return updateStatus;
        }

        private async Task<HttpResponseMessage> JDESetOrderText(String token, String deviceName, int poNum, String poType, String poCompany, String poSuffix, String text)
        {
            string jsonBody = MakeJsonPost_setOrderText(token, deviceName, poNum, poType, poCompany, poSuffix, text);
            return await Util.POST(targetFileUpdateTextURL, jsonBody);
        }

        private string MakeJsonPost_setOrderText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, string text)
        {
            var keyBuilder = new StringBuilder();
            keyBuilder.Append(poNum).Append("|").Append(poCompany).Append("|").Append(poType).Append("|").Append(poSuffix);

            var json = new JObject();
            json.Add("token", token);
            json.Add("deviceName", deviceName);
            json.Add("moStructure", "GT4301");

            var moKeyArr = new JArray();
            moKeyArr.Add(keyBuilder.ToString());

            json.Add("moKey", moKeyArr);

            json.Add("formName", "P4312_W4312A");
            json.Add("version", ServiceConfiguration.JDE_VERSION);
            json.Add("inputText", text);
            json.Add("appendText", false);
            return json.ToString(Newtonsoft.Json.Formatting.None);
        }
        #endregion



        #region Get Order File List
        public async Task<string> GetOrderFileList(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix)
        {
            var response = await JDEGetOrderFileList(token, deviceName, poNum, poType, poCompany, poSuffix);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase));
                return null;
            }

            var bodyStr = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(bodyStr))
                return null;

            return bodyStr;
        }

        private async Task<HttpResponseMessage> JDEGetOrderFileList(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix)
        {
            var jsonBody = MakeJsonPost_getOrderFileList(token, deviceName, poNum, poType, poCompany, poSuffix);
            return await Util.POST(targetFileListURL, jsonBody);
        }

        private string MakeJsonPost_getOrderFileList(String token, String deviceName, int poNum, String poType, String poCompany, String poSuffix)
        {
            var keyBuilder = new StringBuilder();
            keyBuilder.Append(poNum).Append("|").Append(poCompany).Append("|").Append(poType).Append("|").Append(poSuffix);
            var json = new JObject();
            json.Add("token", token);
            json.Add("deviceName", deviceName);
            json.Add("moStructure", "GT4301");
            var moKeyArr = new JArray();
            moKeyArr.Add(keyBuilder.ToString());

            json.Add("moKey", moKeyArr);

            json.Add("formName", "P4312_W4312A");
            json.Add("version", ServiceConfiguration.JDE_VERSION);
            json.Add("includeURLs", false);
            json.Add("includeData", true);
            var moTypes = new JArray();
            moTypes.Add("FILE");
            moTypes.Add("QUEUE");
            moTypes.Add("TEXT");
            moTypes.Add("URL");
            json.Add("moTypes", moTypes);

            json.Add("thumbnailSize", 0);

            return json.ToString(Newtonsoft.Json.Formatting.None);
        }
        #endregion


        public async Task<string> GetOrderLineFileList(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum)
        {
            var response = await JDEGetLineFileList(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase));
                return null;
            }

            var bodyStr = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(bodyStr))
                return null;

            return bodyStr;
        }

        private async Task<HttpResponseMessage> JDEGetLineFileList(String token, String deviceName, int poNum, String poType, string poCompany, string poSuffix, float lineNum)
        {
            var jsonBody = MakeJsonPost_getLineFileList(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum);
            return await Util.POST(targetFileListURL, jsonBody);
        }

        private string MakeJsonPost_getLineFileList(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum)
        {
            var keyBuilder = new StringBuilder();
            keyBuilder.Append(poNum).Append("|").Append(poCompany).Append("|").Append(poType).Append("|").Append(poSuffix).Append("|").Append(lineNum.ToString("G17"));

            var json = new JObject();
            json.Add("token", token);
            json.Add("deviceName", deviceName);
            json.Add("moStructure", "GT4311");
            var moKeyArr = new JArray();
            moKeyArr.Add(keyBuilder.ToString());

            json.Add("moKey", moKeyArr);

            json.Add("formName", "P4312_W4312A");
            json.Add("version", ServiceConfiguration.JDE_VERSION);
            json.Add("includeURLs", false);
            json.Add("includeData", true);
            var moTypes = new JArray();
            moTypes.Add("FILE");
            moTypes.Add("QUEUE");
            moTypes.Add("TEXT");
            moTypes.Add("URL");
            json.Add("moTypes", moTypes);

            json.Add("thumbnailSize", 0);

            return json.ToString(Newtonsoft.Json.Formatting.None);
        }


        /**
       * Sets an attachment for an order line.
       *
       * @param token The AIS access token.
       * @param deviceName The unique device name.
       * @param poNum The PO number.
       * @param poType The PO type.
       * @param poCompany The PO company.
       * @param poSuffix The PO suffix.
       * @param lineNum The line number.
       * @param fileLocation The path to the file to upload.
       * @return The server response string (JSON).
       * @throws RemoteServerException If there is a server-side error returned.
       */
        public async Task<string> SetLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string fileLocation, byte[] imageBytes)
        {
            var response = await JDESetLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, fileLocation, imageBytes);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase));
                return null;
            }

            var bodyStr = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(bodyStr))
                return null;
            return bodyStr;
        }

        //// TODO: More work needed to turn this into a proper API - streaming files, etc.
        private async Task<HttpResponseMessage> JDESetLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string fileName, byte[] imageBytes)
        {
            string jsonBody = MakeJsonPost_setLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, fileName);

            HttpClient httpClient = new HttpClient(new NativeMessageHandler());
            MultipartFormDataContent form = new MultipartFormDataContent();

            form.Add(new StringContent(jsonBody, Encoding.UTF8, "application/json"), "moAdd");
            form.Add(new StreamContent(new MemoryStream(imageBytes)), "file", fileName);
            HttpResponseMessage response = await httpClient.PostAsync(targetFileUpload, form);

            response.EnsureSuccessStatusCode();
            httpClient.Dispose();
            string sd = response.Content.ReadAsStringAsync().Result;
            return response;
        }

        private string MakeJsonPost_setLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string fileName)
        {
            JObject objBuilder = new JObject();
            objBuilder.Add("token", token);
            objBuilder.Add("deviceName", deviceName);
            objBuilder.Add("ssoBuilder", false);
            objBuilder.Add("moStructure", "GT4311");


            var moKey = new JArray();
            moKey.Add(poNum);
            moKey.Add(poCompany);
            moKey.Add(poType);
            moKey.Add(poSuffix);
            moKey.Add(lineNum.ToString("G17"));
            objBuilder.Add("moKey", moKey);


            objBuilder.Add("formName", "P4312_W4312A");
            objBuilder.Add("version", ServiceConfiguration.JDE_VERSION);

            var filePath = Path.Combine(FileUtil.getDocumentPath(), fileName);


            var fileJObject = new JObject();
            fileJObject.Add("fileName", fileName);
            fileJObject.Add("fileLocation", filePath);
            fileJObject.Add("itemName", fileName.Substring(0, fileName.LastIndexOf(".", StringComparison.CurrentCulture)));
            fileJObject.Add("sequence", 0);

            objBuilder.Add("file", fileJObject);

            // Turn the JSON to a string
            return objBuilder.ToString(Newtonsoft.Json.Formatting.None);
        }

        private string GetFileName(string fullFileName)
        {
            return fullFileName.Substring(0, fullFileName.LastIndexOf(".", StringComparison.CurrentCulture));
        }



        public async Task<Stream> GetLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum)
        {
            var response = await JDEGetLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, sequenceNum);
            if (!response.IsSuccessStatusCode)
            {
                App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase));
                return null;
            }
            var stream = await response.Content.ReadAsStreamAsync();
            return stream;
        }

        private async Task<HttpResponseMessage> JDEGetLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum)
        {
            string jsonBody = MakeJsonPost_getLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, sequenceNum);
            try
            {
                var response = await Util.POSTMedia(targetFileDownload, jsonBody);
                return response;
            }
            catch (Exception ex)
            {
                App.Native.log(ex);
            }
            return null;
        }

        private string MakeJsonPost_getLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum)
        {
            JObject objBuilder = new JObject();
            objBuilder.Add("token", token);
            objBuilder.Add("deviceName", deviceName);
            objBuilder.Add("ssoEnabled", false);
            objBuilder.Add("moStructure", "GT4311");

            var moKey = new JArray();
            moKey.Add(poNum);
            moKey.Add(poCompany);
            moKey.Add(poType);
            moKey.Add(poSuffix);
            moKey.Add(lineNum.ToString("G17"));
            objBuilder.Add("moKey", moKey);

            objBuilder.Add("formName", "P4312_W4312A");
            objBuilder.Add("version", ServiceConfiguration.JDE_VERSION);
            objBuilder.Add("sequence", sequenceNum);

            return objBuilder.ToString(Newtonsoft.Json.Formatting.None);
        }


        /**
       * Sets an attachment for an order.
       *
       * @param token The AIS access token.
       * @param deviceName The unique device name.
       * @param poNum The PO number.
       * @param poType The PO type.
       * @param poCompany The PO company.
       * @param poSuffix The PO suffix.
       * @param fileLocation The location of the file to attach.
       * @return The server response (JSON).
       * @throws RemoteServerException If there is a server error.
       */
        public async Task<string> SetOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, string fileLocation, byte[] imageBytes)
        {
            var response = await JDESetOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, fileLocation, imageBytes);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase));
                return null;
            }

            var bodyStr = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(bodyStr))
                return null;
            return bodyStr;
        }

        private async Task<HttpResponseMessage> JDESetOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, string fileName, byte[] imageBytes)
        {
            string jsonBody = MakeJsonPost_setOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, fileName);

            HttpClient httpClient = new HttpClient(new NativeMessageHandler());
            MultipartFormDataContent form = new MultipartFormDataContent();

            form.Add(new StringContent(jsonBody, Encoding.UTF8, "application/json"), "moAdd");
            form.Add(new StreamContent(new MemoryStream(imageBytes)), "file", fileName);
            HttpResponseMessage response = await httpClient.PostAsync(targetFileUpload, form);

            response.EnsureSuccessStatusCode();
            httpClient.Dispose();
            string sd = response.Content.ReadAsStringAsync().Result;
            return response;
        }

        private string MakeJsonPost_setOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, string fileName)
        {
            JObject objBuilder = new JObject();
            objBuilder.Add("token", token);
            objBuilder.Add("deviceName", deviceName);
            objBuilder.Add("ssoBuilder", false);
            objBuilder.Add("moStructure", "GT4301");


            var moKey = new JArray();
            moKey.Add(poNum);
            moKey.Add(poCompany);
            moKey.Add(poType);
            moKey.Add(poSuffix);
            objBuilder.Add("moKey", moKey);


            objBuilder.Add("formName", "P4312_W4312A");
            objBuilder.Add("version", ServiceConfiguration.JDE_VERSION);

            var filePath = Path.Combine(FileUtil.getDocumentPath(), fileName);


            var fileJObject = new JObject();
            fileJObject.Add("fileName", fileName);
            fileJObject.Add("fileLocation", filePath);
            fileJObject.Add("itemName", fileName.Substring(0, fileName.LastIndexOf(".", StringComparison.CurrentCulture)));
            fileJObject.Add("sequence", 0);

            objBuilder.Add("file", fileJObject);

            // Turn the JSON to a string
            return objBuilder.ToString(Newtonsoft.Json.Formatting.None);
        }

        public async Task<Stream> GetOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum)
        {
            var response = await JDEGetOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, sequenceNum);
            if (!response.IsSuccessStatusCode)
            {
                App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase));
                return null;
            }
            var stream = await response.Content.ReadAsStreamAsync();
            return stream;
        }

        private async Task<HttpResponseMessage> JDEGetOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum)
        {
            string jsonBody = MakeJsonPost_getOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, sequenceNum);
            try
            {
                var response = await Util.POSTMedia(targetFileDownload, jsonBody);
                return response;
            }
            catch (Exception ex)
            {
                App.Native.log(ex);
            }
            //Response response = targetFileDownload.request(MediaType.APPLICATION_OCTET_STREAM).buildPost(Entity.json(jsonBody)).invoke();
            return null;
        }

        private string MakeJsonPost_getOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum)
        {
            JObject objBuilder = new JObject();
            objBuilder.Add("token", token);
            objBuilder.Add("deviceName", deviceName);
            objBuilder.Add("ssoEnabled", false);
            objBuilder.Add("moStructure", "GT4301");

            var moKey = new JArray();
            moKey.Add(poNum);
            moKey.Add(poCompany);
            moKey.Add(poType);
            moKey.Add(poSuffix);
            objBuilder.Add("moKey", moKey);

            objBuilder.Add("formName", "P4312_W4312A");
            objBuilder.Add("version", ServiceConfiguration.JDE_VERSION);
            objBuilder.Add("sequence", sequenceNum);

            return objBuilder.ToString(Newtonsoft.Json.Formatting.None);
        }

        #region DELETE BINARY ATTACHMENTS

        public async Task<string> DeleteOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum)
        {
            var response = await JDEDeleteOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, sequenceNum);
            if (!response.IsSuccessStatusCode)
            {
                App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase));
                return null;
            }
            var bodyStr = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(bodyStr))
                return null;
            return bodyStr;
        }

        private async Task<HttpResponseMessage> JDEDeleteOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum)
        {
            string jsonBody = MakeJsonPost_deleteOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, sequenceNum);
            try
            {
                var response = await Util.POST(targetFileDelete, jsonBody);
                return response;
            }
            catch (Exception ex)
            {
                App.Native.log(ex);
            }
            //Response response = targetFileDownload.request(MediaType.APPLICATION_OCTET_STREAM).buildPost(Entity.json(jsonBody)).invoke();
            return null;
        }

        private string MakeJsonPost_deleteOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum)
        {
            JObject objBuilder = new JObject();
            objBuilder.Add("token", token);
            objBuilder.Add("deviceName", deviceName);
            objBuilder.Add("ssoEnabled", false);
            objBuilder.Add("moStructure", "GT4301");

            var moKey = new JArray();
            moKey.Add(poNum);
            moKey.Add(poCompany);
            moKey.Add(poType);
            moKey.Add(poSuffix);
            objBuilder.Add("moKey", moKey);

            objBuilder.Add("formName", "P4312_W4312A");
            objBuilder.Add("version", ServiceConfiguration.JDE_VERSION);
            objBuilder.Add("sequence", sequenceNum);

            return objBuilder.ToString(Newtonsoft.Json.Formatting.None);
        }


        ////////////////
        /// 
        /// 
        public async Task<string> DeleteLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum)
        {
            var response = await JDEDeleteLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, sequenceNum);
            if (!response.IsSuccessStatusCode)
            {
                App.Native.showAlert("Error", string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase));
                return null;
            }
            var bodyStr = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(bodyStr))
                return null;
            return bodyStr;
        }

        private async Task<HttpResponseMessage> JDEDeleteLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum)
        {
            string jsonBody = MakeJsonPost_deleteLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, sequenceNum);
            try
            {
                var response = await Util.POST(targetFileDelete, jsonBody);
                return response;
            }
            catch (Exception ex)
            {
                App.Native.log(ex);
            }
            return null;
        }

        private string MakeJsonPost_deleteLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum)
        {
            JObject objBuilder = new JObject();
            objBuilder.Add("token", token);
            objBuilder.Add("deviceName", deviceName);
            objBuilder.Add("ssoEnabled", false);
            objBuilder.Add("moStructure", "GT4311");

            var moKey = new JArray();
            moKey.Add(poNum);
            moKey.Add(poCompany);
            moKey.Add(poType);
            moKey.Add(poSuffix);
            moKey.Add(lineNum.ToString("G17"));
            objBuilder.Add("moKey", moKey);

            objBuilder.Add("formName", "P4312_W4312A");
            objBuilder.Add("version", ServiceConfiguration.JDE_VERSION);
            objBuilder.Add("sequence", sequenceNum);
            return objBuilder.ToString(Newtonsoft.Json.Formatting.None);
        } 

        #endregion
    }
}