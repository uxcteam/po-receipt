﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net.Http;
using System.Xml;
using ModernHttpClient;

namespace POReceipt.Shared
{
    public class AccessToken
    {
        private ServiceConfiguration conf;

        public AccessToken()
        {
        }

        public async Task<bool> LogoutAsync(string token)
        {
            var bodyObj = new
            {
                token = token
            };
            string bodyString = JsonConvert.SerializeObject(bodyObj);

            var content = new StringContent(bodyString, Encoding.UTF8, "application/json");
            try
            {
                var uri = new Uri(string.Format("{0}{1}{2}", ServiceConfiguration.JDE_HOSTNAME, ServiceConfiguration.JDE_SERVICE_ROOT, ServiceConfiguration.JDE_LOG_OUT));

                var client = new HttpClient(new NativeMessageHandler());
                client.MaxResponseContentBufferSize = ServiceConfiguration.MaxResponseContentBufferSize;
                client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;

                HttpResponseMessage response = await client.PostAsync(uri, content);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
                    return false;
                }
            } catch (Exception ex)
            {
                App.Native.log(ex.Message);
                App.Native.showAlert(ex.Message);
                //App.Native.showAlert("Please make sure the Internet connection is available", "Sign-in failed");
                return false;
            }

            return true;
        }

        public async Task<string> GetTokenAsync(string username, string password, string deviceName)
        {
            var bodyObj = new { 
                username = username, 
                password = password, 
                deviceName = deviceName, 
                environment = ServiceConfiguration.JDE_ENVIRONMENT,
                role = ServiceConfiguration.JDE_ROLE
			};
            string bodyString = JsonConvert.SerializeObject(bodyObj);

			var content = new StringContent(bodyString, Encoding.UTF8, "application/json");
            try
            {
                var uri = new Uri(string.Format("{0}{1}{2}", ServiceConfiguration.JDE_HOSTNAME, ServiceConfiguration.JDE_SERVICE_ROOT, ServiceConfiguration.JDE_TOKENREQUEST));

                var client = new HttpClient(new NativeMessageHandler());
                client.MaxResponseContentBufferSize = ServiceConfiguration.MaxResponseContentBufferSize;
                client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;

                HttpResponseMessage response = await client.PostAsync(uri, content);

				if (response.StatusCode != HttpStatusCode.OK)
					Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
				else
				{
					var contents = await response.Content.ReadAsStringAsync();
                    if (string.IsNullOrEmpty(contents))
                    {
                        Console.Out.WriteLine("Response contained empty body...");
                    }
                    else
                    {
                        var json = JObject.Parse(contents);
                        var userInfo = json["userInfo"];
                        if (userInfo != null)
                        {
                            var token = userInfo["token"].Value<string>();
                            return token;
                        }
                        return null;
                    }
				}
            } catch (Exception ex) {
                App.Native.log(ex.Message);
                App.Native.showAlert(ex.Message);
                //App.Native.showAlert("Please make sure the Internet connection is available", "Sign-in failed");
            }

            return null;
        }
    }
}
