﻿using System;
namespace POReceipt.Shared
{
    public class ServiceConfiguration
    {
        public static string JDE_HOSTNAME = "https://jde02.uxcredrock.com";  // Should be configurable
		public static string JDE_SERVICE_ROOT = "/mobile/jderest/";          // Should be configurable
        public static  string JDE_USERNAME = "redrock";                     // Entered by user
        public static  string JDE_PASSWORD = "Redrock1!";                   // Entered by user
        public static  string JDE_DEVICE_NAME = "testing";                  // Should be some unique string from the calling device
        public static string JDE_ENVIRONMENT = "JPD920";                     // Should be configurable
        public static string JDE_VERSION = "ZJDE0001";                       // Should be configurable and defaulted to "ZJDE0001"
		public static string JDE_ROLE = "*ALL";
        public static TimeSpan TIMEOUT_IN_SECONDS = new TimeSpan(0,0,30);   // 10 seconds
        public static int MaxResponseContentBufferSize = 25600000;

        public static string JDE_TOKENREQUEST = "tokenrequest";
        public static string JDE_GETPOS = "v2/appstack";
        public static string JDE_GETPOS_NEXT = "v2/appstack/next";
        public static string JDE_DATASERVICE = "v2/dataservice";
        public static string JDE_DATASERVICE_NEXT = "v2/dataservice/next";
        public static string JDE_POSERVICE = "v2/poservice";
        public static string JDE_FILE_GETTEXT = "v2/file/gettext";
        public static string JDE_FILE_UPDATETEXT = "v2/file/updatetext";
        public static string JDE_FILE_LIST = "v2/file/list";
        public static string JDE_FILE_UPLOAD = "v2/file/upload";
        public static string JDE_FILE_DONWLOAD = "v2/file/download";
        public static string JDE_FILE_DELETE = "v2/file/delete";
        public static string JDE_LOG_OUT = "tokenrequest/logout";

		public ServiceConfiguration()
        {
        }
    }
}
