﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

/**
 *
 * @author Khang Nguyen
 */
namespace POReceipt.Shared
{
    public class AIS
    {
        public async static Task<string> GetToken(string username, string password, string deviceName)
        {
            AccessToken accessToken = new AccessToken();
            return await accessToken.GetTokenAsync(username, password, deviceName);
        }

        public async static Task<Dictionary<int, string>> GetSuppliersLookup(string token, string deviceName)
        {
            Suppliers suppliers = new Suppliers();
            return await suppliers.GetAllAsync(token, deviceName);
        }

        public async static Task<List<JObject>> GetPOJsonDataAsync(string token, string deviceName)
        {
            var purchaseOrders = new PurchaseOrders();
            return await purchaseOrders.GetOpenAsync(token, deviceName);
        }

        public async static Task<List<JObject>> SearchPOJsonDataAsync(string token, string deviceName, 
                                                                            string poNumber,
                                                                            bool typeEQUAL,
                                                                            string poType,
                                                                            string supplierName,
                                                                            string recent,
                                                                            DateTime fromD, DateTime toD,
                                                                            string projectCode,
                                                                            string user,
                                                                     string account) {
            var purchaseOrders = new PurchaseOrders();
            return await purchaseOrders.SearchAsync(token, deviceName, poNumber ,typeEQUAL,poType, supplierName, recent, fromD, toD, projectCode, user, account);
        }

        public async static Task<bool> ReceiveFullOrder(string token, string deviceName, int num, string type, string company)
        {
            PurchaseOrders purchaseOrders = new PurchaseOrders();
            return await purchaseOrders.ReceiveFullOrder(token, deviceName, num, type, company);
        }

        public async static Task<bool> ReceivePartialOrder(string token, string deviceName, int poNum, string poType, string poCompany, Dictionary<float, JObject> lines)
        {
            PurchaseOrders purchaseOrders = new PurchaseOrders();
            return await purchaseOrders.ReceivePartialOrder(token, deviceName, poNum, poType, poCompany, lines);
        }

        public async static Task<string> SetOrderLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string text)
        {
            var file = new File();
            return await file.SetOrderLineText(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, text);
        }

        public async static Task<string> GetOrderLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum)
        {
            var file = new File();
            return await file.GetOrderLineText(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum);
        }

        public async static Task<string> GetOrderText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix)
        {
            var file = new File();
            return await file.GetOrderText(token, deviceName, poNum, poType, poCompany, poSuffix);
        }

        public async static Task<string> SetOrderText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, string text)
        {
            File file = new File();
            return await file.SetOrderText(token, deviceName, poNum, poType, poCompany, poSuffix, text);
        }

        public async static Task<string> GetOrderFileList(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix)
        {
            var file = new File();
            return await file.GetOrderFileList(token, deviceName, poNum, poType, poCompany, poSuffix);
        }

        public async static Task<string> getOrderLineFileList(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum)
        {
            var file = new File();
            return await file.GetOrderLineFileList(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum);
        }

        public async static Task<string> SetLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string fileLocation, byte[] imageBytes)
        {
            var file = new File();
            return await file.SetLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, fileLocation, imageBytes);
        }

        public async static Task<string> SetOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, string fileLocation, byte[] imageBytes)
        {
            var file = new File();
            return await file.SetOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, fileLocation, imageBytes);
        }

        public async static Task<Stream> GetOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum)
        {
            var file = new File();
            return await file.GetOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, sequenceNum);
        }

        public async static Task<Stream> GetLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum)
        {
            var file = new File();
            return await file.GetLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, sequenceNum);
        }

        public async static Task<string> DeleteOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum)
        {
            var file = new File();
            return await file.DeleteOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, sequenceNum);
        }

        public async static Task<string> DeleteLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum)
        {
            var file = new File();
            return await file.DeleteLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, sequenceNum);
        }

        public async static Task<bool> Logout(string token)
        {
            AccessToken accessToken = new AccessToken();
            return await accessToken.LogoutAsync(token);
        }

        public async static Task RefreshToken(CancellationToken token)
        {
            App.Native.log("Starting Background Service to refresh token");

            await Task.Run(async () =>
            {
                App.Native.log("Starting Background Service infinate while loop");

                while (true)
                {
                    token.ThrowIfCancellationRequested();
                    try
                    {
                        if  (App.UserData != null 
                             && !string.IsNullOrEmpty(App.UserData.Username) 
                         && !string.IsNullOrEmpty(App.UserData.Password) 
                             &&  !string.IsNullOrEmpty(App.deviceName)) {
                            var newToken = await GetToken(App.UserData.Username, App.UserData.Password, App.deviceName);
                            if (!string.IsNullOrEmpty(newToken))
                            {
                                App.token = newToken;
                                App.Native.log("NEWTOKEN: " + App.token);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        App.Native.log("Token refreshing EROR" + ex.Message);
                    }
                    finally
                    {

                    }

                    await Task.Delay(240000); //Wait 2 minutes - Updated by Khang


                } //End while loop
            }, token);
        }
    

        // Khang added on 13/10
        public async Task<Dictionary<float, float>> GetUnitPrice(string token, string deviceName, int poNum, string poType, string poCompany, Dictionary<float, JObject> lines) {
            PurchaseOrders purchaseOrders = new PurchaseOrders();
            return null; 
        }
    }
}
