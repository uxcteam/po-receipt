﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ModernHttpClient;
using Newtonsoft.Json.Linq;

namespace POReceipt.Shared
{
    public class Suppliers
    {
        private static string CONTROLIDS_SUPPLIER_DATASERVICE = "F0401.AN8|F0101.ALPH";
        private string targetRootURL;
        private string targetDataservice;
        private string targetDataserviceNext;

		public Suppliers()
        {
            targetRootURL = string.Format("{0}{1}", ServiceConfiguration.JDE_HOSTNAME, ServiceConfiguration.JDE_SERVICE_ROOT);
            targetDataservice = string.Format("{0}{1}", targetRootURL, ServiceConfiguration.JDE_DATASERVICE);
            targetDataserviceNext = string.Format("{0}{1}", targetRootURL, ServiceConfiguration.JDE_DATASERVICE_NEXT);
        }

        public async Task<Dictionary<int, string>> GetAllAsync(string token, string deviceName)
		{
            var suppliers = new Dictionary<int, string>();

			// Initial POST request for first page
			string jsonBody = MakeJsonPost_getSupplierLookup(token, deviceName);
			// Send the request

            var client = new HttpClient(new NativeMessageHandler());
            client.MaxResponseContentBufferSize = ServiceConfiguration.MaxResponseContentBufferSize;
			client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;

            var content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            var uri = new Uri(targetDataservice);
            var response = await client.PostAsync(uri, content);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                App.Native.showAlert(string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase), "Error - [GetSupplierLookup]");
                return suppliers;
            }

            var bodyStr = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(bodyStr))
				return suppliers;
            var bodyJson = JObject.Parse(bodyStr);

            var rowsetArray = bodyJson["fs_DATABROWSE_V0401H"]["data"]["gridData"]["rowset"].Value<JArray>();

			if (rowsetArray != null)
			{
				var jObjectArray = rowsetArray.Select(jv => (JObject)jv).ToArray();
                foreach (var jobject in jObjectArray) {
                    var suppId = jobject["F0401_AN8"].Value<int>();
                    var suppName = jobject["F0101_ALPH"].Value<string>();
                    if (string.IsNullOrEmpty(suppName))
                        suppName = suppName.Trim();
                    if (!suppliers.ContainsKey(suppId))
                        suppliers.Add(suppId, suppName);
                }
			}

            bool hasMoreRecords = bodyJson["fs_DATABROWSE_V0401H"]["data"]["gridData"]["summary"]["moreRecords"].Value<bool>();
            int stackId = bodyJson["stackId"].Value<int>();
            int stateId = bodyJson["stateId"].Value<int>();
            string rid = bodyJson["rid"].Value<string>();

            // Use GET request for subsequent pages
            while (hasMoreRecords)
            {
				var parameters = new StringBuilder();
				parameters.Append(string.Format("?{0}={1}", "stackId", stackId));
				parameters.Append(string.Format("&{0}={1}", "stateId", stateId));
                parameters.Append(string.Format("&{0}={1}", "rid", Uri.EscapeDataString(rid)));
                parameters.Append(string.Format("&{0}={1}", "fullGridId", 53));
                parameters.Append(string.Format("&{0}={1}", "formOID", "V0401H"));
				parameters.Append(string.Format("&{0}={1}", "outputType", "GRID_DATA"));
                parameters.Append(string.Format("&{0}={1}", "token", Uri.EscapeDataString(token)));
				parameters.Append(string.Format("&{0}={1}", "deviceName", deviceName));
                parameters.Append(string.Format("&{0}={1}", "returnControlIDs", Uri.EscapeDataString(CONTROLIDS_SUPPLIER_DATASERVICE)));

                var url = string.Format("{0}{1}", targetDataserviceNext, parameters);
				uri = new Uri(url);
                var getClient = new HttpClient(new NativeMessageHandler());
				getClient.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;
				response = await getClient.GetAsync(uri);

				if (response.StatusCode != System.Net.HttpStatusCode.OK)
				{
                    App.Native.showAlert(string.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase), "Error" );
					return suppliers;
				}

				var body = await response.Content.ReadAsStringAsync();
				if (string.IsNullOrEmpty(body))
					return suppliers;
				var json = JObject.Parse(body);

                var rowset= json["fs_DATABROWSE_V0401H"]["data"]["gridData"]["rowset"].Value<JArray>();

				if (rowset != null)
				{
					var jObjectArray = rowset.Select(jv => (JObject)jv).ToArray();
					foreach (var jobject in jObjectArray)
					{
                        var suppId = jobject["F0401_AN8"].Value<int>();
                        var suppName = jobject["F0101_ALPH"].Value<string>();
						if (string.IsNullOrEmpty(suppName))
							suppName = suppName.Trim();
                        if (!suppliers.ContainsKey(suppId))
						    suppliers.Add(suppId, suppName);
					}
				}

			    stackId = json["stackId"].Value<int>();
			    stateId = json["stateId"].Value<int>();
			    rid = json["rid"].Value<string>();
                hasMoreRecords = json["fs_DATABROWSE_V0401H"]["data"]["gridData"]["summary"]["moreRecords"].Value<bool>();
            }

            return suppliers;
		}

        private string MakeJsonPost_getSupplierLookup(string token, string deviceName)
        {
            // Build the JSON
            var json = new JObject();
			json.Add("token", token);
			json.Add("deviceName", deviceName);
            json.Add("targetName", "V0401H");
            json.Add("targetType", "view");
			json.Add("outputType", "GRID_DATA");
            json.Add("dataServiceType", "BROWSE");
            json.Add("maxPageSize", "2000"); // TODO: Possibly make maxPageSize configurable?
            json.Add("returnControlIDs", CONTROLIDS_SUPPLIER_DATASERVICE);
            json.Add("enableNextPageProcessing", true);
            json.Add("nextPageTimeInterval1", "0");
            json.Add("findOnEntry", "TRUE");

            return json.ToString(Newtonsoft.Json.Formatting.None);
        }
    }
}
