using System;
using System.IO;

namespace POReceipt.Shared
{
	public static class Info
	{

		/*
		public static string Begin (string xml)
		{
			try {
				xml = (xml ?? "").Trim ();

				if (serviceInfo.serviceCount == 0) {
					FileUtil.deleteDirectory ("Trx");
				}

				var no = serviceInfo.serviceCount++;

				serviceInfo.requestXML = xml;

				var action = string.Empty;
				try {
					var doc = XmlUtil.parseXML (xml);
					if (doc != null) {
						var node = doc.SelectSingleNode ("//Header/Action");
						if (node != null) {
							action = (node.InnerText ?? "").Trim ().ToUpperInvariant ();
						}
					}
				} catch {
				}
				serviceInfo.action = action;

				var key = no.ToString ("0000") + (action.Length == 0 ? "" : ("-" + action));

				FileUtil.writeFile (Path.Combine ("Trx", key, "req.xml"), xml);

				Util.log ("REQUEST: " + key);

				serviceInfo.requestTime = DateTime.Now;

				return key;
			} catch {
				return string.Empty;
			}
		}

		public static void End (string key, string xml)
		{
			try {
				xml = (xml ?? "").Trim ();

				serviceInfo.responseTime = DateTime.Now;

				serviceInfo.responseXML = xml;

				var status = string.Empty;
				try {
					var doc = XmlUtil.parseXML (xml);
					if (doc != null) {
						var code = "";
						var node = doc.SelectSingleNode ("//Header/ResponseCode");
						if (node != null) {
							code = (node.InnerText ?? "").Trim ();
						}

						var text = "";
						node = doc.SelectSingleNode ("//Header/ResponseDescription");
						if (node != null) {
							text = (node.InnerText ?? "").Trim ();
						}

						if (code.Length == 0) {
							status = text;
						} else {
							status = code;
							if (text.Length > 0) {
								status += " : " + text;
							}
						}
					}
				} catch {
				}
				serviceInfo.status = status;

				FileUtil.writeFile (Path.Combine ("Trx", key, "resp.xml"), xml);

				Util.log ("RESPONSE: " + key + "\nDuration: " + serviceInfo.duration + "\nLength: " + (xml.Length / 1024f).ToString ("N1") + " KB\nStatus: " + status);
			} catch {
			}
		}
		*/

	}
}
