using System;
using System.Net;

using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net.Http;
using System.Xml;
using ModernHttpClient;

namespace POReceipt.Shared
{
	public static class Service
	{
		private static string serviceEndpoint = "https://red-campus-mock-up.mybluemix.net/api/v0/";
		private static string serviceEndpointV1 = "https://red-campus-mock-up.mybluemix.net/api/v1/";

		private static string peopleSoftService = "https://POReceipt01.uxcredrock.com/PSIGW/RESTListeningConnector/";
		private static string peopleSoftSoapService = "https://POReceipt01.uxcredrock.com/PSIGW/PeopleSoftServiceListeningConnector/";

		private static int requestTimeOutSeconds = 15;


        private static string JDE_HOSTNAME = "https://jde02.uxcredrock.com";  // Should be configurable
        private static string JDE_SERVICE_ROOT = "/mobile/jderest/";          // Should be configurable
        //  private static final String JDE_USERNAME = "redrock";                     // Entered by user
        //  private static final String JDE_PASSWORD = "Redrock1!";                   // Entered by user
        //  private static final String JDE_DEVICE_NAME = "testing";                  // Should be some unique string from the calling device
        private static string JDE_ENVIRONMENT = "JPD920";                     // Should be configurable
        private static string JDE_VERSION = "ZJDE0001";                       // Should be configurable and defaulted to "ZJDE0001"
        private static string JDE_ROLE = "*ALL";    

		/// <summary>
		/// Calls the rest API async.
		/// </summary>
		/// <returns>The rest API async.</returns>
		/// <param name="url">URL.</param>
		public static async Task<JObject> CallRestApiAsync(string url)
		{
			var request = WebRequest.Create(string.Format("{0}{1}", serviceEndpoint, url));
			request.ContentType = "application/json";
			request.Method = "GET";

			//request.Timeout = requestTimeOutSeconds*1000;

			using (HttpWebResponse response = await request.GetResponseAsync() as HttpWebResponse)
			{
				if (response.StatusCode != HttpStatusCode.OK)
					Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
				else
				{
					using (StreamReader reader = new StreamReader(response.GetResponseStream()))
					{
						var content = reader.ReadToEnd();
						if (string.IsNullOrWhiteSpace(content))
						{
							Console.Out.WriteLine("Response contained empty body...");
						}
						else {
							return JObject.Parse(content);
						}

					}
				}
			}
			return null;
		}

		/// <summary>
		/// Calls the rest API async.
		/// </summary>
		/// <returns>The rest API async.</returns>
		/// <param name="url">URL.</param>
		public static async Task<JObject> CallRestApiAsyncV1(string url)
		{
			var request = WebRequest.Create(string.Format("{0}{1}", serviceEndpointV1, url));
			request.ContentType = "application/json";
			request.Method = "GET";
			//request.Timeout = requestTimeOutSeconds * 1000;

			using (HttpWebResponse response = await request.GetResponseAsync() as HttpWebResponse)
			{
				if (response.StatusCode != HttpStatusCode.OK)
					Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
				else
				{
					using (StreamReader reader = new StreamReader(response.GetResponseStream()))
					{
						var content = reader.ReadToEnd();
						if (string.IsNullOrWhiteSpace(content))
						{
							Console.Out.WriteLine("Response contained empty body...");
						}
						else {
							return JObject.Parse(content);
						}

					}
				}
			}
			return null;
		}

		/// <summary>
		/// CallPostRequest
		/// </summary>
		/// <returns>The post request.</returns>
		/// <param name="url">URL.</param>
		/// <param name="data">Data.</param>
		public static async Task<bool> CallPostRequest(string url, string data)
		{
			var uri = new Uri(string.Format("{0}{1}", serviceEndpoint, url));

            var client = new HttpClient(new NativeMessageHandler());
			client.MaxResponseContentBufferSize = 256000;

			var content = new StringContent(data, Encoding.UTF8, "application/json");
			//client.Timeout = requestTimeOutSeconds * 1000;

			HttpResponseMessage response = await client.PostAsync(uri, content);
			if (response.IsSuccessStatusCode)
				return true;

			return false;
		}


		/// <summary>
		/// CallPostRequest
		/// </summary>
		/// <returns>The post request.</returns>
		/// <param name="url">URL.</param>
		/// <param name="data">Data.</param>
		public static async Task<bool> CallPostRequestV1(string url, string data)
		{
			var uri = new Uri(string.Format("{0}{1}", serviceEndpointV1, url));

            var client = new HttpClient(new NativeMessageHandler());
			client.MaxResponseContentBufferSize = 256000;
			//client.Timeout = requestTimeOutSeconds * 1000;

			var content = new StringContent(data, Encoding.UTF8, "application/json");

			HttpResponseMessage response = await client.PostAsync(uri, content);
			if (response.IsSuccessStatusCode)
				return true;

			return false;
		}

		/// <summary>
		/// CallPutRequest
		/// </summary>
		/// <returns>The put request.</returns>
		/// <param name="url">URL.</param>
		/// <param name="data">Data.</param>
		public static async Task<bool> CallPutRequest(string url, string data)
		{
			var uri = new Uri(string.Format("{0}{1}", serviceEndpointV1, url));

            var client = new HttpClient(new NativeMessageHandler());
			client.MaxResponseContentBufferSize = 256000;
			//client.Timeout = requestTimeOutSeconds * 1000;

			var content = new StringContent(data, Encoding.UTF8, "application/json");

			HttpResponseMessage response = await client.PutAsync(uri, content);
			if (response.IsSuccessStatusCode)
				return true;

			return false;
		}

		/// <summary>
		/// CallPostRequest
		/// </summary>
		/// <returns>The post request.</returns>
		/// <param name="url">URL.</param>
		/// <param name="data">Data.</param>
		public static async Task<string> CallPeopleSoftRequest(string url, string data)
		{
			var uri = new Uri(string.Format("{0}{1}", peopleSoftService, url));

            var client = new HttpClient(new NativeMessageHandler());
			client.MaxResponseContentBufferSize = 256000;
			//client.Timeout = requestTimeOutSeconds * 1000;

			var content = new StringContent(data, Encoding.UTF8, "application/xml");

			HttpResponseMessage response = await client.PostAsync(uri, content);
			if (response.IsSuccessStatusCode)
			{
				var xmlContent = await response.Content.ReadAsStringAsync();
				return xmlContent;
			}
			        
			return null;
		}


		//private static string callServiceSoap(string url, string reqXML)
		//{

		//	SSF_GET_STUDENT_ACCT_REQ studentReq = new SSF_GET_STUDENT_ACCT_REQ();
		//	studentReq.STUDENT_ACCOUNT_REQUEST = new SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUEST();

		//	studentReq.STUDENT_ACCOUNT_REQUEST.EMPLID = "0092";
		//	studentReq.STUDENT_ACCOUNT_REQUEST.BUSINESS_UNIT = "PSUNV";

		//	SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUESTACCOUNT_VIEW_REQUEST viewRequest1 = new SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUESTACCOUNT_VIEW_REQUEST();
		//	viewRequest1.SSF_ACCOUNT_VIEW = SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUESTACCOUNT_VIEW_REQUESTSSF_ACCOUNT_VIEW.Item01;

		//	SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUESTACCOUNT_VIEW_REQUEST viewRequest2 = new SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUESTACCOUNT_VIEW_REQUEST();
		//	viewRequest2.SSF_ACCOUNT_VIEW = SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUESTACCOUNT_VIEW_REQUESTSSF_ACCOUNT_VIEW.Item02;

		//	SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUESTACCOUNT_VIEW_REQUEST viewRequest3 = new SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUESTACCOUNT_VIEW_REQUEST();
		//	viewRequest3.SSF_ACCOUNT_VIEW = SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUESTACCOUNT_VIEW_REQUESTSSF_ACCOUNT_VIEW.Item03;

		//	SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUESTACCOUNT_VIEW_REQUEST viewRequest4 = new SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUESTACCOUNT_VIEW_REQUEST();
		//	viewRequest4.SSF_ACCOUNT_VIEW = SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUESTACCOUNT_VIEW_REQUESTSSF_ACCOUNT_VIEW.Item04;

		//	studentReq.STUDENT_ACCOUNT_REQUEST.ACCOUNT_VIEWS = 
		//		new SSF_GET_STUDENT_ACCT_REQSTUDENT_ACCOUNT_REQUESTACCOUNT_VIEW_REQUEST[] {viewRequest1,viewRequest2 ,viewRequest3 ,viewRequest4 };

		//	var ser = new SSF_FINANCIALS();
		//	ser.AllowAutoRedirect = true;
		//	ser.UseDefaultCredentials = true;
		//	ser.SSF_GET_STUDENT_ACCOUNTAsync(studentReq);

		//	////var result = ser.MobileRequest(reqXML);
		//	//ser.MobileRequest(reqXML);

		//	return null;

		//}

		public static async Task<string> callFinancialSoapAsync(string emplId)
		{
			using (var client = new WebClient()
			{
				Encoding = Encoding.UTF8,
			})
			{
				client.Headers.Add(HttpRequestHeader.ContentType, "text/xml; charset=utf-8");
				client.Headers.Add("SOAPAction", "SSF_GET_STUDENT_ACCOUNT.v1");

				var reqBytes = buildSoapPacket(emplId);
				var respBytes = await client.UploadDataTaskAsync("https://POReceipt01.uxcredrock.com/PSIGW/PeopleSoftServiceListeningConnector/PSFT_CS", "POST", reqBytes);
				var respXML = string.Empty;
				if (respBytes != null && respBytes.Length > 0)
					respXML = Encoding.UTF8.GetString(respBytes);

				return respXML;
			}
		}

		private static byte[] buildSoapPacket(string emplId)
		{
			var packetFormat = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ser=""http://xmlns.oracle.com/Enterprise/HCM/services"">
   <soapenv:Header/>
   <soapenv:Body>
      <ser:SSF_GET_STUDENT_ACCT_REQ>
         <ser:STUDENT_ACCOUNT_REQUEST>
            <ser:EMPLID>{0}</ser:EMPLID>
            <ser:BUSINESS_UNIT>PSUNV</ser:BUSINESS_UNIT>
            <ser:ACCOUNT_VIEWS>
               <ser:ACCOUNT_VIEW_REQUEST>
                  <ser:SSF_ACCOUNT_VIEW>01</ser:SSF_ACCOUNT_VIEW>
               </ser:ACCOUNT_VIEW_REQUEST>
               <ser:ACCOUNT_VIEW_REQUEST>
                  <ser:SSF_ACCOUNT_VIEW>02</ser:SSF_ACCOUNT_VIEW>
               </ser:ACCOUNT_VIEW_REQUEST>
               <ser:ACCOUNT_VIEW_REQUEST>
                  <ser:SSF_ACCOUNT_VIEW>03</ser:SSF_ACCOUNT_VIEW>
               </ser:ACCOUNT_VIEW_REQUEST>
               <ser:ACCOUNT_VIEW_REQUEST>
                  <ser:SSF_ACCOUNT_VIEW>04</ser:SSF_ACCOUNT_VIEW>
               </ser:ACCOUNT_VIEW_REQUEST>
            </ser:ACCOUNT_VIEWS>
         </ser:STUDENT_ACCOUNT_REQUEST>
      </ser:SSF_GET_STUDENT_ACCT_REQ>
   </soapenv:Body>
</soapenv:Envelope>";

			var packet = string.Format(packetFormat, emplId);

			return Encoding.UTF8.GetBytes(packet);
		}

		public static async Task<string> callSoapAsync(string xmlPackage, string action)
		{
			using (var client = new WebClient()
			{
				Encoding = Encoding.UTF8,
			})
			{
				client.Headers.Add(HttpRequestHeader.ContentType, "text/xml; charset=utf-8");
				client.Headers.Add("SOAPAction", action);

				var reqBytes = Encoding.UTF8.GetBytes(xmlPackage);
				var respBytes = await client.UploadDataTaskAsync("https://POReceipt01.uxcredrock.com/PSIGW/PeopleSoftServiceListeningConnector/PSFT_CS", "POST", reqBytes);
				var respXML = string.Empty;
				if (respBytes != null && respBytes.Length > 0)
					respXML = Encoding.UTF8.GetString(respBytes);

				return respXML;
			}
		}

	}
}

