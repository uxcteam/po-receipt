using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO.Compression;
using System.Text;


namespace POReceipt.Shared
{
	public static class ConvUtil
	{
		public static int getInt (string str, int defaultValue = 0)
		{
			if (string.IsNullOrWhiteSpace (str)) {
				return defaultValue;
			}

			int val;
			if (int.TryParse (str, out val)) {
				return val;
			}
			return defaultValue;
		}

		public static string getString (byte[] bytes)
		{
			if (bytes == null) {
				return string.Empty;
			}

			return Encoding.UTF8.GetString (bytes);
		}

		public static byte[] getBytes (string str)
		{
			if (string.IsNullOrEmpty (str)) {
				return new byte[0];
			}

			return Encoding.UTF8.GetBytes (str);
		}

		public static byte[] compress (byte[] raw)
		{
			if (raw == null || raw.Length == 0) {
				return new byte[0];
			}

			using (MemoryStream memory = new MemoryStream ()) {
				using (GZipStream gzip = new GZipStream (memory,
					                         CompressionMode.Compress, true)) {
					gzip.Write (raw, 0, raw.Length);
				}
				return memory.ToArray ();
			}
		}

		public static byte[] decompress (byte[] gzip)
		{
			if (gzip == null || gzip.Length == 0) {
				return new byte[0];
			}

			using (GZipStream stream = new GZipStream (new MemoryStream (gzip), CompressionMode.Decompress)) {
				const int size = 4096;
				byte[] buffer = new byte[size];
				using (MemoryStream memory = new MemoryStream ()) {
					int count = 0;
					do {
						count = stream.Read (buffer, 0, size);
						if (count > 0) {
							memory.Write (buffer, 0, count);
						}
					} while (count > 0);
					return memory.ToArray ();
				}
			}
		}

		public static string strip (string source, string strStart, string strEnd)
		{
			if (!string.IsNullOrWhiteSpace (source)) {
				var posStart = source.IndexOf (strStart);
				if (posStart > -1) {
					posStart += strStart.Length;
					var posEnd = source.IndexOf (strEnd, posStart);
					if (posEnd > posStart) {
						var len = posEnd - posStart;
						if (len <= source.Length) {
							var str = source.Substring (posStart, len); 

							return str.Trim ();
						}
					}
				}
			}

			return string.Empty;
		}


		public static bool serialize (string resource, object obj)
		{
			var filePath = Path.Combine (FileUtil.getDocumentPath (), resource);

			Directory.CreateDirectory (Path.GetDirectoryName (filePath));

			using (var fs = new FileStream (filePath, FileMode.Create, FileAccess.Write)) {
				var formatter = new BinaryFormatter ();
				try {
					formatter.Serialize (fs, obj);

					return true;
				} catch (Exception e) {
					App.Native.log (e);

					return false;
				} finally {
					fs.Close ();
				}
			}
		}

		public static T deserialize<T> (string resource)
		{
			var filePath = Path.Combine (FileUtil.getDocumentPath (), resource);

			using (var fs = new FileStream (filePath, FileMode.Open, FileAccess.Read)) {
				var formatter = new BinaryFormatter ();
				try {
					var obj = formatter.Deserialize (fs);

					if (obj is T) {
						return (T)obj;
					}
				} catch (Exception e) {
					App.Native.log (e);
				} finally {
					fs.Close ();
				}
			}

			return default(T);
		}

		public static Stream toStream (string str)
		{
			var stream = new MemoryStream ();
			var writer = new StreamWriter (stream);

			writer.Write (str ?? string.Empty);
			writer.Flush ();

			stream.Position = 0;

			return stream;
		}
	}
}

