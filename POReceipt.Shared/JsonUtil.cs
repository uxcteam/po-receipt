using System;
using System.IO;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace POReceipt.Shared
{
	public static class JsonUtil
	{
		public static string getJSON (object obj)
		{
			if (obj == null) {
				return string.Empty;
			}

			var settings = new JsonSerializerSettings {
				Formatting = Newtonsoft.Json.Formatting.Indented,
				NullValueHandling = NullValueHandling.Ignore,
				DefaultValueHandling = DefaultValueHandling.Populate,
				DateFormatString = "dd/MM/yyyy"
			};

			return JsonConvert.SerializeObject (obj, settings);
		}

		public static T getObject<T> (string json)
		{
			if (string.IsNullOrEmpty (json)) {
				return default(T);
			}

			var settings = new JsonSerializerSettings {
				NullValueHandling = NullValueHandling.Ignore,
				DefaultValueHandling = DefaultValueHandling.Populate,
				DateFormatString = "dd/MM/yyyy"
			};

			return JsonConvert.DeserializeObject<T> (json, settings);
		}

		public static void writeBSON (string resource, object obj)
		{
			var filePath = Path.Combine (FileUtil.getDocumentPath (), resource);

			Directory.CreateDirectory (Path.GetDirectoryName (filePath));

			using (var fs = new FileStream (filePath, FileMode.Create, FileAccess.Write)) {
				try {
					writeBSON (fs, obj);
				} catch (Exception e) {
					Util.log (e);
				} finally {
					fs.Close ();
				}
			}
		}

		public static void writeBSON (Stream stream, object obj)
		{
			if (stream == null) {
				return;
			}

			using (var bs = new BufferedStream (stream)) {
				using (BsonWriter writer = new BsonWriter (bs)) {
					var serializer = new JsonSerializer () {
						MissingMemberHandling = MissingMemberHandling.Ignore,
						ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
					};
					serializer.Serialize (writer, obj);
				}
			}
		}

		public static T readBSON<T> (string resource)
		{
			var filePath = Path.Combine (FileUtil.getDocumentPath (), resource);

			if (!System.IO.File.Exists (filePath)) {
				return default(T);
			}

			using (var fs = new FileStream (filePath, FileMode.Open, FileAccess.Read)) {
				try {
					return readBSON<T> (fs);
				} catch (Exception e) {
					Util.log (e);
				} finally {
					fs.Close ();
				}
			}

			return default(T);
		}

		public static T readBSON<T> (Stream stream)
		{
			if (stream == null) {
				return default(T);
			}

			using (var bs = new BufferedStream (stream)) {
				using (BsonReader reader = new BsonReader (bs)) {
					var serializer = new JsonSerializer () {
						MissingMemberHandling = MissingMemberHandling.Ignore,
						ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
					};
					return serializer.Deserialize<T> (reader);
				}
			}
		}

		public class SingleOrArrayConverter<T> : JsonConverter
		{
			public override bool CanConvert (Type objectType)
			{
				return (objectType == typeof(List<T>));
			}

			public override object ReadJson (JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
			{
				JToken token = JToken.Load (reader);
				if (token.Type == JTokenType.Array) {
					return token.ToObject<List<T>> ();
				}
				return new List<T> { token.ToObject<T> () };
			}

			public override bool CanWrite {
				get { return true; }
			}

			public override void WriteJson (JsonWriter writer, object value, JsonSerializer serializer)
			{
				serializer.Serialize (writer, value);
			}
		}

		public class SingleConverter<T> : JsonConverter
		{
			public override bool CanConvert (Type objectType)
			{
				return (objectType == typeof(T)) || (objectType == typeof(List<T>));
			}

			public override object ReadJson (JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
			{
				JToken token = JToken.Load (reader);
				if (token.Type == JTokenType.Array) {
					return token.ToObject<List<T>> ().First ();
				}
				return token.ToObject<T> ();
			}

			public override bool CanWrite {
				get { return true; }
			}

			public override void WriteJson (JsonWriter writer, object value, JsonSerializer serializer)
			{
				serializer.Serialize (writer, value);
			}
		}
	}
}

