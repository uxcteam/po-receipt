using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;
using Xamarin.Forms;

//using Plugin.Connectivity;
//using Plugin.ExternalMaps;
using Plugin.Messaging;
//using Plugin.Geolocator;
using Plugin.Connectivity;
using System.Text;
using ModernHttpClient;

namespace POReceipt.Shared
{
	public static class Util
	{

        public static async Task<HttpResponseMessage> POST(string uriStr, string bodyContentStr) {
            var client = new HttpClient(new NativeMessageHandler());
			client.MaxResponseContentBufferSize = 256000;
			client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;

			var content = new StringContent(bodyContentStr, Encoding.UTF8, "application/json");
			var uri = new Uri(uriStr);
			return await client.PostAsync(uri, content);
        }

        public static async Task<HttpResponseMessage> POSTMedia(string uriStr, string bodyContentStr)
        {
            var client = new HttpClient(new NativeMessageHandler());
            client.MaxResponseContentBufferSize = 9999999;
            client.Timeout = ServiceConfiguration.TIMEOUT_IN_SECONDS;

            var content = new StringContent(bodyContentStr, Encoding.UTF8, "application/json");
            var uri = new Uri(uriStr);
            return await client.PostAsync(uri, content);
        }

		public static void showPage (Page page)
		{
			Device.BeginInvokeOnMainThread (() => Application.Current.MainPage.Navigation.PushAsync (page));
		}

		public static bool ifDebug (Action action = null)
		{
			#if DEBUG
			if (action != null) {
				action.Invoke ();
			}
			return true;
			#else
			return false;
			#endif
		}

		public static bool ifRelease (Action action = null)
		{
			#if DEBUG
			return false;
			#else
			if (action != null) {
				action.Invoke ();
			}
			return true;
			#endif
		}

//		public static async Task<Types.Location> getLocation ()
//		{
//			var res = new Types.Location ();

////			if (ifDebug ()) {	// Fix when simulator doesn't provide the location - when in debug, pass these instead
////				res.Latitude = 53.7844933;
////				res.Longitude = -2.6557898;
////				res.Success = true;
////				return res;
////			}

//			try {
//				var locator = CrossGeolocator.Current;
//				locator.DesiredAccuracy = 50;

//				var position = await locator.GetPositionAsync (2000);

//				// Added by Khang
//				if (position != null) {
//					res.Timestamp = position.Timestamp;
//					res.Latitude = position.Latitude;
//					res.Longitude = position.Longitude;
//					res.Success = true;
////					if (!await LoginModel.checkOnline()) {
////						App.Native.log("Update SyncOffset from location data");
////						var offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
////
////						var serverDatetime = position.Timestamp.ToLocalTime().DateTime.Add(offset);
////						App.Native.log(serverDatetime.ToString());
////						//Model.SyncOffset = DateTime.Now.Subtract(serverDatetime);
////					}
//				}
//			} catch (Exception ex) {
//				res.Exception = ex;
//				res.Success = false;
//			}

//			return res;
//		}

		//public static async Task<string> getAddressFromLocation(Types.Location location)
		//{
		//	var geo = new Geocoder();

		//	Position position = new Position(location.Latitude, location.Longitude);
		//	var addresses = await geo.GetAddressesForPositionAsync(position);

		//	string addressText = "";
		//	foreach (var address in addresses)
		//		addressText += address + ", ";
			
		//	return addressText.ToString().Replace("\n", ", "); ;
		//}

		public static bool isNetworkConnected ()
		{
			return CrossConnectivity.Current.IsConnected;
		}

		public static async Task<bool> isHostReachable (string host, int port)
		{
			var status = await CrossConnectivity.Current.IsRemoteReachable (host, port, (int)TimeSpan.FromSeconds (5).TotalMilliseconds);

			log ("Host '" + host + ":" + port + "' reachable : " + status);

			return status;
		}

		public static async Task<byte[]> resizeImage (byte[] imageData, float width, float height, bool allowUpscale)
		{
			return await ImageResizer.ResizeImageAsync (imageData, width, height, allowUpscale);
		}

		public static void navigateToMap (string labelToDisplay, string location)
		{
			Device.BeginInvokeOnMainThread (() => {
				//try {
				//	var parts = new List<string> (location.Split (new string[]{ ", " }, StringSplitOptions.RemoveEmptyEntries));
				//	while (parts.Count < 3) {
				//		parts.Add (string.Empty);
				//	}
				//	CrossExternalMaps.Current.NavigateTo (
				//		street: parts [0], city: parts [1], zip: parts [2], country: "UK",
				//		name: labelToDisplay, state: string.Empty, countryCode: "GBR");	
				//} catch (Exception e) {
				//	log (e);
				//}
			});
		}

		public static void navigateToMap (string labelToDisplay, double latitude, double longitude)
		{
			Device.BeginInvokeOnMainThread (() => {
				//try {
				//	CrossExternalMaps.Current.NavigateTo (labelToDisplay, latitude, longitude);	
				//} catch (Exception e) {
				//	log (e);
				//}
			});
		}

		public static void showAlert (string message, string title)
		{
			App.Native.hideHUD ();

			if (Device.OS == TargetPlatform.iOS) {
				if (string.IsNullOrEmpty (title)) {
					title = message;
					message = null;
				}
			}

			Device.BeginInvokeOnMainThread (() => {
				Application.Current.MainPage.DisplayAlert (title ?? "", message ?? "", "OK");
			});
		}

		public static async Task<bool> showConfirm (string message, string title)
		{
			App.Native.hideHUD ();

			if (Device.OS == TargetPlatform.iOS) {
				if (string.IsNullOrEmpty (title)) {
					title = message;
					message = null;
				}
			}

			var taskCompletion = new TaskCompletionSource<bool> ();

			Device.BeginInvokeOnMainThread (async () => {
				taskCompletion.SetResult (await Application.Current.MainPage.DisplayAlert (title ?? "", message ?? "", "OK", "Cancel"));
			});

			return await taskCompletion.Task;
		}

		public static async Task<bool> showConfirmYesNo (string message, string title)
		{
			App.Native.hideHUD ();

			if (Device.OS == TargetPlatform.iOS) {
				if (string.IsNullOrEmpty (title)) {
					title = message;
					message = null;
				}
			}

			var taskCompletion = new TaskCompletionSource<bool> ();

			Device.BeginInvokeOnMainThread (async () => {
				taskCompletion.SetResult (await Application.Current.MainPage.DisplayAlert (title ?? "", message ?? "", "Yes", "No"));
			});

			return await taskCompletion.Task;
		}

		public static Task wait (int miliseconds)
		{
			return Task.Run (() => {
				Thread.Sleep (TimeSpan.FromMilliseconds (miliseconds));
			});
		}

		public static void invokeLater (Action action)
		{
			Task.Factory.StartNew (() => {
				Thread.Yield ();

				Device.BeginInvokeOnMainThread (action);
			});
		}

		public static void openURL (string url)
		{
			Device.BeginInvokeOnMainThread (() => {
				try {
					Device.OpenUri (new Uri (url));
				} catch (Exception e) {
					showError (e);
				}
			});
		}

		public static async Task<string> showActionSheet (string message, params string[] options)
		{
			App.Native.hideHUD ();

			var taskCompletion = new TaskCompletionSource<string> ();

			Device.BeginInvokeOnMainThread (async () => {
				var resp = await Application.Current.MainPage.DisplayActionSheet (message, "OK", null, options);

				taskCompletion.SetResult (resp);
			});

			return await taskCompletion.Task;
		}

		public static void showError (Exception exception)
		{
			App.Native.hideHUD ();

			if (exception == null) {
				return;
			}

			showAlert(exception.Message, exception.GetType().Name.Replace("Exception", " Exception"));

			/*
			Device.BeginInvokeOnMainThread (async () => {
				var reason = exception.GetType ().Name.Replace ("Exception", " Exception").Replace ("Error", " Error").Replace ("  ", " ");
				var message = reason + "\n\n" + exception.Message;

				if (exception is System.Web.Services.Protocols.SoapException) {
					reason = "The request to the web service did not succeed and the server responded with an error.";
					message = "Service Error\n\n" + reason + "\n\nThis is either data related or there could be an issue with the service availability.\n\nPlease contact the service team.";
				} else if (exception is System.Net.WebException) {
					reason = "The request to the web service did not succeed and the connection responded with an error.";
					message = reason + "\n\nThis could be due to:\n\n●  Network related issues\n\n●  Issues with service availability.\n\nPlease contact the service team.";
				}

				const string paramDisplay = "Display Error";
				const string paramEmail = "Email Error";
				const string paramLog = "Write to Device Log";

				var paramList = new List<string> {
					paramDisplay, paramLog
				};

				var emailTask = MessagingPlugin.EmailMessenger;
				if (emailTask.CanSendEmail) {
					paramList.Insert (1, paramEmail);
				}

				var resp = await showActionSheet (message, paramList.ToArray ());

				var body = "<html><body style='font-family: Helvetica; font-size: 7pt; padding: 1em'><p><b>"
				           + "<p><b>" + XmlUtil.escapeHTML (reason) + "</b></p>"
				           + "<hr><p>Application local time:</p><p><code>" + XmlUtil.escapeHTML (DateTime.Now.ToString ("R")) + "</code></p>"
				           + "<hr><p>Caused by:</p><p><pre>" + XmlUtil.escapeHTML (exception.GetType ().FullName) + "</pre></p>"
				           + "<hr><p>Error message:</p><p><pre style='color:red'>" + XmlUtil.escapeHTML (exception.Message) + "</pre></p>"
				           + "<hr><p>Last request (" + (Info.serviceInfo.requestXML ?? "").Length.ToString ("N0") + " chars):</p><p><pre style='color:navy'>" + XmlUtil.escapeHTML (Info.serviceInfo.requestXML) + "</pre></p>"
				           + "<hr><p>Last response (" + (Info.serviceInfo.responseXML ?? "").Length.ToString ("N0") + " chars):</p><p><pre style='color:navy'>" + XmlUtil.escapeHTML (Info.serviceInfo.responseXML) + "</pre></p>"
				           + "<hr><p>Stack trace:</p><p><pre>" + XmlUtil.escapeHTML (exception.ToString ()) + "</pre></p>"
				           + "<hr><p style='color:gray'>End of message</p></body></html>";

				if (paramDisplay.Equals (resp)) {
					showAlert (exception.Message, exception.GetType ().Name.Replace ("Exception", " Exception"));
				} else if (paramEmail.Equals (resp)) {
					try {
						var email = new EmailMessageBuilder ()
							.Subject ("Server Error")
							.BodyAsHtml (body)
							.Build ();

						emailTask.SendEmail (email);
					} catch {
						showAlert ("Please ensure that the email client is accessible.", "Cannot send email"); 
					}
				} else if (paramLog.Equals (resp)) {
					log ("\n\nERROR:\n" + body + "\n\n");
				}
			});*/

		}

		public static void log (object value)
		{
			Task.Factory.StartNew (() => {
				var val = Convert.ToString (value);
				var isError = value is Exception;
				if (!isError) {
					isError = val.Contains ("<ResponseCode>02</ResponseCode>");
				}

				if (ifDebug ()) {
					var msg = "\n" + (isError ? "-- ERROR --\n" : "") + value + "\n";
					try {
						Console.WriteLine (msg);
					} catch {
						System.Diagnostics.Debug.WriteLine (msg);
					}
				}
			});
		}
	}
}
