using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using System.Threading.Tasks;
using System.Threading;

#if __IOS__
using System.Drawing;
using UIKit;
using CoreGraphics;
#endif

#if __ANDROID__
using Android.Graphics;
#endif

#if WINDOWS_PHONE
using Microsoft.Phone;
using System.Windows.Media.Imaging;
#endif

namespace POReceipt.Shared
{
	static class ImageResizer
	{
		public static async Task<byte[]> ResizeImageAsync (byte[] imageData, float width, float height, bool allowUpscale)
		{
			var completionSource = new TaskCompletionSource<byte[]> ();

			await Task.Factory.StartNew (() => {
				byte[] bytes = null;

				try {
					if (imageData != null && imageData.Length > 0) {
						// fix issue timeout when sending image
						#if __IOS__
						bytes = ImageResizer.ResizeImage (imageData, width, height, allowUpscale, 0.35f);
						#else
						bytes = ImageResizer.ResizeImage (imageData, width, height, allowUpscale, 0.45f);
						#endif
					}
				} catch (Exception e) {
					Util.log (e);
				}

				completionSource.SetResult (bytes);
			});

			return await completionSource.Task;
		}

		static ImageResizer ()
		{
		}

		public static byte[] ResizeImage (byte[] imageData, float width, float height, bool allowUpscale, float quality)
		{
			#if __IOS__
			return ResizeImageIOS (imageData, width, height, allowUpscale, quality);
			#endif
			#if __ANDROID__
			return ResizeImageAndroid (imageData, width, height, allowUpscale, quality);
			#endif
			#if WINDOWS_PHONE
			return ResizeImageWinPhone(imageData, width, height);
			#endif
		}

		#if __IOS__
		
		public static byte[] ResizeImageIOS (byte[] imageData, float width, float height, bool allowUpscale, float quality)
		{
			UIImage originalImage = ImageFromByteArray (imageData);

			if (originalImage == null) {
				return null;
			}

			var origHeight = originalImage.Size.Height;
			var origWidth = originalImage.Size.Width;

			if (origWidth <= width && origHeight <= height) {
				if (!allowUpscale) {
					return imageData;
				}
			}

			nfloat targetHeight = 0;
			nfloat targetWidth = 0;

			if (origHeight > origWidth) {
				targetHeight = height;
				nfloat ratio = origHeight / height;
				targetWidth = origWidth / ratio;
			} else {
				targetWidth = width;
				nfloat ratio = origWidth / width;
				targetHeight = origHeight / ratio;
			}

			width = (float)targetWidth;
			height = (float)targetHeight;

			UIGraphics.BeginImageContext (new SizeF (width, height));
			originalImage.Draw (new RectangleF (0, 0, width, height));

			var resizedImage = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();

			float qual = quality;
			qual = Math.Max (qual, 0f);
			qual = Math.Min (qual, 1f);

			var bytesImage = resizedImage.AsJPEG (qual).ToArray ();
			resizedImage.Dispose ();

			return bytesImage;
		}

		public static UIKit.UIImage ImageFromByteArray (byte[] data)
		{
			if (data == null) {
				return null;
			}
			//
			UIKit.UIImage image;
			try {
				image = new UIKit.UIImage (Foundation.NSData.FromArray (data));
			} catch (Exception e) {
				Console.WriteLine ("Image load failed: " + e.Message);
				return null;
			}
			return image;
		}
		#endif

		#if __ANDROID__
		public static byte[] ResizeImageAndroid (byte[] imageData, float width, float height, bool allowUpscale, float quality)
		{
			Bitmap originalImage = BitmapFactory.DecodeByteArray (imageData, 0, imageData.Length);
			
			var origHeight = originalImage.Height;
			var origWidth = originalImage.Width;

			if (origWidth <= width && origHeight <= height) {
				if (!allowUpscale) {
					return imageData;
				}
			}

			float targetHeight = 0;
			float targetWidth = 0;

			if (origHeight > origWidth) {
				targetHeight = height;
				float teiler = origHeight / height;
				targetWidth = origWidth / teiler;
			} else {
				targetWidth = width;
				float teiler = origWidth / width;
				targetHeight = origHeight / teiler;
			}
			
			Bitmap resizedImage = Bitmap.CreateScaledBitmap (originalImage, (int)targetWidth, (int)targetHeight, false);
			originalImage.Recycle (); // Khang

			int qual = (int)Math.Round (quality * 100f);
			qual = Math.Max (qual, 0);
			qual = Math.Min (qual, 100);

			using (MemoryStream ms = new MemoryStream ()) {
				resizedImage.Compress (Bitmap.CompressFormat.Jpeg, qual, ms);
				return ms.ToArray ();
			}
		}
		#endif

		#if WINDOWS_PHONE
		public static byte[] ResizeImageWinPhone(byte[] imageData, float width, float height)
		{
		byte[] resizedData;

		using (MemoryStream streamIn = new MemoryStream(imageData))
		{
		WriteableBitmap bitmap = PictureDecoder.DecodeJpeg(streamIn, (int)width, (int)height);

		float ZielHoehe = 0;
		float ZielBreite = 0;

		float Hoehe = bitmap.PixelHeight;
		float Breite = bitmap.PixelWidth;

		if (Hoehe > Breite)
		{
		ZielHoehe = height;
		float teiler = Hoehe / height;
		ZielBreite = Breite / teiler;
		}
		else
		{
		ZielBreite = width;
		float teiler = Breite / width;
		ZielHoehe = Hoehe / teiler;
		}
		                
		using (MemoryStream streamOut = new MemoryStream())
		{
		bitmap.SaveJpeg(streamOut, (int)ZielBreite, (int)ZielHoehe, 0, 100);
		resizedData = streamOut.ToArray();
		}
		}
		return resizedData;
		}
		#endif
	}
}