﻿using System;
using CoreGraphics;
using POReceipt;
using POReceipt.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(UnselectableListView), typeof(UnselectableListViewRenderer))]
namespace POReceipt.iOS
{
	public class UnselectableListViewRenderer : ListViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				Control.AllowsSelection = false;
			}
		}
	}
}
