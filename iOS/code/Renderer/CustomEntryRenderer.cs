﻿using System;
using CoreGraphics;
using POReceipt;
using POReceipt.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace POReceipt.iOS
{
	public class CustomEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				Control.TextColor = UIColor.FromRGBA(74,74,74,255);
                Control.Layer.BorderColor = Color.FromHex("F1EEEE").ToCGColor();
				Control.BorderStyle = UITextBorderStyle.None;
				Control.Layer.BorderWidth = 1.5f;
				Control.LeftView = new UIView(new CGRect(0, 0, 8, Control.Frame.Height));
				Control.LeftViewMode = UITextFieldViewMode.Always;

				var customEntry = e.NewElement as CustomEntry;
				if (customEntry == null)
					return;
				// do customization here
			}
		}
	}
}
