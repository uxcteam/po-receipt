﻿using System;
using CoreGraphics;
using POReceipt;
using POReceipt.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace POReceipt.iOS
{
	public class CustomEditorRenderer : EditorRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				// do whatever you want to the UITextField here!
				//Control.BackgroundColor = UIColor.FromRGB(204, 153, 255);
				//Control.BorderStyle = UITextBorderStyle.Line;
				//UITextField textField = (UITextField)Control;
				//textField.Font = UIFont.FromName ("Ubuntu-light", 9);
				//textField.BorderStyle = UITextBorderStyle.None;
				Control.TextColor = UIColor.FromRGBA(74, 74, 74, 255);
				Control.Layer.BorderColor = Color.FromHex("F1EEEE").ToCGColor();
				//Control.BorderStyle = UITextBorderStyle.None;
				Control.Layer.BorderWidth = 1.5f;

			}
		}
	}
}
