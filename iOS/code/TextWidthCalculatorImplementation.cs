﻿using System;
using CoreGraphics;
using POReceipt.iOS;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(TextWidthCalculatorImplementation))]
namespace POReceipt.iOS
{
	public class TextWidthCalculatorImplementation : ITextWidthCalculator
	{
		UILabel uiLabel;
		CGSize length;

		public double CalculateWidth(string text)
		{
			uiLabel = new UILabel();
			uiLabel.MinimumFontSize = 10;
			uiLabel.Font = UIFont.FromName("Helvetica-Bold", 13f);
			uiLabel.Text = text;
			length = uiLabel.Text.StringSize(uiLabel.Font);
			return length.Width;
		}
	}
}
