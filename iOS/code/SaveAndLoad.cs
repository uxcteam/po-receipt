﻿using System;
using System.IO;
using POReceipt.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(SaveAndLoad))]
namespace POReceipt.iOS
{
	public class SaveAndLoad : ISaveAndLoad
	{
		public void SaveText(string filename, string text)
		{
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			var filePath = Path.Combine(documentsPath, filename);
			System.IO.File.WriteAllText(filePath, text);
		}
		public string LoadText(string filename)
		{
			return System.IO.File.ReadAllText(filename); 
		}
	}
}
