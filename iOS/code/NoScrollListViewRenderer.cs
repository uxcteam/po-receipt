using System;
using POReceipt;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(NoScrollListView), typeof(POReceipt.iOS.NoScrollListViewRenderer))]
namespace POReceipt.iOS
{
	public class NoScrollListViewRenderer : ListViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged(e);

			if (e.NewElement == null) {
				return;
			}
			if (Control != null) {
				Control.ScrollEnabled = false;
			}
		}
	}
}
