using System;
using System.Threading;
using System.Threading.Tasks;
using POReceipt.Messages;
using POReceipt;
using Xamarin.Forms;
using UIKit;

namespace POReceipt.iOS
{
	public class iOSLongRunningTask
	{
		nint _taskId;
		CancellationTokenSource _cts;

		public async Task Start ()
		{
			_cts = new CancellationTokenSource ();
			_taskId = UIApplication.SharedApplication.BeginBackgroundTask ("LongRunningTask", OnExpiration);

			try 
			{
                // DO getting Token
                await Shared.AIS.RefreshToken(_cts.Token);
			} 
			catch (OperationCanceledException) 
			{
			} 
			finally 
			{
				if (_cts.IsCancellationRequested) 
				{
					var message = new CancelledMessage();
					Device.BeginInvokeOnMainThread (() => MessagingCenter.Send(message, "CancelledMessage"));
				}
			}

			UIApplication.SharedApplication.EndBackgroundTask (_taskId);
		}

		public void Stop ()
		{
			_cts.Cancel ();
		}

		void OnExpiration ()
		{
			_cts.Cancel ();
		}
	}
}

