using System;
using UIKit;
using POReceipt;
using Xamarin.Forms;
using Xamarin.Forms.Platform;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(NoBorderEntry), typeof(POReceipt.iOS.NoBorderEntryRenderer))]
namespace POReceipt.iOS
{
	public class NoBorderEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);
			Control.BorderStyle = UITextBorderStyle.None;

		}
	}
}

