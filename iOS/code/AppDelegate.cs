using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Foundation;
using UIKit;
using HockeyApp;

using Xamarin.Forms;
using System.IO;
using Xamarin.Forms.Platform.iOS;
using CoreGraphics;
using Newtonsoft.Json.Linq;
using HockeyApp.iOS;
using UserNotifications;

namespace POReceipt.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate, INative
	{
		private iOSLongRunningTask longRunningTask;

		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.FontAwesomeModule())
				  .With(new Plugin.Iconize.Fonts.MaterialModule());
			App.ScreenWidth = (int)UIScreen.MainScreen.Bounds.Width;

			if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
			{
				// Request notification permissions from the user
				UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert, (approved, err) =>
				{
					// Handle approval
				});

				// Watch for notifications while the app is active
				UNUserNotificationCenter.Current.Delegate = new UserNotificationCenterDelegate();
			}
			else if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
			{
				var notificationSettings = UIUserNotificationSettings.GetSettingsForTypes(
					UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null
				);

				app.RegisterUserNotificationSettings(notificationSettings);
			}


			setupAnalytics ();

			global::Xamarin.Forms.Forms.Init ();

			SetIoc();

			doInit ();

			LoadApplication (new App (this));
			UINavigationBar.Appearance.BarTintColor = UIColor.FromRGB(208, 1, 27);
			UINavigationBar.Appearance.TintColor = UIColor.White;
			UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes
			{
				TextColor = UIColor.White
			});
			UIApplication.SharedApplication.SetStatusBarStyle (UIStatusBarStyle.LightContent, false);
			POReceipt.iOS.ImageCornerRenderer.Init();

			FormsPlugin.Iconize.iOS.IconControls.Init();

			return base.FinishedLaunching (app, options);

		}

		private void SetIoc()
		{
			//var resolverContainer = new global::XLabs.Ioc.SimpleContainer();

			//var app = new XFormsAppiOS();
			//app.Init(this);

			//var documents = app.AppDataDirectory;
			//var pathToDatabase = Path.Combine(documents, "xforms.db");

			//resolverContainer.Register<IDevice>(t => AppleDevice.CurrentDevice)
			//	.Register<IDisplay>(t => t.Resolve<IDevice>().Display)
			//	.Register<IFontManager>(t => new FontManager(t.Resolve<IDisplay>()))
			//	.Register<XLabs.Serialization.IJsonSerializer, XLabs.Serialization.JsonNET.JsonSerializer>()
			//	//.Register<IJsonSerializer, Services.Serialization.SystemJsonSerializer>()
			//	.Register<ITextToSpeechService, TextToSpeechService>()
			//	.Register<IEmailService, EmailService>()
			//	.Register<IMediaPicker, MediaPicker>()
			//	.Register<IXFormsApp>(app)
			//	.Register<ISecureStorage, SecureStorage>()
			//	.Register<global::XLabs.Ioc.IDependencyContainer>(t => resolverContainer);

			//XLabs.Ioc.Resolver.SetResolver(resolverContainer.GetResolver());
		}

		public void saveLoginInfo(string username, string password) {
			NSUserDefaults.StandardUserDefaults.SetString(username, "username");
			NSUserDefaults.StandardUserDefaults.SetString(password, "password");
		}

		public string getLoggedInUsername() {
			return NSUserDefaults.StandardUserDefaults.StringForKey("username");
		}

		public string getLoggedInPassword()
		{
			return NSUserDefaults.StandardUserDefaults.StringForKey("password");
		}

		public void makeACall(string phonenumber) {
			phonenumber.Replace('(', ' ');
			phonenumber.Replace(')', ' ');
			phonenumber = phonenumber.Trim();
			phonenumber = phonenumber.Replace(" ", "");
			var url = new NSUrl("tel:" + phonenumber);
			if (!UIApplication.SharedApplication.OpenUrl(url))
			{
				var av = new UIAlertView("Not supported",
				  "Scheme 'tel:' is not supported on this device",
				  null,
				  "OK",
				  null);
				av.Show();
			};
		}

		public string readSampleData(string fileName)
		{
			return System.IO.File.ReadAllText(fileName);
		}

		private void doInit ()
		{
			NSHttpCookieStorage.SharedStorage.AcceptPolicy = NSHttpCookieAcceptPolicy.Always;

			Xamarin.FormsMaps.Init ();
		}

		#region INative implementation

		public string[] getFiles (string resource, string pattern = null)
		{
			return Shared.FileUtil.getFiles (resource, pattern);
		}

		public string[] getDirectories (string resource, string pattern = null)
		{
			return Shared.FileUtil.getDirectories (resource, pattern);
		}

		public string getDocumentPath ()
		{
			return Shared.FileUtil.getDocumentPath ();
		}

		public void writeBSON (string file, object obj)
		{
			Shared.JsonUtil.writeBSON (file, obj);
		}

		public T readBSON<T> (string file)
		{
			return Shared.JsonUtil.readBSON<T> (file);
		}

		public DateTime timeFile (string resource)
		{
			return Shared.FileUtil.timeFile (resource);
		}

		public async Task<string> showActionSheet (string message, params string[] options)
		{
			return await Shared.Util.showActionSheet (message, options);
		}

		public async Task<bool> checkHost ()
		{
			return false; //await Shared.Service.checkHost ();
		}

		public void deleteFile (string file)
		{
			Shared.FileUtil.deleteFile (file);
		}

		public void deleteDirectory (string file)
		{
			Shared.FileUtil.deleteDirectory (file);
		}

		public async Task<double[]> geocodeAddress (string location)
		{
			return null;	// Not needed for iOS
		}

		public void openFile (string filePath, string mimeType = null)
		{
			// Not needed for iOS
		}

		public async Task<bool> isHostReachable (string host, int port)
		{
			return await Shared.Util.isHostReachable (host, port);
		}

		public bool ifRelease (Action action = null)
		{
			return Shared.Util.ifRelease (action);
		}

		public byte[] compress (byte[] bytes)
		{
			return Shared.ConvUtil.compress (bytes);
		}

		public byte[] decompress (byte[] bytes)
		{
			return Shared.ConvUtil.decompress (bytes);
		}

		public string unescapeXML (string xml)
		{
			return Shared.XmlUtil.unescapeXML (xml);
		}

		public string escapeXML (string xml)
		{
			return Shared.XmlUtil.escapeXML (xml);
		}

		public string checkFile (string resource)
		{
			return Shared.FileUtil.checkFile (resource);
		}

		public string readFile (string resource)
		{
			return Shared.FileUtil.readFile (resource);
		}

		public byte[] readBytes (string resource)
		{
			return Shared.FileUtil.readBytes (resource);
		}

		public void writeFile (string resource, string content)
		{
			Shared.FileUtil.writeFile (resource, content);
		}

		public string writeFile (string resource, byte[] bytes)
		{
			return Shared.FileUtil.writeFile (resource, bytes);
		}

		public bool ifDebug (Action action = null)
		{
			return Shared.Util.ifDebug (action);
		}

		public bool isNetworkConnected ()
		{
			return Shared.Util.isNetworkConnected ();
		}

		public string setServiceEndpoint (string url)
		{
			return ""; //Shared.Service.setServiceEndpoint (url);
		}

		public void authenticate (string domain, string username, string password)
		{
			//Shared.Service.authenticate (domain, username, password);
		}

		public async Task<bool> checkService ()
		{
			return false; //await Shared.Service.checkService ();
		}

		public async Task<string> callService (string xml)
		{
			return "";//await Shared.Service.callServiceAsync (xml);
		}

		public async Task<byte[]> resizeImage (byte[] imageData, float width, float height, bool allowUpscale)
		{
			return await Shared.Util.resizeImage (imageData, width, height, allowUpscale);
		}

		public void openURL (string url)
		{
			Shared.Util.openURL (url);
		}

		public void navigateToMap (string label, string location)
		{
			Shared.Util.navigateToMap (label, location);
		}

		public void navigateToMap (string label, double latitude, double longitude)
		{
			Shared.Util.navigateToMap (label, latitude, longitude);
		}

		public string readAsset (string resource)
		{
			if (System.IO.File.Exists (resource)) {
				return System.IO.File.ReadAllText (resource) ?? "";
			} else {
				return null;
			}
		}

		public void showError (Exception exception)
		{
			Shared.Util.showError (exception);
		}

		public string getJSON (object obj)
		{
			return Shared.JsonUtil.getJSON (obj);
		}

		public T getObject<T> (string json)
		{
			return Shared.JsonUtil.getObject<T> (json);
		}

		public void invokeLater (Action action)
		{
			Shared.Util.invokeLater (action);
		}

		public async Task<bool> showConfirm (string message, string title = "")
		{
			return await Shared.Util.showConfirm (message, title);
		}

		public async Task<bool> showConfirmYesNo (string message, string title = "")
		{
			return await Shared.Util.showConfirmYesNo (message, title);
		}

		public void showAlert (string message, string title = "")
		{
			Shared.Util.showAlert (message, title);
		}

		public void log (object message)
		{
			Shared.Util.log (message);
		}

		public async Task wait (int miliseconds)
		{
			await Shared.Util.wait (miliseconds);
		}

		public void setupAnalytics ()
		{

			var manager = BITHockeyManager.SharedHockeyManager;
            manager.Configure("ce08321a22a94cf9aa1392880bf4c61f");
			manager.DisableMetricsManager = true;
			manager.StartManager();

			////We MUST wrap our setup in this block to wire up
			//// Mono's SIGSEGV and SIGBUS signals
			//HockeyApp..EnableCustomCrashReporting (() => {
			//	//Get the shared instance
			//	var manager = BITHockeyManager.SharedHockeyManager;

			//	//Configure it to use our APP_ID
			//	manager.Configure ("3d8a1e715e1141ac839d42fb28737e1d");

			//	//Start the manager
			//	manager.StartManager ();

			//	//Authenticate (there are other authentication options)
			//	manager.Authenticator.AuthenticateInstallation ();

			//	//Rethrow any unhandled .NET exceptions as native iOS 
			//	// exceptions so the stack traces appear nicely in HockeyApp
			//	AppDomain.CurrentDomain.UnhandledException += (sender, e) => 
			//		Setup.ThrowExceptionAsNative (e.ExceptionObject);

			//	TaskScheduler.UnobservedTaskException += (sender, e) => 
			//		Setup.ThrowExceptionAsNative (e.Exception);
			//});
		}

		public void OnPageAppear ()
		{

			try {
				NativeUtil.alignToolbarItems ();
			} catch {
			}
		}

		private BigTed.ProgressHUD hud;

		public void setBusy (bool busy)
		{
			if (busy) {
				showHUD ("Please wait");
			} else {
				hideHUD ();
			}
		}

		public void setBusyWithCancel(bool busy)
		{
			if (busy)
			{
				showHUDWithCancel("Please wait");
			}
			else {
				hideHUD();
			}
		}

		public void showHUD (string message)
		{
			InvokeOnMainThread (() => {
				try {
					if (hud != null) {
						hud.Dismiss ();
					}

					hud = new BigTed.ProgressHUD ();
					hud.Show (message, maskType: BigTed.ProgressHUD.MaskType.Black);

				} catch (Exception e) {
					log (e);
				}
			});
		}

		public void showHUDWithCancel(string message)
		{
			InvokeOnMainThread(() =>
			{
				try
				{
					if (hud != null)
					{
						hud.Dismiss();
					}

					hud = new BigTed.ProgressHUD();
					hud.Show("Cancel", cancelCallback: HandleCancelHub, status:message, maskType: BigTed.ProgressHUD.MaskType.Black);

				}
				catch (Exception e)
				{
					log(e);
				}
			});
		}

		void HandleCancelHub()
		{
			Device.BeginInvokeOnMainThread(() => MessagingCenter.Send("", "HubCancelled"));
			App.mainNavPage.PopToRootAsync(true);
		}

		public void hideHUD ()
		{
			InvokeOnMainThread (() => {
				try {
					if (hud != null) {
						hud.Dismiss ();
						hud = null;
					}
				} catch (Exception e) {
					log (e);
				}
			});
		}

		#endregion

		public Task<bool> takePhoto(EventHandler onFinishTakingPhotoNative)
		{
			//nothing for iOS
			return null;
		}

		// Added by Khang
		public void OnLogout () {
			
		}

		public Size GetImageSize (string imagePath)
		{
			UIImage image = UIImage.FromFile(imagePath);
			return new Size((double)image.Size.Width, (double)image.Size.Height);
		}

		public byte[] readBytesFromPath (string filePath)
		{
			return Shared.FileUtil.readBytesFromPath (filePath);
		}

		public byte[] readResourceBytesSync(string resource)
		{
			return Shared.FileUtil.readResourceBytesSync(resource);
		}

		public byte[] TakeScreenShot (View screenShotView, Size viewSize)
		{

			var view = ConvertFormsToNative (screenShotView, new CGRect(0,0,viewSize.Width, viewSize.Height) );

			UIGraphics.BeginImageContext(view.Frame.Size);
			view.DrawViewHierarchy(view.Frame, true);
			var image = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();

			byte[] bytes;

			//using(var imageData = image.AsPNG())
			using (var imageData = image.AsJPEG(0.35f))
			{
				bytes = new byte[imageData.Length];
				System.Runtime.InteropServices.Marshal.Copy(imageData.Bytes, bytes, 0, Convert.ToInt32(imageData.Length));
			}

			return bytes;
		}

		public void SaveScreenShot (string filePath, View screenShotView, Size viewSize)
		{
			byte[] bytes = TakeScreenShot(screenShotView, viewSize);
			if (bytes != null)
				Shared.FileUtil.writeFileToPath (filePath, bytes);
		}

		public static UIView ConvertFormsToNative(Xamarin.Forms.View view, CGRect size)
		{
			var renderer = RendererFactory.GetRenderer (view);

			renderer.NativeView.Frame = size;

			renderer.NativeView.AutoresizingMask = UIViewAutoresizing.All;
			renderer.NativeView.ContentMode = UIViewContentMode.ScaleToFill;

			renderer.Element.Layout (size.ToRectangle());

			var nativeView = renderer.NativeView;

			nativeView.SetNeedsLayout ();

			return nativeView;
		}

		public async Task<JObject> CallRestApiAsync (string url)
		{
			return await Shared.Service.CallRestApiAsync(url);
		}

		public async Task<JObject> CallRestApiAsyncV1(string url)
		{
			return await Shared.Service.CallRestApiAsyncV1(url);
		}

		public async Task<bool> CallPostRequest(string url, string data)
		{
			return await Shared.Service.CallPostRequest(url, data);
		}

		public async Task<bool> CallPostRequestV1(string url, string data)
		{
			return await Shared.Service.CallPostRequestV1(url, data);
		}

		public async Task<bool> CallPutRequest(string url, string data)
		{
			return await Shared.Service.CallPutRequest(url, data);
		}

		//public async Task<Types.Location> getLocation()
		//{
		//	return await Shared.Util.getLocation();
		//}

		//public async Task<string> getAddressFromLocation(Types.Location location)
		//{
		//	return await Shared.Util.getAddressFromLocation(location);
		//}

		public string readResourceSync(string name)
		{
			return Shared.FileUtil.readResourceSync(name);
		}

		public Size getPhysicalSize()
		{
			var screenScale = UIScreen.MainScreen.Scale;
			var width = (int)(UIScreen.MainScreen.Bounds.Size.Width * screenScale);
			var height = (int)(UIScreen.MainScreen.Bounds.Size.Height * screenScale);
			return new Size(width, height);
		}

		public async Task<string> CallPeopleSoftRequest(string url, string data)
		{
			return await Shared.Service.CallPeopleSoftRequest(url, data);
		}

		/// <summary>
		/// Wires up background service task.
		/// </summary>
		/// <returns>The up background service task.</returns>
		public void WireUpBackgroundServiceTask()
		{
			if (longRunningTask == null)
			{
				longRunningTask = new iOSLongRunningTask();
				longRunningTask.Start();
			}
		}

		public void ExtractZipFile(string archiveFilenameIn, string outFolder)
		{
			Shared.FileUtil.ExtractZipFile(archiveFilenameIn, outFolder);
		}

		public async Task<string> callFinancialSoapAsync(string emplId)
		{
			return await Shared.Service.callFinancialSoapAsync(emplId);
		}

		public async Task<string> callSoapAsync(string xmlPackage, string action)
		{
			return await Shared.Service.callSoapAsync(xmlPackage, action);
		}

		public void PushLocalNotification(string alertBody, string alertAction)
		{
			if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
			{
				var content = new UNMutableNotificationContent();
				content.Title = alertAction;
				content.Subtitle = "";
				content.Body = alertBody;
				content.Badge = 0;

				var trigger = UNTimeIntervalNotificationTrigger.CreateTrigger(2, false);

				var requestID = "sampleRequest";
				var request = UNNotificationRequest.FromIdentifier(requestID, content, trigger);

				UNUserNotificationCenter.Current.AddNotificationRequest(request, (err) =>
				{
					if (err != null)
					{
						// Do something with error...
					}
				});
			}
			else
			{
				// create the notification
				var notification = new UILocalNotification();

				// set the fire date (the date time in which it will fire)
				notification.FireDate = NSDate.FromTimeIntervalSinceNow(1);

				// configure the alert
				notification.AlertAction = alertAction;
				notification.AlertBody = alertBody;

				// modify the badge
				//notification.ApplicationIconBadgeNumber = 1;

				// set the sound to be the default sound
				notification.SoundName = UILocalNotification.DefaultSoundName;

				// schedule it
				UIApplication.SharedApplication.ScheduleLocalNotification(notification);
			}

		}

		public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
		{
			// just have one notification

		}

        // Khang added for PO Receipt APIs

		public async Task <string> GetToken(string username, string password, string deviceName) {
            return await Shared.AIS.GetToken(username, password, deviceName);
		}

        public async Task <List<JObject>> GetPOJsonDataAsync(string token, string deviceName) {
            return await Shared.AIS.GetPOJsonDataAsync(token, deviceName);
        }

        public async Task<List<JObject>> SearchPOJsonDataAsync(string token, string deviceName, 
                                                                            string poNumber,
                                                                            bool typeEQUAL,
                                                                            string poType,
                                                                            string supplierName,
                                                                            string recent,
                                                                            DateTime fromD, DateTime toD,
                                                                            string projectCode,
                                                                            string user,
                                                                            string account)
        {
            return await Shared.AIS.SearchPOJsonDataAsync(token, deviceName,  poNumber, typeEQUAL, poType, supplierName, recent, fromD, toD, projectCode, user, account);
        }

        public async Task<Dictionary<int, string>> GetSupplierLookupAsync(string token, string deviceName) {
            return await Shared.AIS.GetSuppliersLookup(token, deviceName);
        }

        public void UpdateConfigs(string hostname, string serviceRoot, string environment, string version, string deviceName) {
            Shared.ServiceConfiguration.JDE_HOSTNAME = hostname;
            Shared.ServiceConfiguration.JDE_SERVICE_ROOT = serviceRoot;
            Shared.ServiceConfiguration.JDE_ENVIRONMENT = environment;
            Shared.ServiceConfiguration.JDE_VERSION = version;
            Shared.ServiceConfiguration.JDE_DEVICE_NAME = deviceName;
            App.deviceName = deviceName;
        }

		public async Task<bool> ReceiveFullOrder(string token, string deviceName, int num, string type, string company)
		{
            return await Shared.AIS.ReceiveFullOrder(token, deviceName, num, type, company);  
        }

		public async Task<bool> ReceivePartialOrder(string token, string deviceName, int poNum, string poType, string poCompany, Dictionary<float, JObject> lines)
		{
            return await Shared.AIS.ReceivePartialOrder(token, deviceName, poNum, poType, poCompany, lines);
		}

        public async Task<string> SetOrderLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string text) 
        {
            return await Shared.AIS.SetOrderLineText(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, text);
        }

		public async Task<string> GetOrderLineText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum)
		{
			return await Shared.AIS.GetOrderLineText(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum);
		}

        public async Task<string> GetOrderText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix) 
        {
            return await Shared.AIS.GetOrderText(token, deviceName, poNum, poType, poCompany, poSuffix);
        }

        public async Task<string> SetOrderText(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, string text) 
        {
            return await Shared.AIS.SetOrderText(token, deviceName, poNum, poType, poCompany, poSuffix, text);
		}

        public async Task<string> GetOrderFileList(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix) {
            return await Shared.AIS.GetOrderFileList(token, deviceName, poNum, poType, poCompany, poSuffix);
        }

        public async Task<string> GetOrderLineFileList(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum) {
            return await Shared.AIS.getOrderLineFileList(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum);
        }

        public async Task<string> SetLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, string fileLocation, byte[] imageBytes)
        {
            return await Shared.AIS.SetLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, fileLocation, imageBytes);
        }

        public async Task<string> SetOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, string fileLocation, byte[] imageBytes) {
            return await Shared.AIS.SetOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, fileLocation, imageBytes);
        }

        public async Task<Stream> GetOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum) {
            return await Shared.AIS.GetOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, sequenceNum);
        }

        public async Task<Stream> GetLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum) {
            return await Shared.AIS.GetLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, sequenceNum);
        }

        public async Task<string> DeleteOrderBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, int sequenceNum) {
            return await Shared.AIS.DeleteOrderBinary(token, deviceName, poNum, poType, poCompany, poSuffix, sequenceNum);
        }

        public async Task<string> DeleteLineBinary(string token, string deviceName, int poNum, string poType, string poCompany, string poSuffix, float lineNum, int sequenceNum)
        {
            return await Shared.AIS.DeleteLineBinary(token, deviceName, poNum, poType, poCompany, poSuffix, lineNum, sequenceNum);
        }

        public async Task<bool> Logout(string token)
        {
            return await Shared.AIS.Logout(token);
        }
	}
}

