using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UIKit;

namespace POReceipt.iOS
{
	public static class NativeUtil
	{
		public static void alignToolbarItems()
		{
			var w = UIApplication.SharedApplication.KeyWindow;

			UINavigationBar bar = null;
			foreach (var v1 in w.Subviews){
				foreach (var v2 in v1.Subviews) {
					foreach (var v3 in v2.Subviews) {
						var uINavigationBar = v3 as UINavigationBar;
						if (uINavigationBar != null) {
							bar = uINavigationBar;
							goto exit;
						}
					}
				}
			}

			exit: if (bar == null) {
				return;
			}

			var items = bar.Items;
			if (items != null && items.Length == 1) {
				var item = items[0];
				if (item.LeftBarButtonItems == null && item.RightBarButtonItems != null && item.RightBarButtonItems.Length > 0) {
					var rightItems = new List<UIBarButtonItem>(item.RightBarButtonItems);
					var rightItem = rightItems.Last();
					rightItems.Remove(rightItem);

					item.SetLeftBarButtonItem(rightItem, false);
					item.SetRightBarButtonItems(rightItems.ToArray(), false);
				}
			}
			return;
		}
	}
}

