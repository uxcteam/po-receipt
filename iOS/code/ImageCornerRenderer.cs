using System;
using System.ComponentModel;
using System.Diagnostics;

#if __UNIFIED__
using Foundation;
using POReceipt.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
#else
using MonoTouch.Foundation;
#endif

[assembly: ExportRenderer(typeof(POReceipt.ImageCorner), typeof(ImageCornerRenderer))]
namespace POReceipt.iOS
{

	/// <summary>
	/// ImageCircle Implementation
	/// </summary>
	[Preserve(AllMembers = true)]
	public class ImageCornerRenderer : ImageRenderer
	{
		/// <summary>
		/// Used for registration with dependency service
		/// </summary>
		public async static void Init()
		{
			var temp = DateTime.Now;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
		protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged(e);
			if (Element == null)
				return;
			CreateCorner();
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			if (e.PropertyName == VisualElement.HeightProperty.PropertyName ||
				e.PropertyName == VisualElement.WidthProperty.PropertyName ||
			  e.PropertyName == ImageCorner.BorderColorProperty.PropertyName ||
			  e.PropertyName == ImageCorner.BorderThicknessProperty.PropertyName ||
			  e.PropertyName == ImageCorner.FillColorProperty.PropertyName || 
				e.PropertyName == ImageCorner.CornerRadiusProperty.PropertyName)
			{
				CreateCorner();
			}
		}

		private void CreateCorner()
		{
			try
			{
				//double min = Math.Min(Element.Width, Element.Height);
				Control.Layer.CornerRadius = ((ImageCorner)Element).CornerRadius; //(float)(min / 2.0);
				Control.Layer.MasksToBounds = false;
				Control.Layer.BorderColor = ((ImageCorner)Element).BorderColor.ToCGColor();
				Control.Layer.BorderWidth = ((ImageCorner)Element).BorderThickness;
				Control.BackgroundColor = ((ImageCorner)Element).FillColor.ToUIColor();
				Control.ClipsToBounds = true;
			}
			catch (Exception ex)
			{
				Debug.WriteLine("Unable to create circle image: " + ex);
			}
		}
	}

}

